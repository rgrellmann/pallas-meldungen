<?php
use Framework\App;

$start = microtime(true);

// tell the Conf class whether to read the configuration from files or
// to use a cache mechanism (apc, memcache...)
$config_cache = '';

// read configuration, start session
require_once('includes/init.php');

App::__init();

App::run();
