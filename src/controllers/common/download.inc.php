<?php
namespace Controller\common;

use Framework\App;
use Framework\ControllerAbstract;
use Framework\DebugTools;
use Framework\Request;

/**
 * @noinspection PhpUnused
 */
class DownloadController extends ControllerAbstract
{

    public function indexAction(): void
    {
        $allowed = 1;
        $filename = Request::get('filename');

        if (!$this->session->is_logged_in()) {
            $allowed = 0;
        }

        if ($allowed > 0) {

            header("Content-Type: application/pdf");
            header("Content-Length: " . filesize('user_files' . $filename));
            header("Cache-Control: ");  // keeps ie happy
            header("Pragma: ");         // keeps ie happy
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            $handle = fopen('user_files' . $filename, "rb");    // "rb" = Binäres lesen der Datei!
            if (!$handle) {
                App::logger()->error($filename . " could not be opened");
                $this->error_message[] = "Datei kann nicht geöffnet werden.<br>";
            } else {
                fpassthru($handle);
                fclose($handle);
                DebugTools::testoutput("secure_download.log", "[" . date("Y-m-d H:i:s") . "] $_SERVER[REMOTE_ADDR]\t "
                    . $_REQUEST['type'] . " " . $_REQUEST["id"] . " " . $this->session->get_user_id());
                exit;
            }
        }
    }

}
