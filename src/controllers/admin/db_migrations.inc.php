<?php
namespace Controller\Admin;

use Framework\App;
use Framework\Database;
use Framework\Request;
use Framework\ControllerAbstract;
use Framework\SQLException;


class Db_MigrationsController extends ControllerAbstract {

    public function indexAction(): void {
        $db = Database::get_connection();
        /** @noinspection SqlResolve */
        $current_version = $db->select_one_field('SELECT latest_migration FROM '.TABLE_DB_VERSION);
        $this->content->set("current_version",  $current_version);
        $this->findMigrationFiles();
    }

    /**
     * @return void
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function migrate_upAction(): void {
        $file = Request::get('file');
        if (!empty($file) AND $file{0} != "." AND strpos($file, '.sql.php') == (strlen($file) - 8)
            AND is_file('sql/' . $file)) {
            $db = Database::get_connection();
            include('sql/' . $file);
            if (empty($up) OR !is_array($up)) {
                $this->error_message[] = "Keine Aufwärts-Migration in der Datei $file gefunden";
                $this->indexAction();
                return;
            }

            $ignore_errors = Request::get('ignore_errors') ?? false;

            $this->executeMigrations($up, $ignore_errors);

            $db->query("UPDATE ".TABLE_DB_VERSION." SET latest_migration = '".$db->escape($file)."'");
            $this->user_message[] = "Migration erfolgreich ausgeführt";
        } else {
            $this->error_message[] = "Ungültiger Dateiname oder Datei nicht gefunden";
        }
        $this->indexAction();
    }

    /**
     * @return void
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function migrate_downAction(): void {
        $file = Request::get('file');
        if (!empty($file) AND $file{0} != "." AND strpos($file, '.sql.php') == (strlen($file) - 8)
            AND is_file('sql/' . $file)) {
            $db = Database::get_connection();
            include('sql/' . $file);
            if (empty($down) OR !is_array($down)) {
                $this->error_message[] = "Keine Abwärts-Migration in der Datei $file gefunden";
                $this->indexAction();
                return;
            }

            $ignore_errors = Request::get('ignore_errors') ?? false;

            $this->executeMigrations($down, $ignore_errors);

            $files = $this->findMigrationFiles();
            $current = array_search($file, $files);
            if ($current == 0) {
                $previous = 0;
            } else {
                $previous = $files[$current - 1];
            }
            $db->query("UPDATE ".TABLE_DB_VERSION." SET latest_migration = '".$db->escape($previous)."'");
            $this->user_message[] = "Migration erfolgreich ausgeführt";
        } else {
            $this->error_message[] = "Ungültiger Dateiname oder Datei nicht gefunden";
        }
        $this->indexAction();
    }

    /**
     * @param array $migrations
     * @param mixed $ignore_errors
     * @return void
     */
    protected function executeMigrations(array $migrations, $ignore_errors): void {
        $db = Database::get_connection();
        foreach ($migrations as $sql) {
            if (is_string($sql)) {
                try {
                    $db->query($sql);
                } catch (\Exception $e) {
                    App::logger()->error($e->getMessage() . " ($sql)");
                    $this->error_message[] = "Fehler bei der Ausführung der Migration: " . $e->getMessage() . " ($sql)";
                    if (!$ignore_errors) {
                        break;
                    }
                }
            } elseif (is_callable($sql)) {
                $sql($db, $this);
            } else {
                $this->error_message[] = "Ein Migrations-Element muss entweder ein String oder vom Typ callable sein";
            }
        }
    }

    /**
     * @return array
     */
    protected function findMigrationFiles(): array {
        $dir = opendir('sql');
        $files = array();
        while (false !== ($file = readdir($dir))) {
            if ($file{0} != "." AND strpos($file, '.sql.php') == (strlen($file) - 8)) {
                $files[] = $file;
            }
        }
        closedir($dir);
        sort($files);
        $this->content->set("files", $files);
        return $files;
    }

}
