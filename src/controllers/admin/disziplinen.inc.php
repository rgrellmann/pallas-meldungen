<?php
namespace Controller\admin;

use Entity\Disziplin;
use Framework\ApplicationException;
use Framework\DuplicateKeyEntryException;
use Framework\Request;
use Framework\Common;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;
use Framework\ControllerAbstract;

/**
 * @noinspection PhpUnused
 */
class DisziplinenController extends ControllerAbstract
{

    /** @var Disziplin */
    public $item;

    /**
     * @throws SQLException
     */
    public function __construct()
    {
        parent::__construct();
        $this->item = new Disziplin();
    }

    /**
     * @throws ApplicationException
     */
    public function indexAction(): void
    {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) {
            $desc = "";
        } else {
            $desc = " DESC";
        }
        switch (Request::get('sort_by')) {
            case "name":
                $sort_by = "name" . $desc;
                break;
            case "kurzname":
                $sort_by = "kurzname" . $desc;
                break;
            case "sportart":
                $sort_by = "sportart" . $desc;
                break;
            case "id":
                $sort_by = "id" . $desc;
                break;
            default:
                $sort_by = "id";
                $_REQUEST['sort_by'] = "id";
                $_REQUEST['sort_desc'] = 0;
        }

        $where = Search::process("disziplin", "d", '\Entity\Disziplin');
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM " . TABLE_DISZIPLINEN . " AS d"
            . " WHERE 1 $where ORDER BY $sort_by LIMIT $limit OFFSET $offset");
        $items = Common::make_array($res, '\Entity\Disziplin');
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "disziplin");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set('navigator', $navigator);
        $this->content->set("items", $items);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
    }

    /**
     * @throws DuplicateKeyEntryException
     * @throws SQLException
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function save_itemAction(): void
    {
        $item = new Disziplin();
        $item->set('name', Request::get('name'));
        $item->set('kurzname', Request::get('kurzname'));
        $item->set('sportart', Request::get('sportart'));

        if (intval(Request::get('id')) > 0) {
            $item->set('id', intval(Request::get('id')));
            $success = $item->update();
        } else {
            $success = $item->insert();
        }
        if ($success) {
            $this->user_message[] = "Die Disziplin wurde gespeichert.<br>";
            $this->action = '';
        } else {
            $this->error_message[] = "Die Disziplin konnte nicht gespeichert werden.<br>";
            $this->action = 'edit_item';
            $this->edit_itemAction();
            return;
        }
        $this->indexAction();
    }

    /**
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function delete_itemsAction(): void
    {
        $ids = Request::get('ids');
        if (!is_array($ids) OR Common::array_empty($ids)) {
            $this->error_message[] = "Es wurden keine Disziplinen zum Löschen ausgewählt.<br>";
            $this->indexAction();
            return;
        }
        array_walk($ids, array('\Framework\LukeArraywalker', "intval_this"));

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $sql = "SELECT disziplin_id FROM " . TABLE_VERANSTALTUNGEN_DISZIPLINEN . " WHERE disziplin_id IN (" . implode(",", $ids) . ") GROUP BY disziplin_id";
            $res = $this->db->query($sql);
            $disziplinen_mit_veranstaltung = Common::make_array($res, Common::FIRST_FIELD);
            if (count($disziplinen_mit_veranstaltung)) {
                $this->error_message[] = "Disziplinen, für die bereits Veranstaltungen zugeordnet wurden, können nicht gelöscht werden.<br>";
                $ids = array_diff($ids, $disziplinen_mit_veranstaltung);
            }
        }

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $this->db->query("DELETE FROM " . TABLE_DISZIPLINEN . " WHERE id IN (" . implode(",", $ids) . ")");
            $num_deleted = $this->db->affected_rows();
            $this->user_message[] = $num_deleted . " Disziplin" . ($num_deleted == 1 ? "" : "en") . " wurde" . ($num_deleted == 1 ? "" : "n") . " gelöscht.<br>";
        }
        $this->action = '';
        unset($_REQUEST['ids']);
        $this->indexAction();
    }

    /**
     * @throws SQLException
     */
    public function edit_itemAction(): void
    {
        $table = AutoForm::get_table_description(TABLE_DISZIPLINEN);
        array_walk($table["columns"], array('\Framework\AutoForm', "map_type"));

        $no_show = array();
        $no_edit = array("id", "last_change");
        $values = new Disziplin();

        if (intval(Request::get('id')) > 0) {
            $values->retrieve_by_pk(intval(Request::get('id')));
        }

        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);

        $this->content->set("form_items", $form_items);
    }

}
