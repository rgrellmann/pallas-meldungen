<?php
namespace Controller\admin;

use Application\Meldeliste;
use Entity\Veranstaltung;
use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Common;

/**
 * @noinspection PhpUnused
 */
class MeldelisteController extends ControllerAbstract
{

    /**
     * @return void
     */
    public function indexAction(): void
    {
        if (Request::get('veranstaltung')) {
            try {
                $veranstaltung = new Veranstaltung(intval(Request::get('veranstaltung')));
                if (!$veranstaltung->get_id()) {
                    throw new \Exception("Fehler: Veranstaltung nicht gefunden.");
                }
                $disziplin = Request::get('disziplin');
                $disziplinen = explode(",", $veranstaltung->get('disziplinen'));
                if ($disziplin AND !in_array($disziplin, $disziplinen)) {
                    throw new \Exception('Disziplin "' . $disziplin . '" nicht in der Veranstaltung gefunden.');
                }
                $encoding = Request::get('encoding');
                if (!in_array($encoding, array('UTF-8', 'ISO-8859-15'))) {
                    $encoding = 'ISO-8859-15';
                }
                switch (Request::get('format')) {
                    case 'text':
                        Meldeliste::meldeliste_text($veranstaltung, Request::get('sortierung'), $disziplin, $encoding);
                        break;
                    case 'csv':
                        Meldeliste::meldeliste_csv($veranstaltung, Request::get('sortierung'), $disziplin, $encoding);
                        break;
                    case 'pdf':
                        Meldeliste::meldeliste_pdf($veranstaltung, Request::get('sortierung'), $disziplin);
                        break;
                    case 'svb':
                        Meldeliste::meldeliste_kampfrichter($veranstaltung, 'svb', $disziplin, $encoding);
                        break;
                    case 'dsv':
                        Meldeliste::meldeliste_kampfrichter($veranstaltung, 'dsv', $disziplin, $encoding);
                        break;
                }
            } catch (\Exception $e) {
                $this->error_message[] = $e->getMessage();
            }
        }

        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT * FROM " . TABLE_VERANSTALTUNGEN . " ORDER BY id DESC");
        $veranstaltungen = Common::make_array($res, '\Entity\Veranstaltung');

        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT id,disziplinen FROM " . TABLE_VERANSTALTUNGEN);
        $disziplinen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);

        $this->content->set('veranstaltungen', $veranstaltungen);
        $this->content->set('disziplinen', $disziplinen);
    }

}
