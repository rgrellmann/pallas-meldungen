<?php
namespace Controller\admin;

use Entity\AclRole;
use Framework\ApplicationException;
use Framework\ControllerAbstract;
use Framework\DuplicateKeyEntryException;
use Framework\Request;
use Framework\Common;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;

/**
 * @noinspection PhpUnused
 */
class RolesController extends ControllerAbstract {

    /** @var AclRole */
    public $item = null;

    /**
     * @throws SQLException
     */
    public function __construct()
    {
        parent::__construct();
        $this->item = new AclRole();
    }

    /**
     * @throws ApplicationException
     */
    public function indexAction(): void
    {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) {
            $desc = "";
        } else {
            $desc = " DESC";
        }
        switch (Request::get('sort_by')) {
            case "name":
                $sort_by = "role_name" . $desc;
                break;
            case "id":
                $sort_by = "role_id" . $desc;
                break;
            default:
                $sort_by = "role_id";
                $_REQUEST['sort_by'] = "id";
        }

        $where = Search::process("role", "r", '\Entity\AclRole');

        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM " . TABLE_ROLES . " AS r"
            . " WHERE 1 $where ORDER BY $sort_by LIMIT $limit OFFSET $offset");
        $items = Common::make_array($res, '\Entity\AclRole');
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "role");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set('navigator', $navigator);
        $this->content->set("items", $items);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
    }

    /**
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function deleteAction(): void {
        if (!is_array(Request::get('ids')) OR Common::array_empty2(Request::get('ids'))) {
            $this->error_message[] = "Es wurden keine Datensätze zum Löschen ausgewählt.<br>";
            $this->indexAction();
            return;
        }
        $ids = Request::get('ids');
        array_walk($ids, array('Framework\LukeArraywalker', 'intval_this'));

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $this->db->query("DELETE FROM ".TABLE_ROLES." WHERE role_id IN (".implode(",",$ids).")");
            $num_deleted = $this->db->affected_rows();
            $this->user_message[] = $num_deleted." Rolle".($num_deleted==1?"":"n")." wurde".($num_deleted==1?"":"n")." gelöscht.<br>";
        }
        unset($_REQUEST['ids']);
        $this->action = '';
        $this->indexAction();
    }

    /**
     * @throws ApplicationException
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function saveAction(): void {
        $this->item->set_role_name(Request::get('role_name'));

        try {
            if (intval(Request::get('role_id')) > 0) {
                $this->item->set_role_id(intval(Request::get('role_id')));
                $success = $this->item->update();
            } else {
                $success = $this->item->insert();
            }
        } catch (DuplicateKeyEntryException $e) {
            $this->error_message[] = "Der Name ist bereits vergeben.";
            $this->action = 'edit';
            $this->editAction();
            return;
        }
        if ($success) {
            $this->user_message[] = "Die Rolle wurde gespeichert.<br>";
            $this->action = '';
        } else {
            $this->error_message[] = "Die Rolle konnte nicht gespeichert werden.<br>";
            $this->action = 'edit';
            $this->editAction();
            return;
        }
        $this->indexAction();
    }

    public function editAction(): void {
        $table = AutoForm::get_table_description(TABLE_ROLES);
        array_walk($table["columns"], array('\Framework\AutoForm', 'map_type'));

        $no_show = array();
        $no_edit = array("role_id");
        $values = array();

        if (intval(Request::get('id')) > 0) {
            /** @noinspection SqlResolve */
            $res = $this->db->query("SELECT * FROM ".TABLE_ROLES." WHERE role_id = ".intval(Request::get('id')));
            $values = $res->fetch();
        }
        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);
        $this->content->set("form_items", $form_items);
    }

}
