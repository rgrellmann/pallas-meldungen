<?php
namespace Controller\admin;

use Framework\ControllerAbstract;

/**
 * @noinspection PhpUnused
 */
class HomeController extends ControllerAbstract {

    /**
     * @return void
     */
    public function indexAction(): void {

    }

}
