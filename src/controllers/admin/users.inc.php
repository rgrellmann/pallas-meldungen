<?php
namespace Controller\Admin;

use Entity\User;
use Framework\ApplicationException;
use Framework\Auth;
use Framework\Common;
use Framework\DuplicateKeyEntryException;
use Framework\Request;
use Framework\ControllerAbstract;
use Framework\RoleException;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;

/**
 * @noinspection PhpUnused
 */
class UsersController extends ControllerAbstract {

    /** @var User */
    public $item = null;

    /**
     * @throws SQLException
     */
    public function __construct() {
        parent::__construct();
        $this->item = new User();
    }

    /**
     * @return void
     * @throws ApplicationException
     */
    public function indexAction(): void {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) { $desc = ""; }
        else { $desc = " DESC"; }
        switch (Request::get('sort_by')) {
            case "name": 		$sort_by = "last_name".$desc; 	break;
            case "username": 	$sort_by = "username".$desc; 	break;
            case "email": 		$sort_by = "email".$desc; 	    break;
            case "created": 	$sort_by = "created".$desc; 	break;
            case "is_online": 	$sort_by = "is_online".$desc; 	break;
            case "id": 			$sort_by = "user_id".$desc; 	break;
            default:			$sort_by = "user_id"; $_REQUEST['sort_by'] = "id";
        }

        $where = Search::process("user", "u", '\Entity\User');
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS u.*,GROUP_CONCAT(ur.role_id) AS roles FROM ".TABLE_USERS." AS u"
            ." LEFT JOIN ".TABLE_USER_ROLES." as ur USING (user_id)"
            ." WHERE 1 $where GROUP BY user_id ORDER BY $sort_by LIMIT $limit OFFSET $offset");
        $users = Common::make_array($res, '\Entity\User', '', true);
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "user");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set("navigator", $navigator);
        $this->content->set("users", $users);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
    }

    /**
     * @return void
     * @throws SQLException
     */
    public function editAction(): void {
        $table = AutoForm::get_table_description(TABLE_USERS);
        array_walk($table["columns"], array('Framework\AutoForm', 'map_type'));

        $no_show = array();
        $no_edit = array("user_id", "created", "last_change", "last_login", "is_online");
        if (!Auth::is_allowed('actions/modify_roles')) {
            $no_edit[] = "roles";
        }
        $values = $this->item;

        if (intval(Request::get('id')) > 0) {
            $values->retrieve_by_pk(intval(Request::get('id')), true);
            $values->set("new_username", $values->get("username"));
            $values->set("new_password", $values->get("password"));
        } else {
            $values->set("active", "1");
        }

        $table["columns"]["username"]["Field"] = "new_username";
        $table["columns"]["password"]["Field"] = "new_password";
        $table["columns"]["roles"]["Field"] = "roles";
        $table["columns"]["roles"]["Type"] = "checkbox";
        $table["columns"]["roles"]["Values"] = Auth::get_roles_array();
        $table["columns"]["roles"]["Comment"] = "";
        $table["columns"]["roles"]["Label"] = "Rollen";
        $table["columns"]["verein_id"]["Values"] = ['NULL' => 'Verein auswählen'] + $values->get_vereine_array();
        $table["columns"]["verein_id"]["Type"] = "select";

        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);

        $this->content->set("form_items", $form_items);
    }

    /**
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function deleteAction(): void {
        if (!is_array(Request::get('ids')) OR Common::array_empty2(Request::get('ids'))) {
            $this->error_message[] = "Es wurden keine Benutzer zum Löschen ausgewählt.";
            $this->action = '';
            $this->indexAction();
            return;
        }
        $ids = Request::get('ids');
        array_walk($ids, array('Framework\LukeArraywalker', 'intval_this'));

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $this->db->query("DELETE FROM ".TABLE_USERS." WHERE user_id IN (".implode(",",$ids).")");
            $num_deleted = $this->db->affected_rows();
            $this->user_message[] = $num_deleted." Benutzer wurde".($num_deleted==1?"":"n")." gelöscht.<br>";
        }
        $_REQUEST['todo'] = "";
        unset($_REQUEST['ids']);
        $this->indexAction();
    }

    /**
     * @throws \InvalidArgumentException
     * @throws ApplicationException
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function saveAction(): void {
        $user = $this->item;
        $user->set('last_change', "NOW()");

        $user->set('username', Request::get('new_username'));
        if (Request::get('new_password') AND Request::get('new_password') != "standardblabliblub") {
            $user->set('password', md5(Request::get('new_password')));
        }

        $user->set('first_name', Request::get('first_name'));
        $user->set('last_name', Request::get('last_name'));
        $user->set('verein_id', intval(Request::get('verein_id')) > 0 ? intval(Request::get('verein_id')) : null);
        $user->set('email', Request::get('email'));
        $user->set('yubikey_id', str_replace(' ', '', Request::get('yubikey_id')));
        $user->set('active', intval(Request::get('active')));

        try {
            if (intval(Request::get('user_id')) > 0) {
                $user->set_id(intval(Request::get('user_id')));
                $success = $user->update();
            } else {
                if ($user->get_password() == false) {
                    $this->error_message[] = "Es wurde kein Passwort eingetragen.";
                    $this->action = 'edit';
                    $this->editAction();
                    return;
                }
                $success = $user->insert();
            }

            if ($success AND isset($_REQUEST['roles']) AND Auth::is_allowed('actions/modify_roles')) {
                $user->retrieve_roles();
                Auth::update_user_roles($user, Request::get('roles'));
            }

        } catch (DuplicateKeyEntryException $e) {
            $this->error_message[] = "Der Benutzername ist bereits vergeben.";
            $this->action = 'edit';
            $this->editAction();
            return;
        } catch (RoleException $e) {
            $this->error_message[] = $e->getMessage();
            $this->action = '';
            $this->indexAction();
            return;
        }

        if ($success) {
            $this->user_message[] = "Der Benutzer wurde gespeichert.<br>";
            $this->action = '';
            $this->indexAction();
        } else {
            $this->error_message[] = "Der Benutzer konnte nicht gespeichert werden.<br>";
            $this->action = 'edit';
            $this->editAction();
        }
    }

}
