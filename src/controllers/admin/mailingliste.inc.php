<?php
namespace Controller\admin;

use Entity\Veranstaltung;
use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Common;
use Framework\SQLException;

/**
 * @noinspection PhpUnused
 */
class MailinglisteController extends ControllerAbstract {

    /**
     * @throws SQLException
     */
    public function indexAction(): void
    {
        if (Request::get('veranstaltung')) {
            $veranstaltung = new Veranstaltung(intval(Request::get('veranstaltung')));
            if (!$veranstaltung->get_id()) {
                $this->error_message[] = "Fehler: Veranstaltung nicht gefunden.\n";
            } else {
                $bez_kurz = strtr($veranstaltung->get("bezeichnung_kurz"), " ", "_");
                /** @noinspection SqlResolve */
                $sql = "SELECT name,vorname,email FROM " . TABLE_MELDUNGEN
                    . " WHERE veranstaltungs_id = '" . $veranstaltung->get_id() . "' AND status != 'gestrichen' AND email != ''"
                    . " ORDER BY name,vorname";
                $res = $this->db->query($sql);
                header("Content-type: text/plain");
                header("Pragma: no-cache");
                header("Content-Disposition: attachment; filename=meldeliste_" . $bez_kurz . ".ldif");
                $rows = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
                foreach ($rows as $ds) {
                    $ldif = "dn: cn=$ds[vorname] $ds[name],mail=$ds[email]\r\n";
                    $ldif .= "objectclass: person\r\n";
                    $ldif .= "givenName: $ds[vorname]\r\n";
                    $ldif .= "sn: $ds[name]\r\n";
                    $ldif .= "cn: $ds[vorname] $ds[name]\r\n";
                    $ldif .= "mail: $ds[email]\r\n";
                    $ldif .= "\r\n";
                    echo $ldif;
                }
                die;
            }
        } else {
            /** @noinspection SqlResolve */
            $res = $this->db->query("SELECT * FROM " . TABLE_VERANSTALTUNGEN . " ORDER BY datum DESC");
            $veranstaltungen = Common::make_array($res, '\Entity\Veranstaltung');
            $this->content->set('veranstaltungen', $veranstaltungen);
        }
    }

}
