<?php
namespace Controller\admin;

use Framework\Conf;
use Framework\ControllerAbstract;
use Framework\EntityBuilder;
use Framework\Request;
use Framework\Common;
use Framework\SQLException;

/**
 * @noinspection PhpUnused
 */
class Build_entitiesController extends ControllerAbstract
{

    protected function getTableNames(): array {
        $key = "Tables_in_" . Conf::get('database/db_name');
        $res = $this->db->query("SHOW TABLES");
        $table_names = Common::make_array($res);
        foreach ($table_names as &$table_name) {
            $table_name = $table_name[$key];
        }
        return $table_names;
    }

    public function indexAction(): void
    {
        $table_names = $this->getTableNames();
        $this->content->set("table_names", $table_names);
    }

    /**
     * @throws SQLException
     */
    public function generateAction(): void {
        $table_names = $this->getTableNames();
        $this->content->set("table_names", $table_names);

        $save = Request::get('save_file');
        $builder = new EntityBuilder();
        $sourcecode = $builder->build(Request::get('table'), $save ?? false);
        $this->content->set("sourcecode", $sourcecode);
    }

}
