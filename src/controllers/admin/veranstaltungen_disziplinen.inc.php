<?php
namespace Controller\admin;

use Entity\Veranstaltung;
use Entity\VeranstaltungDisziplin;
use Framework\ApplicationException;
use Framework\DuplicateKeyEntryException;
use Framework\Request;
use Framework\Common;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;
use Framework\ControllerAbstract;

/**
 * @noinspection PhpUnused
 */
class Veranstaltungen_disziplinenController extends ControllerAbstract
{

    /** @var VeranstaltungDisziplin */
    public $item;

    /** @var int */
    public $veranstaltung;

    /**
     * @throws SQLException
     */
    public function __construct()
    {
        parent::__construct();
        $this->item = new VeranstaltungDisziplin();
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT id,bezeichnung_kurz FROM " . TABLE_VERANSTALTUNGEN . " ORDER BY id DESC");
        $veranstaltungen = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);
        $this->content->set('veranstaltungen', $veranstaltungen);

        $this->veranstaltung = $this->session->get_from_session_or_request("veranstaltung");

        if (empty($this->veranstaltung) OR !array_key_exists($this->veranstaltung, $veranstaltungen)) {
            reset($veranstaltungen);
            $this->veranstaltung = intval(key($veranstaltungen));
            $this->session->set('veranstaltung', $this->veranstaltung);
        }
    }

    /**
     * @return void
     * @throws ApplicationException
     */
    public function indexAction(): void
    {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) {
            $desc = "";
        } else {
            $desc = " DESC";
        }
        switch (Request::get('sort_by')) {
            case "name":
                $sort_by = "name" . $desc;
                break;
            case "kurzname":
                $sort_by = "kurzname" . $desc;
                break;
            case "id":
                $sort_by = "vd.id" . $desc;
                break;
            default:
                $sort_by = "vd.id";
                $_REQUEST['sort_by'] = "id";
                $_REQUEST['sort_desc'] = 0;
        }

        $where = Search::process("veranstaltung_disziplin", "d", '\Entity\VeranstaltungDisziplin');
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS vd.*,d.name,d.kurzname FROM " . TABLE_VERANSTALTUNGEN_DISZIPLINEN . " AS vd"
            . " INNER JOIN ".TABLE_DISZIPLINEN." AS d ON vd.disziplin_id = d.id"
            . " WHERE vd.veranstaltungs_id = $this->veranstaltung $where ORDER BY $sort_by LIMIT $limit OFFSET $offset");
        $items = Common::make_array($res, '\Entity\VeranstaltungDisziplin', '', true);
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "veranstaltung_disziplin");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set("navigator", $navigator);
        $this->content->set("items", $items);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
    }

    /**
     * @return void
     * @throws SQLException
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function save_itemAction(): void
    {
        $item = $this->item;

        if (intval(Request::get('id')) > 0) {
            $item->retrieve_by_pk(intval(Request::get('id')));
            if (!$item->get_id()) {
                $this->error_message[] = "Die zur Bearbeitung ausgewählte Diziplin wurde inzwischen gelöscht";
                $this->indexAction();
                return;
            }
        } else {
            $item->set_veranstaltungs_id(Request::get('veranstaltungs_id'));
        }

        $item->set_disziplin_id(Request::get('disziplin_id'));
        $item->set_juengster_jg(Request::get('juengster_jg'));
        $item->set_aeltester_jg(Request::get('aeltester_jg'));
        $item->set_bemerkungen(Request::get('bemerkungen'));

        try {
            if (intval(Request::get('id')) > 0) {
                $success = $item->update();
            } else {
                $success = $item->insert();
            }
        } catch (DuplicateKeyEntryException $e) {
            $this->error_message[] = "Diese Disziplin wurde dieser Veranstaltung bereits zugeordnet.";
            $this->action = '';
            $this->indexAction();
            return;
        }
        if ($success) {
            $this->user_message[] = "Die Disziplin wurde gespeichert.";
            $this->action = '';
        } else {
            $this->error_message[] = "Die Disziplin konnte nicht gespeichert werden.";
            $this->action = 'edit_item';
            $this->edit_itemAction();
            return;
        }

        $this->indexAction();
    }

    /**
     * @return void
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function delete_itemsAction(): void
    {
        $ids = Request::get('ids');
        if (!is_array($ids) OR Common::array_empty($ids)) {
            $this->error_message[] = "Es wurden keine Disziplinen zum Löschen ausgewählt.<br>";
            $this->indexAction();
            return;
        }
        array_walk($ids, array('\Framework\LukeArraywalker', "intval_this"));

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $sql = "SELECT veranstaltung_disziplin_id FROM " . TABLE_MELDUNGEN_DISZIPLINEN . " WHERE veranstaltung_disziplin_id IN (" . implode(",", $ids) . ") GROUP BY veranstaltung_disziplin_id";
            $res = $this->db->query($sql);
            $disziplinen_mit_meldung = Common::make_array($res, Common::FIRST_FIELD);
            if (count($disziplinen_mit_meldung)) {
                $this->error_message[] = "Disziplinen, für die bereits Meldungen abgegeben wurden, können nicht gelöscht werden.<br>";
                $ids = array_diff($ids, $disziplinen_mit_meldung);
            }
        }

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $this->db->query("DELETE FROM " . TABLE_VERANSTALTUNGEN_DISZIPLINEN . " WHERE id IN (" . implode(",", $ids) . ")");
            $num_deleted = $this->db->affected_rows();
            $this->user_message[] = $num_deleted . " Disziplin" . ($num_deleted == 1 ? "" : "en") . " wurde" . ($num_deleted == 1 ? "" : "n") . " gelöscht.<br>";
        }
        $this->action = '';
        unset($_REQUEST['ids']);
        $this->indexAction();
    }

    /**
     * @throws SQLException
     */
    public function edit_itemAction(): void
    {
        $table = AutoForm::get_table_description(TABLE_VERANSTALTUNGEN_DISZIPLINEN);
        array_walk($table["columns"], array('\Framework\AutoForm', "map_type"));

        $no_show = array();
        $no_edit = array("id", "last_change", "created", 'veranstaltungs_id');
        $values = $this->item;

        if (intval(Request::get('id')) > 0) {
            $values->retrieve_by_pk(intval(Request::get('id')));
        } else {
            $values->set_veranstaltungs_id($this->veranstaltung);
        }

        $veranstaltung = new Veranstaltung($this->veranstaltung);
        $table["columns"]['veranstaltungs_id']['Type'] = "select";
        $table["columns"]['veranstaltungs_id']['Values'] = [$veranstaltung->get_id() => $veranstaltung->get_bezeichnung_kurz()];

        /** @noinspection SqlResolve */
        $sql = "SELECT id, name FROM ".TABLE_DISZIPLINEN." WHERE sportart = '".$veranstaltung->get_sportart()."'";
        $res = $this->db->query($sql);
        $disziplinen = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);
        $table["columns"]['disziplin_id']['Type'] = "select";
        $table["columns"]['disziplin_id']['Values'] = $disziplinen;

        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);

        $this->content->set("form_items", $form_items);
    }

}
