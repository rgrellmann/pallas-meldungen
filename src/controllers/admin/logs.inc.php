<?php
namespace Controller\admin;

use Framework\Conf;
use Framework\ControllerAbstract;
use Framework\Request;

/**
 * @noinspection PhpUnused
 */
class LogsController extends ControllerAbstract {

    /** @var string */
    protected $selected_logfile = "";

    /** @var string */
    protected $log_file_contents = null;

    /**
     * @return void
     */
    public function indexAction(): void {
        $this->selected_logfile = "";
        $this->log_file_contents = false;
    }

    protected function post_dispatch(): void {
        parent::post_dispatch();
        $this->content->set("selected_logfile",  $this->selected_logfile);
        $this->content->set("log_file_contents", $this->log_file_contents);

        $dir = opendir(Conf::get('logging/file/path'));
        $log_files = array();
        while (false !== ($file = readdir($dir))) {
            if ($file{0} != ".") $log_files[] = $file;
        }
        closedir($dir);
        sort($log_files);
        $this->content->set("log_files", $log_files);
    }

    /**
     * @noinspection PhpUnused
     */
    public function deleteAction(): void {
        if ((substr(Request::get('logfile'), -4, 4) == ".log" OR substr(Request::get('logfile'), -4, 4) == ".txt")) {
            if (preg_match("/apache_access|apache_error/", Request::get('logfile'))) {
                // the Apache access and error logs must not be deleted when the server is running!
                $this->error_message[] = "Die Apache Log-Dateien dürfen nicht gelöscht werden, während der Server läuft";
            } else {
                if (@unlink(Conf::get('logging/file/path').Request::get('logfile'))) {
                    $this->user_message[] = "Die Log-Datei wurde gelöscht";
                } else {
                    $this->error_message[] = "Die Log-Datei konnte nicht gelöscht werden";
                }
            }
            $_REQUEST['logfile'] = null;
            $this->selected_logfile = "";
        } else {
            $this->error_message[] = "Dateien dieses Typs können nicht über die Admin-Oberfläche gelöscht werden";
        }
        $this->indexAction();
    }

    public function logfileAction(): void {
        if (substr(Request::get('logfile'), -4, 4) == ".log" OR substr(Request::get('logfile'), -4, 4) == ".txt") {
            $this->selected_logfile = Request::get('logfile');
            if (!file_exists(Conf::get('logging/file/path') . Request::get('logfile'))) {
                $this->log_file_contents = "[Die Datei konnte nicht ge&ouml;ffnet werden.]";
            } else {
                if (empty($_REQUEST["num_rows"]) OR $_REQUEST["num_rows"] == "all") {
                    $this->log_file_contents = file_get_contents(Conf::get('logging/file/path') . Request::get('logfile'));
                } else {
                    $log_file_contents = file(Conf::get('logging/file/path') . Request::get('logfile'));
                    $log_file_contents = array_slice($log_file_contents, -$_REQUEST["num_rows"]);
                    $this->log_file_contents = implode("", $log_file_contents);
                }
                if (!$this->log_file_contents) {
                    $this->log_file_contents = "[Die Datei ist leer]";
                }
            }
        }
    }

    /**
     * AJAX Request
     * @noinspection PhpUnused
     */
    public function refresh_logfileAction(): void {
        $this->logfileAction();
        echo $this->log_file_contents;
        die();
    }
}
