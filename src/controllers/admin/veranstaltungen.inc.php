<?php
namespace Controller\admin;

use Entity\Veranstaltung;
use Framework\ApplicationException;
use Framework\DuplicateKeyEntryException;
use Framework\Request;
use Framework\Common;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;
use Framework\ControllerAbstract;

/**
 * @noinspection PhpUnused
 */
class VeranstaltungenController extends ControllerAbstract
{

    /** @var Veranstaltung */
    public $item = null;

    /**
     * @throws SQLException
     */
    public function __construct()
    {
        parent::__construct();
        $this->item = new Veranstaltung();
    }

    /**
     * @return void
     * @throws ApplicationException
     */
    public function indexAction(): void
    {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) {
            $desc = "";
        } else {
            $desc = " DESC";
        }
        switch (Request::get('sort_by')) {
            case "bezeichnung":
                $sort_by = "bezeichnung" . $desc;
                break;
            case "bezeichnung_kurz":
                $sort_by = "bezeichnung_kurz" . $desc;
                break;
            case "ort":
                $sort_by = "ort" . $desc;
                break;
            case "id":
                $sort_by = "id" . $desc;
                break;
            case "meldeschluss":
                $sort_by = "meldeschluss" . $desc;
                break;
            case "meldebeginn":
                $sort_by = "meldebeginn" . $desc;
                break;
            case "datum":
                $sort_by = "datum" . $desc;
                break;
            default:
                $sort_by = "datum DESC";
                $_REQUEST['sort_by'] = "datum";
                $_REQUEST['sort_desc'] = 1;
        }

        $where = Search::process("veranstaltung", "v", '\Entity\Veranstaltung');
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT SQL_CALC_FOUND_ROWS v.*,COUNT(m.id) AS anzahl_meldungen FROM " . TABLE_VERANSTALTUNGEN . " AS v"
            . " LEFT JOIN " . TABLE_MELDUNGEN . " AS m ON m.veranstaltungs_id = v.id"
            . " WHERE 1 $where GROUP BY v.id ORDER BY $sort_by LIMIT $limit OFFSET $offset");
        $items = Common::make_array($res, '\Entity\Veranstaltung', '', true);
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "veranstaltung");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set('navigator', $navigator);
        $this->content->set("items", $items);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
    }

    /**
     * @throws SQLException
     */
    public function edit_itemAction()
    {
        $table = AutoForm::get_table_description(TABLE_VERANSTALTUNGEN);
        array_walk($table["columns"], array('\Framework\AutoForm', "map_type"));

        $no_show = array();
        $no_edit = array("id", "created", "last_change");
        $values = new Veranstaltung();
        $values->set('startpass_erforderlich', 'nein');

        if (intval(Request::get('id')) > 0) {
            $values->retrieve_by_pk(intval(Request::get('id')));
            $no_edit[] = 'sportart';
        }

        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);

        $this->content->set("form_items", $form_items);
    }

    /**
     * @return mixed
     * @throws DuplicateKeyEntryException
     * @throws SQLException
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function save_itemAction(): void
    {
        $item = new Veranstaltung();
        $item->set('bezeichnung', Request::get('bezeichnung'));
        $item->set('bezeichnung_kurz', Request::get('bezeichnung_kurz'));
        $item->set('ort', Request::get('ort'));
        $item->set('datum', Request::get('datum'));
        $item->set('url_ausschreibung', Request::get('url_ausschreibung'));
        $item->set('meldeschluss', Request::get('meldeschluss'));
        $item->set('meldebeginn', Request::get('meldebeginn'));
        $item->set('hinweise', Request::get('hinweise'));
        $item->set('startpass_erforderlich', (Request::get('startpass_erforderlich') == 'ja' ? 'ja' : 'nein'));

        if (intval(Request::get('id')) > 0) {
            $item->set('id', intval(Request::get('id')));
            $success = $item->update();
        } else {
            $item->set('sportart', Request::get('sportart'));
            $success = $item->insert();
        }
        if ($success) {
            $this->user_message[] = "Die Veranstaltung wurde gespeichert.<br>";
            $this->action = '';
        } else {
            $this->error_message[] = "Die Veranstaltung konnte nicht gespeichert werden.<br>";
            $this->action = 'edit_item';
            $this->edit_itemAction();
            return;
        }
        $this->indexAction();
    }

    /**
     * @return mixed
     * @throws ApplicationException
     * @noinspection PhpUnused
     */
    public function delete_itemsAction(): void
    {
        $ids = Request::get('ids');
        if (!is_array($ids) OR Common::array_empty($ids)) {
            $this->error_message[] = "Es wurden keine Veranstaltungen zum Löschen ausgewählt.<br>";
            $this->indexAction();
            return;
        }
        array_walk($ids, array('\Framework\LukeArraywalker', "intval_this"));

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $sql = "SELECT veranstaltungs_id FROM " . TABLE_MELDUNGEN . " WHERE veranstaltungs_id IN (" . implode(",", $ids) . ") GROUP BY veranstaltungs_id";
            $res = $this->db->query($sql);
            $veranstaltungen_mit_meldungen = Common::make_array($res, Common::FIRST_FIELD);
            if (count($veranstaltungen_mit_meldungen)) {
                $this->error_message[] = "Veranstaltungen, für die bereits Meldungen abgegeben wurden, können nicht gelöscht werden.<br>";
                $ids = array_diff($ids, $veranstaltungen_mit_meldungen);
            }
        }

        if (count($ids)) {
            /** @noinspection SqlResolve */
            $this->db->query("DELETE FROM " . TABLE_VERANSTALTUNGEN . " WHERE id IN (" . implode(",", $ids) . ")");
            $num_deleted = $this->db->affected_rows();
            $this->user_message[] = $num_deleted . " Veranstaltung" . ($num_deleted == 1 ? "" : "en") . " wurde" . ($num_deleted == 1 ? "" : "n") . " gelöscht.<br>";
        }
        $this->action = '';
        unset($_REQUEST['ids']);
        $this->indexAction();
    }

}
