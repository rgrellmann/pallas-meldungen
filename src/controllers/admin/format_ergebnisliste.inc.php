<?php
namespace Controller\admin;

use Application\Ergebnisliste;
use Framework\ControllerAbstract;
use Framework\Environment;
use Framework\Request;
use Framework\Auth;

/**
 * @noinspection PhpUnused
 */
class Format_ergebnislisteController extends ControllerAbstract {

    /** @var string */
    protected $rootpath;

    public function __construct()
    {
        parent::__construct();

        $this->rootpath = explode("/", $_SERVER['SCRIPT_NAME']);
        array_pop($this->rootpath); array_pop($this->rootpath); array_pop($this->rootpath);
        $this->rootpath = implode("/", $this->rootpath);
    }

    public function indexAction(): void
    {
        /** @noinspection SqlResolve */
        $sql = "SELECT id,bezeichnung_kurz FROM ".TABLE_VERANSTALTUNGEN." ORDER BY datum DESC";
        $res = $this->db->query($sql);
        if ($res->num_rows() < 1) {
            $veranstaltungen = "<option value=\"nix\"> keine Veranstaltung gefunden </option>\n</select>\n";
        } else {
            $veranstaltungen = "<option value=\"nix\"> - bitte ausw&auml;hlen - </option>\n";
            while ($ds = $res->fetch()) {
                $veranstaltungen.= "<option value=\"$ds[id]\"> $ds[bezeichnung_kurz]</option>\n";
            }
        }

        /** @noinspection SqlResolve */
        $sql = "SELECT id,titel1,titel2,disziplin FROM ".TABLE_WETTKAEMPFE." ORDER BY datum DESC";
        $res = $this->db->query($sql);
        if ($res->num_rows() < 1) {
            $wettkaempfe = "<option value=\"nix\"> kein Wettkampf gefunden </option>\n</select>\n";
        } else {
            $wettkaempfe = "<option value=\"nix\"> - bitte ausw&auml;hlen - </option>\n";
            while ($ds = $res->fetch()) {
                $wettkaempfe.= "<option value=\"$ds[id]\"> $ds[titel1] $ds[titel2]</option>\n";
            }
        }

        $this->content->set("veranstaltungen", $veranstaltungen);
        $this->content->set("wettkaempfe", $wettkaempfe);
        $this->content->set("rootpath", $this->rootpath);
        $this->content->set("jahr", date("Y"));
    }

    /**
     * @noinspection PhpUnused
     */
    public function daten_speichernAction(): void
    {
        if (intval(Request::get('veranstaltungs_id')) <= 0) {
            $this->error_message[] = "Sie m&uuml;ssen eine Veranstaltung aus der Liste w&auml;hlen.<br>\n";
            $this->indexAction();
            return;
        }
        /** @noinspection SqlResolve */
        $sql = "REPLACE INTO " . TABLE_WETTKAEMPFE . " (`veranstaltungs_id`,`disziplin`,`titel1`,`titel2`,`organisator`,`wettkampfleiter`,`schiedsrichter`,`streckenchef`,`startrichter`,`zielrichter`,`zeitnahme`,`kurssetzer`,`datum`,`ort`,`anz_tore`,`h_diff`,`s_laenge`,`startzeit`,`temp`,`wetter`)
                  VALUES ('" . intval(Request::get('veranstaltungs_id')) . "','$_REQUEST[titel2]','$_REQUEST[titel1]','$_REQUEST[titel2]','$_REQUEST[organisator]','$_REQUEST[wettkampfleiter]','$_REQUEST[schiedsrichter]','$_REQUEST[streckenchef]','$_REQUEST[startrichter]','$_REQUEST[zielrichter]','$_REQUEST[zeitnahme]','$_REQUEST[kurssetzer]','$_REQUEST[datum_tag].$_REQUEST[datum_monat].$_REQUEST[datum_jahr]','$_REQUEST[ort]','$_REQUEST[anz_tore]','$_REQUEST[h_diff]','$_REQUEST[s_laenge]','$_REQUEST[startzeit]','$_REQUEST[temp_start]','$_REQUEST[wetter]')";
        $this->db->query($sql);
        if ($this->db->affected_rows()) {
            $this->user_message[] = "Die Daten des Wettkampfes wurden gespeichert";
        }
        $this->indexAction();
    }

    /**
     * @noinspection PhpUnused
     */
    public function liste_einfuegenAction(): void
    {
        if (Auth::is_allowed('actions/access_resultlist_sourcecode') AND Request::get('html_quelltext')) {

            if (!is_readable("../" . Request::get('speicherort2'))) {
                $this->error_message[] = "Die Zieldatei konnte nicht eingelesen werden.";
                $this->indexAction();
                return;
            }
            $list = file_get_contents("../" . Request::get('speicherort2'));
            $_REQUEST['platzhalter_nr'] = intval($_REQUEST['platzhalter_nr']);
            $list = str_replace("<!--{PLATZHALTER" . $_REQUEST['platzhalter_nr'] . "}-->", Request::get('html_quelltext'), $list);
            if (!is_writable("../" . Request::get('speicherort2'))) {
                $this->error_message[] = "Die Zieldatei konnte nicht gespeichert werden.";
                $this->error_message[] = $php_errormsg;
                $this->indexAction();
                return;
            }
            file_put_contents("../" . Request::get('speicherort2'), $list);
            $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . $this->rootpath . "/" . Request::get('speicherort2');
            $this->user_message[] = "Die modifizierte Ergebnisliste wurde unter folgender Adresse gespeichert:<br>\n"
                . "<a href=\"$url\">$url</a>\n";
        }
        $this->indexAction();
    }

    /**
     * @noinspection PhpUnused
     */
    public function nur_hochladenAction(): void
    {
        if (Auth::is_allowed('actions/upload_files')) {
            // Ausgabe der Datei
            if (Auth::is_allowed('actions/modify_resultlist_target_path') AND Request::get('anderer_pfad')) {
                $path = "../" . Request::get('speicherort');
            } else {
                $path = "../" . Request::get('bereich') . "/bm" . Request::get('datum_jahr') . "/";
            }
            $filename = $_FILES['datei']['name'];
            if (Request::get('ueberschreiben') OR !file_exists($path . $filename)) {
                /** @noinspection PhpUsageOfSilenceOperatorInspection */
                if (!@move_uploaded_file($_FILES['datei']['tmp_name'], $path . $filename)) {
                    $this->error_message[] = "Die Datei konnte nicht angelegt bzw. &uuml;berschrieben werden.";
                    if (Auth::is_allowed('route/admin/logs')) {
                        $this->error_message[] = $php_errormsg;
                    }
                } else {
                    /** @noinspection PhpUsageOfSilenceOperatorInspection */
                    @chmod("../" . Request::get('speicherort'), 0644);
                    $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . "/" . Request::get('speicherort');
                    $this->user_message[] = "Die Ergebnisliste wurde unter folgender Adresse gespeichert:<br>\n"
                        . "<a href=\"$url\">$url</a>\n";
                }
            } else {
                $this->error_message[] = "Die Datei existiert bereits.";
            }
        }
        $this->indexAction();
    }

    /**
     * @noinspection PhpUnused
     */
    public function hochladen_und_formatierenAction(): void
    {
        $datei = fopen($_FILES['datei']['tmp_name'], "r");
        // Reihenfolge der Felder bestimmen
        $zeile = chop(fgets($datei));
        $daten = explode("\t", $zeile);
        $pos = Ergebnisliste::felder_suchen($daten);
        if ($pos["lauf_1"]) {
            $cols = 11;
        } elseif (Request::get('disziplin') == "staffel") {
            $cols = 8;
        } else {
            $cols = 9;
        }

        $out = Ergebnisliste::document_header(Request::get('titel2') . " " . Request::get('datum_jahr'), $cols);

        $_REQUEST["datum"] = $_REQUEST["datum_tag"] . "." . $_REQUEST["datum_monat"] . "." . $_REQUEST["datum_jahr"];
        $out .= Ergebnisliste::list_header($_REQUEST);

        $disziplin = Ergebnisliste::determine_discipline(Request::get('titel2'));

        $out .= Ergebnisliste::table_header($disziplin, $pos["lauf_1"]);

        $out .= Ergebnisliste::result_list($datei, Request::get('zeilenformat'), $pos, $disziplin, $cols);
        fclose($datei);

        $out .= "<tr><td colspan=\"$cols\" style=\"padding:0; font-size:5px;\"><hr style=\"width: 100%; height: 1px; border: 0 none; color: gray; background: gray;\"></td></tr>\n";
        $out .= "</table>\n";
        $out .= "<!--{PLATZHALTER2}-->\n";
        $out .= "</body>\n</html>";

        // Ausgabe der Datei
        if (Auth::is_allowed('actions/modify_resultlist_target_path') AND Request::get('anderer_pfad')) {
            $path = "../" . Request::get('speicherort');
            $filename = "";
        } else {
            $path = "../" . Request::get('bereich') . "/bm" . Request::get('datum_jahr') . "/";
            $filename = "ergebnisliste_bm" . Request::get('datum_jahr') . "_" . $disziplin . ".html";
        }
        if (Request::get('ausgabe') == "speichern" AND (!file_exists($path . $filename)
            OR (Auth::is_allowed('actions/overwrite_files') AND Request::get('ueberschreiben'))))
        {
            /** @noinspection PhpUsageOfSilenceOperatorInspection */
            if (!$datei = @fopen($path . $filename, "w")) {
                $this->error_message[] = "Die Datei konnte nicht angelegt bzw. &uuml;berschrieben werden.";
                if (Auth::is_allowed('route/admin/logs')) {
                    $this->error_message[] = $php_errormsg;
                }
            } else {
                fwrite($datei, $out);
                fclose($datei);
                if (Request::get('anderer_pfad')) {
                    $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . $this->rootpath . "/" . Request::get('speicherort');
                } else {
                    $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . $this->rootpath . "/" . Request::get('bereich') . "/bm" . Request::get('datum_jahr') . "/" . $filename;
                }
                $this->user_message[] = "Die Ergebnisliste wurde unter folgender Adresse gespeichert:<br>\n"
                    . "<a href=\"$url\">$url</a><br>Zum Speichern den Link mit der rechten Maustaste anklicken und \"Ziel speichern unter\" wählen.\n";
            }
        } else {
            header("Content-Disposition: attachment; filename=" . $filename);
            echo $out;
            die;
        }
        $this->indexAction();
    }

    /**
     * @noinspection PhpUnused
     */
    public function mit_gespeicherten_daten_formatierenAction(): void
    {
        /** @noinspection SqlResolve */
        $sql = "SELECT * FROM " . TABLE_WETTKAEMPFE . " WHERE id = " . intval(Request::get('wettkampf_id'));
        $res = $this->db->query($sql);
        if ($res->num_rows() < 1) {
            $this->error_message[] = "Wettkampf nicht gefunden<br>\n";
            $this->indexAction();
            return;
        } else {
            $ds = $res->fetch();
        }
        $datei = fopen($_FILES['datei']['tmp_name'], "r");

        // Reihenfolge der Felder bestimmen
        $zeile = chop(fgets($datei));
        $daten = explode("\t", $zeile);
        $pos = Ergebnisliste::felder_suchen($daten);

        $disziplin = Ergebnisliste::determine_discipline($ds["titel2"]);

        if ($pos["lauf_1"]) {
            $cols = 11;
        } elseif ($disziplin == "staffel") {
            $cols = 8;
        } else {
            $cols = 9;
        }

        $out = Ergebnisliste::document_header($ds['titel2'] . " " . substr($ds["datum"], -4), $cols);

        $out .= Ergebnisliste::list_header($ds);

        $out .= Ergebnisliste::table_header($disziplin, $pos["lauf_1"]);

        $out .= Ergebnisliste::result_list($datei, Request::get('zeilenformat'), $pos, $disziplin, $cols);

        fclose($datei);
        $out .= "<tr><td colspan=\"$cols\" style=\"padding:0; font-size:5px;\"><hr style=\"width: 100%; height: 1px; border: 0 none; color: gray; background: gray;\"></td></tr>\n";
        $out .= "</table>\n";
        $out .= "<!--{PLATZHALTER2}-->\n";
        $out .= "</body>\n</html>";

        // Ausgabe der Datei
        if (Auth::is_allowed('actions/modify_resultlist_target_path') AND Request::get('anderer_pfad')) {
            $path = "../" . Request::get('speicherort');
            $filename = "";
        } else {
            $path = "../" . Request::get('bereich') . "/bm" . substr($ds["datum"], -4) . "/";
            $filename = "ergebnisliste_bm" . substr($ds["datum"], -4) . "_" . $disziplin . ".html";
        }
        if (Request::get('ausgabe') == "speichern" AND (!file_exists($path . $filename)
            OR (Auth::is_allowed('actions/overwrite_files') AND Request::get('ueberschreiben'))))
        {
            /** @noinspection PhpUsageOfSilenceOperatorInspection */
            if (!$datei = @fopen($path . $filename, "w")) {
                $this->error_message[] = "Die Datei konnte nicht angelegt bzw. &uuml;berschrieben werden.";
                if (Auth::is_allowed('route/admin/logs')) {
                    $this->error_message[] = $php_errormsg;
                }
            } else {
                fwrite($datei, $out);
                fclose($datei);
                if (Request::get('anderer_pfad')) {
                    $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . $this->rootpath . "/" . Request::get('speicherort');
                } else {
                    $url = Environment::scheme() . $_SERVER["SERVER_NAME"] . $this->rootpath . "/" . Request::get('bereich') . "/bm" . substr($ds["datum"], -4) . "/" . $filename;
                }
                $this->user_message[] = "Die Ergebnisliste wurde unter folgender Adresse gespeichert:<br>\n"
                    . "<a href=\"$url\">$url</a><br>Zum Speichern den Link mit der rechten Maustaste anklicken und \"Ziel speichern unter\" w&auml;hlen.\n";
            }
        } else {
            header("Content-Disposition: attachment; filename=" . $filename);
            echo $out;
            die;
        }
        $this->indexAction();
    }

}
