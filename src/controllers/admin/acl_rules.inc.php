<?php
namespace Controller\admin;

use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Common;

/**
 * @noinspection PhpUnused
 */
class Acl_rulesController extends ControllerAbstract
{

    public function indexAction(): void
    {
        switch (Request::get('type')) {
            case 'routes':
                /** @noinspection SqlResolve */
                $sql = "SELECT * FROM ".TABLE_ACL_RULES." WHERE resource LIKE 'routes%' ORDER BY resource";
                break;
            case 'controllers':
                /** @noinspection SqlResolve */
                $sql = "SELECT * FROM ".TABLE_ACL_RULES." WHERE resource LIKE 'controllers%' ORDER BY resource";
                break;
            case 'actions':
            default:
            /** @noinspection SqlResolve */
                $sql = "SELECT * FROM ".TABLE_ACL_RULES." WHERE resource LIKE 'actions%' ORDER BY resource";
        }
        $res = $this->db->query($sql);
        $rules = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);

        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT role_id,role_name FROM ".TABLE_ACL_RULES." INNER JOIN ".TABLE_ROLES." USING (role_id) ORDER BY role_id");
        $roles = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE) + array('NULL' => 'Guest');

        $temp = array();

        foreach ($rules as $rule) {
            if (!isset($temp[$rule['resource']])) {
                $temp[$rule['resource']] = array();
            }
            $temp[$rule['resource']][($rule['role_id']===null?'NULL':$rule['role_id'])] = $rule['rule'];
        }

        $this->content->set("rules",  $temp);
        $this->content->set("roles", $roles);
    }

}
