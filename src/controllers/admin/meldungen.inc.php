<?php
namespace Controller\admin;

use Application\Altersklassen;
use Entity\Meldung;
use Entity\Veranstaltung;
use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Common;
use Framework\Search;
use Framework\Pagination;
use Framework\AutoForm;
use Framework\SQLException;
use Framework\Template;
use Framework\Auth;

/**
 * @noinspection PhpUnused
 */
class MeldungenController extends ControllerAbstract
{

    /** @var Meldung */
    public $item;

    /** @var Veranstaltung */
    public $veranstaltung;

    /**
     * @throws SQLException
     */
    public function __construct()
    {
        parent::__construct();
        $this->item = new Meldung();
        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT id,bezeichnung_kurz FROM " . TABLE_VERANSTALTUNGEN . " ORDER BY id DESC");
        $veranstaltungen = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);

        $veranstaltungs_id = $this->session->get_from_session_or_request("veranstaltung");

        if (empty($veranstaltungs_id) OR !array_key_exists($veranstaltungs_id, $veranstaltungen)) {
            reset($veranstaltungen);
            $veranstaltungs_id = intval(key($veranstaltungen));
            $this->session->set('veranstaltung', $veranstaltungs_id);
        }
        $this->veranstaltung = new Veranstaltung($veranstaltungs_id);

        $this->content->set('veranstaltung', $this->veranstaltung);
        $this->content->set('veranstaltungen', $veranstaltungen);
    }

    /**
     * @return void
     * @throws SQLException
     */
    public function indexAction(): void
    {
        list($limit, $offset) = Pagination::handle_limit_offset();
        if (empty($_REQUEST['sort_desc'])) {
            $desc = "";
        } else {
            $desc = " DESC";
        }
        switch (Request::get('sort_by')) {
            case "name":
                $sort_by = "m.name" . $desc;
                break;
            case "vorname":
                $sort_by = "m.vorname" . $desc;
                break;
            case "geschlecht":
                $sort_by = "m.geschlecht $desc, m.status, m.jahrgang DESC";
                break;
            case "verein":
                $sort_by = "m.verein $desc, m.status, m.geschlecht, m.jahrgang DESC";
                break;
            case "jahrgang":
                $sort_by = "m.jahrgang $desc, m.status, m.geschlecht";
                break;
            case "status":
                $sort_by = "m.status $desc, m.geschlecht, m.jahrgang DESC";
                break;
            case "id":
                $sort_by = "m.id" . $desc;
                break;
            default:
                $sort_by = "m.status, m.geschlecht, m.jahrgang DESC";
                $_REQUEST['sort_by'] = "status";
                $_REQUEST['sort_desc'] = false;
        }

        $where = Search::process("meldung", "m", '\Entity\Meldung');
        /** @noinspection SqlResolve */
        $sql = "SELECT SQL_CALC_FOUND_ROWS m.*,vb.name_5,GROUP_CONCAT(d.name ORDER BY vd.id) AS disziplinen"
            . " FROM " . TABLE_MELDUNGEN . " AS m"
            . " LEFT JOIN " . TABLE_VERBAENDE . " AS vb ON m.verband = vb.code"
            . " LEFT JOIN " . TABLE_MELDUNGEN_DISZIPLINEN . " AS md ON md.meldung_id = m.id"
            . " LEFT JOIN " . TABLE_VERANSTALTUNGEN_DISZIPLINEN . " AS vd ON md.veranstaltung_disziplin_id = vd.id"
            . " LEFT JOIN " . TABLE_DISZIPLINEN . " AS d ON vd.disziplin_id = d.id"
            . " WHERE m.veranstaltungs_id = " . $this->veranstaltung->get_id() . " $where AND status = ? GROUP BY m.id"
            . " ORDER BY $sort_by LIMIT ? OFFSET ?";

        $stmt = $this->db->prepare($sql);

        $status = 'gemeldet';
        $params = [&$status, &$limit, &$offset];
        $res = $this->db->execute($stmt, $params);
        $items = Common::make_array($res, '\Entity\Meldung', '', true);
        $num_total = $this->db->get_found_rows();
        $navigator = Pagination::build_page_tabs($num_total, $limit);

        $altersklassen = Altersklassen::altersklassenliste($items, $this->veranstaltung);

        $status = 'gestrichen';
        $limit = 9999;
        $offset = 0;
        $params = [&$status, &$limit, &$offset];
        $res = $this->db->execute($stmt, $params);
        $items_withdrawn = Common::make_array($res, '\Entity\Meldung', '', true);

        $status = 'mafue';
        $params = [&$status, &$limit, &$offset];
        $res = $this->db->execute($stmt, $params);
        $items_captains = Common::make_array($res, '\Entity\Meldung', '', true);

        $searchform = new Template("partials/searchform");
        $searchform->set("context", "meldung");
        $this->content->set("searchform", $searchform->fetch());

        $this->content->set('navigator', $navigator);
        $this->content->set("items", $items);
        $this->content->set("items_withdrawn", $items_withdrawn);
        $this->content->set("items_captains", $items_captains);
        $this->content->set("offset", $offset);
        $this->content->set("num_total", $num_total);
        $this->content->set("altersklassen", $altersklassen);
    }

    /**
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function edit_meldungAction()
    {
        $table = AutoForm::get_table_description(TABLE_MELDUNGEN);
        array_walk($table["columns"], array('\Framework\AutoForm', "map_type"));

        $no_show = array('verband_1');
        $no_edit = array('id', 'datum', 'created', 'last_change', 'hash', 'veranstaltungs_id');
        $values = new Meldung();

        if (intval(Request::get('id')) > 0) {
            $values->retrieve_by_pk(intval(Request::get('id')));
            $values->fetch_disziplinen();
        } else {
            $values->set('status', 'gemeldet');
            $values->set_veranstaltungs_id($this->veranstaltung->get_id());
        }

        /** @noinspection SqlResolve */
        $res = $this->db->query("SELECT code,CONCAT(name_5,' / ',name) FROM " . TABLE_VERBAENDE);
        $verbaende = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);
        $table["columns"]['verband']['Type'] = "select";
        $table["columns"]['verband']['Values'] = $verbaende;

        $veranstaltung = $this->veranstaltung;
        $table["columns"]['veranstaltungs_id']['Type'] = "select";
        $table["columns"]['veranstaltungs_id']['Values'] = [$veranstaltung->get_id() => $veranstaltung->get_bezeichnung_kurz()];

        $v_disziplinen = $veranstaltung->get_disziplinen_array();
        if (count($v_disziplinen)) {
            $table["columns"]["disziplinen"] = [];
            $table["columns"]["disziplinen"]['Field'] = 'disziplinen';
            $table["columns"]["disziplinen"]["Type"] = "checkbox";
            $table["columns"]["disziplinen"]["Values"] = $v_disziplinen;
            $table["columns"]["disziplinen"]["Comment"] = "";
            $table["columns"]["disziplinen"]["Label"] = "Disziplinen";
        }

        if (intval(Request::get('id')) > 0) {
            $no_edit[] = 'veranstaltungs_id';
            if ($values->get('status') == 'mafue') {
                $no_edit[] = 'status';
                $no_show[] = 'disziplinen';
                $no_show[] = 'jahrgang';
                $no_show[] = 'geschlecht';
                $no_show[] = 'gemeldet_von';
                $no_show[] = 'startpass_nr';
            } else {
                /** @noinspection SqlResolve */
                $res = $this->db->query("SELECT id,CONCAT(vorname,' ' ,name,' / ',verein) FROM " . TABLE_MELDUNGEN
                    . " WHERE status = 'mafue' AND veranstaltungs_id = " . intval($values->get('veranstaltungs_id'))
                );
                $mafues = array('' => '-') + Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);
                $table["columns"]['gemeldet_von']['Type'] = "select";
                $table["columns"]['gemeldet_von']['Values'] = $mafues;
            }
        } else {
            $no_edit[] = 'gemeldet_von';
        }

        $form_items = AutoForm::automated_form($table["columns"], $no_show, $no_edit, $values);

        $this->content->set("form_items", $form_items);
    }

    /**
     * @return void
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function save_meldungAction(): void
    {
        try {
            if (!Auth::is_allowed('edit_personal_data')
                AND !Auth::is_allowed('actions/edit_personal_data_if_club_match')
                AND !Auth::is_allowed('actions/change_registration_status')
                AND !Auth::is_allowed('actions/change_registration_status_if_club_match')
            ) {
                throw new \Exception('Zugriff verweigert');
            }

            $veranstaltung = $this->veranstaltung;
            $item = new Meldung();
            $id = intval(Request::get('id'));
            if ($id > 0) {
                if (!$item->retrieve_by_pk($id)) {
                    throw new \Exception("Datensatz nicht gefunden.");
                }
            } else {
                $item->set('veranstaltungs_id', $veranstaltung->get_id());
                $item->set('datum', 'NOW()');
                $item->set('hash', hash('sha256', 'V'.$veranstaltung->get_id().microtime().mt_rand()));
            }

            if (Auth::is_allowed('actions/edit_personal_data')
                OR (Auth::is_allowed('actions/edit_personal_data_if_club_match')
                    AND $item->get('verein') != ""
                    AND $this->session->get_user()->get('verein') == $item->get('verein')
                )
            ) {
                $item->set('name', Request::get('name'));
                $item->set('vorname', Request::get('vorname'));
                $item->set('jahrgang', Request::get('jahrgang'));
                $item->set('geschlecht', Request::get('geschlecht'));
                $item->set('telefon', Request::get('telefon'));
                $item->set('handy', Request::get('handy'));
                $item->set('email', Request::get('email'));
            }

            if (Auth::is_allowed('actions/edit_personal_data')) {
                $item->set('gemeldet_von', Request::get('gemeldet_von'));
                $item->set('verein', Request::get('verein'));
                $item->set('verband_1', Request::get('verband_1'));
                $item->set('verband', Request::get('verband'));
            }

            if (Auth::is_allowed('actions/change_registration_status')
                OR Auth::is_allowed('actions/change_registration_status_if_club_match')
                    AND $item->get('verein') != ""
                    AND $this->session->get_user()->get('verein') == $item->get('verein')
            ) {
                $item->set('kommentar', Request::get('kommentar'));
                $item->set('status', Request::get('status'));
                $item->set('startpass_nr', Request::get('startpass_nr'));

                if ($v_disziplinen = $veranstaltung->get_disziplinen_array()) {
                    // im Request kommen die Disziplinen-Keys als Array Values ⇒ flip
                    $r_disziplinen = array_flip(Request::get('disziplinen'));
                    $disziplinen_selected = array_intersect_ukey(
                        $v_disziplinen,
                        $r_disziplinen,
                        function($a, $b) { return (strval($a) > strval($b) ? 1 : (strval($a) < strval($b) ? -1 : 0 )); }
                    );
                    $item->set_disziplinen(array_keys($disziplinen_selected));
                }
            }

            if ($item->get_id() > 0) {
                $success = $item->update();
            } else {
                $success = $item->insert();
            }

            if ($success AND $veranstaltung->get_disziplinen()) {
                $item->save_disziplinen();
            }

            if ($success) {
                $this->user_message[] = "Die Meldung wurde gespeichert.<br>";
                $this->action = '';
                $this->indexAction();
                return;
            }
        } catch (\Exception $e) {
            $this->error_message[] = $e->getMessage();
            $this->action = '';
            $this->indexAction();
            return;
        }
        $this->indexAction();
    }

    /**
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function delete_meldungAction(): void
    {

        if (Auth::is_allowed('actions/delete_meldung')) {
            $ids = Request::get('ids');
            if (!is_array($ids) OR Common::array_empty($ids)) {
                $this->error_message[] = "Es wurden keine Meldungen zum Löschen ausgewählt.<br>";
                $this->indexAction();
                return;
            }
            array_walk($ids, array('\Framework\LukeArraywalker', "intval_this"));

            if (count($ids)) {
                /** @noinspection SqlResolve */
                $this->db->query("DELETE FROM " . TABLE_MELDUNGEN . " WHERE id IN (" . implode(",", $ids) . ")");
                $num_deleted = $this->db->affected_rows();
                $this->user_message[] = $num_deleted . " Meldung" . ($num_deleted == 1 ? "" : "en") . " wurde" . ($num_deleted == 1 ? "" : "n") . " gelöscht.<br>";
            }
        }
        unset($_REQUEST['ids']);
        $this->action = '';
        $this->indexAction();
    }

    /**
     * @throws SQLException
     * @noinspection PhpUnused
     */
    public function change_statusAction(): void
    {
        try {
            $item = new Meldung();
            $id = Request::get('id');
            if (empty($id) OR !$item->retrieve_by_pk(intval(Request::get('id')))) {
                throw new \Exception("Datensatz nicht gefunden.");
            }
            if (!Auth::is_allowed('actions/change_registration_status') AND
                !(Auth::is_allowed('actions/change_registration_status_if_club_match') AND $item->get('verein') != "" AND $this->session->get_user()->get('verein') == $item->get('verein'))
            ) {
                throw new \Exception('Zugriff verweigert');
            }
            if (in_array(Request::get('status'), array('gemeldet', 'gestrichen'))) {
                $item->set('status', Request::get('status'));
                $item->update();
                $this->user_message[] = "Der Status der Meldung wurde geändert.<br>";
            }
            $this->action = '';
        } catch (\Exception $e) {
            $this->error_message[] = $e->getMessage();
            $this->action = '';
            $this->indexAction();
            return;
        }
        $this->indexAction();
    }

}
