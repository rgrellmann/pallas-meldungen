<?php
namespace Controller;

use Application\ViewHelpers;
use Entity\Meldung;
use Entity\Veranstaltung;
use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Conf;
use Framework\Common;
use Framework\Mail;
use Framework\SQLException;
use Framework\Template;
use Framework\DateAndTime;
use Framework\DebugTools;

/**
 * @noinspection PhpUnused
 */
class MeldeformularController extends ControllerAbstract
{
    /** @var Veranstaltung */
    protected $veranstaltung;

    /**
     * @throws SQLException
     */
    protected function pre_dispatch(): void
    {
        parent::pre_dispatch();
        $this->veranstaltung = null;
        if (Request::get('veranstaltung') > 0) {
            $veranstaltung = new Veranstaltung(intval(Request::get('veranstaltung')));
            $veranstaltung->fetch_disziplinen();
            if ($veranstaltung->get_id()) {
                $this->veranstaltung = $veranstaltung;
            }
        }
    }

    public function indexAction(): void
    {
        /** @noinspection SqlResolve */
        $sql = "SELECT id,bezeichnung_kurz FROM " . TABLE_VERANSTALTUNGEN . " WHERE meldebeginn < NOW() AND meldeschluss > NOW()";
        $res = $this->db->query($sql);
        $veranstaltungen = Common::make_array($res, '\Entity\Veranstaltung', 'id');
        $this->content->set('veranstaltungen', $veranstaltungen);
    }


    public function einzelmeldungAction(): void {
        $this->meldeformular();
    }

    public function gruppenmeldungAction(): void {
        $this->meldeformular();
    }

    protected function meldeformular(): void {
        try {
            if (!is_object($this->veranstaltung)) {
                throw new \RuntimeException('Veranstaltung nicht gefunden oder keine Veranstaltung angegeben.');
            }
            if ($this->veranstaltung->get('meldebeginn') > DateAndTime::datetime_now()) {
                throw new \RuntimeException('F&uuml;r diese Veranstaltung werden noch keine Meldungen angenommen.');
            }
            if ($this->veranstaltung->get('meldeschluss') < DateAndTime::datetime_now()) {
                throw new \RuntimeException('Der Meldeschlu&szlig; f&uuml;r diese Veranstaltung ist bereits abgelaufen.');
            }
        } catch (\Exception $e) {
            $this->error_message[] = $e->getMessage();
            $this->indexAction();
            return;
        }
        $form = new Template("partials/" . $this->action);
        $form->set('veranstaltung', $this->veranstaltung);
        $this->content->set("partial", $form);
    }

    /**
     * @noinspection PhpUnused
     */
    public function einzelmeldung_speichernAction(): void {
        try {
            $partial = new Template("partials/meldung_speichern");
            $partial->set('veranstaltung', $this->veranstaltung);
            try {
                if (!$this->veranstaltung) {
                    throw new \Exception("Fehler: Veranstaltung nicht gefunden.");
                }

                $jetzt[0] = date("Y-m-d H:i:00");
                $jetzt[1] = date("d.m.Y, H:i:00");
                if ($jetzt[0] > $this->veranstaltung->get("meldeschluss")) {
                    throw new \Exception("Die Meldefrist ist abgelaufen.<br>Es k&ouml;nnen leider keine Meldungen mehr angenommen werden.");
                }

                if (!Request::get("akzeptiert")) {
                    throw new \InvalidArgumentException("Sie m&uuml;ssen ankreuzen, da&szlig; Sie die Ausschreibung gelesen haben und damit einverstanden sind!");
                }

                $meldung = new Meldung();
                $meldung->set('veranstaltungs_id', $this->veranstaltung->get_id());
                $meldung->set('datum', 'NOW()');
                $meldung->required_fields = array('name', 'vorname', 'jahrgang', 'geschlecht');
                if ($this->veranstaltung->get("startpass_erforderlich") == 'ja') {
                    $meldung->required_fields[] = 'startpass_nr';
                }
                if (!$meldung->get_data_from_request('POST')) {
                    throw new \InvalidArgumentException(implode('<br>', $meldung->validation_errors));
                }

                $meldung->check_gemeldet($this->veranstaltung->get_id());
                $meldung->generate_hash();
                $id = $meldung->insert();
                if (!$id) {
                    throw new \Exception("Fehler beim Datenbankzugriff. Die Meldung wurde nicht gespeichert.<br>");
                }
                if ($this->veranstaltung->has_disziplinen()) {
                    $meldung->save_disziplinen();
                }
                $this->user_message[] = "Ihre Meldung wurde in die Datenbank des Skiverbandes Berlin eingetragen.<br>\n";

                $text = "   Meldung für: " . $this->veranstaltung->get("bezeichnung");
                $betreff = "Meldung für: " . $this->veranstaltung->get("bezeichnung");
                $text .= "\n\nName:          " . $meldung->get('name') . "\nVorname:       " . $meldung->get('vorname') . "\nJahrgang:      " . $meldung->get('jahrgang') . "\n";
                $text .= "Geschlecht:    " . $meldung->get('geschlecht') . "\nVerein:        " . $meldung->get('verein') . "\nLandesverband: " . $meldung->get('verband') . "\n";
                $text .= "Telefon:       " . $meldung->get('telefon') . "\nHandy:         " . $meldung->get('handy') . "\nemail:         " . $meldung->get('email') . "\n";
                if ($this->veranstaltung->has_disziplinen()) {
                    $disziplinen = array_intersect_ukey(
                        $this->veranstaltung->get_disziplinen_array(),
                        array_flip($meldung->get_disziplinen()),
                        function($a, $b) { return (strval($a) > strval($b) ? 1 : (strval($a) < strval($b) ? -1 : 0 )); }
                    );
                    $text .= "Disziplin(en): " . implode(",", $disziplinen) . "\n";
                }
                if ($meldung->get('startpass_nr')) {
                    $text .= "Startpass-Nr:  " . $meldung->get('startpass_nr') . " \n";
                }
                $text .= "Kommentar:\n" . $meldung->get('kommentar') . "\n";
                $text .= "Datum/Uhrzeit der Meldung: " . $jetzt[1] . " Uhr\n";
                $text2 = $text . "\n\nVielen Dank für Ihre Anmeldung!\nWenn Sie eine Meldung zurückziehen oder ändern möchten,\nschreiben Sie eine E-Mail an: meldungen@scpallas.de\n";
                // gehen Sie zu folgender URL und loggen Sie sich mit Ihrer E-Mail-Adresse und dem Passwort \"$password\" ein:\n$pfad/meldung_aendern.php\noder

                if ($meldung->get('name') != "TEST") {
                    $empfaenger = "meldungen@scpallas.de";
                    if (Conf::get('is_local_test_system')) {
                        $empfaenger = "test@local.agentmulder.de";
                    }
                    $mail = new Mail();
                    $mail->set_from($meldung->get('email'), "\"" . $meldung->get('vorname') . " " . $meldung->get('name') . "\"");
                    $mail->addTo($empfaenger);
                    $mail->setSubject($betreff);
                    $mail->set_body_text($text);
                    if (!$mail->send()) {
                        DebugTools::add_debug_message(ViewHelpers::debug_msg($php_errormsg, __LINE__));
                        $this->error_message[] = "Das Senden der E-Mail an den Skiverband Berlin ist fehlgeschlagen.<br>\n";
                    }
                } else {
                    DebugTools::add_debug_message("<pre>$text2</pre>");
                }
                if ($meldung->get('email')) {
                    $this->user_message[] = "Sie erhalten in K&uuml;rze eine Best&auml;tigungs-E-Mail.<br><br>";
                    // Ihr Passwort, falls Sie eine Meldung ändern oder zurückziehen möchten: \"$password\"<br>
                    if (Conf::get('is_local_test_system')) {
                        $meldung->set("email", "test@local.agentmulder.de");
                    }
                    $mail = new Mail();
                    $mail->addTo($meldung->get('email'));
                    $mail->setSubject($betreff);
                    $mail->set_body_text($text2);
                    if (!$mail->send()) {
                        DebugTools::add_debug_message(ViewHelpers::debug_msg($php_errormsg, __LINE__));
                        $this->error_message[] = "Das Senden der E-Mail an " . $meldung->get('email') . " ist fehlgeschlagen.<br>\n";
                    }
                }
                $this->content->set("partial", $partial);

            } catch (\InvalidArgumentException $e) {
                $this->error_message[] = $e->getMessage();
                $this->action = 'einzelmeldung';
                $this->einzelmeldungAction();
                return;
            } catch (\Exception $e) {
                $this->error_message[] = $e->getMessage();
                $this->content->set("partial", $partial);
            }
        } catch (\Exception $e) {
            $this->error_message[] = $e->getMessage();
        }
    }

    /**
     * @noinspection PhpUnused
     */
    public function gruppenmeldung_speichernAction(): void {

        $partial = new Template("partials/meldung_speichern");
        try {
            if (!$this->veranstaltung) {
                throw new \Exception("Fehler: Veranstaltung nicht gefunden.");
            }
            $partial->set('veranstaltung', $this->veranstaltung);
            $meldung_mafue = new Meldung();
            $meldung_mafue->set('veranstaltungs_id', $this->veranstaltung->get_id());
            $meldung_mafue->set_status('mafue');

            $jetzt[0] = date("Y-m-d H:i:00");
            $jetzt[1] = date("d.m.Y, H:i:00");
            if ($jetzt[0] > $this->veranstaltung->get("meldeschluss")) {
                throw new \Exception("Die Meldefrist ist abgelaufen.<br>Es k&ouml;nnen leider keine Meldungen mehr angenommen werden.");
            }

            $name_mafue = Request::get("name_mafue");
            $vorname_mafue = Request::get("vorname_mafue");
            $telefon_mafue = Request::get("telefon_mafue");
            $handy_mafue = Request::get("handy_mafue");
            $email_mafue = Request::get("email_mafue");
            $verein = Request::get("verein");
            $verband = Request::get("verband");
            $kommentar = Request::get("kommentar");
            $akzeptiert = Request::get("akzeptiert");

            $korrekt = true;
            $error_message = [];
            if ($meldung_mafue->validate_name($name_mafue)) {
                $meldung_mafue->set_name($name_mafue);
            }
            if ($meldung_mafue->validate_vorname($vorname_mafue)) {
                $meldung_mafue->set_vorname($vorname_mafue);
            }
            if ($telefon_mafue AND $meldung_mafue->validate_telefon($telefon_mafue)){
                $meldung_mafue->set_telefon($telefon_mafue);
            }
            if ($handy_mafue AND $meldung_mafue->validate_handy($handy_mafue)){
                $meldung_mafue->set_handy($handy_mafue);
            }
            if ($email_mafue AND $meldung_mafue->validate_email($email_mafue)){
                $meldung_mafue->set_email($email_mafue);
            }
            if ($kommentar AND $meldung_mafue->validate_kommentar($kommentar)){
                $meldung_mafue->set_kommentar($kommentar);
            }
            $meldung_mafue->check_contact_data_integrity();
            if ($meldung_mafue->validation_errors) {
                $korrekt = false;
                $error_message += $meldung_mafue->validation_errors;
            }
            $meldung_mafue->set_verein($verein);
            if (!$meldung_mafue->get_verein()) {
                $error_message[] = "Sie m&uuml;ssen Ihren Verein aus der Liste w&auml;hlen oder in das Textfeld eintragen!<br>\n";
                $korrekt = false;
            }
            if (($verein == "Gast" OR $verein == "nix") AND (!$verband OR $verband == "21")) {
                $error_message[] = "Sie m&uuml;ssen Ihren Landesverband aus der Liste w&auml;hlen, wenn Sie f&uuml;r einen Verein starten, der nicht zum Skiverband Berlin geh&ouml;rt!<br>\n";
                $korrekt = false;
            }
            $meldung_mafue->set_verband($verband);

            if (!$akzeptiert) {
                $error_message[] = "Sie m&uuml;ssen ankreuzen, da&szlig; Sie die Ausschreibung gelesen haben!<br>\n";
                $korrekt = false;
            }

            if (!$korrekt) {
                throw new \InvalidArgumentException(implode("<br>", $error_message));
            }

            $teilnehmer = [];
            for ($x = 1; $x <= 30; $x++) {
                $name = Request::get("name_" . $x);
                $vorname = Request::get("vorname_" . $x);
                $jahrgang = Request::get("jahrgang_" . $x);
                $geschlecht = Request::get("geschlecht_" . $x);
                $disziplinen = Request::get("disziplinen_" . $x);
                $startpass = Request::get("startpass_" . $x);

                if (!empty($name) OR !empty($vorname) OR !empty($jahrgang)) {
                    $meldung = new Meldung();
                    $meldung->set('veranstaltungs_id', $this->veranstaltung->get_id());
                    $meldung->set_status('gemeldet');
                    $meldung->set_gemeldet_von($meldung_mafue->get_id());
                    $meldung->set_verband($verband);
                    $meldung->set_verein($verein);
                    $meldung->set('x', $x);
                    if ($meldung->validate_name($name)) {
                        $meldung->set_name($name);
                    }
                    if ($meldung->validate_vorname($vorname)) {
                        $meldung->set_vorname($vorname);
                    }
                    if ($meldung->validate_jahrgang($jahrgang)) {
                        $meldung->set_jahrgang($jahrgang);
                    }
                    if ($meldung->validate_geschlecht($geschlecht)) {
                        $meldung->set_geschlecht($geschlecht);
                    }
                    if ($meldung->validate_startpass_nr($startpass, ($this->veranstaltung->get('startpass_erforderlich') == 'ja'))) {
                        $meldung->set_startpass_nr($startpass);
                    }
                    if ($meldung->validate_disziplinen($disziplinen, $this->veranstaltung)) {
                        $meldung->set_disziplinen($disziplinen);
                    }
                    if ($meldung->validation_errors) {
                        $korrekt = false;
                        foreach ($meldung->validation_errors as &$validation_error) {
                            $validation_error = "Teilnehmer $x: " . $validation_error;
                        }
                        $error_message += $meldung->validation_errors;
                    } elseif ($korrekt) {
                        // wenn bereits Validierungsfehler aufgetreten sind, können die folgenden Meldungen verworfen werden
                        // Meldung also nur merken, wenn bis jetzt noch alles korrekt war
                        $teilnehmer[] = $meldung;
                    }
                }
            }
            if (!$korrekt) {
                throw new \InvalidArgumentException(implode("<br>", $error_message));
            }
            $meldung_mafue->generate_hash();
            $meldung_mafue->insert();
            if (!$meldung_mafue->get_id()) {
                throw new \Exception("Fehler beim Datenbankzugriff. Die Meldung wurde nicht gespeichert.<br>");
            }
            
            $text = "   Meldung für: " . $this->veranstaltung->get("bezeichnung");
            $betreff = "Meldung für: " . $this->veranstaltung->get("bezeichnung");
            $text .= "\n\nName:       {$meldung_mafue->get_name()}\nVorname:    {$meldung_mafue->get_vorname()}\n";
            $text .= "Verein:     {$meldung_mafue->get_verein()}\nTelefon:    {$meldung_mafue->get_telefon()}\n";
            $text .= "Handy:      {$meldung_mafue->get_handy()}\nemail:      {$meldung_mafue->get_email()}\n";
            $text .= "Kommentar:\n{$meldung_mafue->get_kommentar()}\n\ngemeldete Teilnehmer:\n";

            $anz_gemeldet = 0;
            foreach ($teilnehmer as $meldung) {
                /** @var Meldung $meldung */
                try {
                    $meldung->check_gemeldet($this->veranstaltung->get_id());
                } catch (\Exception $e) {
                    $this->error_message[] = "Teilnehmer {$meldung->get('x')}: " . $e->getMessage();
                    continue;
                }
                $meldung->insert();
                if (!$meldung->get_id()) {
                    throw new \Exception("Fehler beim Datenbankzugriff. Die Meldung wurde nicht gespeichert.<br>");
                }

                $text_disziplinen = "";
                if ($this->veranstaltung->has_disziplinen()) {
                    $meldung->save_disziplinen();
                    $disziplinen = array_intersect_ukey(
                        $this->veranstaltung->get_disziplinen_array(),
                        array_flip($meldung->get_disziplinen()),
                        function($a, $b) { return (strval($a) > strval($b) ? 1 : (strval($a) < strval($b) ? -1 : 0 )); }
                    );
                    $text_disziplinen = " Disziplin(en): " . implode(",", $disziplinen);
                }
                $text .= sprintf("%-30s", $meldung->get_name() . ", " . $meldung->get_vorname()) . " " . $meldung->get_jahrgang() . "  " . $meldung->get_geschlecht() . " " . sprintf("%-4s", $meldung->get_startpass_nr()) . $text_disziplinen . "\n";
                $anz_gemeldet++;
            }
            if ($anz_gemeldet <= 0) {
                $meldung_mafue->delete();
                throw new \Exception("Es wurden keine Teilnehmer eingetragen oder alle Teilnehmer waren bereits gemeldet.<br>\n");
            }
            $text .= "\nDatum/Uhrzeit der Meldung: " . $jetzt[1] . " Uhr\n";
            $text2 = $text . "\n\nVielen Dank für Ihre Anmeldung!\nWenn Sie eine Meldung zurückziehen oder ändern möchten,\nschreiben Sie eine email an: meldungen@scpallas.de\n"; // gehen Sie zu folgender URL und loggen Sie sich mit Ihrer E-Mail-Adresse und dem Passwort \"$password\" ein:\n$pfad/meldung_aendern.php\noder
            $this->user_message[] = "Ihre Meldung wurde in die Datenbank des Skiverbandes Berlin eingetragen.<br>\n";
            if ($name_mafue != "TEST") {
                $empfaenger = "meldungen@scpallas.de";
                if (Conf::get('is_local_test_system')) {
                    $empfaenger = "test@local.agentmulder.de";
                }
                $mail = new Mail();
                $mail->set_from($email_mafue, "\"" . $vorname_mafue . " " . $name_mafue . "\"");
                $mail->addTo($empfaenger);
                $mail->setSubject($betreff);
                $mail->set_body_text($text);
                if (!$mail->send()) {
                    DebugTools::add_debug_message(ViewHelpers::debug_msg($php_errormsg, __LINE__));
                    $this->error_message[] = "Das Senden der email an den Skiverband ist fehlgeschlagen.<br>\n";
                }
            } else {
                DebugTools::add_debug_message("<pre>$text</pre>");
            }
            if (!empty($email_mafue)) {
                $this->user_message[] = "Sie erhalten in K&uuml;rze eine Best&auml;tigungs-email.<br><br>";
                if (Conf::get('is_local_test_system')) {
                    $email_mafue = "test@local.agentmulder.de";
                }
                $mail = new Mail();
                $mail->addTo($email_mafue);
                $mail->setSubject($betreff);
                $mail->set_body_text($text2);
                if (!$mail->send()) {
                    DebugTools::add_debug_message(ViewHelpers::debug_msg($php_errormsg, __LINE__));
                }
            }
            $partial->set('grupenmeldung', true);
            $this->content->set("partial", $partial);

        } catch (\InvalidArgumentException $e) {
            $this->error_message[] = $e->getMessage();
            $this->action = 'gruppenmeldung';
            $this->gruppenmeldungAction();
            return;
        } catch (\Exception $e) {
            $this->error_message[] = $e->getMessage();
        }
    }

}
