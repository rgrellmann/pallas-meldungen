<?php
namespace Controller\webservice;

use Framework\ControllerAbstract;
use Framework\Request;
use Framework\Common;

/**
 * @noinspection PhpUnused
 */
class MeldungenController extends ControllerAbstract
{

    public function indexAction(): void
    {
        $veranstaltung = (int)Request::get('veranstaltung');
        if ($veranstaltung === 0) {
            http_response_code(400);
            die("veranstaltung fehlt");
        }
        $sql = "SELECT id,name,vorname,jahrgang,geschlecht,verein,verband,telefon,handy,email,kommentar,disziplin,startpass_nr"
            . " FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '$veranstaltung' AND status = 'gemeldet' ORDER BY jahrgang,geschlecht";
        $res = $this->db->query($sql);
        $out = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        $this->return_json($out);
    }

}
