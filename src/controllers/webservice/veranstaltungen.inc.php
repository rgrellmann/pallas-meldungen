<?php
namespace Controller\webservice;

use Framework\ControllerAbstract;
use Framework\Common;

/**
 * @noinspection PhpUnused
 */
class VeranstaltungenController extends ControllerAbstract
{

    public function indexAction(): void
    {
        $sql = "SELECT * FROM ".TABLE_VERANSTALTUNGEN." ORDER BY datum DESC";
        $res = $this->db->query($sql);
        $out = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);

        $this->return_json($out);
    }

}
