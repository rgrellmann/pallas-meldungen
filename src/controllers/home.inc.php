<?php
namespace Controller;

use Framework\App;
use Framework\ControllerAbstract;
use Framework\Logger;
use Framework\Request;
use Framework\Auth;

/**
 * @noinspection PhpUnused
 */
class HomeController extends ControllerAbstract
{

    public function pre_dispatch(): void
    {
        if (App::get_route() == 'login') {
            $this->action = "login";
        } elseif (App::get_route() == 'logout') {
            $this->action = "logout";
        }
    }

    public function indexAction(): void
    {
    }

    /**
     * @noinspection PhpUnused
     */
    public function loginAction(): void {
        $form_errors = array();

        if (strlen(Request::get(PROJECT_IDENTIFIER . '_username', 'POST'))) {
            if ($this->session->get('failed_login_count') >= 10) {
                if ($this->session->get('failed_login_timestamp') > time() - 300) {
                    $form_errors["login_form.password"] = "Zu viele falsche Eingaben. Bitte warten Sie 5 Minuten bis zum nächsten Versuch.";
                } else {
                    $this->session->set('failed_login_count', 0);
                    $this->session->set('failed_login_timestamp', 0);
                }
            }
            Auth::process_login();
            if ($this->session->is_logged_in()) {
                $this->action = "";
                $this->session->set('failed_login_count', 0);
                $this->session->set('failed_login_timestamp', 0);
            } else {
                if (!$this->session->get('failed_login_count')) {
                    $this->session->set('failed_login_count', 0);
                    $this->session->set('failed_login_timestamp', time());
                }
                $this->session->set('failed_login_count', $this->session->get('failed_login_count') + 1);
                $form_errors["login_form.password"] = "Der Benutzername/die E-Mail-Adresse oder das Passwort waren nicht korrekt";
                Logger::get_log()->warn("Login fehlgeschlagen. Benutzername: " . Request::get("username") . " IP: " . $_SERVER['REMOTE_ADDR']);
                if ($this->session->get('failed_login_count') >= 10) {
                    Logger::get_log()->error("Brute Force Attack! IP: " . $_SERVER['REMOTE_ADDR']);
                }
                $this->action = "login";
            }
        }
        $this->content->set("form_errors", $form_errors);
        $this->indexAction();
    }

    /**
     * @noinspection PhpUnused
     */
    public function logoutAction(): void
    {
        $this->session->logout();
        $this->indexAction();
    }

}
