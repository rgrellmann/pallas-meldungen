<?php
use Framework\Common;
use Framework\Link;
/** @var $session */
/** @var string $context */
/** @var  $dummy \Framework\DBTable */
$classname = '\Entity\\'.ucfirst($context);
if (class_exists($classname)) {
    $dummy = new $classname();
?>
<div id="searchform" class="searchform-small">
	<form action="<?=Link::make("self")?>" method="POST" name="<?=$context?>_search">
	<input type="hidden" name="searchform" value="search">
	<div id="searchhandle"><a href="javascript:expand_searchform()" id="searchhandle_link">Suche <img src="<?=IMAGES?>icons/icon_plus.gif" style="vertical-align:top;margin-top:2px;" alt="" id="searchhandle_img"></a></div>
	<div id="searchform_content">Tip: Geben Sie ein Prozentzeichen (%) als Platzhalter ein<br>
		<div id="searchform_rows"></div>
		<div style="float:right; padding-top:2px;">
			<table <?=STD_TABLE?> style="width: 195px;"><tr><td style="width: 85px; text-align: center;">Suchkriterien:</td>
			<td style="width: 45px; text-align: left;"><a href="javascript:searchform_add_row('','')">[mehr]</a></td>
			<td style="width: 60px; text-align: left;"><a href="javascript:searchform_remove_row('')">[weniger]</a></td></tr></table>
		</div>
		&nbsp;&nbsp;<input type="submit" class="submit small" value="suchen">
		&nbsp;&nbsp;<input type="submit" class="submit small" value="zurücksetzen" onclick="this.form.searchform.value='reset';">
	</div>
	<script type="text/javascript">
		function searchform_add_row(field, value) {
			var searchform_row = document.createElement('div');
			var id = 'searchform_row_'+Math.round(Math.random()*1000000);
			searchform_row.innerHTML = '<div class="searchform-row">Feld: <?=$dummy->make_selectbox_search("searchfields[]").'</select>'
			?> Wert: <input type="text" class="text medium" name="searchvalues[]" value="">&nbsp;<a href="javascript:searchform_remove_row(\''+id+'\')">[-]<'+'/a><'+'/div>';
			searchform_row.id = id;
			document.getElementById('searchform_rows').appendChild(searchform_row);
			document.getElementById(id).getElementsByTagName('input')[0].value = value;
			document.getElementById(id).getElementsByTagName('select')[0].value = field;
		}

        <?php if (Common::array_not_empty($session->get($context."_suche_felder"))) {
			$werte = $session->get($context."_suche_werte");
			$search_active = false;
			foreach ($session->get($context."_suche_felder") as $index => $field) {
				if (!empty($werte[$index])) {
					echo "searchform_add_row('$field','".htmlspecialchars($werte[$index], ENT_QUOTES)."');\n";
					$search_active = true;
				}
			}
			if ($search_active) {
				echo "document.getElementById('searchform').className = 'searchform-small-active';\n";
			}
		} else {
			echo "searchform_add_row('','')";
		} ?>

		function searchform_remove_row(id) {
			var container = document.getElementById('searchform_rows');
			if (container.childNodes.length > 1) {
				if (id.length > 3) {
					var row = document.getElementById(id);
					container.removeChild(row);
				} else {
					container.removeChild(container.lastChild);
				}
			} else {
				container.firstChild.getElementsByTagName('input')[0].value = '';
				container.firstChild.getElementsByTagName('select')[0].selectedIndex = 0;
			}
		}
	</script>
	</form>
</div>
<?php } ?>