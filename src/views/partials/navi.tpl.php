<?php
use Framework\App, Framework\Link;
/** @var int $num_pages */
/** @var int $page */
?>

<?php if ($num_pages > 0) { ?>
	<td class="paginicon" style="text-align: center;">
        <?php if ((int)$page > 1) { ?>
				<a href="<?=Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $page-1);?>" title="eine Seite zurück"><img src="<?=IMAGES?>icons/paginicon_left.png" width="20" height="20" alt=""></a>
        <?php } else { ?>
				<img src="<?=IMAGES?>icons/paginicon_left-inactive.png" width="20" height="21" alt="">
        <?php } ?>
	</td>

	<td class="sites" style="text-align: center;">
        <?php if ($num_pages < 10) {
			for ($i=1; $i<=$num_pages; $i++) {
				if ($i != $page) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $i)."\" title=\"Seite $i\">$i</a>\n";
				else echo "					<span class=\"akt\">$i</span>\n";
			}
		} else {
			// example: 1 ... 6 7 8 9 10 ... 16
			if ($page != 1) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, 1)."\" title=\"Erste Seite\">1</a>\n";
			else echo "					<span class=\"akt\">1</span>\n";

			echo ($page > 4) ? " ... " : "";

			if ($page < 5) {
				for ($i=2; $i<=6; $i++) {
					if ($i != $page) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $i)."\" title=\"Seite $i\">$i</a>\n";
					else echo "					<span class=\"akt\">$i</span>\n";
				}
			} elseif ($page+3 < $num_pages) {
				for ($i=$page-2; $i<=$page+2; $i++) {
					if ($i != $page) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $i)."\" title=\"Seite $i\">$i</a>\n";
					else echo "					<span class=\"akt\">$i</span>\n";
				}
			} else {
				for ($i=$num_pages-5; $i < $num_pages; $i++) {
					if ($i != $page) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $i)."\" title=\"Seite $i\">$i</a>\n";
					else echo "					<span class=\"akt\">$i</span>\n";
				}
			}
			// example: ... 16
			echo ($page < $num_pages-3) ? " ... " : "";
			if ($num_pages != $page) echo "					<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $num_pages)."\" title=\"Letzte Seite\">$num_pages</a>\n";
			else echo "					<span class=\"akt\">$num_pages</span>\n";
		} ?>
	</td>

	<td class="paginicon" style="text-align: center;">
        <?php if ((int)$page < $num_pages) {
			echo "			<a href=\"".Link::make(App::get_route(), $_GET, PAGINATION_VARIABLE, $page+1)."\" title=\"nächste Seite\"><img src=\"".IMAGES."icons/paginicon_right.png\" width=\"20\" height=\"20\" alt=\"\"></a>\n";
		} else {
			echo '<img src="'.IMAGES."icons/paginicon_right-inactive.png\" width=\"20\" height=\"20\" alt=\"\">";
		} ?>
        <?php } else { echo "					&nbsp;"; } ?>
	</td>