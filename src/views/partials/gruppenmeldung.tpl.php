<?php
namespace Application;

use Framework\DateAndTime;
use Framework\Link;
use Framework\Request;

/** @var $veranstaltung \Entity\Veranstaltung */
?>

<script type="text/javascript">
    function werte_pruefen () {
        var error_msg = '';
        var status = true;
        var element, participant_number;
        var startpass_erforderlich = <?=((isset($veranstaltung) AND $veranstaltung->get('startpass_erforderlich') == 'ja') ? 'true' : 'false')?>;

        if (!document.forms[0].name_mafue.value || document.forms[0].name_mafue.value === 'Nachname') {
            error_msg += 'Bitte tragen Sie Ihren Namen ein.\n'; status = false;
        }
        if (!document.forms[0].vorname_mafue.value || document.forms[0].vorname_mafue.value === 'Vorname') {
            error_msg += 'Bitte tragen Sie Ihren Vornamen ein.\n'; status = false;
        }
        if (!(document.forms[0].telefon_mafue.value || document.forms[0].handy_mafue.value || document.forms[0].email_mafue.value)) {
            error_msg += 'Bitte tragen Sie mindestens eine Telefonnummer oder email-Adresse ein.\n'; status = false;
        }
        if ((document.forms[0].verein.selectedIndex === 'nix' || document.forms[0].verein.value === 'Gast') && !document.forms[0].gast_verein.value) {
            error_msg += 'Bitte wählen Sie Ihren Verein aus der Liste oder tragen Sie ihn in das Textfeld ein!\n'; status = false;
        }
        if ((document.forms[0].verein.value === 'Gast' || document.forms[0].verein.value === 'nix') && document.forms[0].gast_verein.value && document.forms[0].verband.selectedIndex === 0) {
            error_msg += 'Bitte wählen Sie Ihren Landesverband aus der Liste, wenn Sie für einen Verein starten, der nicht zum Skiverband Berlin gehört!\n'; status = false;
        }
        if (!document.forms[0].akzeptiert.checked) {
            error_msg += 'Bitte bestätigen Sie, daß Sie die Ausschreibung gelesen haben und die von Ihnen gemeldeten Teilnehmer über Terminverschiebungen etc. informieren.\n'; status = false;
        }
        for (var i = 0; i < document.forms[0].elements.length; i++) {
            element = document.forms[0].elements[i];
            participant_number = element.name.slice(-2);
            if (participant_number.slice(0, 1) === '_') {
                participant_number = participant_number.slice(1);
            }
            if (element.type === 'checkbox') {
                if (element.value === 'Super-G' && element.checked) {
                    if (document.forms[0].elements['jahrgang_' + participant_number].value > <?=(date("Y") - 15)?>) {
                        error_msg += 'Am Super-G können nur Erwachsene und Jugendliche ab Jahrgang <?=(date("Y") - 15)?> teilnehmen.\n';
                        status = false;
                        break;
                    }
                }
            }
            if (element.type === 'text') {
                if (element.value === 'Vorname' || element.value === 'Nachname') {
                    element.value = '';
                }
                if (element.name.substr(0, 9) === 'jahrgang_' && element.value === '') {
                    if (document.forms[0].elements['vorname_' + participant_number].value) {
                        error_msg += 'Bitte tragen Sie für alle Teilnehmer den Jahrgang ein.\n';
                        status = false;
                        break;
                    }
                }
                if (startpass_erforderlich && element.name.substr(0, 10) === 'startpass_' && element.value === '') {
                    if (document.forms[0].elements['vorname_' + participant_number].value) {
                        error_msg += 'Für alle Teilnehmer ist eine Startpass-Nummer erforderlich.\n';
                        status = false;
                        break;
                    }
                }
            }
        }
        if (!status) { alert(error_msg); }
        else { document.forms[0].submit(); }
    }
    function toggle_disciplines() {
        for (var i=0; i<document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].type === 'checkbox' && document.forms[0].elements[i].name.match('^disziplinen_')) {
                document.forms[0].elements[i].checked = document.forms[0].alle_markieren.checked;
            }
        }
    }
    function alle_markiert() {
        var alle = true;
        for (var i=0; i<document.forms[0].elements.length; i++) {
            if (document.forms[0].elements[i].type === 'checkbox' && document.forms[0].elements[i].name.match('^disziplinen_')) {
                if (document.forms[0].elements[i].checked === false) { alle = false; }
            }
        }
        document.forms[0].alle_markieren.checked = alle;
    }
</script>

<?
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}
?>

<form name="meldeformular" method="post" action="<?=Link::make('self')?>">
    <input type="hidden" name="todo" value="gruppenmeldung_speichern">
    <input type="hidden" name="veranstaltung" value="<?=$veranstaltung->get_id()?>">
<table class="outer-table" border="1" cellspacing="1" cellpadding="3">
<thead>
    <tr>
        <th align="center">
            Meldeformular f&uuml;r: <?=$veranstaltung->get("bezeichnung")?>
        </th>
    </tr>
</thead>
<tbody>
    <tr>
        <td class="normal1" align="center">
            Termin: <?=DateAndTime::datetime_format($veranstaltung->get("datum"))?> &nbsp;&nbsp;&nbsp;
            Ort: <?=$veranstaltung->get("ort")?> <br>
            Details siehe
            <? if (strlen($veranstaltung->get("url_ausschreibung")) > 10) {
                $ausschreibung = '<a href="'.$veranstaltung->get("url_ausschreibung").'" target="ausschreibung">Ausschreibung</a>';
            } else {
                $ausschreibung = "Ausschreibung";
            }
            echo $ausschreibung; ?>
            &nbsp;&nbsp;&nbsp; Meldeschlu&szlig;: <?=DateAndTime::datetime_format($veranstaltung->get("meldeschluss"), true)?>.
        </td>
    </tr>
    <tr>
        <td class="normal2">Daten des Verantwortlichen f&uuml;r diese Gruppenmeldung:<br>
            (Trainer, Betreuer, Sportwart, Eltern etc.)
        </td>
    </tr>
    <tr>
        <td>
            <table class="" border="0">
            <tr>
                <td class="normal1"><b>Nachname:</b></td>
                <td class="normal1" colspan="2"><input type="text" name="name_mafue" size="20" maxlength="100" value="<?=Request::get('name_mafue') ?: 'Nachname'?>" onfocus="remove_default(this, 'Nachname')"></td>
            </tr>
            <tr>
                <td class="normal1"><b>Vorname:</b></td>
                <td class="normal1" colspan="2"><input type="text" name="vorname_mafue" size="20" maxlength="100" value="<?=Request::get('vorname_mafue') ?: 'Vorname'?>" onfocus="remove_default(this, 'Vorname')"></td>
            </tr>
            <tr>
                <td class="normal1">Telefon:</td>
                <td class="normal1"><input type="text" name="telefon_mafue" size=15 maxlength="50" value="<?=Request::get('telefon_mafue')?>"></td>
                <td class="normal1" rowspan="3">Bitte geben Sie mindestens <br> eine Telefonnummer oder <br> eine email-Adresse an!</td>
            </tr>
            <tr>
                <td class="normal1">Handy:</td>
                <td class="normal1"><input type="text" name="handy_mafue" size="15" maxlength="50" value="<?=Request::get('handy_mafue')?>"></td>
            </tr>
            <tr>
                <td class="normal1">email-Adresse:</td>
                <td class="normal1"><input type="text" name="email_mafue" size="20" maxlength="100" value="<?=Request::get('email_mafue')?>"></td>
            </tr>
            <tr>
                <td class="normal1">Verein:</td>
                <td class="normal1" colspan="2">
                    <select name="verein" size=1 onChange="document.forms[0].verband.selectedIndex = 0">
                        <option value="nix"> - bitte ausw&auml;hlen - </option>
                        <?=ViewHelpers::liste_vereine('ski', Request::get('verein'))?>
                        <option value="Gast"<?=Request::get('verband') > 0 AND Request::get('verband') != '21' ? ' selected' : ''?>>Gast (Verein bitte unten eintragen)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="normal1">
                    Mitglieder anderer Skivereine tragen bitte hier den Namen des Vereins ein:<br>
                    <input type="text" name="gast_verein" size="40" maxlength="100" onChange="document.forms[0].verein.selectedIndex = document.forms[0].verein.options.length - 1" value="<?=Request::get('gast_verein')?>">
                    <select name="verband" size=1 onChange="document.forms[0].verein.selectedIndex = document.forms[0].verein.options.length - 1">
                        <option value="21">Verband - falls nicht SVB</option>
                        <?=ViewHelpers::liste_verbaende('ski', false, Request::get('verband'))?>
                    </select>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <? if ($veranstaltung->get("startpass_erforderlich") == "ja") { ?>
        <tr>
            <td class="normal1">
                F&uuml;r Anmeldungen zu dieser Veranstaltung ist die Nummer des DSV-Startpasses erforderlich. Falls ein Teilnehmer<br>
                noch keinen hat, sollte er die <a href="https://www.scpallas.de/meldungen/Aktivenerklaerung_Startpass.pdf" target="athletenerkl">DSV-Aktivenerkl&auml;rung</a> ausdrucken
                und unterschrieben an die Gesch&auml;ftsstelle seines<br><b>Landesverbandes</b> senden (f&uuml;r Mitglieder Berliner Skivereine: Skiverband Berlin, Jesse-Owens-Allee 2, 14053 Berlin).<br>
                Der Startpass mu&szlig; nicht zur Veranstaltung mitgebracht werden, es reicht aus,<br>
                wenn der Landesverband die Nummer des Startpasses mitgeteilt hat.
            </td>
        </tr>
    <? } ?>

    <!-- **********   Liste der Teilnehmer   ********** -->

    <tr>
        <td class="normal2">Daten der Teilnehmer:</td>
    </tr>
    <tr>
        <td>
            <table class="inner-table" cellspacing="0" cellpadding="2" border="1">
                <tr>
                    <td>Lfd.Nr.</td>
                    <td class="normal2">Nachname:</td>
                    <td class="normal2">Vorname:</td>
                    <td class="normal1">Geburts-Jahrg.</td>
                    <td class="normal1">Geschlecht</td>
                    <? if (strlen($veranstaltung->get("disziplinen")) > 2) { echo "<td class=\"normal1\">Disziplinen (<input type=\"checkbox\" class=\"checkbox\" name=\"alle_markieren\" onClick=\"toggle_disciplines()\">alle)</td>"; } ?>
                    <? if ($veranstaltung->get("startpass_erforderlich") == "ja") { echo "<td class=\"normal1\">Startpass-Nr.</td>"; } ?>
                </tr>
    <?
                if (!Request::get("anz_teiln") OR Request::get("anz_teiln") > 30 OR Request::get("anz_teiln") < 5) {
                    $anzahl_zeilen = 10;
                } else {
                    $anzahl_zeilen = Request::get("anz_teiln");
                }
                $disziplinen = $veranstaltung->get_disziplinen();
                for ($x=1; $x<=$anzahl_zeilen; $x++) {
                    $out = "<tr><td class=\"normal1\">$x:&nbsp;</td>";
                    $out .= "<td class=\"normal1\"><input type=\"text\" name=\"name_$x\" size=\"10\" maxlength=\"100\" value=\"".(Request::get("name_$x") ?: 'Nachname')."\" onfocus=\"remove_default(this, 'Nachname')\"><br><span class=\"sub2\">Nachname</span></td>";
                    $out .= "<td class=\"normal1\"><input type=\"text\" name=\"vorname_$x\" size=\"16\" maxlength=\"100\" value=\"".(Request::get("vorname_$x") ?: 'Vorname')."\" onfocus=\"remove_default(this, 'Vorname')\"><br><span class=\"sub2\">Vorname</span></td>";
                    $out .= "<td class=\"normal1\" align=\"center\"><input type=\"text\" name=\"jahrgang_$x\" size=\"4\" maxlength=\"8\" value=\"".Request::get("jahrgang_$x")."\"><br><span class=\"sub2\">Jahrgang</span></td>\n";
                    $out .= "<td class=\"normal1\"><select name=\"geschlecht_$x\" size=\"1\">";
                    $out .= "<option value=\"m\" ".(Request::get("geschlecht_$x") == 'm' ? 'selected' : '').">m&auml;nnl.</option>
                             <option value=\"w\" ".(Request::get("geschlecht_$x") == 'w' ? 'selected' : '').">weibl.</option>
                             </select><br><span class=\"sub2\">&nbsp;</span>";
                    if ($disziplinen) {
                        $out .= "</td><td class=\"sub1\">\n";
                        $out .= "<input type=\"hidden\" name=\"disziplinen\" value=\"".count($disziplinen)."\">\n";
                        foreach ($disziplinen as $disziplin) {
                            $out .= '<input type="checkbox" class="checkbox" name="disziplinen_'.$x.'[]" value="'.$disziplin->get('id').'" onClick="alle_markiert()" '.((is_array(Request::get("disziplinen_$x")) AND in_array($disziplin->get('id'), Request::get("disziplinen_$x"))) ? 'checked' : '').'>'
                                . $disziplin->get('name')." ".$disziplin->get_hinweise()."<br>\n";
                        }
                        $out .= "</td>\n";
                    }
                    else { $out .= "<input type=\"hidden\" name=\"disziplinen\" value=\"0\"></td>\n"; }
                    if ($veranstaltung->get("startpass_erforderlich") == "ja") {
                        $out .= "<td class=\"normal1\" align=\"center\"><input type=\"text\" name=\"startpass_$x\" size=\"4\" maxlength=\"20\" value=\"".Request::get("startpass_$x")."\"><br><span class=\"sub2\">Startpass-Nr</span></td>\n";
                    }
                    $out .= "</tr>\n";
                    echo $out;
                }
    ?>
            </table>
        </td>
    </tr>
    <tr>
        <td class="normal2">
            <input type="checkbox" class="checkbox" value="1" name="akzeptiert" <?=Request::get('akzeptiert') == '1' ? 'checked' : ''?>>
            Ich erkl&auml;re, da&szlig; ich die Ausschreibung gelesen habe und damit einverstanden bin.<br>
            Die Erziehungsberechtigten der von mir gemeldeten minderj&auml;hrigen Teilnehmer<br>
            sind mit der Teilnahme einverstanden.<br>
            Ich informiere die von mir gemeldeten Teilnehmer &uuml;ber den Inhalt der Ausschreibung sowie<br>
            rechtzeitig &uuml;ber evtl. Terminverschiebungen, Absagen, &Auml;nderungen im Reglement usw.
        </td>
    </tr>
    <tr>
        <td>
            <table class="" border="0">
                <tr>
                    <td class="normal1">Bemerkungen:</td>
                    <td class="normal1" colspan="2">
                        <textarea name="kommentar" rows="3" cols="40" style="white-space: pre;"><?=Request::get('kommentar')?></textarea>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?
    if (strlen($veranstaltung->get("hinweise")) > 2) { ?>
        <tr>
            <td align=center class="normal1">
                <?=$veranstaltung->get("hinweise")?>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td align="center" class="normal1">
            <input type="button" value="Meldung abschicken" onClick="werte_pruefen()">
        </td>
    </tr>
    <tr>
        <td align="center" class="normal1">
    		<span id="message">Bitte nur einmal klicken.</span>
        </td>
    </tr>
</tbody>
</table>
</form>
