<?php
namespace Application;

use Framework\DateAndTime;
use Framework\Link;

/** @var $veranstaltung \Entity\Veranstaltung */
/** @var $error_message string */
/** @var $user_message string */
?>
<table class="outer-table" border="1" cellspacing="1" cellpadding="3">
<tbody>
    <tr>
        <td class="normal1" align="center">
            Meldung f&uuml;r: <?=$veranstaltung->get("bezeichnung")?><br>
            am <?=DateAndTime::datetime_format($veranstaltung->get("datum"), false)?> in <?=$veranstaltung->get("ort")?>
        </td>
    </tr>

<? if (!empty($error_message)) { ?>

    <tr>
        <td class="normal2">
            <span class="message-user message-inline"> <?=$error_message?> </span>
        </td>
    </tr>

<? } ?>

<? if (!empty($user_message)) { ?>

    <tr>
        <td class="normal2">
            <?=$user_message?>
        </td>
    </tr>
    <tr>
        <td class="normal2" align="center">
            Vielen Dank f&uuml;r Ihre Anmeldung!<br>
        </td>
    </tr>

<? } ?>

    <tr>
        <td class="normal2" align="center">
            <? if (!empty($gruppenmeldung)) { $typ = "gruppenmeldung"; } else { $typ = "einzelmeldung"; } ?>
            <a href="<?=Link::make("meldeformular", "veranstaltung", $veranstaltung->get_id(), "todo", $typ)?>">Eine weitere Meldung abgeben</a>
        </td>
    </tr>
</tbody>
</table>
