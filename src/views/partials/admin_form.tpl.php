<?php
use Framework\Link;
/** @var string $action */
/** @var string $todo */
/** @var string|int $id */
/** @var string $form_items */
if (!isset($id)) { $id = null; }
?>

<form name="edit" action="<?=Link::make($action)?>" method="post" enctype="multipart/form-data"<?=isset($autocomplete_off) ? ' autocomplete="off"' : ''?>>
	<input type="hidden" name="todo" value="<?=$todo?>">
	<input type="hidden" name="id" value="<?=$id?>">
	<table style="width: 640px;" border="0" cellspacing="1" cellpadding="1">
	<tr class="tbl-stripes-0">
		<td colspan="2" style="padding-right:5px;">
		<table style="width: 100%;" border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td style="text-align: left;"><input type="submit" value="abbrechen" onclick="this.form.todo.value=''"></td>
			<td style="text-align: right;"><input type="submit" value="Speichern &raquo;"></td>
		</tr>
		</table>
		</td>
	</tr>
	<?=$form_items?>
	<tr class="tbl-stripes-0">
		<td colspan="2" style="padding-right:5px;">
		<table style="width: 100%;" border="0" cellspacing="1" cellpadding="1">
		<tr>
			<td style="text-align: left;"><input type="submit" value="abbrechen" onclick="this.form.todo.value=''"></td>
			<td style="text-align: right;"><input type="submit" value="Speichern &raquo;"></td>
		</tr>
		</table>
		</td>
	</tr>
	</table>
</form>
