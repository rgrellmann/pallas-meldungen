<?php
namespace Application;

use Framework\DateAndTime;
use Framework\Link;
use Framework\Forms;
use Framework\Request;

/** @var $veranstaltung \Entity\Veranstaltung */
?>

<script type="text/javascript">
    function werte_pruefen () {
        var error_msg = '';
        var status = true;
        var startpass_erforderlich = <?=((isset($veranstaltung) AND $veranstaltung->get('startpass_erforderlich') == 'ja') ? 'true' : 'false')?>;
        var disziplin = false;
        var i = 0;
        /* if (document.forms[0].verein.selectedIndex == 0) { alert('Sie müssen Ihren Verein aus der Liste wählen!'); status = false; } */
        if (!document.forms[0].elements['name'].value || document.forms[0].elements['name'].value === 'Nachname') { error_msg += 'Bitte tragen Sie Ihren Nachnamen ein.\n'; status = false; }
        if (!document.forms[0].vorname.value || document.forms[0].vorname.value === 'Vorname') { error_msg += 'Bitte tragen Sie Ihren Vornamen ein.\n'; status = false; }
        if (!(document.forms[0].jahrgang.value > 1900 && document.forms[0].jahrgang.value < <?=date("Y")?>)) { error_msg += 'Bitte tragen Sie Ihren Geburtsjahrgang als vierstellige Zahl ein.\n'; status = false; }
        if (!document.forms[0].geschlecht[0].checked && !document.forms[0].geschlecht[1].checked) { error_msg += 'Bitte geben Sie ihr Geschlecht an.\n'; status = false; }
        if (!(document.forms[0].telefon.value || document.forms[0].handy.value || document.forms[0].email.value)) { error_msg += 'Bitte tragen Sie mindestens eine Telefonnummer oder email-Adresse ein.\n'; status = false; }
        if ((document.forms[0].verein.value === 'nix' || document.forms[0].verein.value === 'Gast') && !document.forms[0].gast_verein.value) {
            error_msg += 'Bitte wählen Sie Ihren Verein aus der Liste oder tragen Sie ihn in das Textfeld ein!\n'; status = false;
        }
        if ((document.forms[0].verein.value === 'Gast' || document.forms[0].verein.value === 'nix') && document.forms[0].gast_verein.value && document.forms[0].verband.selectedIndex === 0) {
            error_msg += 'Bitte wählen Sie Ihren Landesverband aus der Liste, wenn Sie für einen Verein starten, der nicht zum Skiverband Berlin gehört!\n'; status = false;
        }
        if (!document.forms[0].akzeptiert.checked) { error_msg += 'Bitte bestätigen Sie, daß Sie Ausschreibung gelesen haben und damit einverstanden sind.\n'; status = false; }
        if (startpass_erforderlich) {
            if (!document.forms[0].startpass_nr.value) { error_msg += 'Bitte tragen Sie Ihre Startpassnummer ein.\n'; status = false; }
        }
        if (document.forms[0].num_disziplinen.value > 0) {
            for (i=0; i<document.forms[0].elements.length; i++) {
                if (document.forms[0].elements[i].type === 'checkbox' && document.forms[0].elements[i].name === 'disziplinen[]') {
                    if (document.forms[0].elements[i].checked) {
                        disziplin = true;
                    }
                }
            }
            if (!disziplin) { error_msg += 'Bitte kreuzen Sie mindestens eine Disziplin an.\n'; status = false; }
        }
        for (i=0; i<document.forms[0].length; i++) {
            if (document.forms[0].elements[i].type === 'checkbox') {
                if (document.forms[0].elements[i].value === 'Super-G' && document.forms[0].elements[i].checked) {
                    if (document.forms[0].jahrgang.value > <?=(date('Y')-15)?>) { error_msg += 'Am Super-G können nur Erwachsene und Jugendliche ab Jahrgang <?=(date("Y")-15)?> teilnehmen.\n'; status = false; }
                }
            }
        }
        if (!status) { alert(error_msg); }
        else { document.forms[0].submit(); }
    }
</script>

<?
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}
?>

<form name="meldeformular" method="post" action="<?=Link::make('self')?>">
    <input type="hidden" name="todo" value="einzelmeldung_speichern">
    <input type="hidden" name="veranstaltung" value="<?=$veranstaltung->get_id()?>">

<table class="outer-table" border="1" cellspacing="1" cellpadding="3">
<thead>
    <tr>
        <th align="center">
            Meldeformular f&uuml;r: <?=$veranstaltung->get("bezeichnung")?>
        </th>
    </tr>
</thead>
<tbody>
    <tr>
        <td class="normal1" align="center">
            Termin: <?=DateAndTime::datetime_format($veranstaltung->get("datum"))?> &nbsp;&nbsp;&nbsp;
            Ort: <?=$veranstaltung->get("ort")?> <br>
            Details siehe
            <? if (strlen($veranstaltung->get("url_ausschreibung")) > 10) {
                $ausschreibung = '<a href="'.$veranstaltung->get("url_ausschreibung").'" target="ausschreibung"><strong>Ausschreibung</strong></a>';
            } else {
                $ausschreibung = "Ausschreibung";
            }
            echo $ausschreibung; ?>
            &nbsp;&nbsp;&nbsp; Meldeschlu&szlig;: <?=DateAndTime::datetime_format($veranstaltung->get("meldeschluss"), true)?>.
        </td>
    </tr>
    <tr>
        <td>
            <table class="" border="0" cellpadding="0" cellspacing="3">
            <tr>
                <td class="normal1"><b><label for="input_name">Nachname</label></b>:</td>
                <td class="normal1" colspan="2"><?=Forms::form_input('name', Request::get('name') ?: '', '', '', '', 'text', 'size="20" maxlength="100" placeholder="Nachname" required')?></td>
            </tr>
            <tr>
                <td class="normal1"><b><label for="input_vorname">Vorname:</label></b></td>
                <td class="normal1" colspan="2"><input type="text" name="vorname" id="input_vorname" size="20" maxlength="100" value="<?=Request::get('vorname') ?: ''?>" placeholder="Vorname" required></td>
            </tr>
            <tr>
                <td class="normal1"><label for="input_jahrgang">Geburts-Jahrgang:</label></td>
                <td class="normal1" colspan="2"><input type="text" class="text" name="jahrgang" id="input_jahrgang" size="8" maxlength="8" value="<?=Request::get('jahrgang')?>" required> (vierstellig)</td>
            </tr>
            <tr>
                <td class="normal1">Geschlecht:</td>
                <td class="normal1" colspan="2">
                    <input type="radio" class="radio" name="geschlecht" id="input_geschlecht_m" value="m" <?=Request::get('geschlecht') == 'm' ? 'checked' : ''?>><label for="input_geschlecht_m">m&auml;nnlich</label> &nbsp;&nbsp;
                    <input type="radio" class="radio" name="geschlecht" id="input_geschlecht_w" value="w" <?=Request::get('geschlecht') == 'w' ? 'checked' : ''?>><label for="input_geschlecht_w">weiblich</label>
                </td>
            </tr>
            <tr class="divider"><td colspan="3"><img src="<?=IMAGES?>transparent.gif" height="1" width="100%" alt=""></td></tr>
            <tr>
                <td class="normal1"><label for="input_telefon">Telefon:</label></td>
                <td class="normal1"><input type="text" name="telefon" id="input_telefon" size="15" maxlength="50" value="<?=Request::get('telefon')?>"></td>
                <td class="normal1" rowspan="3">Bitte geben Sie mindestens<br>eine Telefonnummer oder<br>eine email-Adresse an!</td>
            </tr>
            <tr>
                <td class="normal1"><label for="input_handy">Handy:</label></td>
                <td class="normal1"><input type="text" name="handy" id="input_handy" size="15" maxlength="50" value="<?=Request::get('handy')?>"></td>
            </tr>
            <tr>
                <td class="normal1"><label for="input_email">email-Adresse:</label></td>
                <td class="normal1"><input type="text" name="email" id="input_email" size="25" maxlength="100" value="<?=Request::get('email')?>"></td>
            </tr>
            <tr class="divider"><td colspan="3"><img src="<?=IMAGES?>transparent.gif" height="1" width="100%" alt=""></td></tr>
            <tr>
                <td class="normal1"><label for="input_verein">Verein:</label></td>
                <td class="normal1" colspan="2">
                    <select name="verein" id="input_verein" size="1" onChange="document.forms[0].verband.selectedIndex = 0">
                        <option value="nix"> - bitte ausw&auml;hlen - </option>
                        <?=ViewHelpers::liste_vereine('ski', Request::get('verein'))?>
                        <option value="Gast"<?=Request::get('verband') > 0 AND Request::get('verband') != '21' ? ' selected' : ''?>>Gast (Verein bitte unten eintragen)</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="normal1">
                    <label for="input_gast_verein">Mitglieder anderer Skivereine tragen bitte hier den Namen des Vereins ein:</label><br>
                    <input type="text" name="gast_verein" id="input_gast_verein" size="40" maxlength="100" onChange="document.forms[0].verein.selectedIndex = document.forms[0].verein.options.length - 1" value="<?=Request::get('gast_verein')?>">
                    <select name="verband" size="1" onChange="document.forms[0].verein.selectedIndex = document.forms[0].verein.options.length - 1">
                        <option value="21"<?=Request::get('verband') == '21' ? ' selected' : ''?>>Verband - falls nicht SVB</option>
                        <?=ViewHelpers::liste_verbaende('ski', false, Request::get('verband'))?>
                    </select>
                </td>
            </tr>

        <? if ($veranstaltung->get("disziplinen")) { ?>
            <tr class="divider"><td colspan="3"><img src="<?=IMAGES?>transparent.gif" height="1" width="100%" alt=""></td></tr>
            <tr>
                <td class="normal1">Disziplinen:</td>
                <td class="normal1">
                <? $disziplinen = $veranstaltung->get_disziplinen(); ?>
                <input type="hidden" name="num_disziplinen" value="<?=count($disziplinen)?>">
                <? /** @var \Entity\VeranstaltungDisziplin $disziplin */
                foreach ($disziplinen as $i => $disziplin) {
                    echo '<input type="checkbox" class="checkbox" name="disziplinen[]" id="input_disziplinen_'.($i).'" value="'.$disziplin->get('id').'" '.((is_array(Request::get('disziplinen')) AND in_array($disziplin->get('id'), Request::get('disziplinen'))) ? 'checked' : '').'> ';
                    echo '<label for="input_disziplinen_'.($i).'">'.$disziplin->get('name')." ".$disziplin->get_hinweise()."</label><br>\n";
                } ?>
                </td>
                <td class="normal1">Bitte markieren Sie die Disziplin(en),<br>an denen Sie teilnehmen m&ouml;chten.</td>
            </tr>
        <? } ?>

        <? if ($veranstaltung->get("startpass_erforderlich") == "ja") { ?>
            <tr class="divider"><td colspan="3"><img src="<?=IMAGES?>transparent.gif" height="1" width="100%" alt=""></td></tr>
            <tr>
                <td colspan="3" class="normal1">
                    F&uuml;r diese Anmeldung ist die Nummer Ihres DSV-Startpasses erforderlich. Falls Sie noch keinen haben,<br>
                    drucken Sie bitte die <a href="https://www.scpallas.de/meldungen/Aktivenerklaerung_Startpass.pdf" target="athletenerkl">DSV-Aktivenerkl&auml;rung</a> aus
                    und senden Sie sie unterschrieben an die Gesch&auml;ftsstelle Ihres<br><b>Landesverbandes</b> (f&uuml;r Mitglieder Berliner Skivereine: Skiverband Berlin, Jesse-Owens-Allee 2, 14053 Berlin).<br>
                    Der Startpass mu&szlig; nicht zur Veranstaltung mitgebracht werden, es reicht aus,<br> wenn Ihr Landesverband Ihnen die Nummer Ihres
                    Startpasses mitgeteilt hat.<br>
                    <b><label for="input_startpass_nr">Startpass-Nummer:</label></b>
                    <input type="text" name="startpass_nr" id="input_startpass_nr" maxlength="20" size="8" value="<?=Request::get('startpass_nr')?>">
                </td>
            </tr>
        <? } ?>

            <tr class="divider"><td colspan="3"><img src="<?=IMAGES?>transparent.gif" height="1" width="100%" alt=""></td></tr>
            <tr>
                <td colspan="3" class="normal2">
                    <input type="checkbox" class="checkbox" name="akzeptiert" id="input_akzeptiert" value="1" <?=Request::get('akzeptiert') == '1' ? 'checked' : ''?>>
                    <label for="input_akzeptiert">Ich erkl&auml;re, da&szlig; ich die <?=$ausschreibung?><br>
                    <? if ($veranstaltung->get("startpass_erforderlich") == "ja") {
                        echo 'und die <a href="https://www.scpallas.de/images/Aktivenerklaerung_Startpass.pdf" target="athletenerkl">DSV-Aktivenerkl&auml;rung</a>';
                    } ?>
                    gelesen habe und damit einverstanden bin.</label>
                </td>
            </tr>
            <tr>
                <td class="normal1"><label for="input_kommentar">Bemerkungen:</label></td>
                <td class="normal1" colspan="2"><textarea name="kommentar" id="input_kommentar" rows="3" cols="40" style="white-space: pre;"><?=Request::get('kommentar')?></textarea></td>
            </tr>
            </table>
        </td>
    </tr>
    <?
    if (strlen($veranstaltung->get("hinweise")) > 2) { ?>
        <tr>
            <td align=center class="normal1">
                <?=$veranstaltung->get("hinweise")?>
            </td>
        </tr>
    <? } ?>
    <tr>
        <td align="center" class="normal1">
            <input type="button" value="Meldung abschicken" onClick="werte_pruefen()">
        </td>
    </tr>
    <tr>
        <td align="center" class="normal1">
    		<span id="message">Bitte nur einmal klicken.</span>
        </td>
    </tr>
</tbody>
</table>
</form>
