<?php use Framework\App, Framework\Link, Framework\Request;
/** @var array $roles */
/** @var array $rules */
?>

<h2>Admin - ACL Rules</h2>

<form name="type_form" action="<?=Link::make(App::get_route())?>" method="POST">
    <select size="1" name="type" style="width:105px; text-align:right;" onchange="this.form.submit()">
        <option value="actions"<?=((Request::get("type") == "all" OR Request::get('type') == '')?" selected":"")?>>Actions&nbsp;</option>
        <option value="controllers"<?=((Request::get("type") == "controllers")?" selected":"")?>>Controllers&nbsp;</option>
        <option value="routes"<?=((Request::get("type") == "routes")?" selected":"")?>>Routes&nbsp;</option>
    </select>
</form>

<table border="0" cellspacing="1" cellpadding="3" style="margin-top:3px;">
<thead>
    <tr class="tbl-stripes-0">
        <td></td>
        <?php foreach ($roles as $role) {
            echo "<td>$role</td>";
        } ?>
    </tr>
</thead>
<tbody>
<?php $i = 0;
    foreach ($rules as $resource => $data) {
    if (fmod($i, 2) == 0) { $row_class =  "tbl-stripes-1"; } else { $row_class =  "tbl-stripes-2"; } ?>
   <tr class="<?=$row_class?>">
       <td><?=$resource?></td>
       <?php foreach ($roles as $role_id => $role) { ?>
           <td><?=($data[$role_id] ?? '')?></td>
       <?php } ?>
   </tr>
        <?php $i++; ?>
    <?php } ?>
</tbody>
</table>
