<?php
use Framework\App, Framework\Link, Framework\DateAndTime, Framework\Lists, Framework\Auth, Framework\Forms, Framework\Request;
/** @var \Entity\Veranstaltung[] $veranstaltungen */
/** @var \Entity\Veranstaltung $veranstaltung */
/** @var string $todo */
/** @var string $searchform */
/** @var int $offset */
/** @var int $num_total */
/** @var array $altersklassen */
/** @var \Framework\Template $navigator */
/** @var \Framework\Session $session */
/** @var \Entity\Meldung[] $items */
/** @var \Entity\Meldung[] $items_withdrawn */
/** @var \Entity\Meldung[] $items_captains */
/** @var \Entity\Meldung $row */
/** @var string $form_items */
?>

<h2>Admin - Meldungen</h2>

<?php
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}

if ($todo == "edit_meldung") {
	$form = new \Framework\Template("partials/admin_form");
	$form->set("form_items", $form_items);
	$form->set("action", "admin/meldungen");
	$form->set("todo", "save_meldung");
	$form->display();

} else {
?>
    <form method="POST" action="<?=Link::make("self")?>">
        Meldungen für Veranstaltung <?=Forms::form_select("veranstaltung", $veranstaltungen, "", 'onchange="this.form.submit()"', 1, false, $session->get("veranstaltung"));?>
        anzeigen
    </form>
	<ul><li><a href="<?=Link::make(App::get_route(), "todo", "edit_meldung", "id", "0")?>">Neue Meldung anlegen</a></li></ul>
	<?=$searchform?>
    <?=count($items) . " Teilnehmer gemeldet"?>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "name", "Name", 'width="115"').
			Lists::table_top_order("", "vorname", "Vorname", 'width="85"').
			Lists::table_top_order("", "geschlecht", "m/w", 'width="25"').
			'<td width="40">AK</td>'.
			Lists::table_top_order("", "jahrgang", "JG", 'width="45"').
			Lists::table_top_order("", "verein", 'Verein', 'width="125"').
			'<td width="40">Verband</td>'.
			'<td width="40">SP-Nr.</td>'.
            '<td width="150">Disziplinen</td>'.
            '<td width="125">gemeldet von</td>'.
            (Auth::is_allowed('actions/view_personal_data') ? '<td width="25"><img src="'.IMAGES.'icons/doc.gif" alt="Kommentar" title="Kommentar"></td>' : '')
?>			<td width="80"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items as $row) {
		echo '<tr valign="middle" class="'.Lists::get_table_stripes_class($i).'" id="teilnehmer_'.$row->get_id().'">';
		echo '<td><span title="gemeldet: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('id').'</span></td>';
		echo '<td>'.$row->get('name').'</td>';
		echo '<td>'.$row->get('vorname').'</td>';
		echo '<td>'.$row->get('geschlecht').'</td>';
		echo '<td>'.$row->get('ak').'</td>';
		echo '<td>'.$row->get('jahrgang').'</td>';
		echo '<td>'.$row->get('verein').'</td>';
		echo '<td>'.$row->get('name_5').'</td>';
		echo '<td>'.$row->get('startpass_nr').'</td>';
		echo '<td class="small">'.str_replace(',', '<br>',$row->get('disziplinen')).'</td>';
		echo '<td'.($row->get('gemeldet_von') > 0 ? ' onmouseover="hl(\'teilnehmer_'.$row->get('gemeldet_von').'\', true)" onmouseout="hl(\'teilnehmer_'.$row->get('gemeldet_von').'\', false)">MaFü '.$row->get('gemeldet_von').'</span>' : '>').'</td>';
		if (Auth::is_allowed('actions/view_personal_data')) {
            echo '<td>';
            if ($row->get('kommentar')) {
                echo '<span title="'.$row->get('kommentar').'"><img src="'.IMAGES.'icons/doc.gif" border="0" alt=""></span>';
            }
            echo '</td>';
		}
		echo '<td class="nowrap">&nbsp;&nbsp;';
        if (Auth::is_allowed('actions/edit_personal_data') OR (Auth::is_allowed('actions/edit_personal_data_if_club_match') AND $row->get('verein') != "" AND $session->get_user()->get('verein') == $row->get('verein'))) {
            echo '<a href="'.Link::make(App::get_route(), "todo", "edit_meldung", "id", $row->get('id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') != 'gemeldet' AND Auth::is_allowed('actions/delete_meldung')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "delete_meldung", "ids[]", $row->get('id'), 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Meldung wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gemeldet' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gestrichen', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="aus der Meldeliste streichen" onclick="return confirm(\'Wollen Sie diesen Läufer aus der Meldeliste streichen?\')"><img src="'.IMAGES.'icons/remove_person.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gestrichen' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gemeldet', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="in die Meldeliste aufnehmen" onclick="return confirm(\'Wollen Sie diesen Läufer wieder in die Meldeliste aufnehmen?\')"><img src="'.IMAGES.'icons/add_person.gif" border="0" alt=""></a> &nbsp;';
        }
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
?>
	<table class="pagin"><tr>
		<td width="200" align="left">Zeige <?=$offset+1?> bis <?=$offset+$i?> von <?=intval($num_total)?></td>
            <?php $navigator->display()?>
		<td width="200" align="right"><?=Lists::items_per_page_form()?></td>
		</tr>
	</table>

    <p></p>
    <h4><?=count($items_withdrawn) . " gestrichene Teilnehmer:"?></h4>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "name", "Name", 'width="115"').
			Lists::table_top_order("", "vorname", "Vorname", 'width="85"').
			Lists::table_top_order("", "geschlecht", "m/w", 'width="25"').
			Lists::table_top_order("", "jahrgang", "JG", 'width="45"').
			Lists::table_top_order("", "verein", 'Verein', 'width="125"').
			'<td width="40">Verband</td>'.
			'<td width="40">SP-Nr.</td>'.
            '<td width="150">Disziplinen</td>'.
            '<td width="125">gemeldet von</td>'.
            (Auth::is_allowed('actions/view_personal_data') ? '<td width="25"><img src="'.IMAGES.'icons/doc.gif" alt="Kommentar" title="Kommentar"></td>' : '')
?>			<td width="80"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items_withdrawn as $row) {
		echo '<tr valign="middle" class="'.Lists::get_table_stripes_class($i).'" id="teilnehmer_'.$row->get_id().'">';
		echo '<td><span title="gemeldet: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('id').'</span></td>';
		echo '<td>'.$row->get('name').'</td>';
		echo '<td>'.$row->get('vorname').'</td>';
		echo '<td>'.$row->get('geschlecht').'</td>';
		echo '<td>'.$row->get('jahrgang').'</td>';
		echo '<td>'.$row->get('verein').'</td>';
		echo '<td>'.$row->get('name_5').'</td>';
		echo '<td>'.$row->get('startpass_nr').'</td>';
		echo '<td class="small">'.str_replace(',', '<br>',$row->get('disziplinen')).'</td>';
		echo '<td'.($row->get('gemeldet_von') > 0 ? ' onmouseover="hl(\'teilnehmer_'.$row->get('gemeldet_von').'\', true)" onmouseout="hl(\'teilnehmer_'.$row->get('gemeldet_von').'\', false)">MaFü '.$row->get('gemeldet_von').'</span>' : '>').'</td>';
		if (Auth::is_allowed('actions/view_personal_data')) {
            echo '<td>';
            if ($row->get('kommentar')) {
                echo '<span title="'.$row->get('kommentar').'"><img src="'.IMAGES.'icons/doc.gif" border="0" alt=""></span>';
            }
            echo '</td>';
		}
		echo '<td class="nowrap">&nbsp;&nbsp;';
        if (Auth::is_allowed('actions/edit_personal_data') OR (Auth::is_allowed('actions/edit_personal_data_if_club_match') AND $row->get('verein') != "" AND $session->get_user()->get('verein') == $row->get('verein'))) {
            echo '<a href="'.Link::make(App::get_route(), "todo", "edit_meldung", "id", $row->get('id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') != 'gemeldet' AND Auth::is_allowed('actions/delete_meldung')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "delete_meldung", "ids[]", $row->get('id'), 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Meldung wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gemeldet' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gestrichen', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="aus der Meldeliste streichen" onclick="return confirm(\'Wollen Sie diesen Läufer aus der Meldeliste streichen?\')"><img src="'.IMAGES.'icons/remove_person.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gestrichen' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gemeldet', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="in die Meldeliste aufnehmen" onclick="return confirm(\'Wollen Sie diesen Läufer wieder in die Meldeliste aufnehmen?\')"><img src="'.IMAGES.'icons/add_person.gif" border="0" alt=""></a> &nbsp;';
        }
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
?>

    <p></p>
    <h4><?=count($items_captains) . " Mannschaftsführer:"?></h4>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "name", "Name", 'width="115"').
			Lists::table_top_order("", "vorname", "Vorname", 'width="85"').
			Lists::table_top_order("", "geschlecht", "m/w", 'width="25"').
			Lists::table_top_order("", "jahrgang", "JG", 'width="45"').
			Lists::table_top_order("", "verein", 'Verein', 'width="125"').
			'<td width="40">Verband</td>'.
            (Auth::is_allowed('actions/view_personal_data') ? '<td width="25"><img src="'.IMAGES.'icons/doc.gif" alt="Kommentar" title="Kommentar"></td>' : '')
?>			<td width="80"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items_captains as $row) {
		echo '<tr valign="middle" class="'.Lists::get_table_stripes_class($i).'" id="teilnehmer_'.$row->get_id().'">';
		echo '<td><span title="gemeldet: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('id').'</span></td>';
		echo '<td>'.$row->get('name').'</td>';
		echo '<td>'.$row->get('vorname').'</td>';
		echo '<td>'.$row->get('geschlecht').'</td>';
		echo '<td>'.$row->get('jahrgang').'</td>';
		echo '<td>'.$row->get('verein').'</td>';
		echo '<td>'.$row->get('name_5').'</td>';
		if (Auth::is_allowed('actions/view_personal_data')) {
            echo '<td>';
            if ($row->get('kommentar')) {
                echo '<span title="'.$row->get('kommentar').'"><img src="'.IMAGES.'icons/doc.gif" border="0" alt=""></span>';
            }
            echo '</td>';
		}
		echo '<td class="nowrap">&nbsp;&nbsp;';
        if (Auth::is_allowed('actions/edit_personal_data') OR (Auth::is_allowed('actions/edit_personal_data_if_club_match') AND $row->get('verein') != "" AND $session->get_user()->get('verein') == $row->get('verein'))) {
            echo '<a href="'.Link::make(App::get_route(), "todo", "edit_meldung", "id", $row->get('id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') != 'gemeldet' AND Auth::is_allowed('actions/delete_meldung')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "delete_meldung", "ids[]", $row->get('id'), 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Meldung wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gemeldet' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gestrichen', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="aus der Meldeliste streichen" onclick="return confirm(\'Wollen Sie diesen Läufer aus der Meldeliste streichen?\')"><img src="'.IMAGES.'icons/remove_person.gif" border="0" alt=""></a> &nbsp;';
        }
        if ($row->get('status') == 'gestrichen' AND Auth::is_allowed('actions/change_registration_status')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "change_status", "id", $row->get('id'), 'status', 'gemeldet', 'sort_by', Request::get('sort_by'), 'sort_desc', Request::get('sort_desc')).'" title="in die Meldeliste aufnehmen" onclick="return confirm(\'Wollen Sie diesen Läufer wieder in die Meldeliste aufnehmen?\')"><img src="'.IMAGES.'icons/add_person.gif" border="0" alt=""></a> &nbsp;';
        }
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
	if (!empty($altersklassen)) {
	    $i = 0;
	    ?>
	    <p></p>
	    <h4>Altersklassen-Verteilung</h4>
    	<table border="0" cellspacing="1" cellpadding="3" style="margin-top:3px;">
    	<tr class="tbl-stripes-0">
    	    <td>AK</td>
    	    <td style="padding: 3px 8px;"><?=implode('</td><td style="padding: 3px 8px;">', array_keys($altersklassen['alle']))?></td>
    	</tr>
        <tr class="<?=Lists::get_table_stripes_class($i++)?>"><td>m (alle)</td>
            <?php foreach ($altersklassen['alle'] as $anzahl) {
            echo "<td class=\"center\">$anzahl[m]</td>";
        } ?>
        <tr class="<?=Lists::get_table_stripes_class($i++)?>"><td>m (SVB)</td>
            <?php foreach ($altersklassen['SVB'] as $anzahl) {
            echo "<td class=\"center\">$anzahl[m]</td>";
        } ?>
        <tr class="<?=Lists::get_table_stripes_class($i++)?>"><td>w (alle)</td>
            <?php foreach ($altersklassen['alle'] as $anzahl) {
            echo "<td class=\"center\">$anzahl[w]</td>";
        } ?>
        <tr class="<?=Lists::get_table_stripes_class($i++)?>"><td>w (SVB)</td>
            <?php foreach ($altersklassen['SVB'] as $anzahl) {
            echo "<td class=\"center\">$anzahl[w]</td>";
        } ?>
        </tr>
        </table>
    <?php }
?>

    <?php
}
?>