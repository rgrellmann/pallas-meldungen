<?php use Framework\App, Framework\Link, Framework\Lists;
/** @var string $todo */
/** @var string $searchform */
/** @var Framework\Template $navigator */
/** @var int $offset */
/** @var int $num_total */
/** @var array $items */
/** @var string $form_items */
?>

<h2>Admin - Rollen</h2>

<?php
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}

if ($todo == "edit") {
	$form = new \Framework\Template("partials/admin_form");
	$form->set("form_items", $form_items);
	$form->set("action", "admin/roles");
	$form->set("todo", "save");
	$form->display();

} else {
?>
	<ul><li><a href="<?=Link::make(App::get_route(), "todo", "edit", "id", "0")?>">Neue Rolle anlegen</a></li></ul>
	<?=$searchform?>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "name", "Name", 'width="125"')
?>			<td style="width: 80px;"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items as $row) {
	    /** @var Framework\DBTable $row */
		if (fmod($i, 2) == 0) { $row_class =  "tbl-stripes-1"; } else { $row_class =  "tbl-stripes-2"; }
		echo '<tr valign="middle" class="'.$row_class.'">';
		echo '<td>'.$row->get('role_id').'</td>';
		echo '<td>'.$row->get('role_name').'</td>';
		echo '<td nowrap>&nbsp;&nbsp;<a href="'.Link::make(App::get_route(), "todo", "edit", "id", $row->get('role_id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
		echo '<a href="'.Link::make(App::get_route(), "todo", "delete", "ids[]", $row->get('role_id')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Rolle wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
?>
	<table class="pagin"><tr>
		<td style="width: 200px; text-align: left;">Zeige <?=$offset+1?> bis <?=$offset+$i?> von <?=intval($num_total)?></td>
            <?php $navigator->display()?>
		<td style="width: 200px; text-align: right;"><?=Lists::items_per_page_form()?></td>
		</tr>
	</table>

    <?php
}
?>