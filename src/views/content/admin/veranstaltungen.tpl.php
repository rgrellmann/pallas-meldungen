<?php
use Framework\App, Framework\Link, Framework\DateAndTime, Framework\Lists;
/** @var \Entity\Veranstaltung $row */
/** @var string $todo */
/** @var string $searchform */
/** @var int $offset */
/** @var int $num_total */
/** @var \Framework\Template $navigator */
/** @var array $items */
/** @var string $form_items */
?>

<h2>Admin - Veranstaltungen</h2>

<?php
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}

if ($todo == "edit_item") {
	$form = new \Framework\Template("partials/admin_form");
	$form->set("form_items", $form_items);
	$form->set("action", "admin/veranstaltungen");
	$form->set("todo", "save_item");
	$form->display();

} else {
?>
	<ul><li><a href="<?=Link::make(App::get_route(), "todo", "edit_item", "id", "0")?>">Neue Veranstaltung anlegen</a></li></ul>
	<?=$searchform?>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "bezeichnung_kurz", "Name", 'width="125"').
			Lists::table_top_order("", "ort", "Ort", 'width="175"').
			Lists::table_top_order("", "datum", "Datum", 'width="75"').
			Lists::table_top_order("", "meldebeginn", "Meldebeginn", 'width="105"').
			Lists::table_top_order("", "meldeschluss", 'Meldeschluss', 'width="105"')
?>			<td width="60">Anz. Meld.</td>
            <td width="30" align="center"><img src="<?=IMAGES?>icons/ausschreibung.gif" alt="Link zur Ausschreibung" title="Link zur Ausschreibung"></td>
			<td width="80"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items as $row) {
		if (fmod($i, 2) == 0) { $row_class =  "tbl-stripes-1"; } else { $row_class =  "tbl-stripes-2"; }
		echo '<tr valign="middle" class="'.$row_class.'">';
		echo '<td><span title="erstellt: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('id').'</span></td>';
		echo '<td><span title="'.$row->get('bezeichnung').'">'.$row->get('bezeichnung_kurz').'</span></td>';
		echo '<td>'.$row->get('ort').'</td>';
		echo '<td>'.DateAndTime::datetime_format($row->get('datum'), 1, '').'</td>';
		echo '<td>'.DateAndTime::datetime_format($row->get('meldebeginn'), 1, '').'</td>';
		echo '<td>'.DateAndTime::datetime_format($row->get('meldeschluss'), 1, '').'</td>';
		echo '<td align="center">'.$row->get('anzahl_meldungen').'</td>';
        echo '<td align="center">';
        if ($row->get('url_ausschreibung')) {
            echo '<a href="'.$row->get('url_ausschreibung').'" target="_blank" title="Ausschreibung öffnen"><img src="'.IMAGES.'icons/ausschreibung.gif" border="0" alt=""></a>';
        }
        echo '</td>';
		echo '<td nowrap>&nbsp;&nbsp;';
		echo '<a href="'.Link::make(App::get_route(), "todo", "edit_item", "id", $row->get('id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
		if (!$row->get('anzahl_meldungen')) {
		    echo '<a href="'.Link::make(App::get_route(), "todo", "delete_items", "ids[]", $row->get('id')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Veranstaltung wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
        } else {
            echo '<a href="'.Link::make('admin/meldungen', "veranstaltung", $row->get('id')).'" title="Meldungen"><img src="'.IMAGES.'icons/show_list.gif" border="0" alt=""></a> &nbsp;';
        }
        echo '<a href="'.Link::make('admin/veranstaltungen_disziplinen', "veranstaltung", $row->get('id')).'" title="Disziplinen"><img src="'.IMAGES.'icons/icon_documents.gif" border="0" alt=""></a> &nbsp;';
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
?>
	<table class="pagin"><tr>
		<td width="200" align="left">Zeige <?=$offset+1?> bis <?=$offset+$i?> von <?=intval($num_total)?></td>
            <?php $navigator->display()?>
		<td width="200" align="right"><?=Lists::items_per_page_form()?></td>
		</tr>
	</table>

    <?php
}
?>