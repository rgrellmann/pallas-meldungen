<?php
use Framework\App, Framework\Link, Framework\DateAndTime, Framework\Lists;
/** @var $todo string */
/** @var $searchform string */
/** @var array $items */
/** @var string $form_items */
/** @var $row \Entity\Disziplin */
/** @var $offset int */
/** @var $num_total int */
/** @var $navigator \Framework\Template */
?>

<h2>Admin - Disziplinen</h2>

<?php
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}

if ($todo == "edit_item") {
	$form = new \Framework\Template("partials/admin_form");
	$form->set("form_items", $form_items);
	$form->set("action", "admin/disziplinen");
	$form->set("todo", "save_item");
	$form->display();

} else {
?>
	<ul><li><a href="<?=Link::make(App::get_route(), "todo", "edit_item", "id", "0")?>">Neue Disziplin anlegen</a></li></ul>
	<?=$searchform?>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="40"').
			Lists::table_top_order("", "sportart", "Sportart", 'width="125"').
			Lists::table_top_order("", "name", "Name", 'width="175"').
			Lists::table_top_order("", "kurzname", "Abk.", 'width="75"')
?>			<td width="60">Letzte Änderung</td>
			<td width="80"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($items as $row) {
		if (fmod($i, 2) == 0) { $row_class =  "tbl-stripes-1"; } else { $row_class =  "tbl-stripes-2"; }
		echo '<tr valign="middle" class="'.$row_class.'">';
		echo '<td><span title="erstellt: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('id').'</span></td>';
		echo '<td>'.$row->get('sportart').'</td>';
		echo '<td>'.$row->get('name').'</td>';
		echo '<td>'.$row->get('kurzname').'</td>';
		echo '<td>'.DateAndTime::datetime_format($row->get('last_change'), 1, '').'</td>';
		echo '<td nowrap>&nbsp;&nbsp;';
		echo '<a href="'.Link::make(App::get_route(), "todo", "edit_item", "id", $row->get('id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a> &nbsp;';
        echo '<a href="'.Link::make(App::get_route(), "todo", "delete_items", "ids[]", $row->get('id')).'" title="löschen" onclick="return confirm(\'Wollen Sie diese Disziplin wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
	echo '</table>';
?>
	<table class="pagin"><tr>
		<td width="200" align="left">Zeige <?=$offset+1?> bis <?=$offset+$i?> von <?=intval($num_total)?></td>
            <?php $navigator->display()?>
		<td width="200" align="right"><?=Lists::items_per_page_form()?></td>
		</tr>
	</table>

    <?php
}
?>