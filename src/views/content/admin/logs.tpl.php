<?php use Framework\App, Framework\Request, Framework\Link;
/** @var array $log_files */
/** @var string $selected_logfile */
/** @var string $log_file_contents */
?>

<h2>Admin - Logs</h2>

<form name="logging_form" action="<?=Link::make(App::get_route())?>" method="POST">
<input type="hidden" name="todo" value="logfile">

<table cellpadding="0" cellspacing="0" style="width: 700px; margin-bottom:5px;">
<tr><td style="text-align: left;">
	<label><select size="1" name="logfile" style="width:150px;" onchange="this.form.submit()">
		<option value="">Datei w&auml;hlen</option>
            <?php
	foreach ($log_files as $log_file) {
		echo '<option value="'.$log_file.'"'.(($log_file==$selected_logfile)?" selected":"").'>'.$log_file.'</option>';
	}
?>
	</select></label>
	&emsp;
    <label><select size="1" name="num_rows" style="width:105px; text-align:right;" onchange="this.form.submit()">
		<option value="all"<?=((Request::get("num_rows") == "all") ? " selected" : "")?>>alle&nbsp;</option>
		<option value="20"<?=((Request::get("num_rows") == "20") ? " selected" : "")?>>die letzten 20&nbsp;</option>
		<option value="50"<?=((Request::get("num_rows") == "50") ? " selected" : "")?>>die letzten 50&nbsp;</option>
		<option value="100"<?=((!Request::get("num_rows") OR Request::get("num_rows") == "100") ? " selected" : "")?>>die letzten 100&nbsp;</option>
	</select> Zeilen anzeigen</label>
	</td>
	<td style="text-align: right;">
        <?php
	if ($selected_logfile) {
		echo "<span id=\"refresh_status\"></span><input type=\"button\" name=\"refresh\" value=\"refresh\" onclick=\"refresh_logfile()\">&emsp;&emsp;";
		echo "<input type=\"submit\" name=\"delete\" value=\"diese Log-Datei l&ouml;schen\" onClick=\"this.form.todo.value='delete'\"";
		if (preg_match("/apache_access|apache_error/", $_REQUEST['logfile'])) { echo " disabled"; }
		echo ">";
	}
?>
	</td>
</tr>
</table>

    <?php if ($selected_logfile) { ?>
    <label><textarea id="logfile_contents" style="width:700px; height:600px;" class="serviceInput"><?=$log_file_contents?></textarea></label>
	<br />
    <?php } ?>

</form>

<script type="text/javascript">
function refresh_logfile() {
	document.getElementById('refresh_status').innerHTML = "...bitte warten...&nbsp;";
	doRequest("<?=Link::make_js(App::get_route())?>?todo=refresh_logfile&num_rows=<?=$_REQUEST["num_rows"]?>&logfile=<?=$_REQUEST['logfile']?>", handle_refresh_request);
}
function handle_refresh_request() {
	if (ajax.readyState === 4) {
		document.getElementById('refresh_status').innerHTML = "";
		if (ajax.responseText.length > 0) {
			document.getElementById('logfile_contents').value = ajax.responseText;
		}
		isRunning = false;
	}
}
</script>
