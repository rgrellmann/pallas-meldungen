<?php use Framework\Auth, Framework\Database; ?>
<h2>Admin - Übersicht</h2>

<br><br>
<?php if (Auth::is_allowed('actions/view_system_info')) { ?>
System Info:<br>
Webserver: <?=$_SERVER['SERVER_SOFTWARE']?><br>
PHP Version: <?=phpversion()?><br>
MySQL Server Version: <?=Database::get_connection()->get_server_info()?><br>
Zend Framework Version: <?=Zend_Version::VERSION?><br>
<?php } ?>