<?php
namespace Application;
use Framework\Environment;
use Framework\Link, Framework\Auth;

/** @var $jahr int */
/** @var $user_message string */
/** @var $error_message string */
/** @var $rootpath string */
/** @var $veranstaltungen string */
/** @var $wettkaempfe string */
?>

<script type="text/javascript">

    var pfad = new Array("alpin/bm", "<?=$jahr?>", "/ergebnisliste_bm", "_", ".html");

    function standard_feld(feld,text,pfad_nr,pfad_text) {
        if (feld.length > 1) { document.forms[0].elements[feld].value = text; }
        pfad[pfad_nr] = pfad_text;
        <? if (Auth::is_allowed('actions/modify_resultlist_target_path')) { ?>
        document.forms[0].elements['speicherort'].value = pfad[0] + pfad[1] + pfad[2] + pfad[1] + pfad[3] + pfad[4];
        document.forms[0].elements['speicherort2'].value = pfad[0] + pfad[1] + pfad[2] + pfad[1] + pfad[3] + pfad[4];
        <? } ?>
    }

    function toggle_todo() {
        if (document.forms[0].todo2[0].checked) {
            document.forms[0].todo.value = document.forms[0].todo2[0].value;
        }
        if (document.forms[0].todo2[1].checked) {
            document.forms[0].todo.value = document.forms[0].todo2[1].value;
        }
        if (document.forms[0].todo2[2].checked) {
            document.forms[0].todo.value = document.forms[0].todo2[2].value;
        }
        if (document.forms[0].todo.value === "daten_speichern") {
            document.getElementById('zeile_datei').style.display = "none";
            document.getElementById('zeile_zeilen').style.display = "none";
            document.getElementById('zeile_alpin').style.display = "none";
        } else {
            document.getElementById('zeile_datei').style.display = "table-row";
            document.getElementById('zeile_zeilen').style.display = "table-row";
            document.getElementById('zeile_alpin').style.display = "table-row";
        }
    }

    function toggle_display() {
        if (document.getElementById('insert_list_subform').style.display === 'none') {
            document.forms[0].todo.value = 'liste_einfuegen';
            document.getElementById('insert_list_subform').style.display = 'block';
            document.getElementById('section_save_list').style.display = 'none';
            document.getElementById('section_additional_data').style.display = 'none';
        } else {
            document.getElementById('insert_list_subform').style.display = 'none';
            document.getElementById('section_save_list').style.display = 'block';
            document.getElementById('section_additional_data').style.display = 'block';
        }
    }
</script>


<? if (!empty($error_message)) {
    echo '<div class="error-message">'.$error_message.'</div>';
} ?>
<? if (!empty($user_message)) {
    echo '<div class="message">'.$user_message.'</div>';
} ?>

<h2>Upload und Formatierung von Ergebnislisten</h2>

<form action="<?=Link::make('self')?>" method="post" enctype="multipart/form-data">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
    <td><img src="../images/transparent.gif" width="25" height="10" alt=""></td>
    <td></td>
</tr>
<tr>
    <td align="left" style="padding-left: 10px;" colspan="2">
        <table class="border-style-1" width="95%">
            <tr>
                <td>Die Datei mu&szlig; folgendes Format haben: Text-Datei, Felder durch Tabulatoren getrennt.
<pre>z.B. zweizeilig:
Klasse	Bemerkung	Rg.	Nr.	Jg.	Name	1. Lauf	2. Lauf	Gesamt
Herren H36		5	61	1964	Bidschun, Andreas	54,52	50,37	1:44,89
				SC Prenzlauer Berg	SVB
oder einzeilig:
Klasse	Bemerkung	Rg.	Nr.	Jg.	Name	Verein	Verb.	Gesamt
Herren H21		6	36	1970	Tornow, Constantin	SC Pallas Berlin	SVB	1:00,39</pre>
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td></td>
    <td>
        <br>
        <table border="0" cellpadding="2" cellspacing="0" id="section_basic_data" class="format-ergebnisliste">
            <colgroup>
                <col style="width: 250px;">
                <col>
            </colgroup>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="todo" value="hochladen_und_formatieren">
                    Aktion auswählen:<br>
                    <input type="radio" name="todo2" value="hochladen_und_formatieren" checked onchange="toggle_todo()"> formatieren und anzeigen bzw. hochladen<br>
                    <? if (Auth::is_allowed('actions/upload_files')) { ?>
                    <input type="radio" name="todo2" value="nur_hochladen" onchange="toggle_todo()"> unformatiert hochladen<br>
                    <? } ?>
                    <input type="radio" name="todo2" value="daten_speichern" onchange="toggle_todo()"> nicht hochladen, nur Daten speichern<br>
                    <input type="radio" name="todo2" value="mit_gespeicherten_daten_formatieren" onchange="toggle_todo()"> hochladen, formatieren; gespeicherte Wettkampfdaten verwenden<br>&nbsp;
                </td>
            </tr>
            <tr>
                <td class="first-col">Wettkampf gehört zu Veranstaltung:</td>
                <td>
                    <select size="1" name="veranstaltungs_id">
                        <?=$veranstaltungen?>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="first-col">gespeicherten Wettkampf verwenden:</td>
                <td>
                    <select size="1" name="wettkampf_id">
                        <?=$wettkaempfe?>
                    </select>
                </td>
            </tr>
            <tr id="zeile_datei">
                <td class="first-col">Ergebnisliste als Text-Datei:</td>
                <td>
                    <input type="file" name="datei" size="30">
                </td>
            </tr>
            <tr id="zeile_zeilen">
                <td>Zeilen pro Läufer in der Text-Datei:</td>
                <td>
                    <input type="radio" name="zeilenformat" value="1-zeilig" checked> 1 Zeile &nbsp;&nbsp;&nbsp;
                    <input type="radio" name="zeilenformat" value="2-zeilig"> 2 Zeilen
                </td>
            </tr>
            <tr id="zeile_alpin">
                <td>alpin / nordisch:</td>
                <td>
                    <input type="radio" name="bereich" value="alpin" checked>alpin &nbsp;&nbsp;
                    <input type="radio" name="bereich" value="nordisch">nordisch
                </td>
            </tr>
            <tr>
                <td>Veranstaltung:</td>
                <td>
                    <input type="text" name="titel1" value="" size="45" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="BM Alpin" onClick="standard_feld('titel1','Berliner Meisterschaften Alpin <?=$jahr?>',0,'alpin/bm');this.form.bereich[0].checked=true;"> &nbsp;/&nbsp;
                    <input type="button" value="BM Nordisch" onClick="standard_feld('titel1','Berliner Meisterschaften Nordisch <?=$jahr?>',0,'nordisch/bm');this.form.bereich[1].checked=true;">
                </td>
            </tr>
            <tr>
                <td>Disziplin:</td>
                <td>
                    <input type="text" name="titel2" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Slalom" onClick="standard_feld('titel2','Slalom',3,'_sl');this.form.bereich[0].checked=true;"> &nbsp;&nbsp;
                    <input type="button" value="Riesenslalom" onClick="standard_feld('titel2','Riesenslalom',3,'_rs');this.form.bereich[0].checked=true;"> &nbsp;&nbsp;
                    <input type="button" value="Super-G" onClick="standard_feld('titel2','Super-G',3,'_sg');this.form.bereich[0].checked=true;"> &nbsp;/&nbsp;
                    <input type="button" value="klassisch" onClick="standard_feld('titel2','kurze Strecke - klassisch',3,'_kurz_kl');this.form.bereich[1].checked=true;"> &nbsp;&nbsp;
                    <input type="button" value="Sprint" onClick="standard_feld('titel2','Sprint - freie Technik',3,'_sprint_ft');this.form.bereich[1].checked=true;"> &nbsp;&nbsp;
                    <input type="button" value="Staffel" onClick="standard_feld('titel2','Staffel - klassisch / klassisch / freie Technik',3,'_staffel');this.form.bereich[1].checked=true;">
                </td>
            </tr>
            <tr>
                <td>Datum:</td>
                <td><?=ViewHelpers::Selectbox_Datum("datum",-1,date("j"),date("n"),false,"standard_feld('','',1,this.options[this.selectedIndex].value);")?></td>
            </tr>

            <tr class="divider"><td colspan="2" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>
        </table>

        <table border="0" cellpadding="2" cellspacing="0" id="section_additional_data" class="format-ergebnisliste">
            <tr>
                <td class="first-col">Ort:</td>
                <td class="middle-col">
                    <input type="text" name="ort" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Steinach" onClick="this.form.ort.value='Steinach / Thüringen';"> &nbsp;/&nbsp;
                    <input type="button" value="Klingenthal" onClick="this.form.ort.value='Klingenthal/Mühlleiten';">
                </td>
                <td rowspan="5" width="250">Diese Felder k&ouml;nnen<br>leer gelassen werden</td>
            </tr>
            <tr>
                <td>Organisator:</td>
                <td>
                    <input type="text" name="organisator" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Schneehasen" onClick="this.form.organisator.value='Berliner Schneehasen';"> &nbsp;/&nbsp;
                    <input type="button" value="Pallas" onClick="this.form.organisator.value='SC Pallas Berlin';">
                </td>
            </tr>
            <tr>
                <td>Wettkampfleiter:</td>
                <td>
                    <input type="text" name="wettkampfleiter" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Tommi" onClick="this.form.wettkampfleiter.value='T. Mikolajski (Schneehasen)';"> &nbsp;/&nbsp;
                    <input type="button" value="T. Zilch" onClick="this.form.wettkampfleiter.value='T. Zilch (Schneehasen)';"> &nbsp;/&nbsp;
                    <input type="button" value="Robert" onClick="this.form.wettkampfleiter.value='R. Grellmann (SC Pallas)';">
                </td>
            </tr>
            <!-- <tr>
                <td>Schiedsrichter:</td>
                <td><input type="text" name="schiedsrichter" value="" size="30" maxlength="50"></td>
            </tr> -->
            <tr>
                <td>Streckenchef:</td>
                <td>
                    <input type="text" name="streckenchef" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="G. Müller" onClick="this.form.streckenchef.value='G. Müller (Steinach)';">
                </td>
            </tr>
            <tr>
                <td>Startrichter:</td>
                <td>
                    <input type="text" name="startrichter" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Wolly" onClick="this.form.startrichter.value='W. Schley (SC Pallas)';">
                </td>
            </tr>
            <!-- <tr>
                <td>Zielrichter:</td>
                <td><input type="text" name="zielrichter" value="" size="30" maxlength="50"></td>
            </tr> -->
            <tr>
                <td>Zeitnahme:</td>
                <td>
                    <input type="text" name="zeitnahme" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="Horst" onClick="this.form.zeitnahme.value='H. Stargardt (Schneehasen)';"> &nbsp;/&nbsp;
                    <input type="button" value="Carsten" onClick="this.form.zeitnahme.value='C. Krüger (SC Pallas)';">
                </td>
            </tr>

            <tr class="divider"><td colspan="3" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>

            <tr>
                <td>Kurssetzer:</td>
                <td>
                    <input type="text" name="kurssetzer" value="" size="30" maxlength="50"> &nbsp;&nbsp;
                    <input type="button" value="G. Müller (Steinach)" onClick="this.form.kurssetzer.value='G. Müller (Steinach)';"> &nbsp;/&nbsp;
                    <input type="button" value="T. Zilch" onClick="this.form.kurssetzer.value='T. Zilch (Schneehasen)';"> &nbsp;/&nbsp;
                    <input type="button" value="Robert" onClick="this.form.kurssetzer.value='R. Grellmann (SC Pallas)';">
                </td>
                <td rowspan="4">Diese Felder k&ouml;nnen<br>leer gelassen werden</td>
            </tr>
            <tr>
                <td>Anzahl Tore:</td>
                <td><input type="text" name="anz_tore" value="" size="10" maxlength="20"></td>
            </tr>
            <tr>
                <td>H&ouml;hendifferenz:</td>
                <td>
                    <input type="text" name="h_diff" value="" size="10" maxlength="20"> &nbsp;
                    <input type="button" value="95m" onClick="this.form.h_diff.value='95m';">
                    <input type="button" value="120m" onClick="this.form.h_diff.value='120m';">
                    <input type="button" value="200m" onClick="this.form.h_diff.value='200m';">
                    <input type="button" value="240m" onClick="this.form.h_diff.value='240m';">
                </td>
            </tr>
            <tr>
                <td>Streckenlänge:</td>
                <td>
                    <input type="text" name="s_laenge" value="" size="10" maxlength="20"> &nbsp;
                    <input type="button" value="450m" onClick="this.form.s_laenge.value='450m';">
                    <input type="button" value="640m" onClick="this.form.s_laenge.value='640m';">
                    <input type="button" value="1050m" onClick="this.form.s_laenge.value='1050m';">
                    <input type="button" value="1400m" onClick="this.form.s_laenge.value='1400m';">
                </td>
            </tr>

            <tr class="divider"><td colspan="3" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>

            <tr>
                <td>Startzeit:</td>
                <td><input type="text" name="startzeit" value="" size="15" maxlength="50"> </td>
                <td rowspan="2">Diese Felder k&ouml;nnen<br>leer gelassen werden</td>
            </tr>
            <tr>
                <td>Temperatur Start/Ziel:</td>
                <td>
                    <input type="text" name="temp_start" value="" size="15" maxlength="50"> &nbsp;/&nbsp;
                    <input type="text" name="temp_ziel" value="" size="15" maxlength="50">
                </td>
            </tr>
            <!-- <tr>
                <td>Wetter / Schnee:</td>
                <td><input type="text" name="wetter" value="" size="30" maxlength="50"> &nbsp;/&nbsp; <input type="text" name="schnee" value="" size="30" maxlength="50"></td>
            </tr> -->

            <tr class="divider"><td colspan="3" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>

            <tr>
                <td>Ausgabe der Liste:</td>
                <td colspan="2">
                    <input type="radio" name="ausgabe" value="anzeigen" checked> anzeigen &nbsp;&nbsp;&nbsp;
                    <input type="radio" name="ausgabe" value="speichern"> speichern
                </td>
            </tr>
        </table>

        <? if (Auth::is_allowed('actions/modify_resultlist_target_path')) { ?>
        <table border="0" cellpadding="2" cellspacing="0" id="section_save_list" class="format-ergebnisliste">
            <tr>
                <td class="first-col">Speicherort:</td>
                <td>
                    <?=Environment::scheme().$_SERVER["SERVER_NAME"].$rootpath ?>/<input type="text" name="speicherort" value="" size="60" maxlength="100">
                    <input type="checkbox" name="anderer_pfad" value="true"><br>
                    <input type="checkbox" name="ueberschreiben" value="true"> überschreiben
                </td>
            </tr>

            <tr class="divider"><td colspan="2" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>
        </table>
        <? } ?>
        <? if (Auth::is_allowed('actions/upload_files')) { ?>
        <table border="0" cellpadding="2" cellspacing="0" id="section_insert_list" class="format-ergebnisliste">
            <tr>
                <td class="first-col">Liste einfügen:</td>
                <td>
                    <input type="checkbox" name="liste_einfuegen" value="true" onchange="toggle_display()"> HTML-Quelltext vor oder nach einer Ergebnisliste einfügen<br>
                    (z.B. um zwei Ergebnislisten eines Wettkampfes mit unterschiedlicher Anzahl von Durchgängen zusammen zu fügen)<br>
                    <div style="display:none;" id="insert_list_subform">
                        <input type="radio" name="platzhalter_nr" value="1" checked> über &nbsp;&nbsp;&nbsp;
                        <input type="radio" name="platzhalter_nr" value="2"> unter der bestehenden Liste einfügen<br>
                        Speicherort der zu ändernden Liste:<br>
                        <?=Environment::scheme().$_SERVER["SERVER_NAME"].$rootpath ?>/<input type="text" name="speicherort2" value="" size="60" maxlength="100"><br>
                        <textarea name="html_quelltext" rows="10" cols="80"></textarea>
                    </div>
                </td>
            </tr>
            <tr class="divider"><td colspan="2" style="padding: 10px 0 10px 0;"><img src="../images/transparent.gif" width="100%" height="1" alt=""></td></tr>
        </table>
        <? } ?>

        <table border="0" cellpadding="2" cellspacing="0" id="section_submit" class="format-ergebnisliste">
            <tr>
                <td align="center">
                    <input type="hidden" name="dummy" value="false"><br>
                    <input type="submit" value="senden">
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</form>
<br><br>
