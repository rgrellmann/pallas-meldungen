<?php use Framework\App, Framework\Link;
/** @var string $current_version */
/** @var array $files */
?>

<h2>Admin - Datenbank-Migrationen</h2>

<?php
if (!empty($error_message)) {
    echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
    echo "<div class=\"message\">$user_message</div>";
}
?>

<h4>Aktuelle Version: <?=$current_version?></h4>

<form name="logging_form" action="<?=Link::make(App::get_route())?>" method="POST">
    <input type="hidden" name="todo" value="index">

    <div class="sub1">
        <label for="input_file">Verfügbare Migrationen:</label><br>
        <select size="10" name="file" id="input_file" style="width:350px;">
            <?php
                foreach ($files as $file) {
                    echo '<option value="'.$file.'">'.$file.'</option>';
                }
                ?>
        </select>
    </div>
    <div class="sub1">
        <label><input type="checkbox" name="ignore_errors" value="1" style="vertical-align: bottom"> Bei Fehler fortfahren</label>
    </div>
    <div class="sub1">
        <input type="submit" onclick="this.form.todo.value='migrate_up'" value="aufwärts"> &nbsp;&nbsp;
        <input type="submit" onclick="this.form.todo.value='migrate_down'" value="abwärts">
    </div>
</form>
