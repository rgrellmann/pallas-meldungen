<?php use Framework\App, Framework\Common, Framework\Link, Framework\DateAndTime, Framework\Lists;
/** @var string $todo */
/** @var string $searchform */
/** @var Framework\Template $navigator */
/** @var int $offset */
/** @var int $num_total */
/** @var string $form_items */
/** @var array $users */
?>

<h2>Admin - Benutzer</h2>

<?php
if (!empty($error_message)) {
	echo "<div class=\"error-message\">$error_message</div>";
}
if (!empty($user_message)) {
	echo "<div class=\"message\">$user_message</div>";
}

if ($todo == "edit") {
	$form = new \Framework\Template("partials/admin_form");
	$form->set("form_items", $form_items);
	$form->set("action", "admin/users");
	$form->set("todo", "save");
	$form->set("autocomplete_off", "true");
	$form->display();

} else {
?>
	<ul><li><a href="<?=Link::make(App::get_route(), "todo", "edit", "id", "0")?>">Neuen Benutzer anlegen</a></li></ul>
	<?=$searchform?>
	<table border="0" cellspacing="1" cellpadding="1" style="margin-top:3px;">
		<tr class="tbl-stripes-0">
<?=			Lists::table_top_order("", "id", "ID", 'width="30"').
			Lists::table_top_order("", "username", "Username", 'width="90"').
			Lists::table_top_order("", "name", "Name", 'width="200"').
			Lists::table_top_order("", "email", "E-Mail", 'width="175"').
			Lists::table_top_order("", "last_change", "geändert", 'width="105"').
			Lists::table_top_order("", "last_login", "online", 'width="105"').
			Lists::table_top_order("", "is_online", '<img src="'.IMAGES.'icons/online.gif" alt="" title="online/offline">', 'style="width: 25px; text-align: center; vertical-align: bottom;"').
			Lists::table_top_order("", "active", '<img src="'.IMAGES.'icons/active.gif" alt="" title="Aktiv">', 'style="width: 25px; text-align: center; vertical-align: bottom;"')
?>			<td style="width: 30px; text-align: center; vertical-align: bottom;"><img src="<?=IMAGES?>icons/roles.gif" alt="" title="Rollen"></td>
			<td style="width: 20px; text-align: center; vertical-align: bottom;"><img src="<?=IMAGES?>icons/logo-yubico.png" width="16" alt="" title="Yubikey-ID"></td>
			<td style="width: 60px;"><img src="<?=EMPTY_IMAGE?>" width="1" height="20" alt=""></td>
		</tr>
    <?php $i = 0;
	foreach ($users as $row) {
	    /** @var Entity\User $row */
		if (fmod($i, 2) == 0) { $row_class =  "tbl-stripes-1"; } else { $row_class =  "tbl-stripes-2"; }
		echo '<tr valign="middle" class="'.$row_class.'">';
		echo '<td><span title="erstellt: '.DateAndTime::datetime_format($row->get('created'),1).'">'.$row->get('user_id').'</span></td>';
		echo '<td>'.$row->get('username').'</td>';
		echo '<td><span title="'.$row->get('first_name').' '.$row->get('last_name').'">'.Common::shorten_to_width(Common::show_name($row->get('first_name'), $row->get('last_name'), '', 'LF'),200).'</span></td>';
		echo '<td><span title="'.$row->get('email').'">'.Common::shorten_to_width($row->get('email'),175).'</span></td>';
		echo '<td><span title="'.DateAndTime::datetime_format($row->get('created'),1,'').'">'.DateAndTime::datetime_format($row->get('last_change'),1,'').'</span></td>';
		echo '<td>'.DateAndTime::datetime_format($row->get('last_login'),1,'').'</td>';
		echo '<td align="center"><img src="'.IMAGES.'icons/'.($row->get('is_online')=="1"?"on":"off").'line.gif" alt=""></td>';
        echo '<td align="center"><img src="'.IMAGES.'icons/'.($row->get('active')=="1"?"":"in").'active.gif" alt="" title="'.($row->get('active')=="1"?"":"in").'aktiv"></td>';
		echo '<td align="center">';
		$roles = $row->get_roles();
        if (in_array('1', $roles)) {
            echo '<img src="'.IMAGES.'icons/crown.png" alt="" title="Super-Admin">';
        } elseif (count($roles) AND $roles !== ['5']) {
            echo '<img src="'.IMAGES.'icons/wrench_screwdriver.png" alt="" title="Privilegierter Benutzer">';
        } else {
            echo '<img src="'.IMAGES.'icons/user.gif" alt="" title="normaler Benutzer">';
        }
		echo '</td>';
		echo '<td align="center">'.($row->get('yubikey_id')?'<img src="'.IMAGES.'icons/logo-yubico.png" width="16" alt="" title="Yubikey ID">':"").'</td>';
		echo '<td nowrap>&nbsp;<a href="'.Link::make(App::get_route(), "todo", "edit", "id", $row->get('user_id')).'" title="bearbeiten"><img src="'.IMAGES.'icons/edit.png" border="0" alt=""></a>&nbsp;&nbsp;&nbsp;&nbsp;';
		echo '<a href="'.Link::make(App::get_route(), "todo", "delete", "ids[]", $row->get('user_id')).'" title="löschen" onclick="return confirm(\'Wollen Sie diesen Benutzer wirklich löschen?\')"><img src="'.IMAGES.'icons/icon_trash.gif" border="0" alt=""></a> &nbsp;';
		echo '</td>';
		echo "</tr>\n";
		$i++;
	}
	if ($i==0) { echo '<tr><td colspan="8">Keine Einträge vorhanden</td></tr>'; }
    echo '</table>';
    ?>
    <table class="pagin"><tr>
            <td style="width: 200px; text-align: left;">Zeige <?=$offset+1?> bis <?=$offset+$i?> von <?=intval($num_total)?></td>
            <?php $navigator->display()?>
            <td style="width: 200px; text-align: right;"><?=Lists::items_per_page_form()?></td>
        </tr>
    </table>

    <?php
}
?>