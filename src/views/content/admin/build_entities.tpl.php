<?php
use Framework\App, Framework\Link;
/** @var string $sourcecode */
/** @var array $table_names */
?>

<h2>Admin - Entity Klasse erzeugen</h2>

<form name="logging_form" action="<?=Link::make(App::get_route())?>" method="POST">
    <input type="hidden" name="todo" value="generate">
    <label>Tabelle: <select name="table">
        <option value="">Tabelle wählen</option>
            <?php foreach ($table_names as $table) { ?>
        <option value="<?=$table?>"><?=$table?></option>
            <?php } ?>
    </select>
    </label>&ensp;
    <label><input type="checkbox" name="save_file" value="1"> Datei speichern</label>&ensp;
    <input type="submit" value="Klasse erzeugen">
</form>

<?php if (!empty($sourcecode)) { ?>
<br>
<code><?=htmlspecialchars($sourcecode)?>
</code>

<?php } ?>
