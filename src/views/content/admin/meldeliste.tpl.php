<?php use Framework\App, Framework\Link;
/** @var array $veranstaltungen */
/** @var array $disziplinen */
?>

<h2>Admin - Meldeliste exportieren</h2>

<?php if (!empty($error_message)) {
    echo '<div class="error-message">'.$error_message.'</div>';
} ?>

<form name="meldeliste_form" action="<?=Link::make(App::get_route())?>" method="POST" onsubmit="return validate_meldeliste_form()">
<table class="form-table">
    <tr>
        <td>Veranstaltung:</td>
        <td>
            <select size="1" name="veranstaltung" style="width:150px;" onChange="change_disciplines(this.value)">
                <option value="">Veranstaltung w&auml;hlen</option>
                <?php
                foreach ($veranstaltungen as $veranstaltung) {
                    echo '<option value="'.$veranstaltung->get_id().'">'.$veranstaltung->get('bezeichnung_kurz').'</option>';
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Disziplin:</td>
        <td>
            <select size="1" name="disziplin" style="width:150px;">
                <option value="" selected>Alle Teilnehmer</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Sortierung:</td>
        <td>
            <select name="sortierung" size="1">
                <option value="Jahrgang" selected> Jahrgang/Geschlecht</option>
                <option value="Name"> Name</option>
                <option value="Verein"> Verein</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>Format:</td>
        <td>
            <input type="radio" class="radio" name="format" value="svb" checked>Liste f&uuml;r das SVB-Kampfrichterprogramm<br>
            <input type="radio" class="radio" name="format" value="dsv">Liste f&uuml;r das DSV-Kampfrichterprogramm<br>
            <input type="radio" class="radio" name="format" value="text">TXT-Datei erzeugen<br>
            <input type="radio" class="radio" name="format" value="pdf">PDF-Datei erzeugen<br>
            <input type="radio" class="radio" name="format" value="csv">CSV-Datei erzeugen (z.B. für Excel oder Calc)
        </td>
    </tr>
    <tr>
        <td>
            <input type="radio" class="radio" name="download" value="true" checked>download
        </td>
        <td>
            <input type="radio" class="radio" name="download" value="false">anzeigen
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" value="Liste generieren">
        </td>
    </tr>
</table>
</form>

<script type="text/javascript">
    function validate_meldeliste_form() {
        var form = document.forms['meldeliste_form'];
        if (form.elements['veranstaltung'].value == "") {
            alert('Bitte wählen Sie eine Veranstaltung aus!');
            return false;
        }
        if (form.elements['download'].value == "false") {
            form.target = '_blank';
        }
        return true;
    }
    var disziplinen = [];
    <?php
    foreach ($disziplinen as $row) {
        if (empty($row['disziplinen'])) {
            continue;
        }
        $v_id = $row['id'];
        $d = explode(",", $row['disziplinen']);
        if (count($d)) {
            echo "\tdisziplinen[$v_id] = ";
            echo "['".implode("', '", $d)."'];\n";
        }
    }
    ?>
    function change_disciplines(veranstaltung) {
        var s = document.forms['meldeliste_form'].elements['disziplin'];
        var first = s.options[0];
        var n = null;
        var i = 0;
        s.options.length = 0;
        s.options[0] = first;
        if (disziplinen[veranstaltung]) {
            for (i = 0; i < disziplinen[veranstaltung].length; i++) {
                n = new Option(disziplinen[veranstaltung][i], disziplinen[veranstaltung][i], false, false);
                s.options[s.options.length] = n;
            }
        }
    }
</script>
