<?php use Framework\App, Framework\Link;
/** @var array $veranstaltungen */
?>

<h2>Admin - Mailingliste</h2>

<?php if (!empty($error_message)) {
    echo '<div class="error-message">'.$error_message.'</div>';
} ?>

<br>
Mailingliste als LDIF-Datei generieren:<br>
<form name="mailingliste_form" action="<?=Link::make(App::get_route())?>" method="POST">
    <select size="1" name="veranstaltung" style="width:150px;" onchange="this.form.submit()">
        <option value="">Veranstaltung w&auml;hlen</option>
        <?php
        foreach ($veranstaltungen as $veranstaltung) {
            echo '<option value="'.$veranstaltung->get_id().'">'.$veranstaltung->get('bezeichnung_kurz').'</option>';
        }
        ?>
    </select>
</form>
