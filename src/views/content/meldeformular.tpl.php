<?php
namespace Application;
use Framework\Link;
use Framework\Request;
use Framework\Template;

/** @var $error_message string */
/** @var $user_message string */
/** @var $debug_message string */
/** @var $data string */
/** @var $veranstaltungen array */

switch (Request::get("todo")) {
    case "einzelmeldung":
    case "gruppenmeldung":
        break;
    default:
        $js = '<script type="text/javascript">';
        $js .= "function werte_pruefen () { \n";
        $js .= "	if (document.forms[0].veranstaltung.selectedIndex > 0) { document.forms[0].submit(); }\n";
        $js .= "}
    	</script>\n";
        echo $js;
}
?>

<?  switch (Request::get("todo")) {
    case 'einzelmeldung_speichern':
    case 'gruppenmeldung_speichern':
    case 'einzelmeldung':
    case 'gruppenmeldung':
        if (isset($partial) AND $partial instanceof Template) {
            $partial->set('error_message', $error_message);
            $partial->set('user_message', $user_message);
            $partial->display();
        }
        break;
    default:
        ?>
        <?= !empty($error_message) ? $error_message : "" ?>
        <form name="meldeformular" method="post" action="<?=Link::make('self')?>">
        <table class="outer-table form-veranstaltung">
            <colgroup>
                <col style="width: 47%">
                <col style="width: 53%">
            </colgroup>
            <thead>
            <tr>
                <th colspan="2">Meldung f&uuml;r Veranstaltungen des Skiverbandes Berlin</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="normal1" rowspan="2">
                    Wenn Sie mehrere Personen gleichzeitig melden wollen, w&auml;hlen Sie &quot;Gruppenmeldung&quot;
                </td>
                <td class="normal1"> &nbsp; <input type="radio" class="radio" checked name="todo" value="einzelmeldung"> Einzelmeldung </td>
            </tr>
            <tr>
                <td class="normal1"> &nbsp; <input type="radio" class="radio" name="todo" value="gruppenmeldung"> Gruppenmeldung f&uuml;r bis zu
                    <select name="anz_teiln" size="1" onChange="document.forms[0].todo[1].checked=true">
                        <option value="5">5</option><option value="10" selected>10</option><option value="20">20</option><option value="30">30</option>
                    </select> Teilnehmer
                </td>
            </tr>
            <tr>
                <td class="normal1" align="center">
                    Bitte w&auml;hlen Sie eine Veranstaltung:
                </td>
                <td class="normal1">
                    &nbsp; <select name="veranstaltung" size="1">
                <? if (!count($veranstaltungen)) { ?>
                    <option value="nix"> keine Veranstaltung gefunden </option>
                <? } else { ?>
                    <option value="nix"> - bitte ausw&auml;hlen - </option>
                <? foreach ($veranstaltungen as $v) { /** @var $v \Entity\Veranstaltung */ ?>
                    <option value="<?=$v->get_id()?>"> <?=$v->get('bezeichnung_kurz')?></option>
                <? } } ?>
                    </select>
                    &nbsp; <input type="button" class="button1" value="Los!" onClick="werte_pruefen()"> &nbsp;
                </td>
            </tr>
            <tr>
                <td class="normal1" colspan="2">
                    Wenn Sie die gew&uuml;nschte Veranstaltung nicht in der Liste finden,
                    werden entweder noch keine Meldungen f&uuml;r diese Veranstaltung angenommen
                    oder der Meldezeitraum ist bereits abgelaufen.
                </td>
            </tr>
            </tbody>
        </table>
        </form>

<? } ?>

<div class="normal2 ul2">
    Wenn Sie eine Meldung zur&uuml;ckziehen m&ouml;chten,<br>schreiben Sie bitte eine E-Mail an:
    <a href="mailto:meldungen@scpallas.de?subject=Meldung%20stornieren">meldungen@scpallas.de</a><!--<br>
    oder gehen Sie zu der URL <a href="<?=BASE?>meldung_aendern.php"><?=BASE?>meldung_aendern.php</a><br>
    und loggen Sie sich mit Ihrer E-Mail-Adresse und dem Passwort ein, das Ihnen bei der Meldung angezeigt wurde-->.
</div>

<div class="normal2 ul2">
    <ul class="ul2">
        <li><a href="<?=Link::make('home')?>">zur&uuml;ck zur Startseite</a><br>&nbsp;</li>
        <li>
            <a href="<?=Link::make("admin/meldungen")?>">Liste der gemeldeten Teilnehmer</a> (Pa&szlig;wort erforderlich)<br>
            <span style="font-weight: normal;">Wenn Sie kein Pa&szlig;wort haben, k&ouml;nnen Sie sich
            mit dem Benutzernamen "gast", Pa&szlig;wort "gast" anmelden.</span>
        </li>
    </ul>
</div>
