<?php
use Framework\Link;
use Framework\Forms;
use Framework\Request;
/** @var $session \Framework\Session */
/** @var $todo string */
?>

<?php if ($todo == "login" AND !$session->is_logged_in()) { ?>
    <div class="header2 form-container">
        <div class="header1">Einloggen</div>
        <span class="sub1">Bitte loggen Sie sich hier mit den Zugangsdaten ein, die Sie erhalten haben.<br>&nbsp;</span>
        <form name="login_form" method="POST" action="<?=Link::make("home")?>">
            <?=Forms::form_hidden("todo", "login")?>
            <table border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td valign="middle"><label for="input_<?=PROJECT_IDENTIFIER?>_username">Username/E-Mail</label></td>
                    <td valign="middle"><?=Forms::form_input(PROJECT_IDENTIFIER."_username", Request::get(PROJECT_IDENTIFIER."_username"), "", "", "", "text", " autofocus required")?></td>
                </tr>
                <tr>
                    <td valign="middle"><label for="input_<?=PROJECT_IDENTIFIER?>_password">Passwort</label></td>
                    <td valign="middle"><?=Forms::form_input(PROJECT_IDENTIFIER."_password", "", "", "", "", "password")?></td>
                </tr>
                <tr id="yubikey_otp">
                    <td valign="middle"><label for="input_<?=PROJECT_IDENTIFIER?>_yubikey_otp">Yubikey OTP</label></td>
                    <td valign="middle">
                        <?=Forms::form_input(PROJECT_IDENTIFIER."_yubikey_otp", "", "", "", "", "text")?>
                        &nbsp; <a href="https://www.yubico.com/why-yubico/for-individuals/" target="_blank"><img src="<?=IMAGES?>icons/icon_help.gif" alt="help" title="Was ist das?" style="vertical-align: bottom;"></a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><?php
                        echo (!empty($form_errors["login_form.username"]))? '<div class="message-user message-inline">'.$form_errors["login_form.username"]."</div>" : "";
                        echo (!empty($form_errors["login_form.password"]))? '<div class="message-user message-inline">'.$form_errors["login_form.password"]."</div>" : "";
                    ?></td>
                </tr>
                <tr>
                    <td>&nbsp;<br><input type="submit" value="anmelden"></td>
                </tr>
            </table>
        </form>
        <div class="yubikey-switch">
            <img src="<?=IMAGES?>icons/logo-yubico.png" alt="Show Yubikey OTP Input Field" title="Show Yubikey OTP Input Field" style="width: 24px; height: 24px;" onclick="show_dr('yubikey_otp')">
        </div>
    </div>
<?php } ?>
    <div class="header2 ul1">
        <ul class="ul1">
            <li>
                <a href="<?=Link::make("meldeformular")?>"> Online-Meldeformular </a>
            </li>
        </ul>
    </div>
    <div class="normal2 ul2">
        Informationen f&uuml;r Kampfrichter/Organisatoren:
        <ul class="ul2">
            <li><a href="<?=Link::make("admin/meldungen")?>">Liste der gemeldeten Teilnehmer</a></li>
        </ul>
        Wenn Sie kein Pa&szlig;wort haben, k&ouml;nnen Sie sich
        mit dem Benutzernamen "gast", Pa&szlig;wort "gast" anmelden.
    </div>
