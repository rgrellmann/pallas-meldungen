<h2>Zugriff verweigert</h2>

Sie haben keine ausreichenden Rechte, um den gewünschten Bereich zu betreten
oder die gewünschte Aktion auszuführen.
