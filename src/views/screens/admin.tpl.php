<!DOCTYPE html>
<html lang="de-DE">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SVB Meldesystem Administration</title>
	<link rel="shortcut icon" href="/<?=\Framework\Conf::get('paths/url_base')?>favicon.ico">
	<link rel="stylesheet" type="text/css" href="/<?=BASE?>css/default.css?v=<?=CSS_JS_VERSION?>">
	<link rel="stylesheet" type="text/css" href="/<?=BASE?>css/scpallas.css?v=<?=CSS_JS_VERSION?>">
	<script language="JavaScript" type="text/javascript" src="/<?=BASE?>js/ajax.js?v=<?=CSS_JS_VERSION?>"></script>
	<script language="JavaScript" type="text/javascript" src="/<?=BASE?>js/common.js?v=<?=CSS_JS_VERSION?>"></script>
	<script language="JavaScript" type="text/javascript" src="/<?=BASE?>js/calendars.js?v=<?=CSS_JS_VERSION?>"></script>
	<script type="text/javascript">
        if (top != window) {
            top.location.href = window.location.href;
        }
		document.write(getCalendarStyles());
	</script>
</head>

<body>

<div class="outer-container">
    <nav id="mobile">
        <div class="nav">
            <a id="menu-toggle" class="anchor-link" href="#" onclick="return toggleMenu()">Menu</a>
            <ul id="mobile-nav" class="simple-toggle">
                <?php \Framework\Navigation::display("main_menu", 'mobile'); ?>
            </ul>
        </div>
    </nav>
    <nav id="desktop">
        <div class="vertical-menu-panel">
            <?php \Framework\Navigation::display("main_menu", 'vertical'); ?>
            <img src="<?=EMPTY_IMAGE?>" width="140" height="1" alt="">
        </div>
    </nav>
    <section id="main">
        <div class="main-content-area">
            <?php \Framework\App::get_content()->display(); ?>
        </div>
    </section>
</div>

<div id="calendarDiv"></div>

<footer>
    <?php /****** Debug ******/ ?>
    <?php if (DEBUG) { ?>
        <div class="debug-message"><?=\Framework\Environment::server_signature()?></div>
        <?php if ($message = \Framework\DebugTools::get_debug_message()) { ?>
        <div class="debug-message">Debug Information:<br />
            <?=$message?>
        </div>
        <?php } } ?>
</footer>

</body>
</html>