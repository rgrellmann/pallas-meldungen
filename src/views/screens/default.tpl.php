<!DOCTYPE html>
<html lang="de-DE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=(!empty($title) ? $title : 'SVB Meldesystem')?></title>
    <link rel="shortcut icon" href="/<?=BASE?>favicon.ico">
    <link rel="stylesheet" type="text/css" href="/<?=BASE?>css/default.css?v=<?=CSS_JS_VERSION?>">
    <link rel="stylesheet" type="text/css" href="/<?=BASE?>css/scpallas.css?v=<?=CSS_JS_VERSION?>">
    <script type="text/javascript" src="/<?=BASE?>js/ajax.js?v=<?=CSS_JS_VERSION?>"></script>
    <script type="text/javascript" src="/<?=BASE?>js/common.js?v=<?=CSS_JS_VERSION?>"></script>
    <script type="text/javascript" src="/<?=BASE?>js/calendars.js?v=<?=CSS_JS_VERSION?>"></script>
    <script type="text/javascript">
        if (top != window) {
            top.location.href = window.location.href;
        }
		document.write(getCalendarStyles());
	</script>
    <?=(!empty($javascript_head) ? $javascript_head : '')?>
</head>

<body>

<header class="logo-container border-style-1">
    <div class="svblogo">
        <a href="https://www.skiverband-berlin.de" target="_parent"><img src="<?=IMAGES?>svblogo2.gif" border="0" width="128" height="30" alt="Skiverband Berlin"></a>
    </div>
    <div class="logo-subtitle">
        Skiverband Berlin
    </div>
</header>

<section>
    <div class="main-content-area">
        <?php \Framework\App::get_content()->display(); ?>
    </div>
</section>

<?php if (\Framework\App::get_session()->is_logged_in()) { ?>
    <div class="normal2" style="padding-left: 10px;">
        <br><br>
        <a href="<?=\Framework\Link::make("home", "todo", "logout")?>">Logout</a>
        <br>
    </div>&nbsp;
<?php } ?>

<div id="calendarDiv"></div>

<footer>
    <?php /****** Debug ******/ ?>
    <?php if (DEBUG) { ?>
        <div class="debug-message"><?=\Framework\Environment::server_signature()?></div>
        <?php if ($message = \Framework\DebugTools::get_debug_message()) { ?>
        <div class="debug-message">Debug Information:<br />
            <?=$message?>
        </div>
        <?php } } ?>
</footer>

</body>
</html>