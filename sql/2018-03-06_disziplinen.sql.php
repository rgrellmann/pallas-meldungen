<?php
/** @var MySQLiDatabase $db */

use Framework\MySQLiDatabase;

$down[] = "DROP TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN;

$down[] = "TRUNCATE TABLE ".TABLE_DISZIPLINEN;
$down[] = "ALTER TABLE ".TABLE_DISZIPLINEN." ADD `mindest_ak` TINYINT(3) UNSIGNED NULL DEFAULT NULL";
$down[] = "ALTER TABLE ".TABLE_DISZIPLINEN." ADD `hoechst_ak` TINYINT(3) UNSIGNED NULL DEFAULT NULL";
$down[] = "ALTER TABLE ".TABLE_DISZIPLINEN." ADD `bemerkungen` VARCHAR(255) NULL DEFAULT NULL";
$down[] = "ALTER TABLE ".TABLE_DISZIPLINEN." CHANGE `sportart` `bereich` ENUM('alpin','nordisch') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL";
$down[] = "ALTER TABLE ".TABLE_DISZIPLINEN." CHANGE `id` `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT";

$down[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN." DROP `sportart`";



$up[] = "ALTER TABLE ".TABLE_DISZIPLINEN." CHANGE `id` `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT";
$up[] = "ALTER TABLE ".TABLE_DISZIPLINEN." DROP `mindest_ak`";
$up[] = "ALTER TABLE ".TABLE_DISZIPLINEN." DROP `hoechst_ak`";
$up[] = "ALTER TABLE ".TABLE_DISZIPLINEN." DROP `bemerkungen`";
$up[] = "ALTER TABLE ".TABLE_DISZIPLINEN." CHANGE `bereich` `sportart` ENUM('ski_alpin','ski_nordisch') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL";

$up[] = "INSERT INTO ".TABLE_DISZIPLINEN." (`id`, `sportart`, `name`, `kurzname`) VALUES
(1, 'ski_alpin', 'Slalom', 'SL'),
(2, 'ski_alpin', 'Riesenslalom', 'RS'),
(3, 'ski_alpin', 'Super-G', 'SG'),
(4, 'ski_alpin', 'Vielseitigkeitslauf', 'VL'),
(5, 'ski_alpin', 'Kids Cross', 'KC'),
(6, 'ski_alpin', 'Mini-Max', 'MM'),
(7, 'ski_nordisch', 'klassisch', 'KL'),
(8, 'ski_nordisch', 'freie Technik', 'FT'),
(9, 'ski_nordisch', 'Staffel', 'ST')";

$up[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN." ADD `sportart` ENUM('ski_alpin','ski_nordisch') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL";
$up[] = "UPDATE ".TABLE_VERANSTALTUNGEN." SET `sportart`='ski_alpin' WHERE `bezeichnung_kurz` LIKE '%alpin%' OR `bezeichnung_kurz` LIKE '%cup%'";
$up[] = "UPDATE ".TABLE_VERANSTALTUNGEN." SET `sportart`='ski_nordisch' WHERE `bezeichnung_kurz` LIKE '%nordisch%'";

$up[] = "CREATE TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." (
  `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `veranstaltungs_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `disziplin_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `juengster_jg` SMALLINT(4) UNSIGNED NULL,
  `aeltester_jg` SMALLINT(4) UNSIGNED NULL,
  `bemerkungen` VARCHAR(255) NULL
) ENGINE = InnoDB";
$up[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." ADD UNIQUE KEY (`veranstaltungs_id`, `disziplin_id`)";
$up[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." 
  ADD CONSTRAINT `fk_veranstaltungen_disziplinen_veranstaltungs_id` FOREIGN KEY (`veranstaltungs_id`) REFERENCES ".TABLE_VERANSTALTUNGEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";
$up[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." ADD INDEX (`disziplin_id`)";
$up[] = "ALTER TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." 
  ADD CONSTRAINT `fk_veranstaltungen_disziplinen_disziplin_id` FOREIGN KEY (`disziplin_id`) REFERENCES ".TABLE_DISZIPLINEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";
