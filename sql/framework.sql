/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

--
-- Tabellenstruktur für Tabelle `acl_roles`
--

CREATE TABLE IF NOT EXISTS `acl_roles` (
  `role_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `acl_roles` (`role_id`, `role_name`) VALUES (1, 'superadmin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `acl_rules`
--

CREATE TABLE IF NOT EXISTS `acl_rules` (
  `resource` varchar(255) NOT NULL,
  `role_id` mediumint(8) UNSIGNED DEFAULT '0',
  `rule` enum('allow','deny') NOT NULL DEFAULT 'allow',
  UNIQUE KEY `resource` (`resource`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Allow everything for the superadmin role
INSERT IGNORE INTO `acl_rules` (`resource`, `role_id`, `rule`) VALUES
  ('controllers', 1, 'allow'), ('routes', 1, 'allow'), ('actions', 1, 'allow');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `debug`
--

CREATE TABLE IF NOT EXISTS `debug` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `route` varchar(255) DEFAULT NULL,
  `num_queries` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `total_query_time` decimal(10,4) UNSIGNED DEFAULT '0.0000',
  `mem_usage` int(10) UNSIGNED DEFAULT '0',
  `rendertime` decimal(10,4) UNSIGNED DEFAULT '0.0000',
  `stopwatch` decimal(12,8) UNSIGNED DEFAULT '0.00000000',
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `user_id` int(10) UNSIGNED DEFAULT '0',
  `data` blob NOT NULL,
  `persistent` enum('0','1') NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_ip` varchar(64) NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `user_id` (`user_id`),
  KEY `expire` (`persistent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `is_online` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_change` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `password` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- create an initial user account
INSERT IGNORE INTO `users` (`user_id`, `username`, `email`, `first_name`, `last_name`, `is_online`, `last_login`, `created`, `last_change`, `password`, `active`)
    VALUES (1, 'admin', NULL, NULL, NULL, '0', NULL, NOW(), '0000-00-00 00:00:00', MD5('admin'), '1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` mediumint(9) UNSIGNED NOT NULL,
  `role_id` mediumint(9) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- assign the initial user to the superadmin role
INSERT IGNORE INTO `user_roles` (`user_id`, `role_id`) VALUES (1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
