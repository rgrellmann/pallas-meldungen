<?php

$down[] = "DELETE FROM ".TABLE_ACL_RULES." WHERE `resource` = 'routes/logout'";

$up[] = "INSERT IGNORE INTO ".TABLE_ACL_RULES." (`resource`, `role_id`, `rule`, `last_change`, `created`) VALUES
('routes/logout', 2, 'allow', '2018-03-22 06:30:34', '2018-03-22 07:30:34'),
('routes/logout', 3, 'allow', '2018-03-22 06:30:34', '2018-03-22 07:30:34'),
('routes/logout', 4, 'allow', '2018-03-22 06:30:48', '2018-03-22 07:30:48'),
('routes/logout', 5, 'allow', '2018-03-22 06:30:48', '2018-03-22 07:30:48')";
