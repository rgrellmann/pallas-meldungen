<?php

$down[] = "ALTER TABLE ".TABLE_MELDUNGEN." DROP FOREIGN KEY `fk_meldungen_veranstaltungs_id`";
$down[] = "ALTER TABLE ".TABLE_MELDUNGEN." DROP INDEX `veranstaltungs_id`";

$down[] = "ALTER TABLE ".TABLE_ACL_RULES." DROP FOREIGN KEY `fk_acl_rules_role_id`";
$down[] = "ALTER TABLE ".TABLE_ACL_RULES." DROP INDEX `role_id`";

$down[] = "UPDATE ".TABLE_SESSIONS." SET `user_id` = 0 WHERE user_id IS NULL";
$down[] = "ALTER TABLE ".TABLE_SESSIONS." DROP FOREIGN KEY `fk_sessions_user_id`";
$down[] = "ALTER TABLE ".TABLE_SESSIONS." CHANGE `user_id` `user_id` INT(10) UNSIGNED NULL DEFAULT NULL";

$down[] = "ALTER TABLE ".TABLE_USERS." DROP FOREIGN KEY `fk_users_verein_id`";
$down[] = "ALTER TABLE ".TABLE_USERS." DROP INDEX `verein_id`";

$down[] = "ALTER TABLE ".TABLE_USER_ROLES." DROP FOREIGN KEY `fk_user_roles_user_id`";
$down[] = "ALTER TABLE ".TABLE_USER_ROLES." DROP FOREIGN KEY `fk_user_roles_role_id`";
$down[] = "ALTER TABLE ".TABLE_USER_ROLES." DROP INDEX `role_id`";

$down[] = "ALTER TABLE ".TABLE_WETTKAEMPFE." DROP FOREIGN KEY `fk_wettkaempfe_veranstaltungs_id`";
$down[] = "ALTER TABLE ".TABLE_WETTKAEMPFE." DROP INDEX `veranstaltungs_id`";


$up[] = "ALTER TABLE ".TABLE_MELDUNGEN." ADD INDEX (`veranstaltungs_id`)";
$up[] = "ALTER TABLE ".TABLE_MELDUNGEN." 
  ADD CONSTRAINT `fk_meldungen_veranstaltungs_id` FOREIGN KEY (`veranstaltungs_id`) REFERENCES ".TABLE_VERANSTALTUNGEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";

$up[] = "ALTER TABLE ".TABLE_ACL_RULES." ADD INDEX (`role_id`)";
$up[] = "ALTER TABLE ".TABLE_ACL_RULES." 
  ADD CONSTRAINT `fk_acl_rules_role_id` FOREIGN KEY (`role_id`) REFERENCES ".TABLE_ROLES." (`role_id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";

$up[] = "UPDATE ".TABLE_SESSIONS." SET `user_id` = NULL WHERE user_id = 0";
$up[] = "ALTER TABLE ".TABLE_SESSIONS." CHANGE `user_id` `user_id` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL";
$up[] = "ALTER TABLE ".TABLE_SESSIONS." 
  ADD CONSTRAINT `fk_sessions_user_id` FOREIGN KEY (`user_id`) REFERENCES ".TABLE_USERS." (`user_id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";

$up[] = "ALTER TABLE ".TABLE_USERS." ADD INDEX (`verein_id`)";
$up[] = "ALTER TABLE ".TABLE_USERS." 
  ADD CONSTRAINT `fk_users_verein_id` FOREIGN KEY (`verein_id`) REFERENCES ".TABLE_VEREINE." (`id`) 
  ON DELETE RESTRICT ON UPDATE RESTRICT";

$up[] = "ALTER TABLE ".TABLE_USER_ROLES." 
  ADD CONSTRAINT `fk_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES ".TABLE_USERS." (`user_id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";
$up[] = "ALTER TABLE ".TABLE_USER_ROLES." ADD INDEX (`role_id`)";
$up[] = "ALTER TABLE ".TABLE_USER_ROLES."
  ADD CONSTRAINT `fk_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES ".TABLE_ROLES." (`role_id`) 
  ON DELETE RESTRICT ON UPDATE CASCADE";

$up[] = "ALTER TABLE ".TABLE_WETTKAEMPFE."
  ADD CONSTRAINT `fk_wettkaempfe_veranstaltungs_id` FOREIGN KEY (`veranstaltungs_id`) REFERENCES ".TABLE_VERANSTALTUNGEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";

