--
-- Tabellenstruktur für Tabelle `disziplinen`
--

CREATE TABLE IF NOT EXISTS `disziplinen` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `bereich` enum('alpin','nordisch') DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `kurzname` varchar(5) DEFAULT NULL,
  `mindest_ak` tinyint(3) unsigned DEFAULT NULL,
  `hoechst_ak` tinyint(3) unsigned DEFAULT NULL,
  `bemerkungen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ergebnisse`
--

CREATE TABLE IF NOT EXISTS `ergebnisse` (
  `teilnehmer_id` mediumint(8) unsigned NOT NULL,
  `wettkampf_id` mediumint(8) unsigned NOT NULL,
  `zeit_1` time DEFAULT NULL,
  `zeit_2` time DEFAULT NULL,
  `zeit_gesamt` time DEFAULT NULL,
  `diff` time DEFAULT NULL,
  `platz` smallint(5) unsigned DEFAULT NULL,
  `platz_bm` smallint(5) unsigned DEFAULT NULL,
  UNIQUE KEY `teilnehmer_id` (`teilnehmer_id`,`wettkampf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `meldungen`
--

CREATE TABLE IF NOT EXISTS `meldungen` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `veranstaltungs_id` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL DEFAULT '',
  `vorname` varchar(50) NULL DEFAULT NULL,
  `jahrgang` smallint UNSIGNED NULL DEFAULT NULL,
  `geschlecht` enum('m','w') NULL DEFAULT NULL,
  `verein` varchar(50) NOT NULL DEFAULT '',
  `verband_1` varchar(20) NULL DEFAULT NULL,
  `verband` varchar(10) NULL DEFAULT NULL,
  `telefon` varchar(50) NULL DEFAULT NULL,
  `handy` varchar(50) NULL DEFAULT NULL,
  `email` varchar(100) NULL DEFAULT NULL,
  `kommentar` varchar(255) NULL DEFAULT NULL,
  `disziplin` varchar(255) NULL DEFAULT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('gemeldet','gestrichen','mafue') NOT NULL DEFAULT 'gemeldet',
  `gemeldet_von` mediumint(8) UNSIGNED NULL DEFAULT NULL,
  `password` varchar(100) NULL DEFAULT NULL,
  `startpass_nr` varchar(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `veranstaltungen`
--

CREATE TABLE IF NOT EXISTS `veranstaltungen` (
  `id` mediumint(9) unsigned NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(100) NOT NULL DEFAULT '',
  `bezeichnung_kurz` varchar(30) DEFAULT NULL,
  `ort` varchar(50) NOT NULL DEFAULT '',
  `datum` date NOT NULL DEFAULT '0000-00-00',
  `url_ausschreibung` varchar(255) DEFAULT NULL,
  `meldeschluss` datetime DEFAULT NULL,
  `meldebeginn` datetime DEFAULT NULL,
  `disziplinen` varchar(255) DEFAULT NULL,
  `hinweise` varchar(255) DEFAULT NULL,
  `startpass_erforderlich` enum('ja','nein') NOT NULL DEFAULT 'nein',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `verbaende`
--

CREATE TABLE IF NOT EXISTS `verbaende` (
  `id` mediumint(9) NOT NULL DEFAULT '0',
  `code` varchar(10) DEFAULT NULL,
  `sportart` varchar(25) NOT NULL DEFAULT 'ski',
  `name_5` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `code` (`code`,`sportart`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vereine`
--

CREATE TABLE IF NOT EXISTS `vereine` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `sportart` varchar(50) DEFAULT 'ski',
  `code_verband` mediumint(9) NOT NULL DEFAULT '0',
  `code` varchar(10) DEFAULT NULL,
  `verein_kurz` varchar(10) DEFAULT NULL,
  `verein_20` varchar(25) DEFAULT NULL,
  `verein_25` varchar(30) DEFAULT NULL,
  `verein` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `wettkaempfe`
--

CREATE TABLE IF NOT EXISTS `wettkaempfe` (
  `id` mediumint(100) unsigned NOT NULL AUTO_INCREMENT,
  `veranstaltungs_id` mediumint(100) unsigned NOT NULL,
  `disziplin` varchar(100) NOT NULL DEFAULT '',
  `titel1` varchar(100) NOT NULL DEFAULT '',
  `titel2` varchar(100) NOT NULL DEFAULT '',
  `organisator` varchar(100) NOT NULL DEFAULT '',
  `wettkampfleiter` varchar(100) NOT NULL DEFAULT '',
  `schiedsrichter` varchar(100) NOT NULL DEFAULT '',
  `streckenchef` varchar(100) NOT NULL DEFAULT '',
  `startrichter` varchar(100) NOT NULL DEFAULT '',
  `zielrichter` varchar(100) NOT NULL DEFAULT '',
  `zeitnahme` varchar(100) NOT NULL DEFAULT '',
  `kurssetzer` varchar(100) NOT NULL DEFAULT '',
  `datum` varchar(100) NOT NULL DEFAULT '',
  `ort` varchar(100) NOT NULL DEFAULT '',
  `anz_tore` varchar(100) NOT NULL DEFAULT '',
  `h_diff` varchar(100) NOT NULL DEFAULT '',
  `s_laenge` varchar(100) NOT NULL DEFAULT '',
  `startzeit` varchar(100) NOT NULL DEFAULT '',
  `temp` varchar(100) NOT NULL DEFAULT '',
  `wetter` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
ALTER TABLE `wettkaempfe` ADD UNIQUE (`veranstaltungs_id`, `disziplin`);

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `user_verein` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `verein_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

INSERT IGNORE INTO `acl_roles` (`role_id`, `role_name`) VALUES (2, 'kampfrichter'), (3, 'verein'), (4, 'user');

INSERT IGNORE INTO `acl_rules` (`resource`, `role_id`, `rule`) VALUES
('controllers', NULL, 'allow'),
('controllers', 2, 'allow'),
('controllers', 3, 'allow'),
('controllers', 4, 'allow'),
('controllers', 5, 'allow'),
('routes/home', NULL, 'allow'),
('routes/home', 2, 'allow'),
('routes/home', 3, 'allow'),
('routes/home', 4, 'allow'),
('routes/home', 5, 'allow'),
('routes/login', NULL, 'allow'),
('routes/meldeformular', NULL, 'allow'),
('routes/meldeformular', 2, 'allow'),
('routes/meldeformular', 3, 'allow'),
('routes/meldeformular', 4, 'allow'),
('routes/meldeformular', 5, 'allow'),
('routes/admin/home', 2, 'allow'),
('routes/admin/home', 3, 'allow'),
('routes/admin/home', 4, 'allow'),
('routes/admin/home', 5, 'allow'),
('routes/admin/meldeliste', 2, 'allow'),
('routes/admin/veranstaltungen', 2, 'allow'),
('routes/admin/meldungen', 2, 'allow'),
('routes/admin/format_ergebnisliste', 2, 'allow'),
('routes/admin/meldungen', 3, 'allow'),
('routes/admin/meldungen', 4, 'allow'),
('routes/admin/meldungen', 5, 'allow'),
('actions/view_personal_data', 2, 'allow'),
('actions/delete_user', 2, 'allow'),
('actions/change_registration_status', 2, 'allow'),
('actions/change_registration_status_if_club_match', 3, 'allow'),
('actions/view_personal_data_if_club_match', 3, 'allow');

INSERT IGNORE INTO `users` (`user_id`, `username`, `email`, `first_name`, `last_name`, `is_online`, `last_login`, `created`, `last_change`, `password`, `active`) VALUES
(2, 'kampfrichter', NULL, 'SVB', 'Kampfrichter', 0, '2014-01-03 09:54:06', '2014-01-03 09:49:07', '0000-00-00 00:00:00', '612cbe62a91cac38c3d85643a3ae5571', 1),
(3, 'gast', NULL, 'Gast', NULL, 0, '2014-01-03 10:01:12', '2014-01-03 10:01:12', '0000-00-00 00:00:00', 'd4061b1486fe2da19dd578e8d970f7eb', 1);

INSERT IGNORE INTO `user_roles` (`user_id`, `role_id`) VALUES (2, 2);

-- --------------------------------------------------------

INSERT IGNORE INTO `verbaende` (`id`, `code`, `sportart`, `name_5`, `name`) VALUES
(1, '10', 'ski', 'BSV', 'Bayern'),
(2, '21', 'ski', 'SVB', 'Berlin'),
(3, '22', 'ski', 'SVSH', 'Schleswig-Holstein'),
(4, '23', 'ski', 'SVBRE', 'Bremen'),
(5, '24', 'ski', 'SBSB', 'Saarland'),
(6, '25', 'ski', 'SVP', 'Pfalz'),
(7, '26', 'ski', 'VHSV', 'Hamburg'),
(8, '27', 'ski', 'SVR', 'Rheinland'),
(9, '28', 'ski', 'NSV', 'Niedersachsen'),
(10, '29', 'ski', 'HSV', 'Hessen'),
(11, '30', 'ski', 'WSV', 'Nordrhein-Westfalen'),
(12, '31', 'ski', 'SVSN', 'Schwarzwald-Nord'),
(13, '32', 'ski', 'SVS', 'Schwarzwald'),
(14, '33', 'ski', 'SSV', 'Schwaben'),
(15, '36', 'ski', 'TSV', 'Thüringen'),
(16, '37', 'ski', 'LSS', 'Sachsen'),
(17, '38', 'ski', 'SVSA', 'Sachsen-Anhalt'),
(18, '39', 'ski', 'LSVBRD', 'Brandenburg'),
(19, '40', 'ski', 'LSMV', 'Meckl.-Vorp.'),
(20, '41', 'ski', 'SVRH', 'Rheinhessen'),
(21, '0', 'ski', 'Gast', 'Gast');

INSERT IGNORE INTO `vereine` (`id`, `sportart`, `code_verband`, `code`, `verein_kurz`, `verein_20`, `verein_25`, `verein`) VALUES
(1, 'ski', 21, '003', 'SCP', 'SC Pallas Berlin', 'SC Pallas Berlin', 'SC Pallas Berlin'),
(2, 'ski', 21, '001', 'BS', 'Berliner Schneehasen', 'Berliner Schneehasen', 'Berliner Schneehasen'),
(3, 'ski', 21, '000', 'SCB', 'SC Berlin', 'Ski-Club Berlin', 'Ski-Club Berlin'),
(4, 'ski', 21, '000', 'SCPB', 'SC Prenzlauer Berg', 'SC Prenzlauer Berg', 'SC Prenzlauer Berg'),
(5, 'ski', 21, '000', 'SGEBB', 'SG Einh.Berliner B&auml;r', 'SG Einheit Berliner B&auml;r', 'SG Einheit Berliner B&auml;r'),
(6, 'ski', 21, '000', 'SSF', 'SSF Lankwitz', 'SSF Lankwitz', 'SSF Lankwitz'),
(7, 'ski', 21, '000', 'DAV', 'Alpenverein S.Berlin', 'Alpenverein Sekt. Berlin', 'Alpenverein Sektion Berlin'),
(8, 'ski', 21, '000', 'LKV', 'Landes-Kanuverband', 'Landes-Kanu-Verband', 'Landes-Kanu-Verband'),
(9, 'ski', 21, '000', 'SGB', 'Skisp.-Gemeinsch. B.', 'Skisport-Gemeinsch.Berlin', 'Skisport-Gemeinschaft Berlin'),
(10, 'ski', 21, '000', 'BSV', 'Berl.Snowboardverein', 'Berliner Snowboardverein', 'Berliner Snowboardverein'),
(11, 'ski', 21, '000', 'RCC', 'RC Charlottenburg', 'Radsp.-Club Charlottenb.', 'Radsport-Club Charlottenburg');

-- --------------------------------------------------------
