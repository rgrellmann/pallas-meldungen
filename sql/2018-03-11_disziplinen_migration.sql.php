<?php
/** @var Framework\Database $db */

use Entity\VeranstaltungDisziplin;
use Framework\Common;
use Framework\DuplicateKeyEntryException;

$down[] = "TRUNCATE TABLE ".TABLE_VERANSTALTUNGEN_DISZIPLINEN;

$up[] = "UPDATE ".TABLE_VERANSTALTUNGEN." SET `disziplinen`=REPLACE(disziplinen,'ids-Cro','ids Cro') WHERE `disziplinen` LIKE '%ids-Cro%'";
$up[] = "UPDATE ".TABLE_MELDUNGEN." SET `disziplin`=REPLACE(disziplin,'ids-Cro','ids Cro') WHERE `disziplin` LIKE '%ids-Cro%'";
$up[] = "UPDATE ".TABLE_MELDUNGEN." SET `disziplin`=REPLACE(disziplin,'".$db->escape("id's Cro")."','ids Cro') WHERE `disziplin` LIKE '".$db->escape("%id's Cro%")."'";
$up[] = "UPDATE ".TABLE_MELDUNGEN." SET `disziplin`=REPLACE(disziplin,'Risesenslalom','Riesenslalom') WHERE `disziplin` LIKE '%Risesenslalom%'";

$up[] = function($db, $controller) {
    /** @var Framework\Database $db */
    /** @var Controller\Admin\Db_MigrationsController $controller */
    $sql = "SELECT id, sportart, disziplinen FROM " . TABLE_VERANSTALTUNGEN;
    $res = $db->query($sql);
    $veranstaltungen = Common::make_array($res);

    $res = $db->query("SELECT * FROM " . TABLE_DISZIPLINEN . " WHERE sportart = 'ski_alpin'");
    $disziplinen['ski_alpin'] = Common::make_array($res);

    $res = $db->query("SELECT * FROM " . TABLE_DISZIPLINEN . " WHERE sportart = 'ski_nordisch'");
    $disziplinen['ski_nordisch'] = Common::make_array($res);

    foreach ($veranstaltungen as $veranstaltung) {
        if (empty($veranstaltung['disziplinen'])) {
            continue;
        }
        $veranstaltung['disziplinen'] = explode(",", $veranstaltung['disziplinen']);
        foreach ($veranstaltung['disziplinen'] as $disziplin_old) {
            $found = false;
            switch ($disziplin_old) {
                case 'Sprint (freie Technik)': $disziplin_old = 'freie Technik'; break;
                case 'RS mit kurzen Torabständen': $disziplin_old = 'Riesenslalom'; break;
            }
            foreach ($disziplinen[$veranstaltung['sportart']] as $disziplin_new) {
                if (strtolower(substr($disziplin_new['name'], 0, 6)) == strtolower(substr($disziplin_old, 0, 6))) {
                    $found = true;
                    $item = new VeranstaltungDisziplin();
                    $item->set_veranstaltungs_id($veranstaltung['id']);
                    $item->set_disziplin_id($disziplin_new['id']);
                    if (strpos($disziplin_old, 'Partner')) {
                        $item->set_bemerkungen(substr($disziplin_old, strpos($disziplin_old, 'Partner'), -1));
                    }
                    if (strpos($disziplin_old, 'JG')) {
                        $jg = substr($disziplin_old, strpos($disziplin_old, 'JG') + 2, 4);
                        if (strpos($disziplin_old, 'u.j.') OR strpos($disziplin_old, 'u. jünger')) {
                            $item->set_aeltester_jg($jg);
                        } elseif (strpos($disziplin_old, 'u.ä.')) {
                            $item->set_juengster_jg($jg);
                        } else {
                            throw new RuntimeException('Unexpected String: ' . $disziplin_old);
                        }
                    }
                    try {
                        $item->insert();
                    } catch (DuplicateKeyEntryException $e) {

                    }
                    continue 2;
                }
            }
            if (!$found) {
                $controller->error_message[] = "Disziplin nicht gefunden: $disziplin_old";
            }
        }
    }
};

