<?php

use Framework\Common;

$down[] = "TRUNCATE TABLE ".TABLE_MELDUNGEN_DISZIPLINEN;

$up[] = "UPDATE ".TABLE_MELDUNGEN." SET disziplin = 'Riesenslalom,Slalom' WHERE disziplin = 'RS und SL'";

$up[] = function($db, $controller) {
    /** @var Framework\Database $db */
    /** @var Controller\Admin\Db_MigrationsController $controller */
    $sql = "SELECT id FROM " . TABLE_VERANSTALTUNGEN;
    $res = $db->query($sql);
    $veranstaltungen = Common::make_array($res, Common::FIRST_FIELD);

    $migrated = 0;
    $skipped = 0;

    foreach ($veranstaltungen as $veranstaltungs_id) {
        $sql = "SELECT vd.id, LOWER(SUBSTRING(name, 1, 6)) AS short_name FROM " . TABLE_VERANSTALTUNGEN_DISZIPLINEN . " AS vd"
            . " INNER JOIN " . TABLE_DISZIPLINEN . " AS d ON d.id = vd.disziplin_id"
            . " WHERE vd.veranstaltungs_id = $veranstaltungs_id";
        $res = $db->query($sql);
        $disziplinen = Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);

        $sql = "SELECT id, disziplin FROM " . TABLE_MELDUNGEN . " WHERE veranstaltungs_id = $veranstaltungs_id";
        $res = $db->query($sql);
        $meldungen = Common::make_array($res);

        foreach ($meldungen as $meldung) {
            if (empty($meldung['disziplin'])) {
                $skipped++;
                continue;
            }
            $meldung['disziplin'] = explode(",", $meldung['disziplin']);
            foreach ($meldung['disziplin'] as $disziplin_old) {
                switch ($disziplin_old) {
                    case 'Sprint (freie Technik)': $disziplin_old = 'freie Technik'; break;
                    case 'RS mit kurzen Torabständen': $disziplin_old = 'Riesenslalom'; break;
                }
                $disziplin_id = array_search(strtolower(substr($disziplin_old, 0, 6)), $disziplinen);
                if ($disziplin_id) {
                    $sql = "INSERT IGNORE INTO " . TABLE_MELDUNGEN_DISZIPLINEN . " (meldung_id, veranstaltung_disziplin_id)"
                        . " VALUES ($meldung[id], $disziplin_id)";
                    $db->query($sql);
                } else {
                    throw new RuntimeException('Disziplin nicht gefunden! Meldung:' . print_r($meldung, 1));
                }
            }
            $migrated++;
        }
    }
    $controller->user_message[] = "$migrated Meldungen migriert, $skipped ohne Disziplinen übersprungen";
};
