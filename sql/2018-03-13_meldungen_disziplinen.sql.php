<?php

$down[] = "DROP TABLE ".TABLE_MELDUNGEN_DISZIPLINEN;


$up[] = "CREATE TABLE ".TABLE_MELDUNGEN_DISZIPLINEN." (
  `meldung_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `veranstaltung_disziplin_id` MEDIUMINT(8) UNSIGNED NOT NULL,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB";
$up[] = "ALTER TABLE ".TABLE_MELDUNGEN_DISZIPLINEN." ADD PRIMARY KEY ( `meldung_id`, `veranstaltung_disziplin_id`)";
$up[] = "ALTER TABLE ".TABLE_MELDUNGEN_DISZIPLINEN." 
  ADD CONSTRAINT `fk_meldungen_disziplinen_meldungs_id` FOREIGN KEY (`meldung_id`) REFERENCES ".TABLE_MELDUNGEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";
$up[] = "ALTER TABLE ".TABLE_MELDUNGEN_DISZIPLINEN." 
  ADD CONSTRAINT `fk_meldungen_disziplinen_veranstaltung_disziplin_id` FOREIGN KEY (`veranstaltung_disziplin_id`) REFERENCES ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." (`id`) 
  ON DELETE CASCADE ON UPDATE CASCADE";

