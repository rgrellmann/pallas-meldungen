
/* simple highlighting for mouseover-effect */
var hl_classname_old = '';
var hl_color_old = '';
function hl(id, status) {
	if (status) {
		hl_color_old = document.getElementById(id).style.backgroundColor;
		hl_classname_old = document.getElementById(id).className;
		document.getElementById(id).style.backgroundColor = "lemonchiffon";
	} else {
		document.getElementById(id).style.backgroundColor = hl_color_old;
		document.getElementById(id).className = hl_classname_old;
	}
}

// check all boxes in a group
function checkAll(elements, checked) {
    if (elements instanceof NodeList) {
        for (i=0; i<elements.length; i++) {
        	if (!checked || elements.item(i).disabled == false) {
            	elements.item(i).checked = checked;
        	}
        }
    } else if (elements instanceof HTMLInputElement) {
       	if (!checked || elements.item(i).disabled == false) {
	        elements.checked = checked;
        }
    }
}

// hide all popup divs
function hide_all(container, id_regexp) {
	if (document.getElementById(container)) {
		var popups = document.getElementById(container).getElementsByTagName('div');
		for (var i=0; i<popups.length; i++) {
			if (id_regexp.test(popups[i].id)) {
				popups[i].style.visibility = 'hidden';
			}
		}
	}
}

// prevent menu divs from changing their width when hovering over them
function fixate_menubar(container, id_regexp) {
	var menu = document.getElementById(container).getElementsByTagName('div');
	for (var i=0; i<menu.length; i++) {
		if (id_regexp.test(menu[i].id)) {
			menu[i].style.width = menu[i].offsetWidth + 'px';
		}
	}
}

// show a DOM object using visibility method
function show_v(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.visibility = 'visible';
	}
}

// hide a DOM object using visibility method
function hide_v(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.visibility = 'hidden';
	}
}

// show a DOM object using display method
function show_d(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.display = 'block';
	}
}

// show a DOM object using display method (inline)
function show_di(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.display = 'inline';
	}
}

// show a DOM object using display method (table-row)
function show_dr(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.display = 'table-row';
	}
}

// hide a DOM object using display method
function hide_d(object_id) {
	if (document.getElementById(object_id)) {
		document.getElementById(object_id).style.display = 'none';
	}
}

var searchform_small_classname = '';
function expand_searchform() {
	searchform_small_classname = document.getElementById('searchform').className;
	document.getElementById('searchform').className = "searchform-big";
	document.getElementById('searchhandle_img').src = document.getElementById('searchhandle_img').src.replace(/plus.gif/, 'minus.gif');
	document.getElementById('searchhandle_link').href = "javascript:shrink_searchform()";
	document.getElementById('searchform_content').style.display = "block";
}

function shrink_searchform() {
	document.getElementById('searchform').className = searchform_small_classname;
	document.getElementById('searchhandle_img').src = document.getElementById('searchhandle_img').src.replace(/minus.gif/, 'plus.gif');
	document.getElementById('searchhandle_link').href = "javascript:expand_searchform()";
	document.getElementById('searchform_content').style.display = "none";
}

function show_help(e, help_text) {
	document.getElementById('help_div').innerHTML = help_text;
	if (help_text.length > 100) {
		document.getElementById('help_div').style.width = "350px";
	} else { document.getElementById('help_div').style.width = "200px"; }
	document.getElementById('help_div').style.left = (e.clientX-5) + "px";
	document.getElementById('help_div').style.top  = (e.clientY-5) + "px";
	show_v('help_div');
}

function remove_default(element, default_value) {
    if (element.value === default_value) {
        element.value = '';
    }
}

function toggleMenu() {
    document.getElementById('mobile-nav').classList.toggle('open');
    return false;
}