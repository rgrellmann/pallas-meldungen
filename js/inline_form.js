var url = ""; // define in script!
var form_name = ""; // define in script!

function show_inline_form(id) {
	hide_d(id + '_display');
	if (document.getElementById(id + '_icon')) {
		hide_d(id + '_icon');
	}
	show_di(id + '_form_container');
}
function save_inline_form(id, item_id) {
	if (document.forms[form_name].elements[id]) {
		// one value
		doRequest(url + '&id=' + item_id + '&name=' + id + "&value=" + encodeURIComponent(document.forms[form_name].elements[id].value), handleInlineFormRequest);
	} else if (!document.forms[form_name].elements[id + '_day']) {
		// an array of values (checkboxes or select multiple)
		var elements = document.forms[form_name].elements;
		var values = "";
		for (var i = 0; i < elements.length; i++) {
			if (elements[i].name == id + '[]' && elements[i].checked) {
				values += "&value[]=" + elements[i].value;
			}
		}
		doRequest(url + '&id=' + item_id + '&name=' + id + values , handleInlineFormRequest);
	} else {
		// date selectboxes
		var values = "&" + id + "_day="   + document.forms[form_name].elements[id + "_day"].value +
					"&" + id + "_month="  + document.forms[form_name].elements[id + "_month"].value +
					"&" + id + "_year="   + document.forms[form_name].elements[id + "_year"].value +
					"&" + id + "_hour="   + document.forms[form_name].elements[id + "_hour"].value +
					"&" + id + "_minute=" + document.forms[form_name].elements[id + "_minute"].value;
		doRequest(url + '&id=' + item_id + '&name=' + id + values , handleInlineFormRequest);
	}
	hide_d(id + '_form_container');
	show_di(id + '_display');
	document.getElementById(id + '_display').innerHTML = '<img src="images/hourglass.gif" alt="">';
}
function handleInlineFormRequest() {
	if (ajax.readyState == 4) {
		if (ajax.responseText.length > 0) {
			eval('var response = ' + ajax.responseText);
			if (response[0] == 'success') {
				document.getElementById(response[1] + '_display').innerHTML = response[2];
				if (document.getElementById(response[1] + '_icon')) {
					show_di(response[1] + '_icon');
				}
			} else {
				document.getElementById(response[1] + '_display').innerHTML = '<img src="images/attention.gif" alt="" title="Fehler beim Speichern des Wertes" style="vertical-align:middle;">';
			}
		}
		isRunning = false;
	}
}
