/* AJAX */

var ajax;
var ajax_post_params = null;
var isRunning = false;

if (typeof XMLHttpRequest != 'undefined') {
	ajax = new XMLHttpRequest();
} else {
    try	{
        ajax = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (error) {
        try {
            ajax = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            ajax = false;
        }
    }
}

// you can send parameters either with the GET string (attach them to the url variable)
// or with POST (set the ajax_post_params variable) or both.
function doRequest(url, handle) {
    if (!isRunning && ajax) {
		ajax.open("POST", url, true);
		ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.onreadystatechange = handle;
        isRunning = true;
        ajax.send(ajax_post_params);
        ajax_post_params = null;
    }
}

/* Example Request Handler
function handleExampleRequest() {
	if (ajax.readyState == 4) {
		if (ajax.responseText.length > 0) {
			// Do something with the responseText or responseXML
		}
		isRunning = false;
	}
}
*/