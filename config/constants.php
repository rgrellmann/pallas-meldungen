<?php

/*
 * Do not include this file!
 * This file contains dynamically defined constants as a hint to the IDE
 */

define('TABLE_ROLES', 'm_acl_roles');
define('TABLE_ACL_RULES', 'm_acl_rules');
define('TABLE_DEBUG', 'm_debug');
define('TABLE_USERS', 'm_users');
define('TABLE_USER_ROLES', 'm_user_roles');
define('TABLE_USER_VEREIN', 'm_user_verein');
define('TABLE_SESSIONS', 'm_sessions');
define('TABLE_VEREINE', 'm_vereine');
define('TABLE_VERBAENDE', 'm_verbaende');
define('TABLE_VERANSTALTUNGEN', 'm_veranstaltungen');
define('TABLE_VERANSTALTUNGEN_DISZIPLINEN', 'm_veranstaltungen_disziplinen');
define('TABLE_ERGEBNISSE', 'm_ergebnisse');
define('TABLE_MELDUNGEN', 'm_meldungen');
define('TABLE_MELDUNGEN_DISZIPLINEN', 'm_meldungen_disziplinen');
define('TABLE_WETTKAEMPFE', 'm_wettkaempfe');
define('TABLE_DISZIPLINEN', 'm_disziplinen');
define('TABLE_DB_VERSION', 'm_db_version');
