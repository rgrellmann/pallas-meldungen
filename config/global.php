<?php
/**
 * The Framework
 * @author Robert
 * @date 04.11.12
 */

$conf = array(
    'errors' => array(
        'error_reporting' => E_ALL,
        'error_msg' => 'Es ist ein Fehler aufgetreten: %1$s<br/>Datei: %2$s<br/>Zeile: %3$s<br/>Trace:<br/>%4$s',
        'show_details' => 1,
    ),
    // set this to 0/1 to disable/enable debugging
    'debug' => 0,
    'is_local_test_system' => 0,
    'config' => array(
        'load_from_database' => 0,
    ),
    'css_js_version' => 1,
    'database' => array(
        'host' => 'localhost',
        'socket' => '',
        'user' => 'scpallas',
        'pass' => 'scpallas',
        'db_name' => 'scpallas',
        'table_prefix' => '',
        'adapter' => 'mysqli',
        'tables' => array(
            // the table names given here will be provided as global constants which must be used
            // in SQL queries. A table prefix should be defined above and not in the table names here.
            'debug' => 'debug',
            'users' => 'users',
            'roles' => 'acl_roles',
            'acl_rules' => 'acl_rules',
            'user_roles' => 'user_roles',
            'db_version' => 'db_version',
            'sessions' => 'sessions',
            'vereine' => 'vereine',
            'verbaende' => 'verbaende',
            'veranstaltungen' => 'veranstaltungen',
            'ergebnisse' => 'ergebnisse',
            'meldungen' => 'meldungen',
            'wettkaempfe' => 'wettkaempfe',
            'disziplinen' => 'disziplinen',
            'veranstaltungen_disziplinen' => 'veranstaltungen_disziplinen',
            'meldungen_disziplinen' => 'meldungen_disziplinen',
        ),
    ),
    'session' => array(
        'handler' => 'user',
        'name' => 'meldungen',
        'lifetime' => 60*60*2,
        'persistent_cookie_lifetime' => 86400*7,
        // string appended to the password before generating a hash for increased security
        'cookie_additional_string' => 'meldungen',
        'user_id_column' => 'user_id',
    ),
    'logging' => array(
        'file' => array(
            'path' => 'logs/',
            'level' => PHPLOG_ALL,
        ),
        'mail' => array(
            'to' => 'webmaster@agentmulder.de',
            'from' => 'meldungen@scpallas.de',
            'level' => PHPLOG_WARN,
        ),
        'slow_query_time' => 0.1,
        'slow_request_time' => 1000000,
        'log_sql_query_times' => 0,
        'sql_level' => 0
    ),
    'paths' => array(
        // the path to prepend to all generated local URLs
        'url_base' => 'meldungen/',
        // the absolute URL path to the directory of static images
        'images' => '/meldungen/images/',
        'views' => 'src/views/',
        'namespace_map' => array(
            'Framework' => 'lib/framework',
            'Entity'      => 'lib/entities',
            'Application' => 'lib/Application',
            'Controller'  => 'src/controllers',
            'Menu'        => 'src/menus',
            'FPDF'        => 'lib/FPDF',
        ),
    ),
    'libraries' => array(
        'ZF1' => '../Zend/Zend/',
    ),
    'auth' => array(
        // these characters will be taken into consideration for auto-generated passwords
        'password_lookup' => 'abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789',
        'min_password_length' => 6,
        // which authorization class to use
        'scheme' => 'acl',
        'activation_mandatory' => 0,
        'password_hash_cost' => 12,
        'yubikey_enabled' => true,
        'allow_yubikey_single_factor' => true,
        'yubico_api_id' => '25315',
        'yubico_api_key' => 'w7Hv5NUZEMf18Da9s1N/mLT2iBc=',
    ),
    'action_param' => 'todo',
    'routes' => array(
        'home' => 'home',
        'login' => 'home',
        'logout' => 'home',
        'error' => 'common/error',
        'activate' => 'auth/activate',
        'deactivated' => 'auth/deactivated',
        'access_denied' => 'auth/members_only',
        'admin/home' => 'admin/home',
    ),
    'mail' => array(
        'from' => 'meldungen@scpallas.de',
        'from_name' => 'Online Meldesystem',
        'return_path' => 'webmaster@agentmulder.de',
        'mail_domain' => 'agentmulder.de',
        'admin_bcc' => 'webmaster@localhost',
        'adapter' => 'smtp',
        'smtp_options' => array(
            'host' => 'local.agentmulder.de',
            'auth' => true,
            'user' => 'webmaster@local.agentmulder.de',
            'pass' => 'password',
            'pass_method' => 'login',
            'port' => 587,
            'ssl' => 'tls',
        ),
    ),
    'view' => array(
        'empty_image' => 'spacer.gif',
        'items_per_page' => 250,
        'short_string_length' => 20,
    ),
    'i18n' => array(
        'translated_templates' => false,
    ),
);

return $conf;
