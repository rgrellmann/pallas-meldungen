<?php
namespace Framework;

define('PROJECT_IDENTIFIER', 'meldungen');

require_once('includes/constants.inc.php');

require_once('lib/framework/Conf.php');
Conf::init();

require_once('includes/autoload.php');

require_once('includes/exceptions.inc.php');

// load classes that are needed during session destruction and/or
// shutdown handling, when autoloading is no longer available
require_once('lib/framework/Common.php');
require_once('lib/framework/DateAndTime.php');
require_once('lib/framework/Logger.php');

/**
 * define a custom exception handler
 * @param \Exception $e
 */
function exception_handler($e) {
    echo $e->getMessage();
}
set_exception_handler('Framework\exception_handler');

/**
 * define a custom shutdown handler
 */
function shutdown_handler() {
	if (DEBUG) {
		try {
			DebugTools::save_debug_info();
		} catch (\Exception $e) {
			echo "/* Error during shutdown handling! */";
		}
    }
}
register_shutdown_function('Framework\shutdown_handler');

/**
 * define a custom error handler
 */
set_error_handler(array('Framework\Logger', 'custom_error_handler'), -1);

//   *****   DEBUG   *****

if (DEBUG) {
	DebugTools::init();
	// Hier wird die Klasse 'Debug' eingebunden, weil bei Ausführung des shutdown_handlers
	// das Autoloading nicht mehr funktioniert
	$dummy = new \Entity\Debug();
	unset($dummy);
}
