<?php
namespace Framework;

class ApplicationException extends \RuntimeException {
}

class RoleException extends \RuntimeException {
}

class DeprecatedException extends \LogicException {
    /**
     * @param string $msg
     */
    public function __construct($msg = "") {
    	if (DEBUG) {
    		Logger::get_log()->error($msg, $this);
    	}
       	parent::__construct($msg, 1);
    }
}

class SQLException extends \RuntimeException {
}

class DuplicateKeyEntryException extends SQLException {
}

class ForeignKeyConstraintException extends SQLException {
}

class NoSuchTableEntry extends SQLException {
}

class NoSuchPrimaryKeyException extends NoSuchTableEntry {
    /**
     * @param bool $log
     * @param mixed $pk
     * @param string $table
     */
    public function __construct($log = true, $pk = null, $table = null) {
    	if (DEBUG AND $log) {
    		Logger::get_log()->error("Found more than one or zero entries for the given primary key (".$table.', '.print_r($pk,1).")", $this);
    	}
       	parent::__construct('Found more than one or zero entries for the given primary key', 3000);
    }
}

class UploadException extends \RuntimeException {
}

class FileAccessException extends \RuntimeException {
}

class ControllerNotFoundException extends \Exception {
    /**
     * @param string $controller
     */
    public function __construct($controller) {
		parent::__construct('Controller '. $controller .' not found');
	}
}

class TemplateNotFoundException extends \Exception {
    /**
     * @param string $route
     */
    public function __construct($route) {
        /**
         *
         */
        parent::__construct('Template for Route '. $route .' not found');
	}
}
