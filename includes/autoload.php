<?php
namespace Framework;
/**
 * The Framework
 * @author Robert
 * @date 09.11.12
 */

// the Conf class cannot be autoloaded, because it is needed in the autoload function
/** @noinspection PhpIncludeInspection */
require_once('lib/framework/Conf.php');

/**
 * @param string $class
 * @return bool
 */
function autoload($class) {

    // do not try to autoload PHP_Invoker (phpunit)
    if ($class == 'PHP_Invoker') {
        return false;
    }

    $namespaces = Conf::get('paths/namespace_map', array());
    if ('\\' == $class[0]) {
        $class = substr($class, 1);
    }
    if (($pos = strrpos($class, '\\')) !== false) {
        // namespaced class name
        $class_path = str_replace('\\', DIRECTORY_SEPARATOR, substr($class, 0, $pos)) . DIRECTORY_SEPARATOR;
        $class_name = substr($class, $pos + 1);
    } else {
        // PEAR-like class name
        $class_path = '';
        $class_name = $class;
    }
    $class_path .= str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';

    foreach ($namespaces as $prefix => $dir) {
        if (strpos($class, $prefix) === 0) {
            if (file_exists($dir . DIRECTORY_SEPARATOR . str_replace($prefix, "", $class_path))) {
                /** @noinspection PhpIncludeInspection */
                include( $dir . DIRECTORY_SEPARATOR . str_replace($prefix, "", $class_path));
                return true;
            }
        }
    }

    // try include_path as last resort
    if (stream_resolve_include_path($class_path)) {
        /** @noinspection PhpIncludeInspection */
        require($class_path);
    } else {
        // do not force autoloading of classes, maybe class_exists() was used or other autoloaders follow
        return false;
    }

    return true;
}

spl_autoload_register('Framework\autoload');
