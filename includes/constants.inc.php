<?php
namespace Framework;

// Authorization scheme simple/advanced
// discrete: one single role is associated to each user, the roles are hierarchical
// db_table: the roles are defined in a database table and the roles for each user are stored in another database table
// numeric: the roles are stored directly in the users table as an integer using a binary system
// acl: the user is associated to one or more groups, each group has a set of rights
define('AUTHN_SCHEME', 'acl');

// Logging
define('PHPLOG_DEBUG', 	32);
define('PHPLOG_INFO', 	16);
define('PHPLOG_NOTICE', 8);
define('PHPLOG_WARN', 	4);
define('PHPLOG_ERROR', 	2);
define('PHPLOG_FATAL', 	1);
define('LOG_SQL', 		64);
define('PHPLOG_ALL', 	127);
define('LOG_SQL_INSERT',    1);
define('LOG_SQL_UPDATE',    2);
define('LOG_SQL_DELETE',    4);
define('LOG_SQL_SELECT',    8);
define('LOG_SQL_SHOW', 	   16);
define('LOG_SQL_CALL', 	   32);
define('LOG_SQL_CREATE',   64);
define('LOG_SQL_DROP', 	  128);
define('LOG_SQL_REPLACE', 256);
define('LOG_SQL_SET', 	  512);
define('LOG_SQL_ALL', 	 1023);

// Miscellaneous
define("PAGINATION_VARIABLE", "page");
define("STD_TABLE", ' border="0" cellpadding="0" cellspacing="0" ');
