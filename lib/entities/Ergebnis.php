<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;
use Framework\SQLException;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Ergebnisse Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_teilnehmer_id()
 * @method $this set_teilnehmer_id(int $value)
 * @method int get_wettkampf_id()
 * @method $this set_wettkampf_id(int $value)
 * @method string get_zeit_1()
 * @method $this set_zeit_1(string $value)
 * @method string get_zeit_2()
 * @method $this set_zeit_2(string $value)
 * @method string get_zeit_gesamt()
 * @method $this set_zeit_gesamt(string $value)
 * @method string get_diff()
 * @method $this set_diff(string $value)
 * @method int get_platz()
 * @method $this set_platz(int $value)
 * @method int get_platz_bm()
 * @method $this set_platz_bm(int $value)
 */
class ErgebnisseTable extends DBTable {

    /**
     * @param int $teilnehmer_id
     * @param int $wettkampf_id
     * @throws SQLException
     */
    public function __construct($teilnehmer_id = null, $wettkampf_id = null) {
        $this->table = TABLE_ERGEBNISSE;
        $this->pk = array("teilnehmer_id", "wettkampf_id");
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'teilnehmer_id' 	=> array('type' => 'numeric', 	'value' => false,   'label' => 'Teilnehmer'),
            'wettkampf_id' 		=> array('type' => 'numeric', 	'value' => false,   'label' => 'Wettkampf'),
            'zeit_1' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Zeit 1. Lauf'),
            'zeit_2' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Zeit 2. Lauf'),
            'zeit_gesamt' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Gesamtzeit'),
            'diff' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Differenz'),
            'platz' 			=> array('type' => 'numeric', 	'value' => false,   'label' => 'Platz'),
            'platz_bm' 			=> array('type' => 'numeric', 	'value' => false,   'label' => 'Platz BM'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($teilnehmer_id, $wettkampf_id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $teilnehmer_id
     * @param int $wettkampf_id
     * @return int
     */
    public static function exists($teilnehmer_id, $wettkampf_id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_ERGEBNISSE." WHERE `teilnehmer_id` = '$teilnehmer_id' AND `wettkampf_id` = '$wettkampf_id'");
    }

}


/**
 * Extensions to the generated ErgebnisseTable Class go here
 *
 */
class Ergebnis extends ErgebnisseTable {

    /**
     * @param int $id
     * @throws SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("teilnehmer_id", "wettkampf_id", "platz", "platz_bm", "zeit_1", "zit_2", "zeit_gesamt", "diff");
        parent::__construct($id);
    }

}
