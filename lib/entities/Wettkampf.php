<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Wettkaempfe Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method int get_veranstaltungs_id()
 * @method $this set_veranstaltungs_id(int $value)
 * @method string get_disziplin()
 * @method $this set_disziplin(string $value)
 * @method string get_titel1()
 * @method $this set_titel1(string $value)
 * @method string get_titel2()
 * @method $this set_titel2(string $value)
 * @method string get_organisator()
 * @method $this set_organisator(string $value)
 * @method string get_wettkampfleiter()
 * @method $this set_wettkampfleiter(string $value)
 * @method string get_schiedsrichter()
 * @method $this set_schiedsrichter(string $value)
 * @method string get_streckenchef()
 * @method $this set_streckenchef(string $value)
 * @method string get_startrichter()
 * @method $this set_startrichter(string $value)
 * @method string get_zielrichter()
 * @method $this set_zielrichter(string $value)
 * @method string get_zeitnahme()
 * @method $this set_zeitnahme(string $value)
 * @method string get_kurssetzer()
 * @method $this set_kurssetzer(string $value)
 * @method string get_datum()
 * @method $this set_datum(string $value)
 * @method string get_ort()
 * @method $this set_ort(string $value)
 * @method string get_anz_tore()
 * @method $this set_anz_tore(string $value)
 * @method string get_h_diff()
 * @method $this set_h_diff(string $value)
 * @method string get_s_laenge()
 * @method $this set_s_laenge(string $value)
 * @method string get_startzeit()
 * @method $this set_startzeit(string $value)
 * @method string get_temp()
 * @method $this set_temp(string $value)
 * @method string get_wetter()
 * @method $this set_wetter(string $value)
 */
class WettkaempfeTable extends DBTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_WETTKAEMPFE;
        $this->pk = array("id");
        $this->auto_increment = true;
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id' 				=> array('type' => 'numeric', 	'value' => false,   'label' => 'ID'),
            'veranstaltungs_id' => array('type' => 'numeric',   'value' => false,   'label' => 'Veranstaltung'),
            'disziplin' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Disziplin'),
            'titel1' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Titel 1'),
            'titel2' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Titel 2'),
            'organisator' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Organisator'),
            'wettkampfleiter' 	=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Wettkampfleiter'),
            'schiedsrichter' 	=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Schiedsrichter'),
            'streckenchef' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Streckenchef'),
            'startrichter' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Startrichter'),
            'zielrichter' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Zielrichter'),
            'zeitnahme' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Zeitnahme'),
            'kurssetzer' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Kurssetzer'),
            'datum' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Datum'),
            'ort' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Ort'),
            'anz_tore' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Anzahl Tore'),
            'h_diff' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Höhendifferenz'),
            's_laenge' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Streckenlänge'),
            'startzeit' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Startzeit'),
            'temp' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Temperatur'),
            'wetter' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Wetter'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_WETTKAEMPFE." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated WettkaempfeTable Class go here
 *
 */
class Wettkampf extends WettkaempfeTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("id", "veranstaltungs_id", "disziplin", "ort", "titel1", "titel2");
        parent::__construct($id);
    }

}
