<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;
use Framework\SQLException;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the AclRole Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_role_id()
 * @method $this set_role_id(int $value)
 * @method string get_role_name()
 * @method $this set_role_name(string $value)
 */
class AclRoleTable extends DBTable {

    /**
     * @param int $role_id
     * @throws SQLException
     */
    public function __construct($role_id = null) {
        $this->table = TABLE_ROLES;
        $this->pk = array("role_id");

        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'role_id'           => array('type' => 'numeric',   'value' => false,   'label' => 'ID'),
            'role_name'         => array('type' => 'NaN',       'value' => false,   'label' => 'Rolle'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($role_id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $role_id
     * @return int
     */
    public static function exists($role_id) {
        $db = Database::get_connection();

        return $db->count("SELECT 1 FROM ".TABLE_ROLES." WHERE `role_id` = '$role_id'");
    }

}


/**
 * Extensions to the generated AclRoleTable Class go here
 *
 */
class AclRole extends AclRoleTable {

    /**
     * @param int $id
     * @throws SQLException
     */
	public function __construct($id = null) {
		$this->search_fields = array('role_id', 'role_name');
		parent::__construct($id);
	}

}
