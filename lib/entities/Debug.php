<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;
use Framework\SQLException;

/**
 * This is a generated class. Do not edit it!
 * Use the Debug Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method string get_time()
 * @method $this set_time(string $value)
 * @method string get_route()
 * @method $this set_route(string $value)
 * @method int get_num_queries()
 * @method $this set_num_queries(int $value)
 * @method float get_total_query_time()
 * @method $this set_total_query_time(float $value)
 * @method int get_mem_usage()
 * @method $this set_mem_usage(int $value)
 * @method float get_rendertime()
 * @method $this set_rendertime(float $value)
 * @method float get_stopwatch()
 * @method $this set_stopwatch(float $value)
 * @method string get_message()
 * @method $this set_message(string $value)
 */
class DebugTable extends DBTable {

    /**
     * @param int $id
     * @throws SQLException
     */
    public function __construct($id = null) {
		$this->table = TABLE_DEBUG;
		$this->pk = array("id");
		$this->auto_increment = true;
		$this->columns = array(
			'id' 				=> array('type' => 'numeric', 	'value' => false, 'label' => 'ID'),
			'time' 				=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Zeitstempel'),
			'route' 			=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Route'),
			'num_queries' 		=> array('type' => 'numeric', 	'value' => false, 'label' => 'Anzahl DB-Abfragen'),
			'total_query_time' 	=> array('type' => 'numeric', 	'value' => false, 'label' => 'Gesamtdauer DB-Abfragen', 'unit' => 's'),
			'mem_usage' 		=> array('type' => 'numeric', 	'value' => false, 'label' => 'Speicherbelegung', 		'unit' => 'B'),
			'rendertime' 		=> array('type' => 'numeric', 	'value' => false, 'label' => 'Verarbeitungszeit', 		'unit' => 's'),
			'stopwatch' 		=> array('type' => 'numeric', 	'value' => false, 'label' => 'Stopuhr', 				'unit' => 's'),
			'message' 			=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Nachricht')
		);
		parent::__construct($id);
	}

	/**
	 * Prüft, ob der Eintrag in der Datenbank existiert
	 *
	 * @param int $id
	 * @return int
	 */
	public static function exists($id) {
		$db = Database::get_connection();
		return $db->count("SELECT 1 FROM ".TABLE_DEBUG." WHERE `id` = '$id'");
	}

}


/**
 * Extensions to the generated DebugTable Class go here
 *
 */
class Debug extends DebugTable {

    /**
     * @param int $id
     * @throws SQLException
     */
    public function __construct($id = null) {
		$this->search_fields = array("id", "time", "route", "message");
		parent::__construct($id);
	}

    /**
     * @param float $value
     */
    public function set_rendertime($value) {
		$this->columns['rendertime']['value'] = (float)$value;
		if ($this->columns['rendertime']['value'] < 0 OR $this->columns['rendertime']['value'] > 99999) {
			$this->columns['rendertime']['value'] = 0;
		}
	}

}
