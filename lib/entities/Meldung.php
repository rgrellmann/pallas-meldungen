<?php
namespace Entity;

use Framework\Common;
use Framework\DBTable;
use Framework\Database;
use Framework\DateAndTime;
use Framework\Request;
use Framework\SQLException;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Meldungen Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method int get_veranstaltungs_id()
 * @method $this set_veranstaltungs_id(int $value)
 * @method string get_name()
 * @method $this set_name(string $value)
 * @method string get_vorname()
 * @method $this set_vorname(string $value)
 * @method int get_jahrgang()
 * @method $this set_jahrgang(int $value)
 * @method string get_geschlecht()
 * @method $this set_geschlecht(string $value)
 * @method string get_verein()
 * @method $this set_verein(string $value)
 * @method string get_verband_1()
 * @method $this set_verband_1(string $value)
 * @method string get_verband()
 * @method $this set_verband(string $value)
 * @method string get_telefon()
 * @method $this set_telefon(string $value)
 * @method string get_handy()
 * @method $this set_handy(string $value)
 * @method string get_email()
 * @method $this set_email(string $value)
 * @method string get_kommentar()
 * @method $this set_kommentar(string $value)
 * @method string get_disziplin()
 * @method $this set_disziplin(string $value)
 * @method string get_datum()
 * @method $this set_datum(string $value)
 * @method string get_status()
 * @method $this set_status(string $value)
 * @method int get_gemeldet_von()
 * @method $this set_gemeldet_von(int $value)
 * @method string get_hash()
 * @method $this set_hash(string $value)
 * @method string get_startpass_nr()
 * @method $this set_startpass_nr(string $value)
 */
class MeldungenTable extends DBTable {

    /**
     * @param int $id
     * @throws SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_MELDUNGEN;
        $this->pk = array("id");
        $this->auto_increment = true;
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id' 				=> array('type' => 'numeric', 	'value' => false,   'label' => 'ID'),
            'veranstaltungs_id' => array('type' => 'numeric', 	'value' => false,   'label' => 'Veranstaltung'),
            'name' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name'),
            'vorname' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Vorname'),
            'jahrgang' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Jahrgang'),
            'geschlecht' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Geschlecht'),
            'verein' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Verein'),
            'verband_1' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Verband'),
            'verband' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Verband'),
            'telefon' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Telefon'),
            'handy' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Handy'),
            'email' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'E-Mail'),
            'kommentar' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Kommentar'),
            'disziplin' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Disziplinen'),
            'datum' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Datum'),
            'status' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Status'),
            'gemeldet_von' 		=> array('type' => 'numeric', 	'value' => false,   'label' => 'gemeldet von'),
            'hash' 			    => array('type' => 'NaN',	 	'value' => false,   'label' => 'Hash'),
            'startpass_nr' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Startpass-Nr'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_MELDUNGEN." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated MeldungenTable Class go here
 *
 */
class Meldung extends MeldungenTable {

    /**
     * @param int $id
     * @throws SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("id", "veranstaltungs_id", "name", "vorname", "jahrgang", "geschlecht", "verein", "verband", "telefon", "handy", "email", "kommentar", "status", "disziplin", "gemeldet_von", "startpass_nr");
        $this->not_editable_fields = array("veranstaltungs_id", 'datum', 'gemeldet_von', 'hash');
        parent::__construct($id);
    }

    /**
     * @throws \LogicException
     */
    public function generate_hash() {
        if (!$this->get_veranstaltungs_id()) {
            throw new \LogicException('Veranstaltung muss gesetzt sein, bevor ein Hash generiert werden kann');
        }
        $this->set_hash(hash('sha256', $this->get_veranstaltungs_id().microtime().mt_rand()));
    }

    /**
     * @param array $value
     */
    public function set_disziplinen(array $value) {
        $this->data['disziplinen'] = $value;
    }

    /**
     * @throws SQLException
     */
    public function fetch_disziplinen() {
        if (!$this->get_id()) {
            throw new \LogicException('Meldungs-ID muss gesetzt sein, um Diziplinen laden zu können');
        }
        $db = Database::get_connection();
        $sql = "SELECT veranstaltung_disziplin_id FROM ".TABLE_MELDUNGEN_DISZIPLINEN
            . " WHERE meldung_id = " . intval($this->get_id())
            . " ORDER BY veranstaltung_disziplin_id";
        $res = $db->query($sql);
        $disziplinen = Common::make_array($res, Common::FIRST_FIELD);
        $this->data['disziplinen'] = $disziplinen;
    }

    /**
     * @return array
     * @throws SQLException
     */
    public function get_disziplinen()
    {
        if (!array_key_exists('disziplinen', $this->data) AND $this->get('id')) {
            $this->fetch_disziplinen();
        }
        return $this->data['disziplinen'] ?? [];
    }

    /**
     * @throws SQLException
     */
    public function save_disziplinen() {
        if (!$this->get_id() OR !$this->get_veranstaltungs_id()) {
            throw new \LogicException('Meldungs- und Veranstaltungs-ID müssen gesetzt sein, um Diziplinen speichern zu können');
        }
        if (!array_key_exists('disziplinen', $this->data)) {
            throw new \LogicException('Disziplinen wurden nicht gesetzt, speichern nicht möglich');
        }

        $disziplinen_new = $this->data['disziplinen'];

        $db = Database::get_connection();
        $sql = "SELECT veranstaltung_disziplin_id FROM ".TABLE_MELDUNGEN_DISZIPLINEN
            . " WHERE meldung_id = " . $this->get_id()
            . " ORDER BY veranstaltung_disziplin_id";
        $res = $db->query($sql);
        $disziplinen_old = Common::make_array($res, Common::FIRST_FIELD);

        $to_delete = array_diff($disziplinen_old, $disziplinen_new);
        $to_add = array_diff($disziplinen_new, $disziplinen_old);

        if (count($to_delete)) {
            $sql = "DELETE FROM ".TABLE_MELDUNGEN_DISZIPLINEN." WHERE meldung_id = " . $this->get_id()
                . " AND veranstaltung_disziplin_id IN (". implode(",", $to_delete) .")";
            $db->query($sql);
        }
        if (count($to_add)) {
            $sql = "INSERT INTO ".TABLE_MELDUNGEN_DISZIPLINEN." (meldung_id,veranstaltung_disziplin_id) VALUES"
                . "(" . $this->get_id() . "," . implode("),(".$this->get_id().",", $to_add) . ")";
            $db->query($sql);
        }
    }

    /**
     * Wenn $value "Gast" ist, wird stattdessen der Wert von REQUEST['gast_verein'] gesetzt
     * @param string $value
     * @return $this
     */
    public function set_verein($value) {
        if ($value == "nix") {
            $value = '';
        }
        if ($value == "Gast") {
            $this->columns['verein']['value'] = Request::get("gast_verein");
        } else {
            $this->columns['verein']['value'] = $value;
        }
        return $this;
    }

    /**
     * @param string $value
     * @param bool $uppercase_words
     * @return $this
     */
    public function set_name($value, $uppercase_words = true) {
        if ($uppercase_words) {
            $value = ucwords($value, " -");
        }
        $this->set('name', $value);
        return $this;
    }

    /**
     * @param string $value
     * @param bool $uppercase_words
     * @return $this
     */
    public function set_vorname($value, $uppercase_words = true) {
        if ($uppercase_words) {
            $value = ucwords($value, " -");
        }
        $this->set('vorname', $value);
        return $this;
    }

    /**
     * @param string $context
     * @param string $prefix
     * @param string $suffix
     * @return bool
     * @throws SQLException
     * @throws \Exception
     */
    public function get_data_from_request($context = "REQUEST", $prefix = "", $suffix = "") {
        $valid = parent::get_data_from_request($context, $prefix, $suffix);
        if ($valid) {
            $veranstaltung = new Veranstaltung($this->get_veranstaltungs_id());
            if ($veranstaltung->has_disziplinen()) {
                $value = Request::get($prefix."disziplinen".$suffix, $context);
                if (!$this->validate_disziplinen($value)) {
                    return false;
                }
                $this->set_disziplinen($value);
            }
        }
        return $valid;
    }


    /**
     * @param string $value
     * @return bool
     */
    public function validate_name($value) {
        if (strlen($value) < 2 OR strlen($value) > 50) {
            $this->validation_errors['name'] = $this->columns['name']['label']." muss zwischen 2 und 50 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_vorname($value) {
        if (strlen($value) < 2 OR strlen($value) > 50) {
            $this->validation_errors['vorname'] = $this->columns['vorname']['label']." muss zwischen 2 und 50 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @param int $value
     * @return bool
     */
    public function validate_jahrgang($value) {
        if ($value < 1900 OR $value >= date('Y')) {
            $this->validation_errors['jahrgang'] = $this->columns['jahrgang']['label']." muss als vierstellige Zahl angegeben werden.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_geschlecht($value) {
        if ($value != 'm' AND $value != 'w') {
            $this->validation_errors['geschlecht'] = $this->columns['geschlecht']['label']." muss angegeben werden.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_telefon($value) {
        if (strlen($value) > 50) {
            $this->validation_errors['telefon'] = $this->columns['telefon']['label']." darf max. 50 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_handy($value) {
        if (strlen($value) > 50) {
            $this->validation_errors['handy'] = $this->columns['handy']['label']." darf max. 50 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_email($value) {
        if (strlen($value) > 100 OR (strlen($value) AND !preg_match('/^[a-zA-Z0-9].*@[a-zA-Z0-9].*\.[a-zA-Z0-9]+$/', $value))) {
            $this->validation_errors['email'] = $this->columns['email']['label']." muss eine gültige E-Mail-Adresse sein.";
            return false;
        }
        return true;
    }

    /**
     * @param array $value
     * @param Veranstaltung $veranstaltung
     * @return bool
     * @throws SQLException
     */
    public function validate_disziplinen($value, $veranstaltung = null) {
        if (!$veranstaltung instanceof Veranstaltung) {
            $veranstaltung = new Veranstaltung($this->get_veranstaltungs_id());
        }
        if ($v_disziplinen = $veranstaltung->get_disziplinen_array()) {
            if (empty($value)) {
                $this->validation_errors['disziplinen'] = "Es muss mindestens eine Disziplin gewählt werden.";
                return false;
            }
            // im Request kommen die Disziplinen-Keys als Array Values ⇒ flip
            $r_disziplinen = array_flip($value);
            $invalid_disziplinen = array_diff_ukey(
                $r_disziplinen,
                $v_disziplinen,
                function($a, $b) { return (strval($a) > strval($b) ? 1 : (strval($a) < strval($b) ? -1 : 0 )); }
            );
            if (count($invalid_disziplinen)) {
                $this->validation_errors['disziplinen'] = "Es wurde eine ungültige Disziplin gewählt.";
            }
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_kommentar($value) {
        if (strlen($value) > 255) {
            $this->validation_errors['kommentar'] = $this->columns['kommentar']['label']." darf max. 255 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @param bool $mandatory
     * @return bool
     */
    public function validate_startpass_nr($value, $mandatory = false) {
        if (strlen($value) > 50) {
            $this->validation_errors['startpass_nr'] = $this->columns['startpass_nr']['label']." darf max. 50 Zeichen lang sein.";
            return false;
        }
        if ($mandatory AND strlen($value) == 0) {
            $this->validation_errors['startpass_nr'] = $this->columns['startpass_nr']['label']." muss angegeben werden.";
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function check_integrity() {
        if (empty($this->columns['verein']['value'])) {
            $this->validation_errors['verein'] = "Sie müssen Ihren Verein aus der Liste wählen oder in das Textfeld eintragen.";
            return false;
        }
        if ($this->columns['gemeldet_von']['value'] > 0 AND $this->columns['status']['value'] != 'mafue') {
            $this->validation_errors['status'] = $this->columns['status']['label']." hat einen ungültigen Wert";
            return false;
        }
        return $this->check_contact_data_integrity();
    }

    /**
     * @return bool
     */
    public function check_contact_data_integrity() {
        $valid = true;
        if ($this->columns['gemeldet_von']['value'] == 0 AND empty($this->columns['telefon']['value']) AND empty($this->columns['handy']['value']) AND empty($this->columns['email']['value'])) {
            $this->validation_errors['email'] = 'Sie müssen mindestens eine Telefonnummer oder eine E-Mail-Adresse eintragen.';
            $valid = false;
        }
        return $valid;
    }

    /**
     * @param int $veranstaltungs_id
     * @throws \Exception
     */
    public function check_gemeldet($veranstaltungs_id) {
        $db = Database::get_connection();
        $sql = "SELECT created FROM ".TABLE_MELDUNGEN." WHERE (veranstaltungs_id = '".$veranstaltungs_id."'"
            . " AND name = '".$this->get('name')."' AND vorname = '".$this->get('vorname') . "'"
            . " AND jahrgang = '".$this->get('jahrgang')."' AND geschlecht = '".$this->get('geschlecht') . "'"
            . " AND verein = '".$this->get('verein')."' AND status = 'gemeldet')";
        $res = $db->query($sql);
        if ($res->num_rows() >= 1) {
            $row = $res->fetch();
            $melde_datum = DateAndTime::format_date($row["created"]);
            throw new \Exception($this->get('vorname')." ".$this->get('name')." (JG ".$this->get('jahrgang').") vom "
                . $this->get('verein')." <br> wurde bereits am $melde_datum f&uuml;r diese Veranstaltung gemeldet.<br>\n");
        }
    }

}
