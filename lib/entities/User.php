<?php
namespace Entity;

use Framework\Auth;
use Framework\DBTable;
use Framework\Database;
use Framework\Common;
use Framework\MySQLiResult;

/**
 * This is a generated class. Do not edit it!
 * Use the User Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_user_id()
 * @method $this set_user_id(int $value)
 * @method string get_username()
 * @method $this set_username(string $value)
 * @method string get_email()
 * @method $this set_email(string $value)
 * @method string get_first_name()
 * @method $this set_first_name(string $value)
 * @method string get_last_name()
 * @method $this set_last_name(string $value)
 * @method string get_verein_id()
 * @method $this set_verein_id(int $value)
 * @method int get_is_online()
 * @method $this set_is_online(int $value)
 * @method string get_last_login()
 * @method $this set_last_login(string $value)
 * @method string get_created()
 * @method $this set_created(string $value)
 * @method string get_last_change()
 * @method $this set_last_change(string $value)
 * @method string get_password()
 * @method $this set_password(string $value)
 * @method string get_yubikey()
 * @method $this set_yubikey(string $value)
 * @method int get_active()
 * @method $this set_active(int $value)
 */
class UserTable extends DBTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
	public function __construct($id = null) {
		$this->table = TABLE_USERS;
		$this->pk = array("user_id");
		$this->auto_increment = true;
		$this->columns = array(
			'user_id' 			=> array('type' => 'numeric', 	'value' => false, 'label' => 'ID'),
			'username' 			=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Benutzername'),
			'email' 			=> array('type' => 'NaN',	 	'value' => false, 'label' => 'E-Mail'),
			'first_name' 		=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Vorname'),
			'last_name' 		=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Nachname'),
			'verein_id' 	    => array('type' => 'NaN',	 	'value' => false, 'label' => 'Verein'),
			'is_online' 		=> array('type' => 'numeric', 	'value' => false, 'label' => 'ist online'),
			'last_login' 		=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Letzter Login'),
			'created' 			=> array('type' => 'NaN', 		'value' => false, 'label' => 'erstellt'),
			'last_change' 		=> array('type' => 'NaN',	 	'value' => false, 'label' => 'letzte Änderung'),
			'password' 			=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Passwort'),
			'yubikey_id' 		=> array('type' => 'NaN',	 	'value' => false, 'label' => 'Yubikey-ID'),
			'active' 			=> array('type' => 'numeric', 	'value' => false, 'label' => 'Aktiv'),
		);
		parent::__construct($id);
	}

	/**
	 * checks if an entry with the given primary key exists in the database
	 *
	 * @param int $id
	 * @return int
	 */
	public static function exists($id) {
		$db = Database::get_connection();
        /** @noinspection SqlResolve */
		return $db->count("SELECT 1 FROM `".TABLE_USERS."` WHERE `user_id` = '$id'");
	}

}


/**
 * Extensions to the generated UserTable Class go here
 *
 */
class User extends UserTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
	public function __construct($id = null) {
		$this->search_fields = array("user_id", "created", "last_login", "is_online", "username", "first_name", "last_name", "email", "active");
		parent::__construct($id);
	}

    /**
     * @return int
     */
    public function get_id() {
		return $this->get("user_id");
	}

    /**
     * @param int $id
     */
    public function set_id($id) {
		$this->set("user_id", $id);
	}

    /**
     * @return bool|MySQLiResult
     * @throws \Framework\SQLException
     */
    public function update_online() {
		$db = Database::get_connection();
        /** @noinspection SqlResolve */
		$sql = "UPDATE $this->table SET is_online = '".intval($this->get('is_online'))."', last_login = NOW() ".
		       "WHERE user_id = ". intval($this->get_id());
		return $db->query($sql);
	}

    /**
     * @param mixed $pk
     * @param bool $retrieve_roles
     * @return bool
     * @throws \Framework\SQLException
     */
    public function retrieve_by_pk($pk = false, $retrieve_roles = false) {
		$success = parent::retrieve_by_pk($pk);
		if ($retrieve_roles AND $this->get_id()) {
			$this->retrieve_roles();
		}
        return $success;
	}

    /**
     * @return array
     * @throws \Framework\SQLException
     */
    public function retrieve_roles() {
		if (!$this->get_id()) {
			return array();
		}
		$db = Database::get_connection();
        /** @noinspection SqlResolve */
        $res = $db->query("SELECT role_id FROM ".TABLE_USER_ROLES." WHERE user_id=".$this->get_id());
		$this->set("roles", Common::make_array($res, 0));
		return $this->get_roles();
	}

    /**
     * returns the user's roles as array
     * this allows the roles to be set as string using GROUP_CONCAT
     * @return array
     */
    public function get_roles() {
	    $roles = $this->get('roles');
	    if (is_array($roles)) {
	        return $roles;
	    } elseif (is_string($roles)) {
	        return explode(',', $roles);
	    } else {
	        return array();
	    }
	}

    /**
     * @param string $type
     * @return string
     */
    public function get_verein($type = 'verein_20') {
        $db = Database::get_connection();
        if (!$this->get_verein_id()) {
            return '';
        }
        $res = $db->select_one_field("SELECT * FROM ".TABLE_VEREINE." WHERE id = ".$this->get_verein_id());
        return strval($res[$type]);
    }

    /**
     * @param int $verband
     * @return array
     * @throws \Framework\SQLException
     */
    public function get_vereine_array($verband = 21) {
        $db = Database::get_connection();
        $res = $db->query("SELECT id,verein_25 FROM ".TABLE_VEREINE." WHERE code_verband = $verband");
        return Common::make_array($res, Common::FIRST_KEY_SECOND_VALUE);
    }

    /**
     * @param string $verein
     * @return bool
     */
    public function registration_change_allowed($verein = "") {
        if (Auth::is_allowed('actions/change_registration_status')) {
            return true;
        } elseif (Auth::is_allowed('actions/change_registration_status_if_club_match') AND $verein == $this->get_verein()) {
            return true;
        } else {
            // @todo: check if the current user is the registered one or the one who registered the given user
        }
        return false;
    }

}
