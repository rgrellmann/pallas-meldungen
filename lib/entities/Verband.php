<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Verbaende Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method string get_code()
 * @method $this set_code(string $value)
 * @method string get_sportart()
 * @method $this set_sportart(string $value)
 * @method string get_name_5()
 * @method $this set_name_5(string $value)
 * @method string get_name()
 * @method $this set_name(string $value)
 */
class VerbaendeTable extends DBTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_VERBAENDE;
        $this->pk = array("id");
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id' 				=> array('type' => 'numeric', 	'value' => false,   'label' => 'ID'),
            'code' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Code'),
            'sportart' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Sportart'),
            'name_5' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name (5 Zeichen)'),
            'name' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_VERBAENDE." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated VerbaendeTable Class go here
 *
 */
class Verband extends VerbaendeTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("id", "sportart", "code", "name_5", "name");
        parent::__construct($id);
    }

}
