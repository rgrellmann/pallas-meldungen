<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;
use Framework\SQLException;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the M_Disziplinen Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method string get_sportart()
 * @method $this set_sportart(string $value)
 * @method string get_name()
 * @method $this set_name(string $value)
 * @method string get_kurzname()
 * @method $this set_kurzname(string $value)
 * @method string get_last_change()
 * @method $this set_last_change(string $value)
 */
class DisziplinenTable extends DBTable {

    /**
     * @param $id
     * @throws SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_DISZIPLINEN;
        $this->pk = array("id");

        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id'                => array('type' => 'numeric',   'value' => false,   'label' => 'ID'),
            'sportart'          => array('type' => 'NaN',       'value' => false,   'label' => 'Sportart'),
            'name'              => array('type' => 'NaN',       'value' => false,   'label' => 'Name'),
            'kurzname'          => array('type' => 'NaN',       'value' => false,   'label' => 'Abkürzung'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();

        return $db->count("SELECT 1 FROM ".TABLE_DISZIPLINEN." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated M_DisziplinenTable Class go here
 *
 */
class Disziplin extends DisziplinenTable {

    /**
     * @param int $id
     * @throws SQLException
     */
	public function __construct($id = null) {
		$this->search_fields = array();
		parent::__construct($id);
	}

}
