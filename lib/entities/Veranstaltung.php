<?php
namespace Entity;

use Framework\Common;
use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Veranstaltungen Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method string get_bezeichnung()
 * @method $this set_bezeichnung(string $value)
 * @method string get_bezeichnung_kurz()
 * @method $this set_bezeichnung_kurz(string $value)
 * @method string get_ort()
 * @method $this set_ort(string $value)
 * @method string get_datum()
 * @method $this set_datum(string $value)
 * @method string get_url_ausschreibung()
 * @method $this set_url_ausschreibung(string $value)
 * @method string get_meldeschluss()
 * @method $this set_meldeschluss(string $value)
 * @method string get_meldebeginn()
 * @method $this set_meldebeginn(string $value)
 * @method string get_disziplinen()
 * @method $this set_disziplinen(string $value)
 * @method string get_hinweise()
 * @method $this set_hinweise(string $value)
 * @method string get_sportart()
 * @method $this set_sportart(string $value)
 * @method string get_startpass_erforderlich()
 * @method $this set_startpass_erforderlich(string $value)
 */
class VeranstaltungenTable extends DBTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_VERANSTALTUNGEN;
        $this->pk = array("id");
        $this->auto_increment = true;
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id' 				=> array('type' => 'numeric', 	'value' => false,   'label' => 'ID'),
            'bezeichnung' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name'),
            'bezeichnung_kurz' 	=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name (kurz)'),
            'ort' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Ort'),
            'datum' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Datum'),
            'url_ausschreibung' => array('type' => 'NaN',	 	'value' => false,   'label' => 'Ausschreibung (URL)'),
            'meldeschluss' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Meldeschluss'),
            'meldebeginn' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Meldebeginn'),
            'disziplinen' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Diziplinen'),
            'hinweise' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Hinweise'),
            'startpass_erforderlich' => array('type' => 'NaN',	'value' => false,   'label' => 'Startpass erforderlich'),
            'sportart'          => array('type' => 'NaN',	    'value' => false,   'label' => 'Sportart'),
            'last_change'       => array('type' => 'NaN',	    'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_VERANSTALTUNGEN." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated VeranstaltungenTable Class go here
 *
 */
class Veranstaltung extends VeranstaltungenTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("id", "bezeichnung", "bezeichnung_kurz", "ort", "datum", "disziplinen", "hinweise");
        $this->form_settable_fields = array("bezeichnung", "bezeichnung_kurz", "ort", "datum", 'url_ausschreibung', "meldebeginn", "meldeschluss", "disziplinen", "hinweise", "startpass_erforderlich");
        parent::__construct($id);
    }

    /**
     * @throws \Framework\SQLException
     */
    public function fetch_disziplinen() {
        if (!$this->get_id()) {
            throw new \LogicException('Veranstaltungs-ID muss gesetzt sein, um Diziplinen laden zu können');
        }
        $db = Database::get_connection();
        $sql = "SELECT vd.*,d.name,d.kurzname FROM ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." AS vd INNER JOIN ".TABLE_DISZIPLINEN." AS d"
            . " ON d.id = vd.disziplin_id"
            . " WHERE veranstaltungs_id = " . intval($this->get_id())
            . " ORDER BY vd.id";
        $res = $db->query($sql);
        $disziplinen = Common::make_array($res, '\Entity\VeranstaltungDisziplin', '', Common::LOAD_ALL_VALUES);
        $this->data['disziplinen'] = $disziplinen;
    }

    /**
     * @return VeranstaltungDisziplin[]
     * @throws \Framework\SQLException
     */
    public function get_disziplinen()
    {
        if (!array_key_exists('disziplinen', $this->data)) {
            if ($this->get('id')) {
                $this->fetch_disziplinen();
            } else {
                return [];
            }
        }
        return $this->data['disziplinen'] ?? [];
    }

    /**
     * for input elements, key: id, value: name
     * @return array
     * @throws \Framework\SQLException
     */
    public function get_disziplinen_array() {
        $out = [];
        $disziplinen = $this->get_disziplinen();
        foreach ($disziplinen as $disziplin) {
            $out[$disziplin->get_id()] = $disziplin->get('name');
        }
        return $out;
    }

    /**
     * @return bool
     * @throws \Framework\SQLException
     */
    public function has_disziplinen() {
        $disziplinen = $this->get_disziplinen();
        if (count($disziplinen)) {
            return true;
        }
        return false;
    }

}
