<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Vereine Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_id()
 * @method $this set_id(int $value)
 * @method string get_sportart()
 * @method $this set_sportart(string $value)
 * @method int get_code_verband()
 * @method $this set_code_verband(int $value)
 * @method string get_code()
 * @method $this set_code(string $value)
 * @method string get_verein_kurz()
 * @method $this set_verein_kurz(string $value)
 * @method string get_verein_20()
 * @method $this set_verein_20(string $value)
 * @method string get_verein_25()
 * @method $this set_verein_25(string $value)
 * @method string get_verein()
 * @method $this set_verein(string $value)
 */
class VereineTable extends DBTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_VEREINE;
        $this->pk = array("id");
        $this->auto_increment = true;
        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id' 				=> array('type' => 'numeric', 	'value' => false,   'label' => 'ID'),
            'sportart' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Sportart'),
            'code_verband' 		=> array('type' => 'numeric', 	'value' => false,   'label' => 'Code Verband'),
            'code' 				=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Code'),
            'verein_kurz' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name (kurz)'),
            'verein_20' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name (20 Zeichen)'),
            'verein_25' 		=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name (25 Zeichen)'),
            'verein' 			=> array('type' => 'NaN',	 	'value' => false,   'label' => 'Name'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_VEREINE." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated VereineTable Class go here
 *
 */
class Verein extends VereineTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->search_fields = array("id", "sportart", "code_verband", "code", "verein", "verein_kurz");
        parent::__construct($id);
    }

}
