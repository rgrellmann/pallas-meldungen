<?php
namespace Entity;

use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the Veranstaltungen_Disziplinen Class at the lower end of this file to implement extensions or overwrite code.
 * Methods available via magic __call():
 * @method int get_veranstaltungs_id()
 * @method $this set_veranstaltungs_id(int $value)
 * @method int get_disziplin_id()
 * @method $this set_disziplin_id(int $value)
 * @method int get_juengster_jg()
 * @method $this set_juengster_jg(int $value)
 * @method int get_aeltester_jg()
 * @method $this set_aeltester_jg(int $value)
 * @method string get_bemerkungen()
 * @method $this set_bemerkungen(string $value)
 * @method string get_last_change()
 * @method $this set_last_change(string $value)
 */
class Veranstaltungen_DisziplinenTable extends DBTable {

    /**
     * @param $id
     * @throws \Framework\SQLException
     */
    public function __construct($id = null) {
        $this->table = TABLE_VERANSTALTUNGEN_DISZIPLINEN;
        $this->pk = array("id");

        // you can add "label" and "unit" elements to any columns where it is applicable
        $this->columns = array(
            'id'                => array('type' => 'numeric',   'value' => false,   'label' => 'ID'),
            'veranstaltungs_id' => array('type' => 'numeric',   'value' => false,   'label' => 'Veranstaltung'),
            'disziplin_id'      => array('type' => 'numeric',   'value' => false,   'label' => 'Disziplin'),
            'juengster_jg'      => array('type' => 'numeric',   'value' => false,   'label' => 'jüngster Jahrgang'),
            'aeltester_jg'      => array('type' => 'numeric',   'value' => false,   'label' => 'ältester Jahrgang'),
            'bemerkungen'       => array('type' => 'NaN',       'value' => false,   'label' => 'Bemerkungen'),
            'last_change'       => array('type' => 'NaN',       'value' => false,   'label' => 'letzte Änderung'),
            'created'           => array('type' => 'NaN',	    'value' => false,   'label' => 'angelegt')
        );
        parent::__construct($id);
    }

    /**
     * checks if an entry with the given primary key exists in the database
     *
     * @param int $id
     * @return int
     */
    public static function exists($id) {
        $db = Database::get_connection();
        return $db->count("SELECT 1 FROM ".TABLE_VERANSTALTUNGEN_DISZIPLINEN." WHERE `id` = '$id'");
    }

}


/**
 * Extensions to the generated Veranstaltungen_DisziplinenTable Class go here
 *
 */
class VeranstaltungDisziplin extends Veranstaltungen_DisziplinenTable {

    /**
     * @param int $id
     * @throws \Framework\SQLException
     */
	public function __construct($id = null) {
		$this->search_fields = array();
		parent::__construct($id);
	}

    /**
     * @param int $value
     * @return bool
     */
	public function validate_juengster_jg($value) {
        if ($value < 1900 OR $value >= date('Y')) {
            $this->validation_errors['juengster_jg'] = $this->columns['juengster_jg']['label']." muss als vierstellige Zahl angegeben werden.";
            return false;
        }
        return true;
    }

    /**
     * @param int $value
     * @return bool
     */
	public function validate_aeltester_jg($value) {
        if ($value < 1900 OR $value >= date('Y')) {
            $this->validation_errors['aeltester_jg'] = $this->columns['aeltester_jg']['label']." muss als vierstellige Zahl angegeben werden.";
            return false;
        }
        return true;
    }

    /**
     * @param string $value
     * @return bool
     */
    public function validate_bemerkungen($value) {
        if (strlen($value) > 255) {
            $this->validation_errors['bemerkungen'] = $this->columns['bemerkungen']['label']." darf max. 255 Zeichen lang sein.";
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
	public function get_hinweise() {
	    $out = [];
	    if ($this->get_juengster_jg() AND $this->get_aeltester_jg()) {
	        $out[] = "JG " . $this->get_juengster_jg() . " bis " . $this->get_aeltester_jg() . "";
        } elseif ($this->get_aeltester_jg()) {
            $out[] = "JG " . $this->get_aeltester_jg() . " und jünger";
        } elseif ($this->get_juengster_jg()) {
            $out[] = "JG " . $this->get_juengster_jg() . " und älter";
        }
        if ($this->get('bemerkungen')) {
	        $out[] = $this->get('bemerkungen');
        }
        return $out ? "(" . implode(", ", $out) . ")" : "";
    }

}
