<?php
namespace Application;

class Ergebnisliste {

	/**
	 * converts a time in 00:00:00,00 format to seconds
	 *
	 * @param string $zeit
	 * @return float
	 */
	public static function time2sec(string $zeit) {
		$zeit = trim($zeit);
		if (substr_count($zeit,":") == 2) {
			$stunden = strtok($zeit,":");
			$minuten = strtok(":");
			$sekunden = strtok(",");
			$hundertstel = "0.".strtok(",");
		}
		else {
			$stunden = 0;
			if (strpos($zeit,":")) {
				$minuten = strtok($zeit,":");
				$sekunden = strtok(",");
				$hundertstel = "0.".strtok(",");
			}
			else {
				$minuten = 0;
				$sekunden = str_replace(",",".",$zeit);
				$hundertstel = 0;
			}
		}
		$zeit = $stunden * 3600 + $minuten * 60 + $sekunden + $hundertstel;
		return $zeit;
	}

	/**
	 * calculates the difference between to times in 00:00:00,00 format
	 * and returns either seconds, percent or points
	 *
	 * @param string $bestzeit
	 * @param string $zeit
	 * @param string $modus
	 * @param string $disziplin
	 * @return string
	 */
	public static function zeit_differenz(string $bestzeit, string $zeit, string $modus="sekunden", string $disziplin=""): string {
		$genauigkeit = strlen(substr(strstr($zeit,","),1));
		$bestzeit = self::time2sec($bestzeit);
		$zeit = self::time2sec($zeit);
		switch ($modus) {
			// Zeitdifferenz in Sekunden
			case "sekunden":
				$diff = $zeit - $bestzeit;
				if ($diff > 60) {
					$minuten = floor($diff / 60).":";
					$diff = $diff - $minuten * 60;
					$sekunden = number_format($diff,$genauigkeit,",",".");
					if ($diff < 10) { $sekunden = "0".$sekunden; }
				}
				else {
					$minuten = "";
					$sekunden = number_format($diff,$genauigkeit,",",".");
				}
				$diff = "+".$minuten.$sekunden;
				break;
			// Zeitdifferenz in Prozent
			case "prozent":
				$diff = (($zeit / $bestzeit) - 1) * 100;
				$diff = "+".number_format($diff,2,",",".");
				break;
			// Zeitdifferenz in Punkten (DSV)
			case "punkte":
				switch ($disziplin) {
					case "sl": $faktor =  600; break;
					case "rs": $faktor =  870; break;
					case "sg": $faktor = 1050; break;
					case "af": $faktor = 1320; break;
					default:   $faktor = 1000;
				}
				$diff = (($zeit / $bestzeit) - 1) * $faktor;
				$diff = number_format($diff,2,",",".");
				break;
			default:
				$diff = "+0,00";
		}
	 	return "&nbsp;".$diff;
	}

	/**
	 * finds the positions of various fields in an array
	 *
	 * @param array $daten
	 * @return array
	 */
	public static function felder_suchen(array $daten): array {
		$pos = array();
		$pos["platz"] = array_search("Rg.", $daten);
		$pos["stnr"] = array_search("Nr.", $daten);
		$pos["jahrg"] = array_search("Jg.", $daten);
		$pos["name"] = array_search("Name", $daten);
		$pos["verein"] = array_search("Verein", $daten);
		$pos["verband"] = array_search("Verb.", $daten);
		$pos["lauf_1"] = array_search("1. Lauf", $daten);
		$pos["lauf_2"] = array_search("2. Lauf", $daten);
		if (!$pos["gesamtzeit"] = array_search("Gesamt", $daten)) {
			$pos["gesamtzeit"] = array_search("Gesamtzeit", $daten);
		}
		return $pos;
	}

	/**
	 * returns the head section of the html document
	 *
	 * @param string $title
	 * @param int $cols
	 * @return string
	 */
	public static function document_header(string $title, int $cols = 11): string {
		// header ausgeben
		$out = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n<html lang=\"de\">
		<head>
			<title>Ergebnisliste $title</title>
			<style type=\"text/css\">\n
				body			{ font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 9pt; background-color: rgb(245,245,245); }
				td				{ font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 9pt; }
				div.header-container { width: ".($cols==11?"700":"600")."px; text-align: center; }
				table.header	{ width: 600px; margin: auto; }
				td.header		{ font-size: 18pt; font-weight: bold; text-decoration: underline; text-align: center; }
				td.header1		{ font-size: 15pt; font-weight: bold; text-decoration: underline; text-align: center; }
				td.header2		{ font-size: 12pt; font-weight: bold; text-decoration: underline; text-align: center; }
				td.header-left	{ font-size: 9pt; font-weight: bold; text-align: left; }
				td.header-center { font-size: 9pt; font-weight: bold; text-align: center; }
				td.header-right { font-size: 9pt; font-weight: bold; text-align: right; }
				td.data-right	{ font-size: 9pt; text-align: right; }
				td.data-center	{ font-size: 9pt; text-align: center; }
				td.data-left	{ font-size: 9pt; text-align: left; }
				td.standard		{ font-size: 9pt; }
				table.bm-result { table-layout: fixed; width: ".($cols==11?"704":"594")."px; }
				table.bm-result td { overflow: hidden; white-space: nowrap; padding: 1px 0 1px 0; }
				table.bm-result td.platz		{ width: 25px; }
				table.bm-result td.platz-bm		{ width: 25px; }
				table.bm-result td.startnr		{ width: 45px; }
				table.bm-result td.name			{ width: 145px; }
				table.bm-result td.jahrgang		{ width: 44px; }
				table.bm-result td.verein		{ width: 140px; }
				table.bm-result td.verband		{ width: 60px; }
				table.bm-result td.lauf-1		{ width: 55px; }
				table.bm-result td.lauf-2		{ width: 55px; }
				table.bm-result td.gesamt		{ width: 55px; }
				table.bm-result td.diff			{ width: 55px; }
				@media print {
					table.bm-result { width: 174mm; }
					table.bm-result td.platz		{ width: 7mm; }
					table.bm-result td.platz-bm		{ width: 7mm; }
					table.bm-result td.startnr		{ width: 12mm; }
					table.bm-result td.name			{ width: 35mm; }
					table.bm-result td.jahrgang		{ width: 11mm; }
					table.bm-result td.verein		{ width: 32mm; }
					table.bm-result td.verband		{ width: 14mm; }
					table.bm-result td.lauf-1		{ width: 14mm; }
					table.bm-result td.lauf-2		{ width: 14mm; }
					table.bm-result td.gesamt		{ width: 14mm; }
					table.bm-result td.diff			{ width: 14mm; }
				}
			</style>
		</head>\n";
		$out.= "<body>\n";
		return $out;
	}

	/**
	 * returns the top section of the result list
	 *
	 * @param array $data
	 * @return string
	 */
	public static function list_header(array $data): string {
		$out = "<div class=\"header-container\">\n";
		$out.= "<table class=\"header\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
		if (!empty($data["titel1"])) 			{ $out.= "<tr><td class=\"header\" colspan=\"3\"> ".htmlentities($data['titel1'])." </td></tr>\n"; }
		if (!empty($data["titel2"])) 			{ $out.= "<tr><td class=\"header1\" colspan=\"3\"> ".htmlentities($data['titel2'])." </td></tr>\n"; }
		$out.= "<tr><td valign=\"top\">\n\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
		if (!empty($data["organisator"]))		{ $out.= "\t<tr><td class=\"data-left\"> Organisator: </td><td class=\"data-left\"> ".htmlentities($data['organisator'])." </td>\n"; }
		if (!empty($data["wettkampfleiter"]))	{ $out.= "\t<tr><td class=\"data-left\"> Wettkampfleiter: &nbsp; </td><td class=\"data-left\"> ".htmlentities($data['wettkampfleiter'])." </td>\n"; }
		if (!empty($data["schiedsrichter"]))	{ $out.= "\t<tr><td class=\"data-left\"> Schiedsrichter: </td><td class=\"data-left\"> ".htmlentities($data['schiedsrichter'])." </td>\n"; }
		if (!empty($data["streckenchef"]))		{ $out.= "\t<tr><td class=\"data-left\"> Streckenchef: </td><td class=\"data-left\"> ".htmlentities($data['streckenchef'])." </td>\n"; }
		if (!empty($data["startrichter"]))		{ $out.= "\t<tr><td class=\"data-left\"> Startrichter: </td><td class=\"data-left\"> ".htmlentities($data['startrichter'])." </td>\n"; }
		if (!empty($data["zielrichter"]))		{ $out.= "\t<tr><td class=\"data-left\"> Zielrichter: </td><td class=\"data-left\"> ".htmlentities($data['zielrichter'])." </td>\n"; }
		if (!empty($data["zeitnahme"]))			{ $out.= "\t<tr><td class=\"data-left\"> Zeitnahme: </td><td class=\"data-left\"> ".htmlentities($data['zeitnahme'])." </td>\n"; }
		if (!empty($data["kurssetzer"]))		{ $out.= "\t<tr><td class=\"data-left\"> Kurssetzer: </td><td class=\"data-left\"> ".htmlentities($data['kurssetzer'])." </td>\n"; }
		$out.= "\t</table>\n</td><td>&nbsp;</td><td valign=\"top\">\n\t<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n";
		if (!empty($data["datum"]))	 			{ $out.= "\t<tr><td class=\"data-left\"> Datum: </td><td class=\"data-left\"> ".htmlentities($data['datum'])." </td></tr>\n"; }
		if (!empty($data["ort"]))				{ $out.= "\t<tr><td class=\"data-left\"> Ort: </td><td class=\"data-left\"> ".htmlentities($data['ort'])." </td>\n"; }
		if (!empty($data["anz_tore"]))			{ $out.= "\t<tr><td class=\"data-left\"> Anzahl Tore: </td><td class=\"data-left\"> ".htmlentities($data['anz_tore'])." </td></tr>\n"; }
		if (!empty($data["h_diff"]))			{ $out.= "\t<tr><td class=\"data-left\"> H&ouml;hendifferenz: </td><td class=\"data-left\"> ".htmlentities($data['h_diff'])." </td>\n"; }
		if (!empty($data["s_laenge"]))			{ $out.= "\t<tr><td class=\"data-left\"> Streckenl&auml;nge: </td><td class=\"data-left\"> ".htmlentities($data['s_laenge'])." </td></tr>\n"; }
		if (!empty($data["startzeit"]))			{ $out.= "\t<tr><td class=\"data-left\"> Startzeit: </td><td class=\"data-left\"> ".htmlentities($data['startzeit'])."</td>\n"; }
		if (!empty($data["temp"]))				{ $out.= "\t<tr><td class=\"data-left\"> Temperatur Start/Ziel: </td><td class=\"data-left\"> ".htmlentities($data['temp'])." </td>\n"; }
		if (!empty($data["wetter"]))			{ $out.= "\t<tr><td class=\"data-left\"> Wetter/Schnee: </td><td class=\"data-left\"> ".htmlentities($data['wetter'])." / ".htmlentities($data['schnee'])." </td>\n"; }
		$out.= "\t</table>\n</td></tr>\n<tr><td colspan=\"3\">&nbsp;</td></tr>\n</table>\n</div>\n";
		return $out;
	}

	/**
	 *
	 *
	 * @param string $title
	 * @return string
	 */
	public static function determine_discipline(string $title): string {
		switch ($title) {
		  case "Super-G": $disziplin = "sg"; break;
		  case "Slalom": $disziplin = "sl"; break;
		  case "Riesenslalom": case "Riesentorlauf": $disziplin = "rs"; break;
		  case "kurze Strecke - klassisch": $disziplin = "kurz_kl"; break;
		  case "Sprint - freie Technik": $disziplin = "sprint_ft"; break;
		  case "Staffel - klassisch / klassisch / freie Technik": $disziplin = "staffel"; break;
		  default: $disziplin = "";
		}
		return $disziplin;
	}

	/**
	 * returns the top section of the list table
	 *
	 * @param string $disziplin
	 * @param int $lauf_1
	 * @return string
	 */
	public static function table_header(string $disziplin, int $lauf_1 = 0): string {
		$out = "<!--{PLATZHALTER1}-->\n";
		$out.= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"bm-result\">\n";
		$out.= "<tr><td class=\"header-center platz\">&nbsp;</td>";
		$out.= "<td class=\"header-center platz-bm\">BM</td>";
		$out.= "<td class=\"header-center startnr\">StartNr</td>";
		$out.= "<td class=\"header-left name\">Name</td>";
		if ($disziplin != "staffel") { $out.= "<td class=\"header-center jahrgang\">Jahrg</td>"; }
		$out.= "<td class=\"header-left verein\">&nbsp;Verein</td>";
		$out.= "<td class=\"header-left verband\">&nbsp;&nbsp;Verband</td>";
		// Wettbewerbe mit einem oder zwei Durchgängen?
		if ($lauf_1) {
			$out.= "<td class=\"header-right lauf-1\"> 1. Lauf </td>";
			$out.= "<td class=\"header-right lauf-2\"> 2. Lauf </td>";
		}
		$out.= "<td class=\"header-right gesamt\">Gesamt </td>";
		$out.= "<td class=\"header-right diff\"> Diff &nbsp; </td></tr>\n";
		return $out;
	}

    /**
     * returns the list with the results of all competitors
     *
     * @param resource $datei
     * @param string $zeilenformat
     * @param array $pos
     * @param string $disziplin
     * @param int $cols
     * @return string
     */
    public static function result_list($datei, string $zeilenformat, array $pos, string $disziplin, int $cols = 11): string {
        // Teilnehmerdaten einlesen
        $alte_klasse = "";
        $out = "";
        $colspan = $cols - 3;
        $platz_bm = 0;
        $bestzeit = 0;
		while ($zeile = chop(fgets($datei))) {
			$daten = explode("\t", $zeile);
			if ($zeilenformat == "2-zeilig") {
				$zeile2 = chop(fgets($datei));
				$daten2 = explode("\t", $zeile2);
				$verein = substr(($daten2[4] ?? ''), 0, 25);
				$verband = substr(($daten2[5] ?? ''), 0, 5);
			}
			else {
				$verein = substr($daten[$pos["verein"]], 0, 25);
				$verband = substr($daten[$pos["verband"]], 0, 5);
			}
			if ($daten[0] == "" AND $daten[2] == "Nr.") {
				$pos = Ergebnisliste::felder_suchen($daten);
				$out.= "<tr><td colspan=\"$cols\" style=\"padding:0; font-size:5px;\"><hr style=\"width: 100%; height: 1px; border: 0 none; color: gray; background: gray;\"></td></tr>\n";
				$out.= "<tr><td colspan=\"3\" class=\"header-left\">Nicht im Ziel</td>";
				$out.= "<td colspan=\"$colspan\">&nbsp;</td></tr>\n";
			}
			elseif ($daten[0] == "" AND $daten[7] == "D" OR $daten[7] == "A") {
				$out.= "<tr><td class=\"header-center\">&nbsp;</td>";
				$out.= "<td class=\"header-center\">&nbsp;</td>";
				$out.= "<td class=\"data-center\">".$daten[$pos["stnr"]]."</td>";
				$out.= "<td class=\"data-left\">".htmlentities($daten[$pos["name"]], null, 'ISO-8859-1')."</td>";
				$out.= "<td class=\"data-center\">".$daten[$pos["jahrg"]]."</td>";
				$out.= "<td class=\"data-left\">".htmlentities($verein)."</td>";
				$out.= "<td class=\"data-left\">&nbsp;&nbsp;$verband</td>";
				$out.= "<td class=\"data-center\">$daten[7]</td><td colspan=\"".($cols-8)."\"></td></tr>";
			}
			else {
				$klasse = $daten[0]." ".$daten[1];
				if ($klasse != $alte_klasse) {
					$platz_bm = 0;
					$out.= "<tr><td colspan=\"$cols\" style=\"padding:0; font-size:5px;\"><hr style=\"width: 100%; height: 1px; border: 0 none; color: gray; background: gray;\"></td></tr>\n";
					$out.= "<tr><td colspan=\"4\" class=\"header-left\">$klasse</td>";
					$out.= "<td colspan=\"".($colspan-1)."\">&nbsp;</td></tr>\n";
					$alte_klasse = $klasse;
					$bestzeit = $daten[$pos["gesamtzeit"]];
				}
				if ($verband == "SVB") {
					$platz_bm++;
					$out_platz_bm = $platz_bm;
				}
				else { $out_platz_bm = "&nbsp;"; }
				$diff_s = Ergebnisliste::zeit_differenz($bestzeit, $daten[$pos["gesamtzeit"]]);
				$out.= "<tr><td class=\"header-center\">".$daten[$pos["platz"]]."</td>";
				$out.= "<td class=\"header-center\">$out_platz_bm</td>";
				$out.= "<td class=\"data-center\">".$daten[$pos["stnr"]]."</td>";
				$out.= "<td class=\"data-left\">".htmlentities($daten[$pos["name"]], null, 'ISO-8859-1')."</td>";
				if ($disziplin != "staffel") {
					$out.= "<td class=\"data-center\">".$daten[$pos["jahrg"]]."</td>";
				}
				$out.= "<td class=\"data-left\">".htmlentities($verein)."</td>";
				$out.= "<td class=\"data-left\">&nbsp;&nbsp;$verband</td>";
				if ($pos["lauf_1"]) {
					$out.= "<td class=\"data-right\">".$daten[$pos["lauf_1"]]."</td>";
					$out.= "<td class=\"data-right\">".$daten[$pos["lauf_2"]]."</td>";
				}
				$out.= "<td class=\"data-right\">".$daten[$pos["gesamtzeit"]]."</td>";
				$out.= "<td class=\"data-right\">$diff_s</td></tr>\n";
				if ($disziplin == "staffel" AND $daten[13]) {
					$out.= "<tr><td class=\"data-left\" colspan=\"3\">&nbsp;</td><td class=\"data-left\">".$daten[8]."</td><td class=\"data-left\" colspan=\"2\">&nbsp;</td><td class=\"data-right\">".$daten[9]."</td><td class=\"data-left\">&nbsp;</td></tr>";
					$out.= "<tr><td class=\"data-left\" colspan=\"3\">&nbsp;</td><td class=\"data-left\">".$daten[10]."</td><td class=\"data-left\" colspan=\"2\">&nbsp;</td><td class=\"data-right\">".$daten[11]."</td><td class=\"data-left\">&nbsp;</td></tr>";
					$out.= "<tr><td class=\"data-left\" colspan=\"3\">&nbsp;</td><td class=\"data-left\">".$daten[12]."</td><td class=\"data-left\" colspan=\"2\">&nbsp;</td><td class=\"data-right\">".$daten[13]."</td><td class=\"data-left\">&nbsp;</td></tr>";
				}
			}
		}
		return $out;
	}

}

?>