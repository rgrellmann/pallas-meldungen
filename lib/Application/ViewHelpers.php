<?php
namespace Application;

use Framework\Database;
use Framework\App;
use Framework\Auth;
use Framework\Environment;
use Framework\DebugTools;
use Framework\Link;

class ViewHelpers {

    /**
     * @param string $modus
     * @return string
     */
    public static function tabellen_ueberschrift($modus) {
        $out = "<tr>\n";
        $out .= "	<td class=\"normal2\" align=\"center\">Name</td>\n";
        if ($modus == "mafue") {
            $out .= "	<td class=\"normal2\" align=\"center\" colspan=\"3\">Nummer</td>\n";
        }
        else {
            $out .= "	<td class=\"normal2\" align=\"center\">m/w</td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\">Jahrg.</td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\"><span title=\"Altersklasse\">AK</span></td>\n";
        }
        $out .= "	<td class=\"normal2\" align=\"center\" colspan=\"2\">Verein</td>\n";
        $out .= "	<td class=\"normal2\" align=\"center\">Telefon</td>\n";
        $out .= "	<td class=\"normal2\" align=\"center\">Handy</td>\n";
        $out .= "	<td class=\"normal2\" align=\"center\">email</td>\n";
        if ($modus == "mafue") {
            $out .= "	<td class=\"normal2\" align=\"center\"><img src=\"".IMAGES."doc.gif\" width=\"10\" height=\"10\" title=\"Kommentar\" alt=\"\"></td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\"> &nbsp; </td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\"> &nbsp; </td>\n";
        }
        else {
            $out .= "	<td class=\"normal2\" align=\"center\"><img src=\"".IMAGES."doc.gif\" width=\"10\" height=\"10\" title=\"Kommentar\" alt=\"\"></td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\"><span title=\"Startpass-Nummer\">SP-Nr</span></td>\n";
            $out .= "	<td class=\"normal2\" align=\"center\">Disziplinen</td>\n";
        }
        $out .= "	<td class=\"normal2\" align=\"center\">&nbsp;</td>\n";
        $out .= "</tr>\n";
        return $out;
    }

    /**
     * @param \Entity\Meldung $meldung
     * @param string $modus
     * @param string $sortierung
     * @return string
     */
    public static function teilnehmerdaten_anzeigen($meldung, $modus="normal", $sortierung = "") {
        $db = Database::get_connection();
        $session = App::get_session();
        $query = "SELECT name_5 AS verband FROM ".TABLE_VERBAENDE." WHERE code='".$meldung->get("verband")."'";
        $verband = $db->select_one_field($query);
        $out = "";
        switch ($modus) {
            case "mafue":
                $out .= "<tr id=\"zeile_".$meldung->get("id")."\" onClick=\"toggle_highlight('zeile_".$meldung->get("id")."')\">\n";
                $out .= "	<td class=\"sub1 nowrap\"><a name=\"mafue_".$meldung->get("id")."\"></a>".$meldung->get("name").", ".$meldung->get("vorname")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\" colspan=\"3\" nowrap>".$meldung->get("id")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\">".$meldung->get("verein")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\">$verband&nbsp;</td>\n";
                if (Auth::is_allowed('actions/view_personal_data')) {
                    $out .= "	<td class=\"sub1 nowrap\">".$meldung->get("telefon")."&nbsp;</td>\n";
                    $out .= "	<td class=\"sub1 nowrap\">".$meldung->get("handy")."&nbsp;</td>\n";
                    $out .= "	<td class=\"sub1\">&nbsp;"; if ($meldung->get("email")) { $out .= "<a href=\"mailto:".$meldung->get("email")."\">email</a>"; } $out .= "&nbsp;</td>\n";
                    $out .= "	<td class=\"sub1\">";
                    if (strlen($meldung->get("kommentar")) > 2) {
                        $meldung->set("kommentar", trim($meldung->get("kommentar")));
                        if (!Environment::isIE()) {
                            $meldung->set("kommentar", str_replace("\n"," ", $meldung->get("kommentar")));
                            $meldung->set("kommentar", str_replace("\r","", $meldung->get("kommentar")));
                        }
                        $out .= "<img src=\"".IMAGES."doc.gif\" width=\"10\" height=\"10\" title=\"".$meldung->get("kommentar")."\" alt=\"\">";
                    }
                    else { $out .= "&nbsp;"; }
                    $out .= "</td>\n";
                }
                else {
                    $out .= "	<td class=\"sub1\" colspan=\"4\" align=\"center\"> *** </td>\n";
                }
                $out .= "	<td class=\"sub2\"></td><td class=\"sub2\"></td>\n";
                if (Auth::is_allowed('actions/delete_user')) {
                    $query = "SELECT id FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id='".$meldung->get("veranstaltungs_id")."' AND gemeldet_von='".$meldung->get("id")."'";
                    $res = $db->query($query);
                    if ($res->num_rows() == 0) {
                        $out .= "	<td class=\"sub1\" align=\"center\"><a href=\"meldeliste.php?veranstaltung=".$meldung->get("veranstaltungs_id")."&sortierung=$sortierung&todo=teilnehmer_loeschen&teilnehmer=".$meldung->get("id")."\">";
                        $out .= "	<img src=\"".IMAGES."delete_person.gif\" width=\"22\" height=\"19\" border=\"0\" alt=\"aus der Datenbank l&ouml;schen\" title=\"aus der Datenbank l&ouml;schen\"></a></td></tr>\n";
                    }
                    else { $out .= "	<td class=\"sub1\">&nbsp;</td></tr>\n"; }
                }
                else { $out .= "	<td class=\"sub1\">&nbsp;</td></tr>\n"; }
                break;

            case "normal":
                $out .= "<tr id=\"zeile_".$meldung->get("id")."\" onClick=\"toggle_highlight('zeile_".$meldung->get("id")."')\">\n";
                $out .= "	<td class=\"sub1 nowrap\"><span".($meldung->get("startpass_nr") ? " title=\"Startpass-Nr.: ".$meldung->get("startpass_nr")."\"": "").">".$meldung->get("name").", ".$meldung->get("vorname")."</span>&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\" nowrap>".$meldung->get("geschlecht")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\" nowrap>".$meldung->get("jahrgang")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\" nowrap>".Altersklassen::jg2ak($meldung->get("jahrgang"))."</td>\n";
                $out .= "	<td class=\"sub1\">".$meldung->get("verein")."&nbsp;</td>\n";
                $out .= "	<td class=\"sub1\">$verband&nbsp;</td>\n";
                if ($meldung->get("gemeldet_von") != 0) {
                    $out .= "	<td class=\"sub1\" colspan=\"4\" align=\"center\" nowrap> - siehe MaF&uuml; <a href=\"#mafues\" onClick=\"highlight('zeile_".$meldung->get("gemeldet_von")."');\">".$meldung->get("gemeldet_von")."</a> - </td>\n";
                }
                else {
                    if (Auth::is_allowed('actions/view_personal_data')) {
                        $out .= "	<td class=\"sub1 nowrap\">".$meldung->get("telefon")."&nbsp;</td>\n";
                        $out .= "	<td class=\"sub1 nowrap\">".$meldung->get("handy")."&nbsp;</td>\n";
                        $out .= "	<td class=\"sub1\">&nbsp;";
                        if ($meldung->get("email")) { $out .= "<a href=\"mailto:".$meldung->get("email")."\" title=\"".$meldung->get("email")."\">email</a>"; }
                        $out .= "&nbsp;</td>\n";
                        $out .= "	<td class=\"sub1\">";
                        if (strlen($meldung->get("kommentar")) > 2) {
                            $meldung->set("kommentar", trim($meldung->get("kommentar")));
                            if (!Environment::isIE()) {
                                $meldung->set("kommentar", str_replace("\n"," ", $meldung->get("kommentar")));
                                $meldung->set("kommentar", str_replace("\r","", $meldung->get("kommentar")));
                            }
                            $out .= "<img src=\"".IMAGES."doc.gif\" width=\"10\" height=\"10\" title=\"".$meldung->get("kommentar")."\" alt=\"\">";
                        }
                        else { $out .= "&nbsp;"; }
                        $out .= "</td>\n";
                    }
                    else {
                        $out .= "	<td class=\"sub1\" colspan=\"5\" align=\"center\"> *** </td>\n";
                    }
                }
                if (Auth::is_allowed('actions/view_personal_data')) {
                    $out .= "	<td class=\"sub1 nowrap\">".$meldung->get("startpass_nr")."&nbsp;</td>\n";
                }
                $out .= "	<td class=\"sub2\">".str_replace(",","<br>", $meldung->get("disziplin"))."&nbsp;</td>\n";
                if ($session->get_user()->registration_change_allowed($meldung->get("verein"))) {
                    if ($meldung->get("status") == "gestrichen") {
                        $out .= "	<td class=\"sub1\" align=\"center\" nowrap><a href=\"javascript:ziel_festlegen('teilnehmer_einsetzen','".$meldung->get("id")."')\">";
                        $out .= "	<img src=\"".IMAGES."add_person.gif\" width=\"22\" height=\"19\" border=\"0\" alt=\"zur Liste hinzuf&uuml;gen\" title=\"zur Liste hinzuf&uuml;gen\"></a>\n";
                        if (Auth::is_allowed('actions/delete_user')) {
                            $out .= "	&nbsp; <a href=\"javascript:ziel_festlegen('teilnehmer_loeschen','".$meldung->get("id")."')\">";
                            $out .= "	<img src=\"".IMAGES."delete_person.gif\" width=\"22\" height=\"19\" border=\"0\" alt=\"aus der Datenbank l&ouml;schen\" title=\"aus der Datenbank l&ouml;schen\"></a>\n";
                        }
                    }
                    else {
                        $out .= "	<td class=\"sub1\" align=\"center\" nowrap><a href=\"javascript:ziel_festlegen('teilnehmer_streichen','".$meldung->get("id")."')\">";
                        $out .= "	<img src=\"".IMAGES."remove_person.gif\" width=\"19\" height=\"19\" border=\"0\" alt=\"aus der Liste streichen\" title=\"aus der Liste streichen\"></a>\n";
                    }
                    $out .= "	&nbsp; <a href=\"javascript:ziel_festlegen('teilnehmer_bearbeiten','".$meldung->get("id")."')\">";
                    $out .= "	<img src=\"".IMAGES."edit_person.gif\" width=\"22\" height=\"19\" border=\"0\" alt=\"Teilnehmer bearbeiten\" title=\"Teilnehmer bearbeiten\"></a>\n";
                }
                else { $out .= "	<td class=\"sub1 nowrap\">&nbsp;\n"; }
                $out .= "</td></tr>\n";
                break;

            case "formular":
                $out .= "<form method=\"post\" name=\"teilnehmer_form\" action=\"".Link::make("meldeliste")."\" onsubmit=\"return werte_pruefen()\">";
                $out .= "<tr>\n	<td class=\"sub1 nowrap\">";
                $out .= " <input type=\"text\" name=\"name\" value=\"".$meldung->get("name")."\" maxlength=\"40\" size=\"12\">,";
                $out .= " <input type=\"text\" name=\"vorname\" value=\"".$meldung->get("vorname")."\" maxlength=\"40\" size=\"10\"></td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\"><select name=\"geschlecht\" size=\"1\">";
                $out .= "	<option value=\"m\""; if ($meldung->get("geschlecht") == "m") { $out .= " selected"; } $out .= ">m</option>";
                $out .= "	<option value=\"w\""; if ($meldung->get("geschlecht") == "w") { $out .= " selected"; } $out .= ">w</option></select></td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\" colspan=\"2\"><input type=\"text\" name=\"jahrgang\" value=\"".$meldung->get("jahrgang")."\" maxlength=\"6\" size=\"4\"></td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\"><input type=\"text\" name=\"verein\" value=\"".$meldung->get("verein")."\" maxlength=\"40\" size=\"15\"></td>\n";
                $out .= "	<td class=\"sub1\" align=\"center\"><select name=\"verband\" size=\"1\">";
                $out .= liste_verbaende("ski",true, $meldung->get("verband"),false);
                $out .= "</select></td>\n";
                if ($meldung->get("gemeldet_von") > 0) {
                    $out .= "	<td class=\"sub1\" colspan=4 align=\"center\" nowrap>Maf&uuml;: <input type=\"hidden\" name=\"gemeldet_von\" value=\"".$meldung->get("gemeldet_von")."\">".$meldung->get("gemeldet_von")."\n";
                    $out .= "	<input type=\"hidden\" name=\"telefon\" value=\"".$meldung->get("telefon")."\">\n";
                    $out .= "	<input type=\"hidden\" name=\"handy\" value=\"".$meldung->get("handy")."\">\n";
                    $out .= "	<input type=\"hidden\" name=\"email\" value=\"".$meldung->get("email")."\"></td>\n";
                }
                else {
                    $out .= "	<td class=\"sub1\" align=\"center\"><input type=\"text\" name=\"telefon\" value=\"".$meldung->get("telefon")."\" maxlength=\"30\" size=\"10\"></td>\n";
                    $out .= "	<td class=\"sub1\" align=\"center\"><input type=\"text\" name=\"handy\" value=\"".$meldung->get("handy")."\" maxlength=\"30\" size=\"10\"></td>\n";
                    $out .= "	<td class=\"sub1\" align=\"center\"><input type=\"text\" name=\"email\" value=\"".$meldung->get("email")."\" maxlength=\"80\" size=\"22\">\n";
                    $out .= "	<input type=\"hidden\" name=\"gemeldet_von\" value=\"".$meldung->get("gemeldet_von")."\"></td>\n";
                    $out .= "	<td class=\"sub1 nowrap\">";
                    if (strlen($meldung->get("kommentar")) > 2) {
                        $meldung->set("kommentar", trim($meldung->get("kommentar")));
                        if (!Environment::isIE()) {
                            $meldung->set("kommentar", str_replace("\n"," ", $meldung->get("kommentar")));
                            $meldung->set("kommentar", str_replace("\r","", $meldung->get("kommentar")));
                        }
                        $out .= "<img src=\"".IMAGES."doc.gif\" width=\"10\" height=\"10\" title=\"".$meldung->get("kommentar")."\" alt=\"\">";
                    }
                    else { $out .= "&nbsp;"; }
                    $out .= "<input type=\"text\" name=\"kommentar\" value=\"".$meldung->get("kommentar")."\" maxlength=\"250\" size=\"8\" title=\"Kommentar\"></td>\n";
                }
                $out .= "	<td class=\"sub1\" align=\"center\"><input type=\"text\" name=\"startpass_nr\" value=\"".$meldung->get("startpass_nr")."\" maxlength=\"20\" size=\"4\" title=\"Startpass-Nr\"></td>\n";
                $query = "SELECT disziplinen FROM ".TABLE_VERANSTALTUNGEN." WHERE id='".$meldung->get("veranstaltungs_id")."'";
                $res = $db->query($query);
                $disziplinen = $res->fetch();
                if ($disziplinen["disziplinen"]) {
                    $out .= "	<td class=\"sub2\" align=\"center\"><select class=\"sub2\" multiple name=\"disziplin[]\" size=3>\n";
                    $disziplinen = explode(",", $disziplinen["disziplinen"]);
                    $teilnehmer_disziplinen = explode(",", $meldung->get("disziplin"));
                    foreach ($disziplinen as $disziplin) {
                        $out .= "<option";
                        if (array_search($disziplin, $teilnehmer_disziplinen) !== FALSE) { $out .= " selected"; }
                        $out .= ">$disziplin</option>\n";
                    }
                    $out .= "</select></td>\n";
                }
                else { $out .= "<td>&nbsp;</td>"; }
                if ($session->get_user()->registration_change_allowed($meldung->get("verein"))) {
                    $out .= "<td class=\"sub1\"><input type=\"submit\" class=\"sub1\" value=\"speichern\"><br>";
                }
                else { $out .= "<td class=\"sub1\">&nbsp;\n"; }
                $out .= "<input type=\"button\" class=\"sub1\" value=\"abbrechen\" onClick=\"this.form.todo.value='';this.form.submit();\">\n";
                $out .= "	<input type=\"hidden\" name=\"todo\" value=\"teilnehmer_";
                if ($meldung->get("id")) { $out.= "update"; } else { $out.= "insert"; }
                DebugTools::add_debug_message("Teilnehmer-ID: ".$meldung->get("id"));
                $out .= "\">\n";
                $out .= "	<input type=\"hidden\" name=\"teilnehmer\" value=\"".$meldung->get("id")."\">\n";
                $out .= "	<input type=\"hidden\" name=\"veranstaltung\" value=\"".$meldung->get("veranstaltungs_id")."\">\n";
                $out .= "	<input type=\"hidden\" name=\"sortierung\" value=\"$sortierung\">\n";
                $out .= "	<input type=\"hidden\" name=\"dummy\" value=\"false\">\n";
                $out .= "</td></tr></form>\n";
                break;
        }
        return $out;
    }

    /**
     * @param \Entity\Meldung $meldung
     * @param string $modus
     * @return string
     */
    public static function veranstaltungsdaten_anzeigen($meldung, $modus="tabelle") {
        $meldung = $meldung->get_data();
        $out = "";
        if ($modus == "tabelle") {
            $meldeschluss = substr($meldung["meldeschluss"],8,2).".".substr($meldung["meldeschluss"],5,2).".".substr($meldung["meldeschluss"],0,4)."<br>".substr($meldung["meldeschluss"],11,2)." Uhr";
            $meldebeginn = substr($meldung["meldebeginn"],8,2).".".substr($meldung["meldebeginn"],5,2).".".substr($meldung["meldebeginn"],0,4)."<br>".substr($meldung["meldebeginn"],11,2)." Uhr";
            $datum = substr($meldung["datum"],8,2).".".substr($meldung["datum"],5,2).".".substr($meldung["datum"],0,4);
            $out = "<tr>\n	<td class=\"sub1 nowrap\">$datum&nbsp;</td>\n";
            $out .= "	<td class=\"sub1\">$meldung[bezeichnung_kurz]&nbsp;</td>\n";
            $out .= "	<td class=\"sub1\">$meldung[bezeichnung]&nbsp;</td>\n";
            $out .= "	<td class=\"sub1\" align=\"center\">$meldebeginn</td>\n";
            $out .= "	<td class=\"sub1\" align=\"center\">$meldeschluss</td>\n";
            $out .= "	<td class=\"sub1\" align=\"center\">$meldung[anzahl_teilnehmer]&nbsp;</td>\n";
            $out .= "	<td class=\"sub1\">$meldung[ort]&nbsp;</td>\n";
            $out .= "	<td class=\"sub1\">";
            if (strlen($meldung["url_ausschreibung"]) > 10) {
                $out .= "<a href=\"$meldung[url_ausschreibung]\" target=\"_blank\"><img src=\"".IMAGES."ausschreibung.gif\" border=\"1\" title=\"Link zur Ausschreibung\" alt=\"Link zur Ausschreibung\"></a>";
            } else { $out .= "&nbsp;"; }
            $out.= "</td>\n";
            $out .= "<td class=\"sub1\" align=\"center\">".($meldung["startpass_erforderlich"] == "ja" ? "ja" : "&nbsp;")."</td>\n";
            $out .= "	<td class=\"sub1\" align=\"center\"><a href=\"meldeliste.php?veranstaltung=$meldung[id]\"><img src=\"".IMAGES."show_list.gif\" width=\"30\" height=\"20\" border=\"0\" alt=\"Teilnehmerliste anzeigen\" title=\"Teilnehmerliste anzeigen\"></a></td>\n";
            if ($_SESSION["status"] >= KAMPFRICHTER) {
                $out .= "	<td class=\"sub1\" align=\"center\"><a href=\"javascript:ziel_festlegen('veranstaltung_bearbeiten','$meldung[id]');\"><img src=\"".IMAGES."edit.gif\" width=\"23\" height=\"20\" border=0 alt=\"Veranstaltung bearbeiten\" title=\"Veranstaltung bearbeiten\"></a></td>\n";
                if ($meldung["anzahl_teilnehmer"] > 0) {
                    $out .= "	<td class=\"sub1\" align=\"center\"><img src=\"".IMAGES."delete_deakt.gif\" width=\"23\" height=\"20\" border=\"0\" alt=\"l&ouml;schen nicht m&ouml;glich\" title=\"l&ouml;schen nicht m&ouml;glich\"></td>\n</tr>\n";
                }
                else {
                    $out .= "	<td class=\"sub1\" align=\"center\"><a href=\"javascript:ziel_festlegen('veranstaltung_loeschen','$meldung[id]');\"><img src=\"".IMAGES."delete.gif\" width=\"23\" height=\"20\" border=\"0\" alt=\"Veranstaltung l&ouml;schen\" title=\"Veranstaltung l&ouml;schen\"></a></td>\n</tr>\n";
                }
            }
            else { $out .= "<td colspan=\"2\"> &nbsp; </td>"; }
        }
        return $out;
    }

    /**
     * @param string $sportart
     * @param bool $alle
     * @param int|string $selected
     * @param bool $name_lang
     * @return string
     */
    public static function liste_verbaende($sportart = "ski", $alle = false, $selected = -1, $name_lang = true) {
        $db = Database::get_connection();
        $out = "";
        $where = "";
        if (!$alle) { $where = "AND code > 0 AND code != 21"; }
        $query = "SELECT * FROM ".TABLE_VERBAENDE." WHERE sportart='$sportart' ".$where." ORDER BY name";
        $res = $db->query($query);
        while ($meldung = $res->fetch()) {
            $out .= "		<option value=\"$meldung[code]\"";
            if ($selected == $meldung["code"]) { $out .= " selected"; }
            $out .= ">$meldung[name_5]";
            if ($name_lang) { $out .= " ($meldung[name])"; }
            $out .= "</option>\n";
        }
        return $out;
    }

    /**
     * @param string $sportart
     * @param int|string $selected
     * @param bool $name_lang
     * @return string
     */
    public static function liste_vereine($sportart = "ski", $selected = -1, $name_lang = true) {
        $db = Database::get_connection();
        $out = "";
        $query = "SELECT * FROM ".TABLE_VEREINE." WHERE sportart='$sportart' ORDER BY id";
        $res = $db->query($query);
        while ($meldung = $res->fetch()) {
            $out .= "		<option value=\"$meldung[verein_20]\"";
            if ($selected == $meldung["verein_20"]) { $out .= " selected"; }
            if ($name_lang) {
                $out .= ">$meldung[verein]";
            } else {
                $out .= ">$meldung[verein_25]";
            }
            $out .= "</option>\n";
        }
        return $out;
    }

    /**
     * @param string $name
     * @param int $Zeit
     * @param int $Tag
     * @param int $Monat
     * @param bool $Jahr
     * @param string $onChangeJahr
     * @return string
     */
    public static function Selectbox_Datum($name, $Zeit = -1, $Tag = 1, $Monat = 1, $Jahr = false, $onChangeJahr = "") {
        $out = "<select name=\"".$name."_tag\">";
        for ($i = 1; $i < 32; $i++) {
            $j = sprintf("%02u",$i);
            if ($i == $Tag) { $out.= "<option value=\"$j\" selected>$j"; }
            else { $out.= "<option value=\"$j\">$j";	}
        }
        $out.= "</select> . ";

        $out.= "<select name=\"".$name."_monat\">";
        for ($i = 1; $i < 13; $i++) {
            $j = sprintf("%02u",$i);
            if ($i == $Monat) {	$out.= "<option value=\"$j\" selected>$j"; }
            else { $out.= "<option value=\"$j\">$j"; }
        }
        $out.= "</select> . ";

        if (!$Jahr) { $Jahr = date("Y"); }
        $out.= "<select name=\"".$name."_jahr\"";
        if (strlen($onChangeJahr) > 3) { $out.= " onChange=\"$onChangeJahr\""; }
        $out.= ">";
        $a = (date("Y")) + 5;
        for ($i = 2000; $i < $a; $i++) {
            if ($i == $Jahr) { $out.= "<option value=\"$i\" selected>$i"; }
            else { $out.= "<option value=\"$i\">$i"; }
        }
        $out.= "</select>";

        if ($Zeit >= 0) {
            $out.= " &nbsp;&nbsp; <select name=\"".$name."_zeit\">";
            $Zeiten = Array(9,12,15,18,20);
            foreach ($Zeiten as $z)	{
                if ($z == $Zeit) { $out.= "<option value=\"$z\" selected>$z Uhr"; }
                else {	$out.= "<option value=\"$z\">$z Uhr"; }
            }
            $out.= "</select>";
        }
        return $out;
    }

    /**
     * @param string $error_msg
     * @param int $line
     * @return string
     */
    public static function debug_msg(string $error_msg, int $line): string {
        $out = "Error at Line $line: $error_msg <br>\n";
        return $out;
    }

    /**
     * @param string $error_msg
     * @param string $message_class
     * @param string $box_type
     * @return string
     */
    public static function format_error_msg(string $error_msg, string $message_class = 'user', string $box_type = 'inline'): string {
        $msg = '<div class="message';
        if (!empty($message_class)) {
            $msg .= "-".$message_class;
        }
        if (!empty($message_class)) {
            $msg.= " message-".$box_type;
        }
        $msg .= '">'.$error_msg.'</div>';
        return $msg;
    }

}
