<?php
namespace Application;

use Entity\Meldung;
use Entity\Veranstaltung;
use Framework\Auth;
use Framework\Common;
use Framework\Database;
use Framework\DateAndTime;
use Framework\PDFfpdf;
use Framework\Request;

/**
 * Class Meldeliste
 */
class Meldeliste {

    public static $cols = array();
    public static $cols2 = array();

    /**
     * @param Veranstaltung $veranstaltung
     * @param string $sortierung
     * @param string $disziplin
     * @param string $encoding
     * @return void
     */
    public static function meldeliste_text(Veranstaltung $veranstaltung, string $sortierung = 'Jahrgang', string $disziplin = '', string $encoding = "ISO-8859-15") {
        $db = Database::get_connection();

        header("Pragma: no-cache");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        header("Content-type: text/plain; charset=$encoding");

        $bez_kurz = strtr($veranstaltung->get("bezeichnung_kurz"), " ", "_");
        if ($disziplin) {
            $disziplin_kurz = "_".\Application\Common::disziplin_kurz($disziplin);
        } else {
            $disziplin_kurz = "";
        }
        if (Request::get("download") == "true") {
            header("Content-Disposition: attachment; filename=meldeliste_".$bez_kurz.$disziplin_kurz.".txt");
        } else {
            header("Content-Disposition: filename=meldeliste_".$bez_kurz.$disziplin_kurz.".txt");
        }

        if (!$veranstaltung->get_id()) {
            die("Keine Veranstaltung angegeben oder ungültige Veranstaltung");
        }

        $out = "";

        $datum = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATE_DE);

        $out.= "Teilnehmerliste: ".$veranstaltung->get("bezeichnung")."\r\nam $datum in ".$veranstaltung->get('ort')."\r\n";
        if ($disziplin) { $out.= "Disziplin: $disziplin\r\n"; }
        $datum = date("d.m.Y, H:i");
        $meldeschluss = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATETIME_DE)." Uhr";

        $out.= "Stand: $datum Uhr  |  Sortierung nach: $sortierung" . str_repeat(' ', 14 - strlen($sortierung)) . "| Meldeschluß: $meldeschluss\r\n";
        $out.= str_repeat('-', 100) . "\r\n\r\n";
        $out.= "Name                    | m/w |  JG  | Verein                   | Verb. | Disziplin(en)\r\n";
        $out.= str_repeat('-', 100) . "\r\n";
        switch ($sortierung) {
            case "Name":
                $order = " ORDER BY name,vorname";
                break;
            case "Verein":
                $order = " ORDER BY verein,name,vorname";
                break;
            case "Jahrgang":
            default:
                $order = " ORDER BY geschlecht,jahrgang DESC,verein,name";
        }
        $sql = "SELECT m.*,vb.name_5 FROM " . TABLE_MELDUNGEN . " AS m"
            . " LEFT JOIN " . TABLE_VERBAENDE . " AS vb ON m.verband = vb.code"
            . " WHERE veranstaltungs_id = '" . $veranstaltung->get_id() . "' AND status = 'gemeldet' "
            . $order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Meldung::class, '', true);
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",", $ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                else { $ds["disziplin"] = $disziplin; }
            }
            $out.= self::teilnehmer_zeile($ds);
        }
        if (count($meldungen) < 1) {
            $out.= "\r\nKeine Eintragung\r\n";
        } else {
            $out.= "\r\nAnzahl: ".count($meldungen)."\r\n";
            $out.= "\r\nAltersklassen:\r\n";
            $altersklassen = Altersklassen::altersklassenliste($meldungen, $veranstaltung);
    	    $out.= "AK        " . implode('  ', array_keys($altersklassen['alle'])) . "\r\n";

            $out.= "m (alle)";
            foreach ($altersklassen['alle'] as $anzahl) {
                $out.= sprintf("%5d", $anzahl['m']);
            }
            $out.= "\r\nm (SVB) ";
            foreach ($altersklassen['SVB'] as $anzahl) {
                $out.= sprintf("%5d", $anzahl['m']);
            }
            $out .= "\r\nw (alle)";
            foreach ($altersklassen['alle'] as $anzahl) {
                $out.= sprintf("%5d", $anzahl['w']);
            }
            $out .= "\r\nw (SVB) ";
            foreach ($altersklassen['SVB'] as $anzahl) {
                $out.= sprintf("%5d", $anzahl['w']);
            }
            $out.= "\r\n";
        }

        $out.= str_repeat('-', 100) . "\r\n";
        $out.= "\r\nzurückgezogene Meldungen:\r\n";
        $out.= "Name                    | m/w |  JG  | Verein                   | Verb. | Disziplin(en)\r\n";
        $out.= str_repeat('-', 100) . "\r\n";

        $sql = "SELECT m.*,vb.name_5 FROM " . TABLE_MELDUNGEN . " AS m"
            . " LEFT JOIN " . TABLE_VERBAENDE . " AS vb ON m.verband = vb.code"
            . " WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'gestrichen' "
            . $order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Meldung::class, '', true);
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",", $ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                else { $ds["disziplin"] = $disziplin; }
            }
            $out.= self::teilnehmer_zeile($ds);
        }
        if (count($meldungen) < 1) {
            $out.= "\r\nKeine Eintragung\r\n";
        } else {
            $out.= "\r\nAnzahl: ".count($meldungen)."\r\n";
        }
        if ($encoding != "UTF-8") {
            $out = mb_convert_encoding($out, $encoding, 'UTF-8');
        }
        echo $out;
        die;
    }

    /**
     * @param array $ds
     * @return string
     */
    protected static function teilnehmer_zeile(array $ds): string {
        $teiln_name = substr("$ds[name], $ds[vorname]", 0, 26);
        $out = $teiln_name . str_repeat(' ', max(0, 26 - mb_strlen($teiln_name)));
        $out.= " $ds[geschlecht] ";
        $out.= "   $ds[jahrgang]   ";
        $teiln_verein = substr($ds["verein"],0,25);
        $out.= $teiln_verein . str_repeat(' ', max(0, 25 - mb_strlen($teiln_verein)));
        $out.= "  " . $ds["name_5"] . str_repeat(' ', max(0, 8 - mb_strlen($ds["name_5"])));
        $out.= $ds["disziplin"] . str_repeat(' ', max(0, 28 - mb_strlen($ds["disziplin"])));
        $out.= " \r\n";
        return $out;
    }

    /**
     * @param Veranstaltung $veranstaltung
     * @param string $sortierung
     * @param string $disziplin
     * @param string $encoding
     * @return bool|void
     */
    public static function meldeliste_csv(Veranstaltung $veranstaltung, string $sortierung = 'Jahrgang', string $disziplin = '', string $encoding = "ISO-8859-15") {
        $db = Database::get_connection();

        if (!$veranstaltung->get_id()) {
            return false;
        }

        if (empty($sortierung)) { $sortierung = "Jahrgang"; }

        $bez_kurz = strtr($veranstaltung->get("bezeichnung_kurz"), " ", "_");
        if ($disziplin) {
            $disziplin_kurz = "_".\Application\Common::disziplin_kurz($disziplin);
        } else {
            $disziplin_kurz = "";
        }

        header("Pragma: no-cache");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        if (Request::get("download") == "true") {
            header("Content-type: application/comma-separated-value; charset=$encoding");
            header("Content-Disposition: attachment; filename=meldeliste_".$bez_kurz.$disziplin_kurz.".csv");
        } else {
            header("Content-type: text/comma-separated-value; charset=$encoding");
            header("Content-Disposition: filename=meldeliste_".$bez_kurz.$disziplin_kurz.".csv");
        }

        $out = "";

        if ($disziplin) {
            $disziplinen = array($disziplin);
        } else {
            $disziplinen = explode(",", $veranstaltung->get('disziplinen'));
        }

        $out.= "Name;Vorname;m/w;JG;Verein;Verband;".(count($disziplinen) ? implode(";", $disziplinen).";" : "")."Kommentar\r\n";

        switch ($sortierung) {
            case "Name":
                $order = " ORDER BY name,vorname";
                break;
            case "Verein":
                $order = " ORDER BY verein,name,vorname";
                break;
            case "Jahrgang":
            default:
                $order = " ORDER BY geschlecht,jahrgang DESC,verein,name";
        }

        $sql = "SELECT "."* FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'gemeldet' ";
        $sql .= $order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);

        foreach ($meldungen as $ds) {
            $ds["disziplin"] = explode(",", $ds["disziplin"]);
            if ($disziplin) {
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                $ds["disziplin"] = array($disziplin);
            }
            $out.= "$ds[name];";
            $out.= "$ds[vorname];";
            $out.= "$ds[geschlecht];";
            $out.= "$ds[jahrgang];";
            $teiln_verein = substr($ds["verein"], 0, 25);
            $out.= $teiln_verein.";";
            $out.= $ds["verband"].";";
            foreach ($disziplinen as $d) {
                if (in_array($d, $ds["disziplin"])) {
                    $out.= "X;";
                } else {
                    $out.= ";";
                }
            }
            $out.= " \r\n";
        }
        if (count($meldungen) < 1) { $out.= "Keine Eintragung\r\n"; }
        $out.= "\r\nzurückgezogene Meldungen:\r\n";
        $sql = "SELECT "."* FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'gestrichen' ".$order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",", $ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                else { $ds["disziplin"] = $disziplin; }
            }
            $teiln_name = substr("$ds[name], $ds[vorname]", 0, 25);
            $out.= $teiln_name.";";
            $out.= "$ds[geschlecht];";
            $out.= "$ds[jahrgang];";
            $teiln_verein = substr($ds["verein"],0,25);
            $out.= $teiln_verein.";";
            $out.= $ds["verband"].";";
            $out.= $ds["disziplin"];
            $out.= " \r\n";
        }
        if (count($meldungen) < 1) { $out.= "Keine Eintragung\r\n"; }
        if ($encoding != "UTF-8") {
            $out = mb_convert_encoding($out, $encoding, 'UTF-8');
        }
        echo $out;
        die;
    }

    /**
     * @param Veranstaltung $veranstaltung
     * @param string $formatierung
     * @param string $disziplin
     * @param string $encoding
     * @return bool|void
     */
    public static function meldeliste_kampfrichter(Veranstaltung $veranstaltung, string $formatierung = 'svb', string $disziplin = '', string $encoding = "ISO-8859-15") {
        $db = Database::get_connection();

        if (!$veranstaltung->get_id()) {
            return false;
        }

        $out = "";

        $bez_kurz = strtr($veranstaltung->get("bezeichnung_kurz"), " ", "_");
        if ($disziplin) {
            $disziplin_kurz = "_".\Application\Common::disziplin_kurz($disziplin);
        } else {
            $disziplin_kurz = "";
        }

        header("Pragma: no-cache");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        $datum = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATE_DE);

        $jetzt = date("d.m.Y, H:i");
        $meldeschluss = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATETIME_DE)." Uhr";

        header("Content-type: text/plain; charset=$encoding");
        if (Request::get("download") == "true") {
            header("Content-Disposition: attachment; filename=meldeliste_".$bez_kurz.$disziplin_kurz.".txt");
        } else {
            header("Content-Disposition: filename=meldeliste_".$bez_kurz.$disziplin_kurz.".txt");
        }
        if ($formatierung == "svb")	{
            $out.= "Name\tVorname\tJahrgang\tGeschlecht\tVerein\tVerband\r\n";
            $out.= "*-------------------------------------------------------------\r\n";
            $out.= "* Teilnehmerliste: ".$veranstaltung->get("bezeichnung")."\r\n";
            $out.= "* am $datum in ".$veranstaltung->get("ort")."\r\n";
            if ($disziplin) { $out.= "* Disziplin: $disziplin\r\n"; }
            $out.= "* Stand: $jetzt Uhr   |   Meldeschluß: $meldeschluss\r\n";
            $out.= "* Formatierung: SVB-Kampfrichterprogramm\r\n";
            $out.= "*-------------------------------------------------------------\r\n";
        }
        elseif ($formatierung == "dsv") {
            $out.= "";
        }

        $sql = "SELECT "."m.*,v.name_5 FROM ".TABLE_MELDUNGEN." AS m LEFT JOIN ".TABLE_VERBAENDE." AS v ON m.verband=v.code WHERE veranstaltungs_id='".$veranstaltung->get_id()."' AND status='gemeldet' ORDER BY geschlecht,jahrgang DESC;";
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        $i = 0; $h = 0; $d = 0;
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",",$ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $_REQUEST["disziplin"]) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
            }
            $i++;
//				***** Meldeliste für SVB-Programm *****
            if ($formatierung == "svb")	{
                $out.= " ".$ds["name"]."\t".$ds["vorname"]."\t".$ds["jahrgang"]."\t".$ds["geschlecht"]."\t".$ds["verein"]."\t".$ds["name_5"]."\r\n";
            }
//				***** Meldeliste für DSV-Programm *****
            elseif ($formatierung == "dsv") {
                switch ($ds["verein"]) {
                    case "SC Pallas Berlin": $out.= "21003"; break;
                    case "Berliner Schneehasen": $out.= "21001"; break;
                    default: $out.= "21000";
                }
                if ($ds["geschlecht"] == "m") {
                    $out.= "H"; $h++;
                    if ($h < 10) { $out.= "0$h	"; } else { $out.= "$h	"; }
                }
                else {
                    $out.= "D"; $d++;
                    if ($d < 10) { $out.= "0$d	"; } else { $out.= "$d	"; }
                }
                $out.= str_pad(strtoupper(substr($ds["name"],0,19)),20);
                $out.= str_pad(substr($ds["vorname"],0,13),14);
                $out.= str_pad(substr($ds["jahrgang"],2,2),10);
                $out.= str_pad(strtoupper(substr($ds["verein"],0,20)),32);
                $out.= "999.99\r\n";
            }
//				**********
        }
        if ($i < 1) { $out.= "\r\n* Keine Eintragung\r\n"; }
        else {
            if ($formatierung == "svb")	{
                $out.= "*-------------------------------------------------------------\r\n";
                $out.= "* Anzahl: $i \r\n";
            }
            elseif ($formatierung == "dsv") {
                $out.= "1000      SKIVERBAND BERLIN   $i \r\n";
            }
        }
        if ($encoding != "UTF-8") {
            $out = mb_convert_encoding($out, $encoding, 'UTF-8');
        }
        echo $out;
        die;
    }


    /**
     * @param Veranstaltung $veranstaltung
     * @param string $sortierung
     * @param string $disziplin
     * @return bool|void
     */
    public static function meldeliste_pdf(Veranstaltung $veranstaltung, string $sortierung = 'Jahrgang', string $disziplin = '') {
        $db = Database::get_connection();
        if (!$veranstaltung->get_id()) {
            return false;
        }

        if (empty($sortierung)) { $sortierung = "Jahrgang"; }

        $pdf = new PDFfpdf("UTF-8");

        $error_msg = "";
        self::$cols = array(75, 195, 225, 250, 375);
        self::$cols2 = array(85, 135, 220, 305);
        $zeilen_pro_seite = 25;
        $i = 0;
        $seite = 1;
        $heute = date("d.m.Y, H:i");
        $kein_seitenwechsel = true;

        $bez_kurz = strtr($veranstaltung->get("bezeichnung_kurz"), " ", "_");
        if ($disziplin) {
            $disziplin_kurz = "_".\Application\Common::disziplin_kurz($disziplin);
        } else {
            $disziplin_kurz = "";
        }

        $datum = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATE_DE);
        $header = "Teilnehmerliste: ".$veranstaltung->get("bezeichnung")."\nam $datum in ".$veranstaltung->get('ort')."\n";

        if ($disziplin) { $header .= "Disziplin: $disziplin\n"; }
        $meldeschluss = DateAndTime::format_date($veranstaltung->get('datum'), DateAndTime::DATETIME_DE)." Uhr";
        $header .= "Stand: $heute Uhr	|	Meldeschluß: $meldeschluss\nSortierung nach: $sortierung";

        $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
        $y_koord = self::pdf_tabellen_titel($pdf, $y_koord);

        switch ($sortierung) {
            case "Name":
                $order = " ORDER BY name,vorname";
                break;
            case "Verein":
                $order = " ORDER BY verein,name,vorname";
                break;
            case "Jahrgang":
            default:
                $order = " ORDER BY geschlecht,jahrgang DESC,verein,name";
        }
        // *************************************************************************************************
        //		gemeldete Teilnehmer
        // *************************************************************************************************
        $sql = "SELECT "."* FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'gemeldet' ";
        $sql .= $order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        $laufende_nr = 0;
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",",$ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                else { $ds["disziplin"] = $disziplin; $laufende_nr++; $i++; }
            }
            else { $laufende_nr++; $i++; }
            if ($i > $zeilen_pro_seite) {
                self::pdf_seitenende($pdf, $seite);
                $i = 1; $seite++;
                $pdf->add_page();
                $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
                $y_koord = self::pdf_tabellen_titel($pdf, $y_koord);
            }
            if ($i > 1) { $y_koord += 15; }
            $pdf->set_font(9);
            $pdf->write_left($laufende_nr, 58, $y_koord);
            $pdf->set_font(9, "bold");
            $pdf->write_left("$ds[name], $ds[vorname]", self::$cols[0], $y_koord);
            $pdf->set_font(9);
            $pdf->write_left("    $ds[geschlecht]", self::$cols[1], $y_koord);
            $pdf->write_left(" $ds[jahrgang]", self::$cols[2], $y_koord);
            $pdf->write_left("  $ds[verein]", self::$cols[3], $y_koord);
            $pdf->write_left("  $ds[disziplin]", self::$cols[4], $y_koord);
            $y_koord += 10;
            $pdf->write_left("  $ds[verband]", self::$cols2[0], $y_koord);
            if (Auth::is_allowed('actions/view_personal_data')) {
                if ($ds["gemeldet_von"] == 0) {
                    $pdf->write_left("  $ds[telefon]", self::$cols2[1], $y_koord);
                    $pdf->write_left("  $ds[handy]", self::$cols2[2], $y_koord);
                    $pdf->write_left("  $ds[email]", self::$cols2[3], $y_koord);
                } else {
                    $pdf->write_left("  - siehe MaFü $ds[gemeldet_von] -", self::$cols2[2], $y_koord);
                }
            }
            $pdf->line(55, $y_koord + 4, 550, $y_koord + 4);
        }
        if (count($meldungen) < 1 OR ($i == 0 AND $seite == 1)) {
            $pdf->set_font(12, "bold");
            $pdf->write_left("Keine Eintragung", 75, $y_koord);
        }

        // *************************************************************************************************
        //		gestrichene Teilnehmer
        // *************************************************************************************************

        $sql = "SELECT "."* FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'gestrichen' ".$order;
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        if ((($y_koord + count($meldungen) * 25) > 700) AND ($y_koord > 420)) {
            self::pdf_seitenende($pdf, $seite);
            $i = 0; $seite++;
            $pdf->add_page();
            $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
        }
        else {
            $pdf->line(55, $y_koord + 13, 550, $y_koord + 13);
            $i += 3;
            $y_koord += 25;
            $kein_seitenwechsel = true;
        }
        $y_koord = self::pdf_tabellen_titel($pdf, $y_koord);
        $pdf->set_font(12, "bold");
        $pdf->write_left("zurückgezogene Meldungen:", 75, $y_koord+5);
        $pdf->line(55, $y_koord + 13, 550, $y_koord + 13);
        $y_koord += 25;
        $laufende_nr = 0;
        if (count($meldungen) < 1) { $pdf->write_left("Keine Eintragung", 75, $y_koord); }
        foreach ($meldungen as $ds) {
            if ($disziplin) {
                $ds["disziplin"] = explode(",",$ds["disziplin"]);
                $gemeldet = false;
                foreach ($ds["disziplin"] as $teilnehmer_disziplin) {
                    if ($teilnehmer_disziplin == $disziplin) { $gemeldet = true; }
                }
                if (!$gemeldet) { continue; }
                else { $ds["disziplin"] = $disziplin; $laufende_nr++; $i++; }
            }
            else { $laufende_nr++; $i++; }
            if ($i > $zeilen_pro_seite) {
                self::pdf_seitenende($pdf, $seite);
                $i = 1; $seite++;
                $pdf->add_page();
                $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
                $y_koord = self::pdf_tabellen_titel($pdf, $y_koord);
                $pdf->set_font(12, "bold");
                $pdf->write_left("zurückgezogene Meldungen:", 75, $y_koord+5);
                $pdf->line(55, $y_koord+15, 550, $y_koord+15);
                $y_koord += 25;
            }
            if ($kein_seitenwechsel) { $kein_seitenwechsel = false; }
            elseif ($i > 1) { $y_koord += 15; }
            $pdf->set_font(9);
            $pdf->write_left($laufende_nr, 58, $y_koord);
            $pdf->set_font(9, "bold");
            $pdf->write_left("$ds[name], $ds[vorname]", self::$cols[0], $y_koord);
            $pdf->set_font(9);
            $pdf->write_left("    $ds[geschlecht]", self::$cols[1], $y_koord);
            $pdf->write_left(" $ds[jahrgang]", self::$cols[2], $y_koord);
            $pdf->write_left("  $ds[verein]", self::$cols[3], $y_koord);
            $pdf->write_left("  $ds[disziplin]", self::$cols[4], $y_koord);
            $y_koord += 10;
            $pdf->write_left("  $ds[verband]", self::$cols2[0], $y_koord);
            if (Auth::is_allowed('actions/view_personal_data')) {
                if ($ds["gemeldet_von"] == 0) {
                    $pdf->write_left("  $ds[telefon]", self::$cols2[1], $y_koord);
                    $pdf->write_left("  $ds[handy]", self::$cols2[2], $y_koord);
                    $pdf->write_left("  $ds[email]", self::$cols2[3], $y_koord);
                } else {
                    $pdf->write_left("  - siehe MaFü $ds[gemeldet_von] -", self::$cols2[2], $y_koord);
                }
            }
            $pdf->line(55, $y_koord+5, 550, $y_koord+5);
        }

        // *************************************************************************************************
        //		Mannschaftsführer
        // *************************************************************************************************

        $sql = "SELECT "."* FROM ".TABLE_MELDUNGEN." WHERE veranstaltungs_id = '".$veranstaltung->get_id()."' AND status = 'mafue' ORDER BY id";
        $res = $db->query($sql);
        $meldungen = Common::make_array($res, Common::COMPLETE_ROW_ASSOC);
        if ((($y_koord + count($meldungen) * 25) > 700) AND ($y_koord > 420)) {
            self::pdf_seitenende($pdf, $seite);
            $i=0; $seite++;
            $pdf->add_page();
            $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
        }
        else {
            $pdf->line(55, $y_koord + 28, 550, $y_koord + 28);
            $y_koord += 30;
            $i += 3;
            $kein_seitenwechsel = true;
        }
        $y_koord = self::pdf_tabellen_titel($pdf, $y_koord, "MaFü");
        $pdf->set_font(12, "bold");
        $pdf->write_left("Mannschaftsführer:", 75, $y_koord+5);
        $pdf->line(55, $y_koord + 13, 550, $y_koord + 13);
        $y_koord += 25;
        if (count($meldungen) < 1) { $pdf->write_left("Keine Eintragung", 75, $y_koord); }
        foreach ($meldungen as $ds) {
            $i++;
            if ($i > $zeilen_pro_seite) {
                self::pdf_seitenende($pdf, $seite);
                $i = 1; $seite++;
                $pdf->add_page();
                $y_koord = self::pdf_seitenkopf($pdf, 0, $header, 12, 0, true);
                $y_koord = self::pdf_tabellen_titel($pdf, $y_koord);
                $pdf->set_font(12, "bold");
                $pdf->write_left("Mannschaftsführer:", 75, $y_koord+5);
                $pdf->line(55, $y_koord+15, 550, $y_koord+15);
                $y_koord += 25;
            }
            if ($kein_seitenwechsel) { $kein_seitenwechsel = false; }
            elseif ($i > 1) { $y_koord += 15; }
            $pdf->set_font(9);
            $pdf->write_left($ds["id"], 58, $y_koord);
            $pdf->set_font(9, "bold");
            $pdf->write_left("$ds[name], $ds[vorname]", self::$cols[0], $y_koord);
            $pdf->set_font(9);
            $pdf->write_left("  $ds[verein]", self::$cols[3], $y_koord);
            $y_koord += 10;
            $pdf->write_left("  $ds[verband]", self::$cols2[0], $y_koord);
            if (Auth::is_allowed('actions/view_personal_data')) {
                $pdf->write_left("  $ds[telefon]", self::$cols2[1], $y_koord);
                $pdf->write_left("  $ds[handy]", self::$cols2[2], $y_koord);
                $pdf->write_left("  $ds[email]", self::$cols2[3], $y_koord);
            }
            $pdf->line(55, $y_koord+5, 550, $y_koord+5);
        }
        if (empty($error_msg)) {
            self::pdf_seitenende($pdf, $seite);
            // $dateiname = "pdfdocs/svb_teilnehmer_liste_".$bez_kurz.".pdf";
            $pdf->output("meldeliste_".$bez_kurz.$disziplin_kurz.".pdf");
            die;
        } else {
            echo $error_msg;
        }
        return false;
    }

    /**
     * @param PDFfpdf $pdf
     * @param $text
     * @param $x
     * @param $y
     * @param $zeilenabstand
     * @return mixed
     */
    protected static function pdf_text_mehrzeilig($pdf, $text, $x, $y, $zeilenabstand) {
        $text = explode("\n", $text);
        foreach ($text as $line) {
            $pdf->write_left($line, $x, $y);
            $y += $zeilenabstand;
        }
        return $y;
    }

    /**
     * @param PDFfpdf $pdf
     * @param $seite
     */
    protected static function pdf_seitenende($pdf, $seite) {
        $pdf->line(20, 280,  30, 280, 1);
        $pdf->line(20, 421,  30, 421, 1);
        $pdf->line(55, 785, 550, 785, 1);
        $pdf->set_font(9, "bold");
        $pdf->write_centered("Skiverband Berlin", 800, 0);
        $pdf->write_left(date("d.m.Y"), 60, 800);
        $pdf->write_right("Seite $seite", 550, 800);
    }

    /**
     * @param PDFfpdf $pdf
     * @param $logo
     * @param string $header
     * @param float|int $header_size
     * @param float|int $unterstreichen
     * @param bool $mehrzeilig
     * @return int
     */
    protected static function pdf_seitenkopf($pdf, $logo, $header, $header_size, $unterstreichen, bool $mehrzeilig = false): int {
        $pdf->set_font($header_size, "bold");
        if ($logo) {
            $pdf->image("../../images/svblogo_200.jpg", 225, 40, 200);
            if ($mehrzeilig) {
                $y_koord = self::pdf_text_mehrzeilig($pdf, $header, 60, 80, 13);
            } else {
                $pdf->write_centered($header, 80, $unterstreichen);
                $y_koord = 95;
            }
        } else {
            if ($mehrzeilig) {
                $pdf->set_font(20, "bold");
                $pdf->write_centered("Skiverband Berlin", 40, $unterstreichen);
                $pdf->set_font($header_size, "bold");
                $y_koord = self::pdf_text_mehrzeilig($pdf, $header, 60, 65, 13);
            } else {
                $pdf->write_centered($header, 40, $unterstreichen);
                $y_koord = 95;
            }
        }
        $pdf->line(55, $y_koord+7, 550, $y_koord+7);
        return $y_koord+10;
    }

    /**
     * @param PDFfpdf $pdf
     * @param int $y_koord
     * @param string $modus
     * @return int
     */
    protected static function pdf_tabellen_titel($pdf, $y_koord, string $modus="normal"): int {
        $pdf->set_font(10, "bold");
        $y_koord += 10;
        if ($modus == "normal") {
            $pdf->write_left("Name", self::$cols[0], $y_koord);
            $pdf->write_left("| m/w", self::$cols[1], $y_koord);
            $pdf->write_left("| JG", self::$cols[2], $y_koord);
            $pdf->write_left("|	Verein", self::$cols[3], $y_koord);
            $pdf->write_left("| Disziplin(en)", self::$cols[4], $y_koord);
        } else {
            $pdf->write_left("Name", self::$cols[0], $y_koord);
            $pdf->write_left("|	Verein", self::$cols[3], $y_koord);
        }
        $y_koord += 12;
        $pdf->write_left("Verband", self::$cols2[0], $y_koord);
        $pdf->write_left("| Telefon", self::$cols2[1], $y_koord);
        $pdf->write_left("| Handy", self::$cols2[2], $y_koord);
        $pdf->write_left("| email", self::$cols2[3], $y_koord);
        $pdf->line(55, $y_koord+8, 550, $y_koord+8);

        return $y_koord+20;
    }

}
