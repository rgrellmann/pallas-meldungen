<?php
/**
 * @author Robert
 * @since 21.02.2019
 */

namespace Application;

use Entity\Meldung;
use Entity\Veranstaltung;

class Altersklassen
{
    /**
     * Generiert eine Liste von Altersklassen, Gesamt und Skiverband Berlin,
     * und setzt in den einzelnen Meldungen die AK im Feld "ak"
     *
     * @param Meldung[] $meldungen
     * @param Veranstaltung $veranstaltung
     * @return array
     */
    public static function altersklassenliste(array &$meldungen, Veranstaltung $veranstaltung): array {
        $altersklassen = [
            'alle' => [],
            'SVB' => []
        ];
        foreach ($meldungen as $item) {
            $ak = self::jg2ak($item->get('jahrgang'), $veranstaltung->get_datum());
            $item->set('ak', $ak);

            if (!array_key_exists($ak, $altersklassen['alle'])) {
                $altersklassen['alle'][$ak] = ['m' => 0, 'w' => 0];
                $altersklassen['SVB'][$ak] = ['m' => 0, 'w' => 0];
            }
            $altersklassen['alle'][$ak][$item->get_geschlecht()]++;

            if ($item->get('name_5') === 'SVB') {
                $altersklassen['SVB'][$ak][$item->get_geschlecht()]++;
            }
        }
        uksort($altersklassen['alle'], 'self::compare_ak');
        uksort($altersklassen['SVB'], 'self::compare_ak');

        return $altersklassen;
    }

    /**
     * Gibt zu einem Jahrgang die Altersklasse laut DWO zurück,
     * wahlweise zum Zeitpunkt der Veranstaltung oder zum aktuellen Datum
     *
     * @param int $jahrgang
     * @param string|null $veranstaltungsdatum im Format "Y-m-d.*"
     * @return string
     */
    public static function jg2ak(int $jahrgang, ?string $veranstaltungsdatum = null): string {
        if ($veranstaltungsdatum === null) {
            $veranstaltungsdatum = date("Y-m-d");
        }
        $jahr = substr($veranstaltungsdatum, 0, 4);
        if (substr($veranstaltungsdatum, 5, 2) >= 10) {
            // ab Oktober nächste Saison benutzen
            $jahr++;
        }
        $alter = $jahr - $jahrgang;
        if     ($alter >= 55) { $ak = 'Ü55'; }
        elseif ($alter <= 10) { $ak = 'U10'; }
        elseif ($alter <= 12) { $ak = 'U12'; }
        elseif ($alter <= 14) { $ak = 'U14'; }
        elseif ($alter <= 16) { $ak = 'U16'; }
        elseif ($alter <= 18) { $ak = 'U18'; }
        elseif ($alter >= 36) { $ak = 'Ü36'; }
        else                  { $ak = 'D/H'; }
        return $ak;
    }

    /**
     * Sortier-Funktion für Altersklassen
     * Sortiert "D/H" zwischen "U.." und "Ü.." ein
     * @param string $a
     * @param string $b
     * @return int
     */
    protected static function compare_ak(string $a, string $b): int {
        if ($a === $b) {
            return 0;
        }
        if ($a[0] === 'U' AND $b[0] !== 'U') {
            return -1;
        }
        if ($b[0] === 'U' AND $a[0] !== 'U') {
            return 1;
        }
        if ($a[0] === 'Ü' AND $b[0] !== 'Ü') {
            return 1;
        }
        if ($b[0] === 'Ü' AND $a[0] !== 'Ü') {
            return -1;
        }
        return strnatcmp($a, $b);
    }

    /**
     * @deprecated
     * @param int $ak
     * @return array
     */
    public static function ak_to_jahrgang(int $ak): array {
        $jg = array();
        if ($ak == 51) { $jg = array(1900, date("Y") - 51); }
        elseif ($ak == 36) { $jg = array(date("Y") - 50, date("Y") - 36); }
        elseif ($ak == 21) { $jg = array(date("Y") - 35, date("Y") - 21); }
        elseif ($ak == 18) { $jg = array(date("Y") - 20, date("Y") - 18); }
        elseif ($ak == 16) { $jg = array(date("Y") - 17, date("Y") - 16); }
        elseif ($ak == 14) { $jg = array(date("Y") - 15, date("Y") - 14); }
        elseif ($ak == 12) { $jg = array(date("Y") - 13, date("Y") - 12); }
        elseif ($ak == 10) { $jg = array(date("Y") - 11, date("Y") - 10); }
        elseif ($ak == 8)  { $jg = array(date("Y") - 9,  date("Y") - 1); }
        return $jg;
    }

}