<?php
namespace Application;

class Common {

    const GAST = 1;
    const VEREIN = 2;
    const KAMPFRICHTER = 3;
    const ADMIN = 4;

    /**
     * @param string|array $date
     * @param int $modus
     * @return string|array
     */
    public static function format_date($date, int $modus = 1) {
        $out = "";
        switch ($modus) {
          case 0:	// Deutsches Datumsformat aus Datenbanks-Format
            $out = substr($date, 8, 2).".".substr($date, 5, 2).".".substr($date, 0, 4);
            if (strlen($date) > 11) {
                $out .= " ".substr($date,11,8)." Uhr";
            }
            break;
          case 1:	// Datenbanks-Format aus deutschem Datumsformat
            $out = substr($date, 0, 4)."-".substr($date, 5, 2)."-".substr($date, 8, 2);
            break;
          case 2:	// Assoziatives Array aus Datenbanks-Format
            $out = array();
            $out["Tag"] = substr($date, 8, 2);
            $out["Monat"] = substr($date, 5, 2);
            $out["Jahr"] = substr($date, 0, 4);
            $out["Stunde"] = substr($date, 11, 2);
            $out["Minute"] = substr($date, 14, 2);
            $out["Sekunde"] = substr($date, 17, 2);
            break;
          case 3:	// Datenbanks-Format aus Array
            $tag = sprintf("%02u",$date[2]);
            $monat = sprintf("%02u",$date[1]);
                $out = $date[0]."-".$monat."-".$tag;
            if (!empty($date[3])) {
                $stunde = sprintf("%02u", $date[3]);
                $out .= " ".$stunde.":00:00";
            }
            break;
        }
        return $out;
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generate_password(int $length = 6): string {
        $chars = "ABCDEFGHKLMNPQRSTUVWXYZ123456789";
        $password = "";
        for ($i = 1; $i <= $length; $i++) {
            $password.= $chars[mt_rand(0,31)];
        }
        return $password;
    }

    /**
     * @param string|array $str
     * @return string|array|false
     */
    public static function utf82iso($str) {
        return mb_convert_encoding($str, 'ISO-8859-15', 'UTF-8');
    }

    /**
     * @param string $disziplin
     * @return string
     */
    public static function disziplin_kurz(string $disziplin): string {
        switch ($disziplin) {
            case "Slalom": return "SL";
            case "Riesenslalom": return "RS";
            case "Super-G": return "SG";
            case strpos($disziplin, 'Vielseitigkeit') === 0: return "VS";
            case strpos($disziplin, 'Mini-Max') === 0: return "MM";
            case "klassisch": return "KL";
            case "freie Technik":
            case "Sprint (freie Technik)": return "FT";
            case "Staffel": return "ST";
        }
        return $disziplin;
    }

}
