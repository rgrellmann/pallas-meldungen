<?php
namespace tests\lib\framework;

use Framework\SvnItem;

class SvnItemTest extends FrameworkTestAbstract {

    protected $xml = '<?xml version="1.0" encoding="UTF-8"?>
<status>
    <target path="index.php">
        <entry path="index.php">
            <wc-status item="modified" props="none" revision="422">
                <commit revision="415">
                    <author>robert</author>
                    <date>2015-06-21T10:54:28.786463Z</date>
                </commit>
                <lock>
                    <token>opaquelocktoken:21223972-3022-4683-8d91-236790a79bf9</token>
                    <owner>robert</owner>
                    <comment>first_name last_name (ID 4711)</comment>
                    <created>2016-02-15T21:25:44.107088Z</created>
                </lock>
            </wc-status>
            <repos-status item="none" props="none">
                <lock>
                    <token>opaquelocktoken:21223972-3022-4683-8d91-236790a79bf9</token>
                    <owner>robert</owner>
                    <comment>first_name last_name (ID 4711)</comment>
                    <created>2016-02-15T21:25:44.107088Z</created>
                </lock>
            </repos-status>
        </entry>
        <against revision="422"/>
    </target>
</status>';

    public function testSetStatus() {
        $this->mockSession();
        $xmlArray = explode("\n", $this->xml);
        $svnItem = new SvnItem($xmlArray);
        $this->assertEquals('index.php', $svnItem->filename);

        $svnItem = new SvnItem(str_replace('wc-status item="modified"', 'wc-status item="missing"', $this->xml));
        $this->assertEquals('index.php', $svnItem->filename);
        $this->assertFalse($svnItem->modified_local);
        $this->assertFalse($svnItem->modified_remote);
        $this->assertTrue($svnItem->missing);

        $svnItem = new SvnItem(str_replace('repos-status item="none"', 'repos-status item="modified"', $this->xml));
        $this->assertEquals('index.php', $svnItem->filename);
        $this->assertTrue($svnItem->modified_local);
        $this->assertTrue($svnItem->modified_remote);
        $this->assertFalse($svnItem->missing);
        $this->assertEquals(1455567944, $svnItem->lock_date);
        $this->assertTrue($svnItem->locked_by_me);
        $this->assertTrue($svnItem->locked);
    }

}
