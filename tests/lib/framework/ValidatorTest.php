<?php
namespace tests\lib\framework;

use Framework\Validator;

class ValidatorTest extends \PHPUnit_Framework_TestCase {

    public function testInteger() {
        $result = Validator::integer('foo');
        $this->assertEquals(0, $result);

        $result = Validator::integer('foo', false, false, 1);
        $this->assertEquals(1, $result);

        $_REQUEST['foo'] = 3;

        $result = Validator::integer('foo');
        $this->assertEquals(3, $result);

        $result = Validator::integer('foo');
        $this->assertEquals(3, $result);

        $result = Validator::integer('foo', 5);
        $this->assertEquals(5, $result);

        $result = Validator::integer('foo', false, 2);
        $this->assertEquals(2, $result);

        $result = Validator::integer('foo', false, false, null, 'GET');
        $this->assertNull($result);

    }

}
