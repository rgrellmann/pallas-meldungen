<?php
namespace tests\lib\framework;

use Framework\Router;

class RouterTest extends \PHPUnit_Framework_TestCase {

    public function testSetScriptPath() {
        Router::set_script_path(getcwd());
        $property = new \ReflectionProperty('Framework\Router', 'script_path');
        $property->setAccessible(true);
        $this->assertEquals(getcwd(), $property->getValue());

    }

    public function testSetScriptPathFails() {
        $this->expectException(\RuntimeException::class);
        Router::set_script_path('\doesnotexist');
    }

    public function testSetScriptExtension() {
        Router::set_script_extension('foo');
        $property = new \ReflectionProperty('Framework\Router', 'script_extension');
        $property->setAccessible(true);
        $this->assertEquals('foo', $property->getValue());
    }

    public function testAdd() {
        Router::add('foo', 'bar');
        $property = new \ReflectionProperty('Framework\Router', 'routes');
        $property->setAccessible(true);
        $this->assertEquals(array('foo' => 'bar'), $property->getValue());

        Router::add('foo', 'baz');
        $this->assertEquals(array('foo' => 'baz'), $property->getValue());

        Router::add('baz', 'boom');
        $this->assertEquals(array('foo' => 'baz', 'baz' => 'boom'), $property->getValue());
    }

    public function testSetRoutes() {
        $result = Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        $this->assertTrue($result);
        $property = new \ReflectionProperty('Framework\Router', 'routes');
        $property->setAccessible(true);
        $this->assertEquals(array('foo' => 'boom', 'bar' => 'baz'), $property->getValue());

        $result = Router::set_routes('foo');
        $this->assertFalse($result);
        $this->assertEquals(array('foo' => 'boom', 'bar' => 'baz'), $property->getValue());
    }

    public function testGetTarget() {
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        $result = Router::get_target('foo');
        $this->assertEquals('boom', $result);
    }

    public function testGetTargetFails() {
        $this->expectException(\RuntimeException::class);
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        Router::get_target('bla');
    }

    public function testGetTargetImplicit() {
        Router::set_script_extension('php');
        Router::set_script_path(__DIR__ . DIRECTORY_SEPARATOR);
        $result = Router::get_target(basename(__FILE__, '.php'), true);
        $this->assertEquals('RouterTest', $result);
    }

    public function testGetTargetFailsImplicit() {
        $this->expectException(\RuntimeException::class);
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        Router::get_target('bla', true);
    }

    public function testGetControllerFileFails() {
        $this->expectException(\RuntimeException::class);
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        Router::get_controller_file('bla');
    }

    public function testGetControllerFileFailsImplicit() {
        $this->expectException(\RuntimeException::class);
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        Router::get_controller_file('bla', true);
    }

    public function testGetControllerFileFailsNotFound() {
        $this->expectException(\RuntimeException::class);
        Router::set_routes(array('foo' => 'boom', 'bar' => 'baz'));
        Router::get_controller_file('foo');
    }

    public function testGetControllerFileImplicit() {
        Router::set_script_extension('php');
        Router::set_script_path(__DIR__ . DIRECTORY_SEPARATOR);
        $result = Router::get_controller_file(basename(__FILE__, '.php'), true);
        $this->assertEquals(__DIR__ . DIRECTORY_SEPARATOR . 'RouterTest.php', $result);
    }

    public function testGetControllerFile() {
        Router::set_routes(array('foo' => 'RouterTest'));
        Router::set_script_extension('php');
        Router::set_script_path(__DIR__ . DIRECTORY_SEPARATOR);
        $result = Router::get_controller_file('foo');
        $this->assertEquals(__DIR__ . DIRECTORY_SEPARATOR . 'RouterTest.php', $result);
    }

}
