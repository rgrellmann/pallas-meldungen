<?php
namespace tests\lib\framework;

use Framework\App;
use Framework\Search;

class SearchTest extends FrameworkTestAbstract {

    public function testProcess() {
        $this->mockSession();
        $session = App::get_session();

        $_REQUEST = array();
        $result = Search::process('foo', 'foo', '\tests\lib\framework\DBTableMock');
        $this->assertEquals('', $result);

        $_REQUEST['searchform'] = "search";
        $_REQUEST['searchfields'] = array('foo', 'bar', 'all', 'nolabel', 'bla', 'notinsearchfields');
        $_REQUEST['searchvalues'] = array('baz', null, 'boom', 'bleep', 'blub', 'bli');
        $result = Search::process('foo', 'foo', '\tests\lib\framework\DBTableMock', false);
        $this->assertEquals('', $result);
        $this->assertEquals(array(0 => 'foo', 2 => 'all',  4 => 'bla',  5 => 'notinsearchfields'), $session->get("foo_suche_felder"));
        $this->assertEquals(array(0 => 'baz', 2 => 'boom', 4 => 'blub', 5 => 'bli'), $session->get("foo_suche_werte"));

        $_REQUEST = array();
        $result = Search::process('foo', 'foo', '\tests\lib\framework\DBTableMock');
        $this->assertEquals(" AND foo.foo LIKE 'baz' AND (foo.bla='%boom%' OR foo.bar LIKE '%boom%') AND foo.bla='blub' AND foo.notinsearchfields LIKE 'bli'", $result);

        $_REQUEST['searchform'] = "reset";
        $result = Search::process('foo', 'foo', '\tests\lib\framework\DBTableMock');
        $this->assertEquals('', $result);
        $this->assertFalse($session->get("foo_suche_felder"));
        $this->assertFalse($session->get("foo_suche_werte"));
    }

}
