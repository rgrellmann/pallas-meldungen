<?php
namespace tests\lib\framework;

use Framework\App;
use Framework\ApplicationException;
use Framework\Conf;
use Framework\Router;
use Framework\Template;

/**
 *
 */
class TemplateTest extends FrameworkTestAbstract
{
    protected static $multilangConfTemp = false;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        self::$multilangConfTemp = Conf::get('i18n/translated_templates');
    }

    public static function tearDownAfterClass() {
        Conf::$conf['i18n']['translated_templates'] = self::$multilangConfTemp;
        parent::tearDownAfterClass();
    }

    public function setUp() {
        parent::setUp();
        Conf::$conf['i18n']['translated_templates'] = false;
    }

    public function testConstructor() {
        $template = new Template('test', 'tests/lib/framework/views/');
        $this->assertTrue($template instanceof Template);

        $this->mockSession();
        Conf::$conf['i18n']['translated_templates'] = true;
        $template = new Template('test', 'tests/lib/framework/views/');
        $this->assertTrue($template instanceof Template);

        $template = new Template('test', 'tests/lib/framework/views/', 'de');
        $this->assertTrue($template instanceof Template);
    }

    public function testFetch()
    {
        $template = new Template('test', 'tests/lib/framework/views/');
        $out = $template->fetch();
        $this->assertEquals('foo', $out);

        $template = new Template('', 'tests/lib/framework/views/');
        $out = $template->fetch();
        $this->assertEquals('', $out);

        Conf::$conf['i18n']['translated_templates'] = true;
        $template = new Template('test', 'tests/lib/framework/views/');
        $out = $template->fetch();
        $this->assertEquals('foo', $out);
    }

    public function testDisplay()
    {
        $template = new Template('test', 'tests/lib/framework/views/');
        ob_start();
        $template->display();
        $out = ob_get_clean();
        $this->assertEquals('foo', $out);
    }

    public function testExtend()
    {
        $template = new Template('', 'tests/lib/framework/views/');
        $template->set('foo', 'bar');
        ob_start();
        $template->extend('test');
        $out = ob_get_clean();
        $this->assertEquals('foo', $out);
    }

    public function testCloak_email()
    {
        $out = Template::cloak_email('foo@bar.de');
        $this->assertNotContains('foo@bar.de', $out);
        $out = Template::cloak_email('foo@bar.de', true);
        $this->assertNotContains('foo@bar.de', $out);
        $this->assertContains('<a', $out);
    }

    public function testGet_context()
    {
        $context = Template::get_context();
        $this->assertNull($context);
    }

    public function testIs_set()
    {
        $template = new Template('test', 'tests/lib/framework/views/');
        $is_set = $template->is_set('foo');
        $this->assertFalse($is_set);
        $template->set('foo', 'bar');
        $is_set = $template->is_set('foo');
        $this->assertTrue($is_set);
    }

    public function testAppend()
    {
        $template = new Template('test', 'tests/lib/framework/views/');
        $reflection = new \ReflectionProperty('\Framework\Template', 'vars');
        $reflection->setAccessible(true);

        $template->set('foo', 'bar');
        $vars = $reflection->getValue($template);
        $this->assertEquals('bar', $vars['foo']);

        $template->append('foo', 'baz');
        $vars = $reflection->getValue($template);
        $this->assertEquals('barbaz', $vars['foo']);

        $template->append('baz', 'boom');
        $vars = $reflection->getValue($template);
        $this->assertEquals('boom', $vars['baz']);
    }

    public function testGetSetLang()
    {
        $template = new Template('test', 'tests/lib/framework/views/');
        $lang = $template->get_lang();
        $this->assertEquals('de', $lang);

        $template->set_lang('en');
        $lang = $template->get_lang();
        $this->assertEquals('en', $lang);
    }

    public function testGetSetFile()
    {
        $template = new Template('', 'tests/lib/framework/views/');
        $file = $template->get_file();
        $this->assertEquals('', $file);
        $template->set_file('test');
        $file = $template->get_file();
        $this->assertEquals('test', $file);
    }

    public function testSet_script_extension() {
        $reflection = new \ReflectionProperty('\Framework\Template', 'script_extension');
        $reflection->setAccessible(true);

        $value = $reflection->getValue();
        $this->assertEquals('tpl.php', $value);

        Template::set_script_extension('.twig');
        $value = $reflection->getValue();
        $this->assertEquals('.twig', $value);
        Template::set_script_extension('tpl.php');
    }

    public function testSet_path() {
        $template = new Template('');
        $reflection = new \ReflectionProperty('\Framework\Template', 'path');
        $reflection->setAccessible(true);

        $value = $reflection->getValue($template);
        $this->assertEquals('src/views/', $value);

        $template->set_path('tests/lib/framework/views/');
        $value = $reflection->getValue($template);
        $this->assertEquals('tests/lib/framework/views/', $value);
    }

    public function testTranslation_available() {
        Conf::$conf['i18n']['translated_templates'] = false;
        $template = new Template('test', 'tests/lib/framework/views/');
        $value = $template->translation_available();
        $this->assertTrue($value);

        Conf::$conf['i18n']['translated_templates'] = true;
        $template->set_lang('en');
        $value = $template->translation_available();
        $this->assertFalse($value);
    }

    public function testDetermine_file1() {
        $template = new Template('test', 'tests/lib/framework/views/');
        $template->fetch();
        // second fetch
        $template->fetch();

        // be sure that the router has no error route
        $reflection = new \ReflectionProperty('\Framework\Router', 'routes');
        $reflection->setAccessible(true);
        $routes = $reflection->getValue();
        unset($routes['error']);
        $reflection->setValue($routes);

        $template->set_file('foo');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage("Route 'error' not defined and implicit route does not point to a readable controller script");
        $template->fetch();
    }

    public function testDetermine_file2() {
        $template = new Template('foo', 'tests/lib/framework/views/');

        Router::add('error', 'error');
        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage("A template could not be found and also the error page template could not be found");
        $template->fetch();
    }

    public function testDetermine_file3() {
        $template = new Template('screens/foo', 'tests/lib/framework/views/');

        Router::add('error', 'error');
        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage("The screen template could not be found");
        $template->fetch();
    }

    public function testDetermine_file4() {
        Conf::$conf['i18n']['translated_templates'] = true;
        $template = new Template('foo', 'tests/lib/framework/views/');

        Router::add('error', 'error');
        $this->expectException(ApplicationException::class);
        $this->expectExceptionMessage("A template could not be found and also the error page template could not be found");
        $template->fetch();
    }

    public function testDetermine_file5() {
        Conf::$conf['i18n']['translated_templates'] = true;
        $session = App::get_session();
        $session->set('lang', 'fr');
        $template = new Template('test', 'tests/lib/framework/views/');
        $out = $template->fetch();
        $this->assertEquals('baguette', $out);
    }

}
