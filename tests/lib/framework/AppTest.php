<?php
namespace tests\lib\framework;

use Framework\App;

class AppTest extends \PHPUnit_Framework_TestCase {

    public function testGetRouteHierarchy() {
        $result = App::get_route_hierarchy('/');
        $this->assertEquals(array('/'), $result);

        $result = App::get_route_hierarchy('foo');
        $this->assertEquals(array('foo'), $result);

        $result = App::get_route_hierarchy('foo/bar');
        $this->assertEquals(array('foo/bar', 'foo'), $result);
    }

}
