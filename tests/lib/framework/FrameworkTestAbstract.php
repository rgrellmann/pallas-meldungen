<?php
namespace tests\lib\framework;

use Framework\Session;

abstract class FrameworkTestAbstract extends \PHPUnit_Framework_TestCase {

    public static function tearDownAfterClass() {
        $sessionProperty = new \ReflectionProperty('\Framework\App', 'session');
        $sessionProperty->setAccessible(true);
        $sessionProperty->setValue(null);

        parent::tearDownAfterClass();
    }

    protected function mockSession() {
        $sessionMock = new SessionMock();
        $sessionProperty = new \ReflectionProperty('\Framework\App', 'session');
        $sessionProperty->setAccessible(true);
        $sessionProperty->setValue($sessionMock);
    }

    protected function mockDatabase() {
        $connectionProperty = new \ReflectionProperty('\Framework\Database', 'connection');
        $connectionProperty->setAccessible(true);
        $connectionMock = new MySQLiDatabaseMock();
        $connectionProperty->setValue($connectionMock);
    }

}

class SessionMock extends Session {

    public $data = array();

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct() { }
    /** disable destructor */
    public function __destruct() { }
    /** disable realtime_write */
    public function realtime_write() { }

    /**
     * set a variable in the session->data array
     *
     * @param string $key
     * @param mixed $value
     */
    public function set($key, $value) {
        if (is_null($value)) {
            unset($this->data[$key]);
        } else {
            $this->data[$key] = $value;
        }
    }

    /**
     * get a variable from the session->data array
     *
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return null;
    }

    /**
     * @return int
     */
    public function get_user_id() {
        return 4711;
    }

    /**
     * @return UserMock
     */
    public function get_user() {
        return new UserMock();
    }

}

class MySQLiDatabaseMock {

    /**
     * @param string $str
     * @return string
     */
    public function escape($str) {
        $str = str_replace(array("\n", "\r", "\\", "'", '"'), array("\\n", "\\r", "\\\\", "\\'", "\\\""), $str);
        return $str;
    }

    public function query($query_string) {

    }

}

class UserMock {

    /**
     * @param string $name
     * @return string
     */
    public function get($name) {
        return $name;
    }
}

class DBTableMock {

    /**
     * @param string $name
     * @return string
     */
    public function get_label($name = '') {
        if ($name == 'nolabel' OR $name == 'all') {
            return "";
        } else {
            return $name;
        }
    }

    /**
     * @param string $table
     * @param string $value
     * @return string
     */
    public function get_search_sql_bla($table, $value) {
        return "$table.bla='$value'";
    }

    /**
     * @return array
     */
    public function search_fields() {
        return array('bla', 'bar');
    }

}

class GatekeeperMock {

    /**
     * @param string $resource
     * @param int|string $role
     * @return bool
     */
    public function is_allowed($resource, /** @noinspection PhpUnusedParameterInspection */ $role = null) {
        if ($resource == "routes/forbidden") {
            return false;
        }
        return true;
    }

}