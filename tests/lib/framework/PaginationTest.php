<?php
namespace tests\lib\framework;

use Framework\App;
use Framework\Conf;
use Framework\Pagination;

class PaginationTest extends FrameworkTestAbstract {

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();

        $connectionProperty = new \ReflectionProperty('\Framework\Database', 'connection');
        $connectionProperty->setAccessible(true);
        $connectionMock = new MySQLiDatabaseMock();
        $connectionProperty->setValue($connectionMock);

        if (!defined('PAGINATION_VARIABLE')) {
            define('PAGINATION_VARIABLE', 'page');
        }
    }

    public function setUp()
    {
        parent::setUp();
        $this->mockSession();
    }

    public function testHandleLimitOffset() {
        $items = Conf::get('view/items_per_page');
        $session = App::get_session();

        $result = Pagination::handle_limit_offset();
        $this->assertEquals(array($items, 0), $result);

        $session->set('limit', 42);
        unset($_REQUEST['limit']);
        $_REQUEST['page'] = -1;
        $result = Pagination::handle_limit_offset();
        $this->assertEquals(array(42, 0), $result);

        $_REQUEST['limit'] = 23;
        $_REQUEST['page'] = 2;
        $result = Pagination::handle_limit_offset();
        $this->assertEquals(array(23, 23), $result);

        $_REQUEST['limit'] = -1;
        $_REQUEST['page'] = 0;
        $result = Pagination::handle_limit_offset();
        $this->assertEquals(array(1000000, 0), $result);
    }

    public function testDetermineCurrpage() {
        $_REQUEST = array();
        $result = Pagination::determine_currpage();
        $this->assertEquals(1, $result);

        $_REQUEST[PAGINATION_VARIABLE] = 2;
        $result = Pagination::determine_currpage();
        $this->assertEquals(2, $result);

        $_REQUEST[PAGINATION_VARIABLE] = 0;
        $result = Pagination::determine_currpage();
        $this->assertEquals(1, $result);

        $_REQUEST[PAGINATION_VARIABLE] = -1;
        $result = Pagination::determine_currpage();
        $this->assertEquals(1, $result);

        $_REQUEST[PAGINATION_VARIABLE] = 'foo';
        $result = Pagination::determine_currpage();
        $this->assertEquals(1, $result);

        $_REQUEST[PAGINATION_VARIABLE] = 2;
        $_REQUEST['foo'] = 3;
        $result = Pagination::determine_currpage('foo');
        $this->assertEquals(3, $result);
    }

    public function testDetermineNumPages() {
        $items = Conf::get('view/items_per_page');
        $result = Pagination::determine_num_pages($items * 2);
        $this->assertEquals(2, $result);

        $result = Pagination::determine_num_pages(0);
        $this->assertEquals(1, $result);

        $result = Pagination::determine_num_pages($items * 2 + 1);
        $this->assertEquals(3, $result);

        $result = Pagination::determine_num_pages(7, 3);
        $this->assertEquals(3, $result);

    }

    public function testBuildPageTabs() {
        $result = Pagination::build_page_tabs(1);
        $this->assertInstanceOf('\Framework\Template', $result);
    }

}
