<?php
namespace tests\lib\framework;

use Framework\TypeHelpers;

class TypeHelpersTest extends \PHPUnit_Framework_TestCase {

    public function testArrayEmpty() {
        $a = null;
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = false;
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = '';
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = 0;
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = 1;
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array();
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array(null);
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array(false);
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array('');
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array(0);
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array('0');
        $this->assertTrue(TypeHelpers::array_empty($a));
        $a = array('key' => false);
        $this->assertTrue(TypeHelpers::array_empty($a));

        $a = array(1);
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array('null');
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array('false');
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array(' ');
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array(false, false);
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array('', '');
        $this->assertFalse(TypeHelpers::array_empty($a));
        $a = array(0, 0);
        $this->assertFalse(TypeHelpers::array_empty($a));
    }

    public function testArrayNotEmpty() {
        $a = array();
        $this->assertFalse(TypeHelpers::array_not_empty($a));
        $a = array(1);
        $this->assertTrue(TypeHelpers::array_not_empty($a));
    }

    public function testCheckUtf() {
        $str = "a";
        $result = TypeHelpers::check_utf($str);
        $this->assertFalse($result);

        $str = "utf8 äöü";
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-8', $result);

        $str = chr(0xEF) . chr(0xBB) . chr(0xBF) . "utf8-BOM äöü";
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-8', $result);

        $str = "ISO-8859-15 " . chr(0xA4) . chr(0xA4) . chr(0xF6) . chr(0xFC);
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('ISO-8859-1', $result);

        $str = chr(0xFE).chr(0xFF).chr(0)."u".chr(0)."t".chr(0)."f".chr(0)."1".chr(0)."6".chr(0)."-".chr(0)."B".chr(0)."E".chr(0).chr(0xE4).chr(0).chr(0xF6).chr(0).chr(0xFC);
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-16BE', $result);
        $this->assertEquals(mb_convert_encoding('utf16-BEäöü', 'ISO-8859-15'), $str);

        $str = chr(0xFF).chr(0xFE)."u".chr(0)."t".chr(0)."f".chr(0)."1".chr(0)."6".chr(0)."-".chr(0)."L".chr(0)."E".chr(0).chr(0xE4).chr(0).chr(0xF6).chr(0).chr(0xFC).chr(0);
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-16LE', $result);
        $this->assertEquals(mb_convert_encoding('utf16-LEäöü', 'ISO-8859-15'), $str);

        $str = chr(0)."u".chr(0)."t".chr(0)."f".chr(0)."1".chr(0)."6".chr(0)."-".chr(0)."B".chr(0)."E".chr(0).chr(0xE4).chr(0).chr(0xF6).chr(0).chr(0xFC);
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-16BE', $result);
        $this->assertEquals(mb_convert_encoding('utf16-BEäöü', 'ISO-8859-15'), $str);

        $str = "u".chr(0)."t".chr(0)."f".chr(0)."1".chr(0)."6".chr(0)."-".chr(0)."L".chr(0)."E".chr(0).chr(0xE4).chr(0).chr(0xF6).chr(0).chr(0xFC).chr(0);
        $result = TypeHelpers::check_utf($str);
        $this->assertEquals('UTF-16LE', $result);
        $this->assertEquals(mb_convert_encoding('utf16-LEäöü', 'ISO-8859-15'), $str);

    }

    public function testCurrency() {
        $result = TypeHelpers::currency(1234.23);
        $this->assertEquals('1.234,23', $result);
        $result = TypeHelpers::currency(1234.23, "usd");
        $this->assertEquals('1,234.23', $result);
    }

    public function testEnumValue() {
        $result = TypeHelpers::enum_value(null);
        $this->assertEquals('NULL', $result);
        $result = TypeHelpers::enum_value(false);
        $this->assertEquals('NULL', $result);
        $result = TypeHelpers::enum_value(array());
        $this->assertEquals('NULL', $result);
        $result = TypeHelpers::enum_value('null');
        $this->assertEquals('NULL', $result);

        $result = TypeHelpers::enum_value(0);
        $this->assertEquals(0, $result);
        $result = TypeHelpers::enum_value(1);
        $this->assertEquals(1, $result);
        $result = TypeHelpers::enum_value('1');
        $this->assertEquals('1', $result);
    }

    public function testIntValue() {
        $result = TypeHelpers::int_value(0);
        $this->assertEquals(0, $result);
        $result = TypeHelpers::int_value(0, 'NULL');
        $this->assertEquals('NULL', $result);
        $result = TypeHelpers::int_value(0, 0, 5);
        $this->assertEquals(5, $result);
        $result = TypeHelpers::int_value(5, 0, 0, 0);
        $this->assertEquals(0, $result);
        $result = TypeHelpers::int_value(0, 5);
        $this->assertEquals('00000', $result);
    }

    public function testShortenStr() {
        $result = TypeHelpers::shorten_str('abc', 5);
        $this->assertEquals('abc', $result);
        $result = TypeHelpers::shorten_str('abcdef', 5);
        $this->assertEquals('abcde&hellip;', $result);
        $result = TypeHelpers::shorten_str('abc&hellip;', 5);
        $this->assertEquals('abc&hellip;', $result);
    }

    public function testShortenWords() {
        $result = TypeHelpers::shorten_words("test test", 5);
        $this->assertEquals('test test', $result);
        $result = TypeHelpers::shorten_words("testarossa test", 5);
        $this->assertEquals('testa rossa test', $result);
        $result = TypeHelpers::shorten_words("testar-ossa test", 5);
        $this->assertEquals('testa r- ossa test', $result);
        $result = TypeHelpers::shorten_words("testa-rossa test", 5);
        $this->assertEquals('testa -ross a test', $result);
        $result = TypeHelpers::shorten_words("test-arossa test", 5);
        $this->assertEquals('test- aross a test', $result);
        $result = TypeHelpers::shorten_words("testarossatestarossa", 5);
        $this->assertEquals('testa rossa testa rossa', $result);
    }

    public function testHumanFileSize() {
        $result = TypeHelpers::human_file_size(0);
        $this->assertEquals('0', $result);
        $result = TypeHelpers::human_file_size(512);
        $this->assertEquals('512 Bytes', $result);
        $result = TypeHelpers::human_file_size(2048);
        $this->assertEquals('2 kB', $result);
        $result = TypeHelpers::human_file_size(2048 * 1024);
        $this->assertEquals('2 MB', $result);
    }

    public function testHex2Bin() {
        $result = TypeHelpers::hex2bin('414141');
        $this->assertEquals('AAA', $result);
    }

}
