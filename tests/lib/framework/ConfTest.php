<?php
namespace tests\lib\framework;

use Framework\Conf;

class ConfTest extends \PHPUnit_Framework_TestCase {

    public function testArray_merge_recursive() {
        $a = ['foo' => 'bar', 1 => 2, 'egg' => 'white',  2 => [3, 4],   'baz' => ['boom' => 'bang']];
        $b = [                1 => 3, 'egg' => 'yellow', 2 => [5 => 5], 'baz' => ['a' => 'b'], 'bla' => 'blub'];
        $result = Conf::array_merge_recursive($a, $b);
        $this->assertEquals(['foo' => 'bar', 1 => 3, 'egg' => 'yellow',  2 => [3, 4, 5 => 5], 'baz' => ['boom' => 'bang', 'a' => 'b'], 'bla' => 'blub'], $result);
    }

    public function testGet() {
        $value = Conf::get('');
        $this->assertFalse($value);
        $value = Conf::get('', true);
        $this->assertTrue($value);
        $value = Conf::get(1);
        $this->assertFalse($value);
        $value = Conf::get(1, true);
        $this->assertTrue($value);
        $value = Conf::get('nonexistentunittestkey', true);
        $this->assertTrue($value);
        $value = Conf::get('nonexistentunittestkey');
        $this->assertFalse($value);
    }

    public function testLoadFails() {
        $this->expectException(\RuntimeException::class);
        Conf::load('doesnotexist');
    }

    public function testLoad() {
        Conf::load('doesnotexist', true);
        Conf::$path = 'tests/lib/framework/';
        Conf::load('test');
        $value = Conf::get('unittestkey');
        $this->assertEquals('foo', $value);
    }

}
