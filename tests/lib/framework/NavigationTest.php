<?php
namespace tests\lib\framework {

    use Framework\Link;
    use Framework\Navigation;

    class NavigationTest extends \PHPUnit_Framework_TestCase {

        /** @var string */
        public static $cwd;

        public static function setUpBeforeClass() {
            parent::setUpBeforeClass();

            $gatekeeperProperty = new \ReflectionProperty('\Framework\Auth', 'gatekeeper');
            $gatekeeperProperty->setAccessible(true);
            $gatekeeperMock = new GatekeeperMock();
            $gatekeeperProperty->setValue($gatekeeperMock);
            // load classes before directory change
            /** @noinspection PhpExpressionResultUnusedInspection */
            new Link();
            /** @noinspection PhpExpressionResultUnusedInspection */
            new Navigation();

            self::$cwd = getcwd();
            chdir(dirname(__FILE__));
        }

        public static function tearDownAfterClass() {
            parent::tearDownAfterClass();
            chdir(self::$cwd);
        }

        public function testDisplay() {
            ob_start();
            $result = Navigation::display('MenuMock');
            $this->assertTrue($result);

            $result = Navigation::display('doesnotexist');
            $this->assertFalse($result);

            $result = Navigation::display('testmenu');
            $this->assertTrue($result);
            $result = Navigation::display('testmenu', 'mobile');
            $this->assertTrue($result);
            $result = Navigation::display('testmenu', 'horizontal_dropdown');
            $this->assertTrue($result);
            $result = Navigation::display('testmenu', 'horizontal_plain');
            $this->assertTrue($result);
            ob_end_clean();
        }

    }

}

namespace Menu {

    use Framework\MenuInterface;

    class MenuMock implements MenuInterface {

        /**
         * @return void
         */
        public static function display() {

        }
    }

}
