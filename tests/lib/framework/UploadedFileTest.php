<?php
namespace tests\lib\framework;

use Framework\UploadedFile;

class UploadedFileTest extends \PHPUnit_Framework_TestCase {

    public function testGetRouteHierarchy() {
        $foo = new UploadedFile('bar.jpg', 'image/jpeg');
        $this->assertEquals('bar.jpg', $foo->file);
        $this->assertEquals('image/jpeg', $foo->type);

        $foo = new UploadedFile('bar.jpg', IMAGETYPE_JPEG);
        $this->assertEquals('bar.jpg', $foo->file);
        $this->assertEquals('image/jpeg', $foo->type);
    }

}
