<?php
namespace tests\lib\framework;

use Framework\Environment;

class EnvironmentTest extends \PHPUnit_Framework_TestCase {

    public function testIsIEVersionless7() {
        $_SERVER['HTTP_USER_AGENT'] = '';
        $value = Environment::isIEVersionless7();
        $this->assertFalse($value);
        // Firefox
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0';
        $value = Environment::isIEVersionless7();
        $this->assertFalse($value);
        // Edge
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393';
        $value = Environment::isIEVersionless7();
        $this->assertFalse($value);
        // IE 11
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko';
        $value = Environment::isIEVersionless7();
        $this->assertFalse($value);
        // IE 7
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)';
        $value = Environment::isIEVersionless7();
        $this->assertFalse($value);
        // IE 6
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
        $value = Environment::isIEVersionless7();
        $this->assertTrue($value);
    }

    public function testIsIE() {
        $_SERVER['HTTP_USER_AGENT'] = '';
        $value = Environment::isIE();
        $this->assertFalse($value);
        // Firefox
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0';
        $value = Environment::isIE();
        $this->assertFalse($value);
        // Edge
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; ServiceUI 8) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393';
        $value = Environment::isIE();
        $this->assertFalse($value);
        // IE 11
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko';
        $value = Environment::isIE();
        $this->assertFalse($value);
        // IE 7
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)';
        $value = Environment::isIE();
        $this->assertTrue($value);
        // IE 6
        $_SERVER['HTTP_USER_AGENT'] = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
        $value = Environment::isIE();
        $this->assertTrue($value);
    }

    public function testIsLocalhost() {
        $value = Environment::is_localhost();
        $this->assertFalse($value);

        $_SERVER['SERVER_SOFTWARE'] = 'Apache/2.4.23 (Win64) OpenSSL/1.0.2j PHP/7.0.13';
        $_SERVER['SERVER_ADDR'] = '192.168.0.100';
        $value = Environment::is_localhost();
        $this->assertFalse($value);

        $_SERVER['SERVER_ADDR'] = '127.0.0.1';
        $_SERVER['SERVER_SOFTWARE'] = 'Apache/2.4.23 (Win64) OpenSSL/1.0.2j PHP/7.0.13';
        $value = Environment::is_localhost();
        $this->assertTrue($value);

        $_SERVER['SERVER_ADDR'] = '::1';
        $_SERVER['SERVER_SOFTWARE'] = 'Apache/2.4.23 (Win32) OpenSSL/1.0.2j PHP/7.0.13';
        $value = Environment::is_localhost();
        $this->assertTrue($value);
    }

    public function testRemote_address() {
        $_SERVER['REMOTE_ADDR'] = '::1';
        $value = Environment::remote_address();
        $this->assertEquals('::1', $value);

        unset($_SERVER['REMOTE_ADDR']);
        $value = Environment::remote_address();
        $this->assertEquals('CLI', $value);
    }

    public function testServer_signature() {
        $value = Environment::server_signature();
        $this->assertContains('MySQL', $value);


    }

}
