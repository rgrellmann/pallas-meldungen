<?php
namespace tests\lib\framework;

use Framework\DateAndTime;

class DateAndTimeTest extends \PHPUnit_Framework_TestCase {

    public function testDatetimeNow() {
        $result = DateAndTime::datetime_now();
        $this->assertRegExp('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $result);
        $this->assertGreaterThanOrEqual(date('Y-m-d H:i:s', time() - 5), $result);
        $this->assertLessThanOrEqual(date('Y-m-d H:i:s', time()), $result);

        // nonexistent format generates no error and returns standard format
        $result = DateAndTime::datetime_now('foo');
        $this->assertRegExp('/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}/', $result);

        $result = DateAndTime::datetime_now('DE');
        $this->assertRegExp('/[0-9]{2}\.[0-9]{2}\.[0-9]{4} [0-9]{2}:[0-9]{2}/', $result);
    }

    public function testMakeRfc822Date() {
        $result = DateAndTime::make_rfc822_date('1975-04-23 08:23:00');
        $this->assertEquals('Wed, 23 Apr 1975 08:23:00 +0100', $result);
    }

    public function testCheckDate() {
        $result = DateAndTime::check_date('1970-01-01 00:00:00');
        $this->assertEquals('1970-01-01', $result);
        $result = DateAndTime::check_date('1970-25-00 00:00:00');
        $this->assertEquals('0000-00-00', $result);
        $result = DateAndTime::check_date('1970-25-00 00:00:00', false, 'NULL');
        $this->assertEquals('NULL', $result);

        $result = DateAndTime::check_date('1970-01-01 00:00:00', true);
        $this->assertEquals('1970-01-01 00:00:00', $result);
        $result = DateAndTime::check_date('1970-25-00 12:00:00', true);
        $this->assertEquals('0000-00-00', $result);
        $result = DateAndTime::check_date('1970-01-01 25:71:00', true);
        $this->assertEquals('1970-01-01 00:00:00', $result);
        $result = DateAndTime::check_date('1970-25-00 12:00:00', true, null);
        $this->assertNull($result);

        $result = DateAndTime::check_date('01.01.1970 00:00:00');
        $this->assertEquals('1970-01-01', $result);
        $result = DateAndTime::check_date('00.25.1970 00:00:00');
        $this->assertEquals('0000-00-00', $result);

        $result = DateAndTime::check_date('01/01/1970 00:00:00');
        $this->assertEquals('0000-00-00', $result);
    }

    public function testDatetimeFormat() {
        $result = DateAndTime::datetime_format('1970-01-01 00:00:00');
        $this->assertEquals('01.01.1970', $result);
        $result = DateAndTime::datetime_format('1970-01-01 00:00:00', true);
        $this->assertEquals('01.01.1970&nbsp;00:00', $result);
        $result = DateAndTime::datetime_format('0000-00-00 00:00:00', true, '');
        $this->assertEquals('', $result);
        $result = DateAndTime::datetime_format('0000-00-00 00:00:00', true);
        $this->assertEquals(date('d.m.Y&\n\b\s\p;H:i'), $result);
    }

    public function testFormatDate() {
        $result = DateAndTime::format_date('2016-02-17 00:59:59');
        $this->assertEquals('17.02.2016', $result);
        $result = DateAndTime::format_date('2016-02-17 00:00:00');
        $this->assertEquals('17.02.2016', $result);
        $result = DateAndTime::format_date('2016-02-16 23:59:59');
        $this->assertEquals('16.02.2016', $result);
        $result = DateAndTime::format_date('2016-02-16 23:59:59', 'i:s');
        $this->assertEquals('59:59', $result);
    }

    public function testFormatTime() {
        $result = DateAndTime::format_time('11:12:13');
        $this->assertEquals('11:12', $result);
        $result = DateAndTime::format_time('2016-02-17 11:12:13');
        $this->assertEquals('11:12', $result);
        $result = DateAndTime::format_time('');
        $this->assertEquals('00:00', $result);
    }

    public function testXlsToMysqlTime() {
        $result = DateAndTime::xls_to_mysql_time(0);
        $this->assertEquals('00:00:00', $result);
        $result = DateAndTime::xls_to_mysql_time(1);
        $this->assertEquals('00:00:00', $result);
        $result = DateAndTime::xls_to_mysql_time(0.5);
        $this->assertEquals('12:00:00', $result);
        $result = DateAndTime::xls_to_mysql_time(0.75);
        $this->assertEquals('18:00:00', $result);
    }

    public function testFormatDatetime() {
        $result = DateAndTime::format_datetime(0);
        $this->assertEquals('01.01.1970', $result);
        $result = DateAndTime::format_datetime(0, true);
        $this->assertEquals('01.01.1970 01:00', $result);
    }

    public function testGetTimeOffset() {
        $result = DateAndTime::get_time_offset();
        $this->assertEquals(0, $result);
    }

    public function testMakeTimestamp() {
        $result = DateAndTime::make_timestamp('1970-01-01 01:00:00');
        $this->assertEquals(0, $result);
        $result = DateAndTime::make_timestamp('1970-01-01');
        $this->assertEquals(-3600, $result);
    }

    public function testSecToTime() {
        $result = DateAndTime::sec_to_time(0);
        $this->assertEquals('00:00', $result);
        $result = DateAndTime::sec_to_time(60);
        $this->assertEquals('00:01', $result);
        $result = DateAndTime::sec_to_time(3600);
        $this->assertEquals('01:00', $result);
    }

    public function testGetTimeframeDelimiter() {
        $result = DateAndTime::get_timeframe_delimiter('1970-01-01');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('today');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('yesterday');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('last_week');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('this_week');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('last_month');
        $this->assertEquals(2, count($result));
        $result = DateAndTime::get_timeframe_delimiter('this_month');
        $this->assertEquals(2, count($result));
    }

    public function testCalPopup() {
        $result = DateAndTime::cal_popup('ABCD', 'EFGH', "dd.MM.yyyy", 'IJKL');
        $this->assertContains('ABCD', $result);
        $this->assertContains('EFGH', $result);
        $this->assertContains('IJKL', $result);
        $result = DateAndTime::cal_popup('ABCD', 'EFGH', "dd.MM.yyyy", 'IJKL', "", "ABCD", "EFGH");
        $this->assertContains('ABCD', $result);
        $this->assertContains('EFGH', $result);
    }

    public function testAddWorkdays() {
        $result = DateAndTime::add_workdays(11);
        $this->assertInternalType('integer', $result);
        $this->assertGreaterThanOrEqual(time() + 13 * 60 * 60 * 24, $result);
    }

    public function testConvertDatetime() {
        $result = DateAndTime::convert_datetime(array(2000, 1, 2, 3, 4, 5));
        $this->assertEquals('2000-01-02 03:04:05', $result);
        $result = DateAndTime::convert_datetime(array(2000, 13, 42, 3, 4, 5));
        $this->assertEquals('0000-00-00 00:00:00', $result);

        $result = DateAndTime::convert_datetime('2000-01-02 03:04:05');
        $this->assertEquals('02.01.2000 03:04:05', $result);
        $result = DateAndTime::convert_datetime('2000-01-02 03:04:05', 'array');
        $this->assertSame(array('year' => '2000','month' => '01', 'day' => '02', 'hour' => '03', 'minute' => '04', 'second' => '05'), $result);

        $result = DateAndTime::convert_datetime('2000-01-02 03:04:05', 'array', true);
        $this->assertSame(
            array(
                'year' => '2000', 'month' => '01', 'day' => '02', 'hour' => '03', 'minute' => '04', 'second' => '05',
                'tm_year' => '100', 'tm_mon' => '0', 'tm_mday' => '02', 'tm_hour' => '03', 'tm_min' => '04', 'tm_sec' => '05'
            ),
            $result
        );

        $result = DateAndTime::convert_datetime("0000-00-00 00:00:00");
        $this->assertFalse($result);
        $result = DateAndTime::convert_datetime('2000-01-02 03:04:05.123456');
        $this->assertFalse($result);
        $result = DateAndTime::convert_datetime('2000-13-42 03:04:05');
        $this->assertFalse($result);

        $result = DateAndTime::convert_datetime('1970-01-01 00:00:00', 'timestamp');
        $this->assertEquals(0 - date('Z'), $result);
        $result = DateAndTime::convert_datetime(null, 'timestamp');
        $this->assertLessThanOrEqual(time(), $result);
        $this->assertGreaterThanOrEqual(time() - 3, $result);
    }

}
