<?php
namespace tests\lib\framework;

use Framework\Session;

class SessionTest extends \PHPUnit_Framework_TestCase
{
    /** @var Session */
    public static $session;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$session = new Session();
    }

    public static function tearDownAfterClass()
    {
        self::$session->purge();
        self::$session = null;
        parent::tearDownAfterClass();
    }

    public function testSetGetPersistent()
    {
        $session = self::$session;
        $value = $session->get_persistent();
        $this->assertEquals(0, $value);

        $session->set_persistent(1);
        $value = $session->get_persistent();
        $this->assertEquals(1, $value);

        $session->set_persistent(false);
        $value = $session->get_persistent();
        $this->assertEquals(0, $value);

        $session->set_persistent(true);
        $value = $session->get_persistent();
        $this->assertEquals(1, $value);
    }

    public function testSetGet()
    {
        $session = self::$session;
        $value = $session->get('foo');
        $this->assertNull($value);

        $session->set('foo', 'bar');
        $value = $session->get('foo');
        $this->assertEquals('bar', $value);

        $value = $session->get_data();
        $this->assertEquals(['foo' => 'bar'], $value);

        $session->set('foo', null);
        $session->purge();
    }

    public function testGetDebugOutput() {
        $session = self::$session;
        $session->set('debug', 1);
        $session->set('foo', 'bar');
        $value = $session->get_debug_output();
        $this->assertContains('foo', $value);
    }

}
