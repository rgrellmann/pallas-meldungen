<?php
namespace tests\lib\framework;

use Framework\Translator;

class TranslatorTest extends \PHPUnit_Framework_TestCase {

    public function testTranslate() {
        $t = Translator::get_instance();
        $result = $t->translate('foo');
        $this->assertEquals('foo', $result);
    }

    public function testTranslateAlias() {
        $t = Translator::get_instance();
        $result = $t->_('foo');
        $this->assertEquals('foo', $result);
    }

    public function testSetLanguage() {
        $t = Translator::get_instance();
        $t->set_language('ab_CD');
        $this->assertEquals('ab_CD', $t->language);
    }

}
