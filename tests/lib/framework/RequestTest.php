<?php
namespace tests\lib\framework;

use Framework\Request;

class RequestTest extends \PHPUnit_Framework_TestCase {

    public function setUp() {
        parent::setUp();
        if (isset($_REQUEST['foo'])) {
            unset($_REQUEST['foo']);
        }
        if (isset($_GET['foo'])) {
            unset($_GET['foo']);
        }
        if (isset($_POST['foo'])) {
            unset($_POST['foo']);
        }
        if (isset($_COOKIE['foo'])) {
            unset($_COOKIE['foo']);
        }
    }

    public function testGet() {
        $result = Request::get('foo');
        $this->assertNull($result);

        $result = Request::get('foo', 'REQUEST');
        $this->assertNull($result);

        $_REQUEST['foo'] = 'bleep';
        $result = Request::get('foo');
        $this->assertEquals('bleep', $result);
    }

    public function testGetFromGet() {
        $result = Request::get('foo', 'GET');
        $this->assertNull($result);

        $_GET['foo'] = 'bar';
        $result = Request::get('foo', 'GET');
        $this->assertEquals('bar', $result);
        $result = Request::get('foo', 'REQUEST');
        $this->assertNull($result);
    }

    public function testGetFromPost() {
        $result = Request::get('foo', 'POST');
        $this->assertNull($result);

        $_POST['foo'] = 'baz';
        $result = Request::get('foo', 'POST');
        $this->assertEquals('baz', $result);
        $result = Request::get('foo', 'REQUEST');
        $this->assertNull($result);
    }

    public function testGetFromCookie() {
        $result = Request::get('foo', 'COOKIE');
        $this->assertNull($result);

        $_COOKIE['foo'] = 'boom';
        $result = Request::get('foo', 'COOKIE');
        $this->assertEquals('boom', $result);
        $result = Request::get('foo', 'REQUEST');
        $this->assertNull($result);
    }

    public function testGetFromInvalid() {
        $result = Request::get('foo', 'DOESNOTEXIST');
        $this->assertNull($result);
    }

}
