<?php
namespace tests\lib\framework;

class LukeArraywalkerTest extends \PHPUnit_Framework_TestCase {

    public function testIntvalThis() {
        $testArray = array(2, "3", "-4");
        array_walk($testArray, array('\Framework\LukeArraywalker', 'intval_this'));
        $this->assertSame(array(2, 3, -4), $testArray);
    }

    public function testTrimThis() {
        $testArray = array(" a", "b ", " c ", "d e");
        array_walk($testArray, array('\Framework\LukeArraywalker', 'trim_this'));
        $this->assertSame(array("a", "b", "c", "d e"), $testArray);
    }

    public function testRoot() {
        $testArray = array(9, 16);
        array_walk($testArray, array('\Framework\LukeArraywalker', 'root'));
        $this->assertSame(array(3.0, 4.0), $testArray);

        $testArray = array(array('foo' => 9, 'bar' => 4), array('foo' => '16'));
        array_walk($testArray, array('\Framework\LukeArraywalker', 'root'), 'foo');
        $this->assertSame(array(array('foo' => 3.0, 'bar' => 4), array('foo' => 4.0)), $testArray);

        $testArray = array(array(0 => 9));
        array_walk($testArray, array('\Framework\LukeArraywalker', 'root'), 0);
        $this->assertSame(array(array(0 => 3.0)), $testArray);
    }

    public function testEscapeStrings() {
        $testArray = array("a", 1, 2.0, "NULL", "NOW()", "a'b", "a\nb");
        array_walk($testArray, array('\Framework\LukeArraywalker', 'escape_strings'));
        $this->assertSame(array("'a'", 1, 2.0, 'NULL', 'NOW()', '\'a\\\'b\'', "'a\\nb'"), $testArray);
    }

    public function testEscapeThis() {
        $testArray = array("a", 1, 2.0, "NULL", "NOW()", "a'b", "a\nb");
        array_walk($testArray, array('\Framework\LukeArraywalker', 'escape_this'));
        $this->assertSame(array("a", '1', '2', 'NULL', 'NOW()', 'a\\\'b', "a\\nb"), $testArray);
    }

    public function testBr2nl() {
        $testArray = array("a<br>b", "a<br />b", "a\nb", "a\n<br>\nb");
        array_walk($testArray, array('\Framework\LukeArraywalker', 'br2nl'));
        $this->assertSame(array("a\nb", "a\nb", "a\nb", "a\n\n\nb"), $testArray);
    }

    public function testStripTags() {
        $testArray = array("a<br>b", "a<br />b", "a<?=b", "a<?php b", 'a<script>b=c');
        array_walk($testArray, array('\Framework\LukeArraywalker', 'strip_tags'));
        $this->assertSame(array("ab", "ab", "a", "a", "ab=c"), $testArray);
    }

}
