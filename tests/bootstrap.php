<?php
namespace FrameworkTest;

use Framework\Conf;

// report all errors
error_reporting(-1);

// cd to the application's root directory
chdir(__DIR__.'/..');

class Bootstrap
{

    /**
     *
     */
    public static function init()
    {
        // tell the Conf class whether to read the configuration from files or
        // to use a cache mechanism (apc, memcache...)
        $GLOBALS['config_cache'] = '';

        require_once('includes/constants.inc.php');

        require_once('lib/framework/Conf.php');
        Conf::init();

        require_once('includes/autoload.php');

        require_once('includes/exceptions.inc.php');

    }

}

Bootstrap::init();
