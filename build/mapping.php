<?php
//namespace Framework;

$types = array();

// Date and Time Types

$types['DATETIME']['mapto'] = 'string';
$types['DATETIME']['needsize'] = false;
$types['DATETIME']['default'] = "'0000-00-00 00:00:00'";
$types['DATETIME']['type'] = 'NaN';

$types['DATE']['mapto'] = 'string';
$types['DATE']['needsize'] = false;
$types['DATE']['default'] = "'0000-00-00'";
$types['DATE']['type'] = 'NaN';

$types['TIME']['mapto'] = 'string';
$types['TIME']['needsize'] = false;
$types['TIME']['default'] = "'00:00:00'";
$types['TIME']['type'] = 'NaN';

$types['TIMESTAMP']['mapto'] = 'string';
$types['TIMESTAMP']['needsize'] = false;
$types['TIMESTAMP']['default'] = "'0000-00-00 00:00:00'";
$types['TIMESTAMP']['type'] = 'NaN';

// Numeric Types

$types['BOOLEAN']['mapto'] = 'bool';
$types['BOOLEAN']['needsize'] = false;
$types['BOOLEAN']['default'] = "false";
$types['BOOLEAN']['type'] = 'numeric';

$types['TINYINT']['mapto'] = 'int';
$types['TINYINT']['needsize'] = true;
$types['TINYINT']['default'] = '0';
$types['TINYINT']['type'] = 'numeric';

$types['SMALLINT']['mapto'] = 'int';
$types['SMALLINT']['needsize'] = true;
$types['SMALLINT']['default'] = '0';
$types['SMALLINT']['type'] = 'numeric';

$types['MEDIUMINT']['mapto'] = 'int';
$types['MEDIUMINT']['needsize'] = true;
$types['MEDIUMINT']['default'] = '0';
$types['MEDIUMINT']['type'] = 'numeric';

$types['INT']['mapto'] = 'int';
$types['INT']['needsize'] = true;
$types['INT']['default'] = '0';
$types['INT']['type'] = 'numeric';

$types['BIGINT']['mapto'] = 'int';
$types['BIGINT']['needsize'] = true;
$types['BIGINT']['default'] = '0';
$types['BIGINT']['type'] = 'numeric';

$types['DECIMAL']['mapto'] = 'float';
$types['DECIMAL']['needsize'] = false;
$types['DECIMAL']['default'] = "0";
$types['DECIMAL']['type'] = 'numeric';

$types['FLOAT']['mapto'] = 'float';
$types['FLOAT']['needsize'] = false;
$types['FLOAT']['default'] = "0";
$types['FLOAT']['type'] = 'numeric';

$types['DOUBLE']['mapto'] = 'float';
$types['DOUBLE']['needsize'] = false;
$types['DOUBLE']['default'] = "0";
$types['DOUBLE']['type'] = 'numeric';

// String Types

$types['VARCHAR']['mapto'] = 'string';
$types['VARCHAR']['needsize'] = true;
$types['VARCHAR']['default'] = "''";
$types['VARCHAR']['type'] = 'NaN';

$types['CHAR']['mapto'] = 'string';
$types['CHAR']['needsize'] = true;
$types['CHAR']['default'] = "''";
$types['CHAR']['type'] = 'NaN';

$types['TINYTEXT']['mapto'] = 'string';
$types['TINYTEXT']['needsize'] = false;
$types['TINYTEXT']['default'] = "''";
$types['TINYTEXT']['type'] = 'NaN';

$types['MEDIUMTEXT']['mapto'] = 'string';
$types['MEDIUMTEXT']['needsize'] = false;
$types['MEDIUMTEXT']['default'] = "''";
$types['MEDIUMTEXT']['type'] = 'NaN';

$types['TEXT']['mapto'] = 'string';
$types['TEXT']['needsize'] = false;
$types['TEXT']['default'] = "''";
$types['TEXT']['type'] = 'NaN';

$types['LONGTEXT']['mapto'] = 'string';
$types['LONGTEXT']['needsize'] = false;
$types['LONGTEXT']['default'] = "''";
$types['LONGTEXT']['type'] = 'NaN';

$types['ENUM']['mapto'] = 'string';
$types['ENUM']['needsize'] = false;
$types['ENUM']['default'] = "''";
$types['ENUM']['type'] = 'NaN';

$types['SET']['mapto'] = 'string';
$types['SET']['needsize'] = false;
$types['SET']['default'] = "''";
$types['SET']['type'] = 'NaN';

// | max | required | mask | minlen | maxlen

$rules = array();

// $1%s = $var ; $2%s = value
// wenn die bedingung true ergibt, wird sie als fehlerhaft gewertet (siehe 'min')

$rules['min'] = '%1$s < %2$d';
$rules['max'] = '%1$s > %2$d';
$rules['required'] = '(string)%1$s == \'\'';
$rules['mask'] = "!preg_match('%2\$s', %1\$s)";
$rules['minlen'] = 'strlen(%1$s) < %2$d';
$rules['maxlen'] = 'strlen(%1$s) > %2$d';
$rules['equal'] = "%1\$s != '%2\$s'";
