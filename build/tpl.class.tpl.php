<?='<?php'; ?>

namespace Entity;

use Framework\DBTable;
use Framework\Database;

/**
 * This is a generated class. Only edit the 'label' and 'unit' fields!
 * Use the <?=$class_name;?> Class at the lower end of this file to implement extensions or overwrite code.
 *
 */
class <?=$class_name;?>Table extends DBTable {

	public function __construct($<?=implode(' = null, $', $vars['primkey'])?> = null) {
		$this->table = "<?=$table_name;?>";
		$this->pk = array("<?=implode('", "', $vars['primkey'])?>");
<?=(!empty($auto_increment))?'		$this->auto_increment = true;'."\n":""?>
		// you can add "label" and "unit" elements to any columns where it is applicable
		$this->columns = array(
<?php $out = "";
	foreach ($vars['vars'] as $k => $v) {
		$out.=	"\t\t\t'".$k."' ".(strlen($k)<5?"\t":"").(strlen($k)<9?"\t":"").(strlen($k)<13?"\t":"").'	=> array(\'type\' => \''.$v['type']."',".($v['type'] == 'NaN' ? "\t":"")." 	'value' => false,   'label' => ''),\n";
	}
	$out = substr($out, 0, -2)."\n";
	echo $out;
?>
		);
		parent::__construct($<?=implode(', $', $vars['primkey'])?>);
	}

<?php if (count($vars['primkey'])) { ?>
	/**
	 * checks if an entry with the given primary key exists in the database
	 *
    <?php foreach ($vars['primkey'] as $key) { ?>
	 * @param <?=$vars['vars'][$key]['type']=='NaN'?"string":"int"?> $<?=$key?>

    <?php } ?>
	 * @return int
	 */
	public static function exists($<?=implode(', $', $vars['primkey'])?>) {
		$db = Database::get_connection();
    <?php $where = array();
	foreach ($vars['primkey'] as $key) {
		$where[] = "`$key` = '$".$key."'";
	}
?>
		return $db->count("SELECT 1 FROM `<?=$table_name;?>` WHERE <?=implode(" AND ", $where)?>");
	}
<?php } ?>

}


/**
 * Extensions to the generated <?=$class_name;?>Table Class go here
 *
 */
class <?=$class_name;?> extends <?=$class_name;?>Table {

}

<?='?>';?>