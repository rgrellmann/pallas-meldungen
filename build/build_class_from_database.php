<?php
namespace Framework;

header('Content-type: text/plain');

/**
 * @param $name
 * @return mixed|string
 */
function format_class_name($name) {
	// macht aus "test_test"  "TestTest"
	$out = preg_replace_callback('/_(.)/i', function ($matches) { return strtoupper($matches[0]); }, ucfirst(strtolower($name)));
	if (substr($out, -1) == "s") { $out = substr($out, 0, -1); }
	if (substr($out, -2) == "ie") { $out = substr($out, 0, -2)."y"; }
	return $out;
}

/**
 * @param $name
 * @return array
 */
function get_table_description($name) {
	global $db;
	$table = array();
	// Sicherheit: Alles nach dem ersten Leerzeichen abschneiden
	$table["name"] = strtolower(strtok($name," "));

	// using mysql-native functions due to an unknown error of sql-class
	$res = $db->query("SHOW COLUMNS FROM ".$table["name"]);
	// $table["columns"] = Common::make_array($res, '');
	while ($row = $res->fetch()) {
		$table["columns"][] = $row;
	}

	return $table;
}

require_once('./mapping.php');
chdir("..");
require_once('./includes/init.php');
App::__init();

$tpl = new Template('tpl.class',"./");
$db = Database::get_connection();
chdir("build");

$tables = array();

if (!empty($_REQUEST["table"])) {
	if ($_REQUEST["table"] == "all") {
		$res = $db->query("SHOW TABLES");
		$table_names = Common::make_array($res, '');
		foreach ($table_names as $table_name) {
			$tables[] = get_table_description($table_name["Tables_in_".SQL_DB]);
		}
	} else {
		$tables[] = get_table_description($_REQUEST["table"]);
	}
} else {
	die("No Table Name specified.");
}

foreach ($tables as $table) {

	$variables = array();
	$variables['primkey'] = array();
	$variables['nonprimkeys'] = array();
	$variables['formatkeys'] = array();
	$variables['vars'] = array();
	$auto_incr = '';

	$table['can-save-primkey'] = true;
	foreach ($table["columns"] as $col) {
		if ($col["Key"] == "PRI" AND $col["Extra"] == "auto_increment") {
			$table['can-save-primkey'] = false; break;
		}
	}

	foreach ($table["columns"] as $col) {
		$key	= $col['Field'];
		$type	= strtoupper(strtok($col['Type'],"("));
		// $format  = ""; // (string)$col['format'];
		$primkey = ($col['Key'] == "PRI" ? true : false);
		$default = $col['Default'];
		if ($col['Extra'] == "auto_increment" && $auto_incr == '') {
			$auto_incr = $key;
		}

		$variables['vars'][$key] = array();
		// $variables['vars'][$key]['cast'] =	$types[$type]['mapto'];
		$variables['vars'][$key]['default'] = $default;
		$variables['vars'][$key]['type'] =	$types[$type]['type'];
		// $variables['vars'][$key]['format'] =  $format;

		// if ($format != '')
		//	$variables['formatkeys'][] = $key;

		if ($primkey)
			$variables['primkey'][] = $key;
		else
			$variables['nonprimkeys'][] = $key;
	}

	$tpl->set('auto_increment', $auto_incr);
	$tpl->set('can_save_primkey', (boolean)$table['can-save-primkey']);
	$tpl->set('table_name', $table['name']);
	$tpl->set('class_name', format_class_name($table['name']));
	$tpl->set('vars', $variables);

	$fname = './out/'. format_class_name($table['name']) .'.php';

	if (!empty($_REQUEST["save"])) {
		// in Datei speichern
		if (file_exists($fname)) {
			echo basename($fname). " already exists\n";
			echo $tpl->fetch();
		} else {
			file_put_contents($fname, $tpl->fetch());
			echo $fname;
		}
	} else {
		// oder ausgeben
		echo $fname."\n".$tpl->fetch();
	}
}

?>